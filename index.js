import ClaimFormVj from "./src/sdk_modules/ld_claim_form_vj";
import Tnds from "./src/sdk_modules/ld_tnds";
// import FormBhsk from "./src/sdk_modules/ld_bhsk";
import FormBhsk from "./src/sdk_modules/ld_bhsk_new";
import ClaimLostBaggage from "./src/sdk_modules/ld_claim_bh_hanh_ly";

export { FormBhsk, ClaimFormVj, ClaimLostBaggage, Tnds };
