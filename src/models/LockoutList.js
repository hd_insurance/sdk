import LayoutModel from "./layoutModel.js";

export default class LockoutList extends LayoutModel {
  constructor() {
    super();
    this.data = null;
    this.setData = this.setData.bind(this);
    this.setDataEvent = this.setDataEvent.bind(this);
    this.onDataChange = (list)=>{
    	 console.error("handleEventDataCallback is required.")
    };
  }

  setData(data) {
    this.data = data
  }

  setDataEvent(event) {
    this.onDataChange = event
  }


 
}
