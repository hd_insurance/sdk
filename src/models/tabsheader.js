import LayoutModel from "./layoutModel.js";

export default class TabsHeader extends LayoutModel {
  constructor() {
    super();
    this.data = [];
    this.active = null
    this.addClickEvent = this.addClickEvent.bind(this);
    this.addItemClick = null
    this.onItemSelect = null;
    this.setTabActive = null;
    this.amout = 0;
    this.setData = this.setData.bind(this);
    this.setTotalAmount = this.setTotalAmount.bind(this);
    this.reload = null;
  }

  setActive(id_active){
    this.active = id_active
    if(this.setTabActive){
        this.setTabActive(id_active)
    }else{
    }
  }
  setData(newdata) {
    this.data = newdata;
    if (this.reload) {
      this.reload();
    }
  }

  setTotalAmount(totalamount) {
    this.amout = totalamount;
  }

  addClickEvent(n_event){
    this.onAddItemClick = n_event
  }
  onAddItemClick(_event) {
    // console.log("vao onAddItemClick")
    this.addItemClick = _event
  }
}
