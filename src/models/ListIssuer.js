import LayoutModel from "./layoutModel.js";

export default class ListIssuer extends LayoutModel {
  constructor() {
    super();
    this.data = null;
    this.setDataEvent = this.setDataEvent.bind(this);
    this.setData = this.setData.bind(this);
    this.onClickItemEdit = null
    this.onDataChange = (list)=>{
    	 console.error("handleEventDataCallback is required.")
    };
    this.previewGCN = null;
  }

  setDataEvent(event) {
    this.onDataChange = event
  }
  setPreviewGCN(event) {
    this.previewGCN = event
  }
  setData(data) {
    this.data = data
  }
 
}
