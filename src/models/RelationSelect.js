import LayoutModel from "./layoutModel.js";

export default class RelationSelect extends LayoutModel {
  constructor() {
    super();
    this.data = null;
    this.setData = this.setData.bind(this);
    this.openRelation = null;
  }

  setData(data) {
    this.define.list = data;
    this.render = new Date().getTime();
  }

  // openRelation(){

  // }
}
