export default class LayoutModel {
  constructor() {
    this.template_id = "";
    this.type = "";
    this.extend_class = "";
    this.col = "";
    this.name = null;
    this.define = null;
    this.ref = null;
    this.layout = null;
    this.onValueChange = null;
    this.isDisable = null;
    this.mapstate = null;
    this.setValue = this.setValue.bind(this);
    this.onClick = this.onClick.bind(this);
    this.toggle = this.toggle.bind(this);
    this.setRequire = this.setRequire.bind(this);
    this.setDisable = this.setDisable.bind(this);
    this.setPattern = this.setPattern.bind(this);
    this.setLabel = this.setLabel.bind(this);
    this.hidden = this.hidden.bind(this);
    this.show = this.show.bind(this);
    this.setDataRadio = this.setDataRadio.bind(this);
  }
  onClick() {
    
  }

  hidden(isHide){
    if(this.template_id == "row_tainan"){
      console.log("Model set is hidden = ", isHide)
    }
    if (this.define) {
      this.define.isHide = isHide;
    }else{
      this.define = {isHide:isHide};
    }
  }
  show(){
    this.define.hidden = false
  }

  setRequire(isRequire) {
    if (this.define) {
      this.define.is_required = isRequire;
    }
  }
  setValue(newval) {
    if (this.define) {
      this.define.value = newval;
    }
  }
  setDisableEditor(isDisable) {
    if (this.define) {
      this.define.disable = isDisable;
    }
  }
  toggle(isHide) {
    if (this.define) {
      this.define.isHide = isHide;
    }else{
      this.define = {isHide:isHide};
    }
  }
  
  getValue() {
    if (this.define) {
      return this.define.value;
    }
  }
  setDisable(isDisable) {
    if (this.define) {
      this.define.disable = isDisable;
    }
  }
  setPattern(val) {
    if (this.define) {
      this.define.pattern = val;
    }
  }
  setLabel(val) {
    if (this.define) {
      this.define.label = val;
    }
  }
  setDataRadio(data) { // dung cho flradio
    if(this.define){
      this.define.list = data
    }
  }
}
