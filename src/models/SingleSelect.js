import LayoutModel from "./layoutModel.js";

export default class SingleSelect extends LayoutModel {
  constructor() {
    super();
    this.setData = this.setData.bind(this);
  }
  setValue(dkmvalue){
    this.define.value = dkmvalue
    if(this.onValueset){
      this.onValueset(dkmvalue)
    }
  }
  setData(data) {
    this.define.list = data
    if(this.onDataset){
    	this.onDataset(data)
    }
  }
 
}
