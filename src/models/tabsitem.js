import LayoutModel from "./layoutModel.js";

export default class TabsItem extends LayoutModel {
  constructor() {
    super();
    this.data = null;
    this.component = null;
    this.addItem = this.addItem.bind(this);
    this.removeItem = this.removeItem.bind(this);
    this.onReloadTabs = null;
    this.setActive = this.setActive.bind(this);
    this.setTabActive = null;
    this.active = null
    this.addItemClick = null
    this.onAddItemClick = this.onAddItemClick.bind(this);
  }

  setActive(id_active){
    this.active = id_active
    if(this.setTabActive){
      this.setTabActive(id_active)
    }else{
   
    }
  }

  addItem(child_component) {
    let list = this.define.children;
    list.push(child_component);
    this.define.children = [...list];
  }
  removeItem(index) {
    let list = this.define.children;
    list.splice(index, 1);
    this.define.children = [...list];
  }
  getItem() {
    return this.define.children;
  }
  setItem(list_issuer) {
    this.define.children = [...list_issuer];
    if(this.ref.current){
      this.ref.current.setTabItem(list_issuer)
    }
  }

  onAddItemClick(_event) {
    this.addItemClick = _event
  }
}
