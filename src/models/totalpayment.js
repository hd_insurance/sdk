import LayoutModel from "./layoutModel.js";

export default class TotalPayment extends LayoutModel {
  constructor() {
    super();
    this.data = 0;
    this.setData = this.setData.bind(this);
  }

  setData(data) {
    this.define.data = data;
  }
 
}
