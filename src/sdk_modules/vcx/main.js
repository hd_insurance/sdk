import React, { useEffect, useState, createRef, useRef } from "react";
import Footer from "../../common/footer/footer";
import styles from "../../css/style.module.css";
import { animateScroll } from "react-scroll";
import DynamicRender from "../../common/render/DnmRender";
import StageSpinner from "../../common/loadingpage";
import util from "../../util";
import { connect } from "react-redux";
import { setOBJState } from "../../redux/actions/action-lachan.js";
import { confirmAlert } from "react-confirm-alert";
import api from "../../services/Network.js";
import moment, { now } from "moment";
import LoadingForm from "../../common/loadingform";
import cogoToast from "cogo-toast";
import currencyFormatter from "currency-formatter";

function Main(props) {
  const footerRef = useRef();
  const [step, setStep] = useState(0);
  const [isDisableFooter, setDisableFooter] = useState(false);
  const [loading, setLoading] = useState(true);
  const [define, setDefine] = useState({});
  const [listmaped, setMaped] = useState({});

  const [loadingForm, setLoadingForm] = useState(false);

  const config_define = {
    ...props.productConfig,
  };
  const [buyer_info, setBuyerInfo] = useState({
    GENDER: "",
    NAME: "",
    PHONE: "",
    EMAIL: "",
    ADDRESS: "",
    addressCode: {
      dist: "",
      label: "",
      prov: "",
      ward: "",
    },
  });
  const setValBuyerInfo = (key, val) => {
    setBuyerInfo((prevState) => ({
      ...prevState,
      [key]: val,
    }));
  };
  const mapDatasubmit = (listFile) => {
    return {
      org_code: config_define.ORG_CODE,
      name: buyer_info.NAME,
      province: buyer_info.addressCode.prov,
      district: buyer_info.addressCode.dist,
      wards: buyer_info.addressCode.ward,
      address: buyer_info.ADDRESS,
      email: buyer_info.EMAIL,
      phone: buyer_info.PHONE,
      note: "",
      channel: "",
      attact_files: listFile,
    };
  };
  const uploadFile = (listFile) => {
    return new Promise((resolve, reject) => {
      let formData = new FormData();
      listFile.forEach((file) => {
        formData.append("files", file.file, file.newFileName);
      });
      resolve(formData);
    });
  };
  const onNextClick = async (e) => {
    animateScroll.scrollToTop();
    if (listmaped["GDKX"]) {
      setLoadingForm(true);
      const listFileGDKX = listmaped["GDKX"].ref.current.getListFile();
      const listFileHDBHVC = listmaped["HDBHVC"].ref.current.getListFile();
      try {
        const listFile = await uploadFile([...listFileHDBHVC, ...listFileGDKX]);
        //check list file lenght ???
        const responseFile = await api.upload(listFile);
        if (responseFile.success) {
          const attact_files = responseFile.data.map((e, i) => {
            return {
              file_name: e.file_org_name,
              path: e.file_key,
            };
          });
          const data = mapDatasubmit(attact_files);
          console.log("data", data);
          const org = config_define.ORG_CODE ? config_define.ORG_CODE : "HDI"
          const response = await api.post(`/api/sdk-form/registerVCX/${org}`, data);
          console.log("response", response);
          if (response.success) {
            setLoadingForm(false);
            setStep(1);
            listmaped["step_layout"].ref.current.setStep(1);
          }
        }
        // console.log(responseFile.data);
        // console.log([...listFileHDBHVC, ...listFileGDKX]);

        // console.log(config_define);
      } catch (err) {
        console.log(err);
      }
    }
  };
  const onPrevClick = (e) => {};

  useEffect(() => {
    const { obj_config, map } = util.rootObjectComponent(props.page_layout);
    setMaped(map);
    map["button_2"].onClick = () => {
      window.location.href = "https://hdinsurance.com.vn";
    };
    setDefine(obj_config);
    setTimeout(() => {
      setLoading(false);
      footerRef.current.setNextTitle("Đăng ký");
    }, 200);
  }, []);

  return (
    <div className={styles.main_container}>
      {loadingForm && <LoadingForm />}
      {loading ? (
        <div className={styles.main_loading}>
          <div className={styles.ff_loading}>
            <StageSpinner size={60} frontColor="#329945" loading={loading} />
          </div>
        </div>
      ) : (
        <div className={`container ${styles.kklm}`}>
          <DynamicRender
            layout={define}
            gender={buyer_info.GENDER}
            name={buyer_info.NAME}
            phone={buyer_info.PHONE}
            email={buyer_info.EMAIL}
            address={buyer_info.ADDRESS}
            addressCode={buyer_info.addressCode}
            onInputChange={(name, value) => {
              console.log(name, value);
              setValBuyerInfo(name, value);
            }}
          />
          <Footer
            ref={footerRef}
            isDisable={isDisableFooter}
            step={step}
            onPrev={onPrevClick}
            onNext={onNextClick}
            lastStep={1}
          />
        </div>
      )}
    </div>
  );
}

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => ({
  setOBJState: (obj) => dispatch(setOBJState(obj)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Main);
