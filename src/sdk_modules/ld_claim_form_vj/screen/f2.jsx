import React, {
  useEffect,
  useState,
  createRef,
  forwardRef,
  useImperativeHandle,
} from "react";
import TopTitle from "../../../common/toptitle";
import cogoToast from "cogo-toast";
import { connect } from "react-redux";
import {
  fetchPackages,
  setUserList,
  checkRegistered,
} from "../../../redux/actions/action-bhsk.js";
import { confirmAlert } from "react-confirm-alert";
import moment from "moment";
import Popover from "react-popover";
import { Button, Col, Row, Form } from "react-bootstrap";
// import FLTextArea from "../../../libs/flinput/textarea";
import { FLInput, FLBankList, FLSelect, FLAmount } from "hdi-uikit";
// import FLDate from "../../../libs/flinput/date.js";
// import FLTime from "../../../libs/flinput/timepicker.jsx";
// import FLAddress from "../../../libs/flinput/address.js";
// import FLBankList from "../../../libs/flinput/bank.js";
// import FLSelect from "../../../libs/flinput/listselect.js";
// import FLSelectMultiple from "../../../libs/flinput/listselectmultiple.js";
// import FLAmount from "../../../libs/flinput/amount.js";

import FormTainan from "../benefit/tainan";
import FormIsolation from "../benefit/cachly";
import styles from "../../../css/style.module.css";

const Form2 = forwardRef((props, ref) => {
  const formRef = createRef();
  const form1Ref = createRef();
  const form2Ref = createRef();

  const [validated, setValidated] = useState(false);
  const [ben1, setBen1] = useState(false);
  const [ben2, setBen2] = useState(false);

  const [amountClaim, setAmoutClaim] = useState(null);
  const [payMethod, setPaymethod] = useState("CK");
  const [bankNum, setBankNum] = useState();
  const [bank, setBank] = useState({ label: null, id: null });
  const [bankAccount, setBankAccount] = useState(null);

  const [addressCode, setAddressCodeValue] = useState({
    label: null,
    prov: null,
    dist: null,
    ward: null,
  });

  useImperativeHandle(ref, () => ({
    handleSubmit() {
      const form = formRef.current;
      if (form.checkValidity() === false) {
        setValidated(true);
        return false;
      }
      let dataTainan = {};
      let dataCachly = {};
      if (ben1) {
        dataTainan = form1Ref.current.getData();
      }
      if (ben2) {
        dataCachly = form2Ref.current.getData();
      }
      const bankInfo = {
        bank,
        bankAccount,
        bankNum,
        payMethod,
        amountClaim,
      };
      return {
        ben1,
        ben2,
        dataTainan,
        dataCachly,
        bankInfo,
      };
    },
  }));

  const cbModeChange = (e) => {
    if (e.target.name == "BEN1") {
      setBen1(e.target.checked);
    } else if (e.target.name == "BEN2") {
      setBen2(e.target.checked);
    }
  };

  return (
    <div className={`${styles.indemnify_form} f2`}>
      <TopTitle title={lng.get("uyituyq0")} subtitle={""} />

      <Form className="f-orm" ref={formRef} noValidate validated={validated}>
        <label className="form-switch">
          <input
            type="checkbox"
            name={"BEN1"}
            checked={ben1}
            onChange={(e) => cbModeChange(e)}
          />
          <i></i> {lng.get("lbdbosdb")}
        </label>

        {ben1 && <FormTainan ref={form1Ref} />}

        <div className={styles.isolation}>
          <label className="form-switch">
            <input
              type="checkbox"
              name={"BEN2"}
              onChange={(e) => cbModeChange(e)}
            />
            <i></i> {lng.get("uqrnye90")}
          </label>

          {ben2 && <FormIsolation ref={form2Ref} />}

          <div className={styles.bank_info}>
            <Row>
              <div className="col-md-3 mt-15">
                <FLAmount
                  required={true}
                  value={amountClaim}
                  changeEvent={setAmoutClaim}
                  label={lng.get("pso6fxqw")}
                />
              </div>
              <div className="col-md-3 mt-15">
                <FLSelect
                  disable={false}
                  label={lng.get("yooywyv9")}
                  value={payMethod}
                  changeEvent={setPaymethod}
                  dropdown={true}
                  required={true}
                  data={[{ label: lng.get("24sdbgn6"), value: "CK" }]}
                />
              </div>
            </Row>

            {payMethod == "CK" && (
              <Row>
                <div className="col-md-3 mt-15">
                  <FLInput
                    required={true}
                    changeEvent={setBankNum}
                    value={bankNum}
                    label={lng.get("fjvajqcl")}
                  />
                </div>
                <div className="col-md-3 mt-15">
                  <FLBankList
                    disable={false}
                    changeEvent={setBank}
                    value={bank}
                    label={lng.get("8flfvfio")}
                    required={true}
                  />
                </div>
                <div className="col-md-3 mt-15">
                  <FLInput
                    required={true}
                    value={bankAccount}
                    changeEvent={setBankAccount}
                    isUpperCase={true}
                    label={lng.get("b7peygbc")}
                  />
                </div>
              </Row>
            )}
          </div>
        </div>
      </Form>
    </div>
  );
});

const mapStateToProps = (state) => {
  return {
    packages: state.data,
    isruserlist: state.isruserlist,
  };
};
const mapDispatchToProps = (dispatch) => ({
  setUserList: (usrl) => dispatch(setUserList(usrl)),
  checkRegistered: (isChecked) => dispatch(checkRegistered(isChecked)),
});

export default connect(mapStateToProps, mapDispatchToProps, null, {
  forwardRef: true,
})(Form2);
