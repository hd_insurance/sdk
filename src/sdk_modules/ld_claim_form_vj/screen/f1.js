import React, {
  useEffect,
  useState,
  createRef,
  forwardRef,
  useImperativeHandle,
} from "react";
import TopTitle from "../../../common/toptitle";
import cogoToast from "cogo-toast";
import { connect } from "react-redux";
import {
  fetchPackages,
  setUserList,
  checkRegistered,
} from "../../../redux/actions/action-bhsk.js";
import { confirmAlert } from "react-confirm-alert";
import moment from "moment";
import Popover from "react-popover";
import { Button, Col, Row, Form } from "react-bootstrap";
import { FLInput, FLDate, FLAddress, FLSelect, FLSelectMultiple } from "hdi-uikit";
// import FLDate from "../../../libs/flinput/date.js";
// import FLAddress from "../../../libs/flinput/address.js";
// import FLSelect from "../../../libs/flinput/listselect.js";
// import FLSelectMultiple from "../../../libs/flinput/listselectmultiple.js";
import styles from "../../../css/style.module.css";


const Form1 = forwardRef((props, ref) => {
  const formRef = createRef();
  const [validated, setValidated] = useState(false);
  const [mode, setMode] = useState(0);
  //
  const [isuData, setIsuData] = useState(null);

  //nguoi khai ho
  const [atn_gender, setAtnGender] = useState("M");
  const [atn_name, setAtnName] = useState(null);
  const [atn_relation, setAtnRelation] = useState(null);
  const [atn_phonenumber, setAtnPhonenumber] = useState(null);
  const [atn_email, setAtnEmail] = useState(null);
  const [atn_passport, setAtnPassport] = useState(null);
  const [atn_dob, setAtnDob] = useState(null);
  const [atn_address, setAtnAddress] = useState(null);
  const [ant_addressCode, setAntAddressCodeValue] = useState({
    label: null,
    prov: null,
    dist: null,
    ward: null,
  });

  //nguoi duoc bao hiem
  const [isu_phonenumber, setIsuPhonenumber] = useState(null);
  const [isu_email, setIsuEmail] = useState(null);
  const [isu_passport, setIsuPassport] = useState(null);
  const [isu_dob, setIsuDob] = useState(null);

  const relationList = [
    {
      title: "Bố/mẹ",
      subtitle: "Chọn mối quan hệ",
      list: [
        { title: "Bố", value: "BOME-M" },
        { title: "Mẹ", value: "BOME-F" },
      ],
    },
    {
      title: l.g("bhsk.form.lb_title_children"),
      subtitle: "Chọn mối quan hệ",
      list: [
        { title: l.g("bhsk.form.lb_title_son"), value: "CON_CAI-M" },
        { title: l.g("bhsk.form.lb_title_daughters"), value: "CON_CAI-F" },
      ],
    },
    {
      title: l.g("bhsk.form.lb_title_legal_spouse"),
      subtitle: "Chọn mối quan hệ",
      list: [
        { title: l.g("bhsk.form.lb_title_legal_husband"), value: "VO_CHONG-M" },
        { title: l.g("bhsk.form.lb_title_legal_wife"), value: "VO_CHONG-F" },
      ],
    },
    { title: "Anh/chị/em", subtitle: "Chọn mối quan hệ", value: "ANHCHIEM-M" },
  ];

  useImperativeHandle(ref, () => ({
    handleSubmit() {
      const form = formRef.current;
      if (form.checkValidity() === false) {
        setValidated(true);
        return false;
      }
      var output = {};
      const isuer = {
        isu_phonenumber,
        isu_email,
        isu_passport,
        isu_dob,
      };
      if (mode == 1) {
        output.atn = {
          atn_gender,
          atn_name,
          atn_relation,
          atn_phonenumber,
          atn_email,
          atn_passport,
          atn_dob,
          atn_address,
          ant_addressCode,
        };
      }
      output.isuer = isuer;
      return output;
    },
  }));

  useEffect(() => {
    setIsuData(props.isuData);
  }, [props.isuData]);

  const onRdChange = (e) => {
    if (e.target.checked) {
      setMode(e.target.value);
    }
  };

  return (
    <div className={`${styles.indemnify_form} f1`}>
      <TopTitle title={lng.get("o2bjchek")} subtitle={""} />

      <div className={styles.top_select_box}>
        <div className={styles.jchild}>
          <p>{lng.get("lhfvwjwp")}</p>

          <div className={styles.rd_group}>
            <input
              className="radio"
              type="radio"
              name="rd1"
              value={0}
              onChange={(e) => onRdChange(e)}
              id="radio-1"
              defaultChecked={mode == 0}
            />
            <label for="radio-1">{lng.get("zzsalhgc")}</label>
            <div className="sp-hoz" />
            <input
              className="radio"
              type="radio"
              name="rd1"
              value={1}
              onChange={(e) => onRdChange(e)}
              id="radio-2"
              defaultChecked={mode == 1}
            />
            <label for="radio-2">{lng.get("9e43wr14")}</label>
          </div>
        </div>
      </div>

      <Form className="f-orm" ref={formRef} noValidate validated={validated}>
        {mode == 1 && (
          <Row className="declaration-form">
            <div className="col-md-3 mt-15">
              <FLSelect
                disable={false}
                label={l.g("bhsk.form.lbl_title")}
                value={atn_gender}
                changeEvent={setAtnGender}
                dropdown={true}
                required={true}
                data={[
                  { label: l.g("bhsk.form.f3_title15"), value: "M" },
                  { label: l.g("bhsk.form.f3_title16"), value: "F" },
                ]}
              />
            </div>
            <div className="col-md-3 mt-15">
              <FLInput
                disable={false}
                value={atn_name}
                changeEvent={setAtnName}
                isUpperCase={true}
                label={lng.get("osc2y9h7")}
                required={true}
              />
            </div>
            <div className="col-md-3 mt-15">
              <FLSelectMultiple
                disable={false}
                label={lng.get("mcmcphxr")}
                sublabel={lng.get("1fvj6zzn")}
                value={atn_relation}
                changeEvent={setAtnRelation}
                dropdown={true}
                required={true}
                data={relationList}
              />
            </div>
            <div className="col-md-3 mt-15">
              <FLInput
                changeEvent={setAtnPhonenumber}
                value={atn_phonenumber}
                label={lng.get("dlii8f7l")}
                pattern="[0-9]{9,12}"
                required={true}
              />
            </div>

            <div className="col-md-3 mt-15">
              <FLInput
                label={"Email"}
                pattern={`[a-z0-9._%+-]+@+[a-z0-9._%+-]+`}
                changeEvent={setAtnEmail}
                value={atn_email}
                required={true}
              />
            </div>
            <div className="col-md-3 mt-15">
              <FLInput
                label={lng.get("rxrrlbfu")}
                // pattern="[0-9]{9,22}"
                changeEvent={setAtnPassport}
                value={atn_passport}
                required={true}
              />
            </div>
            <div className="col-md-3 mt-15">
              <FLDate
                disable={false}
                value={atn_dob}
                changeEvent={setAtnDob}
                label={lng.get("xoiaux01")}
                required={true}
              />
            </div>
            <div className="col-md-3 mt-15"></div>
            <div className="col-md-12 mt-15">
              <div className="group-ipnv mobile-mt">
                <FLInput
                  required={true}
                  value={atn_address}
                  changeEvent={setAtnAddress}
                  label={l.g("bhsk.form.street")}
                  hideborder={true}
                />
                <div className="ver-line"></div>
                <div className="kkjs">
                  <FLAddress
                    disable={false}
                    changeEvent={setAntAddressCodeValue}
                    value={ant_addressCode}
                    label={l.g("bhsk.form.address")}
                    required={true}
                    hideborder={true}
                  />
                </div>
              </div>
            </div>
          </Row>
        )}

        {isuData && (
          <div className={styles.insur_info}>
            <Row>
              <div className="col-md-4 mt-15">
                <div className={styles.f1a}>
                  <div className={styles.l}>{lng.get("va9clnf0")}:</div>
                  <div className={styles.r}>{isuData.PRODUCT_NAME}</div>
                </div>
              </div>
              <div className="col-md-4 mt-15">
                <div className={styles.f1a}>
                  <div className={styles.l}>{lng.get("5pxdpuwx")}:</div>
                  <div className={styles.r}>{isuData.CERTIFICATE_NO}</div>
                </div>
              </div>
              <div className="col-md-4 mt-15">
                <div className={styles.f1a}>
                  <div className={styles.l}>{lng.get("8jrol4t4")}:</div>
                  <div className={styles.r}>{isuData.NAME}</div>
                </div>
              </div>
              <div className="col-md-4 mt-15">
                <div className={styles.f1a}>
                  <div className={styles.l}>{lng.get("gcvppmdy")}:</div>
                  <div className={styles.r}>{isuData.BOOKING_ID}</div>
                </div>
              </div>
              <div className="col-md-4 mt-15">
                <div className={styles.f1a}>
                  <div className={styles.l}>{lng.get("jcqxauhp")}:</div>
                  <div className={styles.r}>{isuData.FLI_NO}</div>
                </div>
              </div>
              <div className="col-md-4 mt-15">
                <div className={styles.f1a}>
                  <div className={styles.l}>{lng.get("fdjhx4bj")}:</div>
                  <div className={styles.r}>
                    {isuData.DEP_NAME} - {isuData.ARR_NAME}
                  </div>
                </div>
              </div>
              <div className="col-md-4 mt-15">
                <div className={styles.f1a}>
                  <div className={styles.l}>{lng.get("5yjtbbp1")}:</div>
                  <div className={styles.r}>{isuData.FLI_R_DATE}</div>
                </div>
              </div>
              <div className="col-md-4 mt-15">
                <div className={styles.f1a}>
                  <div className={styles.l}>{lng.get("90nhfruf")}:</div>
                  <div className={styles.r}>{isuData.SEAT}</div>
                </div>
              </div>
              <div className="col-md-4"></div>
            </Row>
          </div>
        )}

        <p className={styles.note_a1}>{lng.get("mnazberz")}</p>

        <Row>
          <div className="col-md-3">
            <FLInput
              changeEvent={setIsuPhonenumber}
              value={isu_phonenumber}
              label={lng.get("dlii8f7l")}
              pattern="[0-9]{9,12}"
              required={true}
            />
          </div>
          <div className="col-md-3">
            <FLInput
              label={"Email"}
              pattern={`[a-z0-9._%+-]+@+[a-z0-9._%+-]+`}
              changeEvent={setIsuEmail}
              value={isu_email}
              required={true}
            />
          </div>
          <div className="col-md-3">
            <FLInput
              label={lng.get("rxrrlbfu")}
              // pattern="[0-9]{9,22}"
              changeEvent={setIsuPassport}
              value={isu_passport}
              required={true}
            />
          </div>
          <div className="col-md-3">
            <FLDate
              disable={false}
              value={isu_dob}
              changeEvent={setIsuDob}
              label={lng.get("xoiaux01")}
              required={true}
            />
          </div>
        </Row>
      </Form>
    </div>
  );
});

const mapStateToProps = (state) => {
  return {
    packages: state.data,
    isruserlist: state.isruserlist,
  };
};
const mapDispatchToProps = (dispatch) => ({
  setUserList: (usrl) => dispatch(setUserList(usrl)),
  checkRegistered: (isChecked) => dispatch(checkRegistered(isChecked)),
});

export default connect(mapStateToProps, mapDispatchToProps, null, {
  forwardRef: true,
})(Form1);
