import React, {
  useEffect,
  useState,
  useContext,
  createRef,
  forwardRef,
  useImperativeHandle,
} from "react";
import {
  Button,
  Form,
  Col,
  Row,
  Accordion,
  AccordionContext,
  useAccordionToggle,
} from "react-bootstrap";
import TopTitle from "../../../common/toptitle";
import api from "../../../services/Network.js";
import currencyFormatter from "currency-formatter";
import styles from "../../../css/style.module.css";

const Form4 = forwardRef((props, ref) => {
  const formRef = createRef();
  const [isuData, setIsuData] = useState(null);
  const [dataF1, setDataF1] = useState({ isuer: {} });
  const [dataF2, setDataF2] = useState({ bankInfo: {} });

  useImperativeHandle(ref, () => ({
    handleSubmit() {
      const form = formRef.current;
      if (form.checkValidity() === false) {
        setValidated(true);
        return false;
      }

      return null;
    },
  }));

  useEffect(() => {}, []);

  useEffect(() => {
    setIsuData(props.isuData);
  }, [props.isuData]);

  useEffect(() => {
    if (props.dataF1 != null) {
      setDataF1(props.dataF1);
    }
  }, [props.dataF1]);
  useEffect(() => {
    if (props.dataF2) {
      setDataF2(props.dataF2);
    }
  }, [props.dataF2]);

  return (
    <div className={`${styles.indemnify_form} f4`}>
      <TopTitle title={lng.get("0lgdflcg")} subtitle={""} />

      <div className={styles.insur_info}>
        {isuData && (
          <Row>
            <div className={`col-md-4 mt-15`}>
              <div className={styles.f1a}>
                <div className={styles.l}>{lng.get("va9clnf0")}:</div>
                <div className={styles.r}>{isuData.PRODUCT_NAME}</div>
              </div>
            </div>
            <div className={`col-md-4 mt-15`}>
              <div className={styles.f1a}>
                <div className={styles.l}>{lng.get("5pxdpuwx")}:</div>
                <div className={styles.r}>{isuData.CERTIFICATE_NO}</div>
              </div>
            </div>
            <div className={`col-md-4 mt-15`}>
              <div className={styles.f1a}>
                <div className={styles.l}>{lng.get("8jrol4t4")}:</div>
                <div className={styles.r}>{isuData.NAME}</div>
              </div>
            </div>
            <div className={`col-md-4 mt-15`}>
              <div className={styles.f1a}>
                <div className={styles.l}>{lng.get("gcvppmdy")}:</div>
                <div className={styles.r}>{isuData.BOOKING_ID}</div>
              </div>
            </div>
            <div className={`col-md-4 mt-15`}>
              <div className={styles.f1a}>
                <div className={styles.l} lng={"jcqxauhp"}>
                  {lng.get("jcqxauhp")}:
                </div>
                <div className={styles.r}>{isuData.FLI_NO}</div>
              </div>
            </div>
            <div className={`col-md-4 mt-15`}>
              <div className={styles.f1a}>
                <div className={styles.l}>{lng.get("fdjhx4bj")}:</div>
                <div className={styles.r}>
                  {isuData.DEP_NAME} - {isuData.ARR_NAME}
                </div>
              </div>
            </div>
            <div className={`col-md-4 mt-15`}>
              <div className={styles.f1a}>
                <div className={styles.l}>{lng.get("5yjtbbp1")}:</div>
                <div className={styles.r}>{isuData.FLI_R_DATE}</div>
              </div>
            </div>
            <div className={`col-md-4 mt-15`}>
              <div className={styles.f1a}>
                <div className={styles.l}>{lng.get("90nhfruf")}:</div>
                <div className={styles.r}>{isuData.SEAT}</div>
              </div>
            </div>
            <div className={`col-md-4 mt-15`}>
              <div className={styles.f1a}>
                <div className={styles.l}>{lng.get("spckrdbz")}:</div>
                <div className={styles.r}>{dataF1.isuer.isu_phonenumber}</div>
              </div>
            </div>

            <div className={`col-md-4 mt-15`}>
              <div className={styles.f1a}>
                <div className={styles.l}>Email:</div>
                <div className={`${styles.r} ${styles.text_normal}`}>
                  {dataF1.isuer.isu_email}
                </div>
              </div>
            </div>
            <div className={`col-md-4 mt-15`}>
              <div className={styles.f1a}>
                <div className={styles.l}>{lng.get("mxockjz4")}:</div>
                <div className={styles.r}>{dataF1.isuer.isu_passport}</div>
              </div>
            </div>
            <div className={`col-md-4 mt-15`}>
              <div className={styles.f1a}>
                <div className={styles.l}>{lng.get("qhhb7cfh")}:</div>
                <div className={styles.r}>{dataF1.isuer.isu_dob}</div>
              </div>
            </div>
            <div className={`col-md-4 mt-15`}>
              <div className={styles.f1a}>
                <div className={styles.l}>{lng.get("lcc1uvh7")}:</div>
                <div className={styles.r}>
                  {currencyFormatter.format(dataF2.bankInfo.amountClaim, {
                    code: l.g("bhsk.currency"),
                    precision: 0,
                    format: "%v %s",
                    symbol: l.g("bhsk.currency"),
                  })}
                </div>
              </div>
            </div>
            <div className={`col-md-4 mt-15`}>
              <div className={styles.f1a}>
                <div className={styles.l}>{lng.get("xygtume0")}:</div>
                <div className={styles.r}>
                  {dataF2.bankInfo.payMethod == "CK"
                    ? lng.get("24sdbgn6")
                    : "Tiền mặt"}
                </div>
              </div>
            </div>
            {dataF2.bankInfo.payMethod == "CK" && (
              <div className={`col-md-4 mt-15`}>
                <div className={styles.f1a}>
                  <div className={styles.l}>{lng.get("zofx5xfl")}:</div>
                  <div className={styles.r}>
                    {dataF2.bankInfo ? dataF2.bankInfo.bankNum : ""}
                  </div>
                </div>
              </div>
            )}
            {dataF2.bankInfo.payMethod == "CK" && (
              <div className={`col-md-4 mt-15`}>
                <div className={styles.f1a}>
                  <div className={styles.l}>{lng.get("uiuzuvhv")}:</div>
                  <div className={styles.r}>
                    {dataF2.bankInfo ? dataF2.bankInfo.bank.label : ""}
                  </div>
                </div>
              </div>
            )}

            {dataF2.bankInfo.payMethod == "CK" && (
              <div className={`col-md-4 mt-15`}>
                <div className={styles.f1a}>
                  <div className={styles.l}>{lng.get("8gtrlu9p")}:</div>
                  <div className={`r ${styles.text_uppercase}`}>
                    {dataF2.bankInfo ? dataF2.bankInfo.bankAccount : ""}
                  </div>
                </div>
              </div>
            )}
          </Row>
        )}
      </div>
    </div>
  );
});

export default Form4;
