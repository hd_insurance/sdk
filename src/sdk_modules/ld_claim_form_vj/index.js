import React from "react";
import Main from "./main";
import { Provider } from "react-redux";
import store from "../../redux/store";

const ClaimForm = (props) => {
  return (
    <Provider store={store}>
      <Main {...props} />
    </Provider>
  );
};

export default ClaimForm;
