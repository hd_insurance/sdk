import React, { useEffect, useState, createRef, forwardRef, useImperativeHandle } from "react";
import { Button, Col, Row, Form} from 'react-bootstrap';

import {FLTextArea, FLInput, FLDate, FLTime, FLAddress} from "hdi-uikit";
// import FLInput from "../../../libs/flinput";
// import FLDate from "../../../libs/flinput/date.js";
// import FLTime from "../../../libs/flinput/timepicker.jsx";
// import FLAddress from "../../../libs/flinput/address.js";
// import FLSelect from "../../../libs/flinput/listselect.js";
// import FLSelectMultiple from "../../../libs/flinput/listselectmultiple.js";
// import FLAmount from "../../../libs/flinput/amount.js";


const Tainan = forwardRef((props, ref) => {
	const [consequence, setConsequence] = useState("OTHER");

	const [shortSumary, setShortSumary] = useState(null);
	const [address, setAddress] = useState(null);
	const [addressCode, setAddressCode] = useState({label: null, prov:null, dist: null, ward: null});
	const [date, setDate] = useState(null);
	const [time, setTime] = useState(null);


	//hospitalize
	const [dateInHospital, setDateInHospital] = useState(null);
	const [dateOutHospital, setDateOutHospital] = useState(null);

	

	const [addressHospitalCode, setAddressHospitalCode] = useState({label: null, prov:null, dist: null, ward: null});
	const [nameHospital, setNameHospital] = useState(null);

	const [note, setNote] = useState(null);

const cbConsequence = (e)=>{
	if(e.target.checked){
		setConsequence(e.target.value)
	}
}

 useImperativeHandle(ref, () => ({
    getData() {
      let outdata = {
	      	shortSumary,
	      	address,
	      	addressCode,
	      	date,
	      	time,
	      	consequence
	      }
	      outdata.hospitalinfo = {}
	   if(consequence == "TTVV" || consequence == "OTHER"){
	   		outdata.hospitalinfo = {
	   			dateInHospital,
	   			dateOutHospital,
	   			nameHospital,
	   			addressHospitalCode
	   		}
	   }
	   if(consequence == "OTHER"){
	   		outdata.note = note
	   }


      return outdata
    }

}));


return (<div>
			<Row className="declaration-form">
			

			<div className="col-md-12 mt-15">
				<FLTextArea
		           label={lng.get("4n12tqwj")}
		           value={shortSumary}
		           changeEvent={setShortSumary}
		           required={true}
		         />
			</div>
			
			<div className="col-md-12 mt-15">
					<div className="group-ipnv">
					   <FLInput
				            required={true}
		            		value={address}
		            		changeEvent={setAddress}
				            label={lng.get("0oxnt3my")}
				            hideborder={true}
				          />
			          <div className="ver-line"></div>
			          <div className="kkjs">
			          	<FLAddress
				            disable={false}
				            changeEvent={setAddressCode}
		        			value={addressCode}
				            label={lng.get("q8f9rjax")}
				            required={true}
				            hideborder={true}
				          />
			          </div>
			        </div>
			</div>

			<div className="col-md-3 mt-15">
				<FLDate
		            disable={false}
		            changeEvent={setDate}
		            value={date}
		            label={lng.get("3gcxnaen")}
		            required={true}
	          	/>
			</div>
			<div className="col-md-3 mt-15">
				<FLTime
		            disable={false}
		            changeEvent={setTime}
		            value={time}
		            label={lng.get("bk6alezm")}
		            required={true}
	          	/>
			</div>


			<div className="col-md-12 mt-15">
				<div className="select-box">
					<div className="jchild">
						<p lng="7rhbersj">{lng.get("7rhbersj")}</p>

						<div className="rd-group">
							  <input className="radio" type="radio" name="rd_hq" id="rd-1" value={"DIE"} onChange={(e)=>cbConsequence(e)} checked={consequence == "DIE"}/>
			                  <label for="rd-1" lng="cucfyscu">{lng.get("cucfyscu")}</label>
			                  <div className="sp-hoz" />
							  <input className="radio" type="radio" name="rd_hq" id="rd-2" value={"TTVV"} onChange={(e)=>cbConsequence(e)} checked={consequence == "TTVV"}/>
			  				  <label for="rd-2" lng="65h6eus8">{lng.get("65h6eus8")}</label>
			  				  <div className="sp-hoz" />
			  				  <input className="radio" type="radio" name="rd_hq" id="rd-3" value={"OTHER"} onChange={(e)=>cbConsequence(e)} checked={consequence == "OTHER"}/>
			  				  <label for="rd-3" lng="vweceuxn">{lng.get("vweceuxn")}</label>
						 </div>
					 </div>

				</div>
			</div>




		</Row>

		{(consequence=="TTVV" || consequence=="OTHER")&&<Row>
			<div className="col-md-3 mt-15">
				<FLDate
		            disable={false}
		            changeEvent={setDateInHospital}
		            value={dateInHospital}
		            label={lng.get("4pqnxwfx")}
		            required={true}
	          	/>
			</div>
			<div className="col-md-3 mt-15">
				<FLDate
		            disable={false}
		            changeEvent={setDateOutHospital}
		            value={dateOutHospital}
		            label={lng.get("3c9rvuxs")}
		            required={true}
	          	/>
			</div>
			<div className="col-md-6 mt-15">
				<div className="group-ipnv">
				   <FLAddress
		            disable={false}
		            changeEvent={setAddressHospitalCode}
        			value={addressHospitalCode}
        			mode={1} //chi lay thanh pho
		            label={lng.get("fdguycdu")}
		            hideborder={true}
		            dropdown={true}
		            required={true}
		          />
		          <div className="ver-line"></div>
		          <div className="kkjs">
		          	<FLInput
		            	required={true}
		        		changeEvent={setNameHospital}
		        		value={nameHospital}
		        		hideborder={true}
			            label={lng.get("mccqlikz")}
			          />
		          </div>
		        </div>
			
			</div>
			</Row>}

			{consequence=="OTHER"&&<Row>
				<div className="col-md-12 mt-15">
					<FLTextArea
			           label={lng.get("jsmwdctb")}
			           changeEvent={setNote}
			           value={note}
			           required={false}
			         />
				</div>
			</Row>}

  		</div>)

})


export default Tainan;