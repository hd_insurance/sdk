import React, { useEffect, useState, createRef, forwardRef, useImperativeHandle } from "react";
import { Button, Col, Row, Form} from 'react-bootstrap';

import {FLTextArea, FLInput, FLDate, FLAddress} from "hdi-uikit";
// import FLInput from "../../../libs/flinput";
// import FLDate from "../../../libs/flinput/date.js";
// import FLTime from "../../../libs/flinput/timepicker.jsx";
// import FLAddress from "../../../libs/flinput/address.js";
// import FLSelect from "../../../libs/flinput/listselect.js";
// import FLSelectMultiple from "../../../libs/flinput/listselectmultiple.js";
// import FLAmount from "../../../libs/flinput/amount.js";


const CachLy = forwardRef((props, ref) => {
	const [addressCode, setAddressCodeValue] = useState({label: null, prov:null, dist: null, ward: null});

	const [reason, setReason] = useState(null);
	const [nameCQCN, setNameCQCN] = useState(null);
	const [nameIsolationArea, setNameIsolationArea] = useState(null);
	const [addressIsolationArea, setAddressIsolationArea] = useState(null);
	const [isolationCode, setIsolationCode] = useState({label: null, prov:null, dist: null, ward: null});
	const [dateIn, setDateIn] = useState(null);
	const [dateOut, setDateOut] = useState(null);

useImperativeHandle(ref, () => ({
    getData() {
      var outdata = {
      	reason,
      	nameCQCN,
      	nameIsolationArea,
      	addressIsolationArea,
      	addressCode,
		isolationCode,
      	dateIn,
      	dateOut
      }
      return outdata;
    }

}));

return (<Row>
			<div className="col-md-12 mt-15">
				<FLTextArea
		           label={lng.get("jjcvtauo")}
		           value={reason}
		           changeEvent={setReason}
		           required={true}
		         />
		    </div>
		    <div className="col-md-3 mt-15">
		    	<FLInput
		            required={true}
		            value={nameCQCN}
	        		changeEvent={setNameCQCN}
		            label={lng.get("goptqpze")}
		          />
		    </div>
		    <div className="col-md-3 mt-15">
		    	<FLInput
		            required={true}
	        		changeEvent={setNameIsolationArea}
	        		value={nameIsolationArea}
		            label={lng.get("y6bgulqu")}
		          />
		    </div>
		    <div className="col-md-6 mt-15">
		    	<FLInput
		            required={true}
	        		changeEvent={setAddressIsolationArea}
	        		value={addressIsolationArea}
		            label={lng.get("6awkcgcy")}
		          />
		    </div>

		    <div className="col-md-3 mt-15">
		        <FLAddress
		            disable={false}
		            changeEvent={setIsolationCode}
        			value={isolationCode}
        			mode={1} //chi lay thanh pho
		            label={lng.get("50pkrlva")}
		            required={true}
		          />
			        
		    </div>

		     <div className="col-md-3 mt-15">
		    	<FLDate
		            disable={false}
		            changeEvent={setDateIn}
		            value={dateIn}
		            label={lng.get("8me8rzzi")}
		            required={true}
	          	/>
		    </div>
		    <div className="col-md-3 mt-15">
		    	<FLDate
		            disable={false}
		            changeEvent={setDateOut}
		            value={dateOut}
		            label={lng.get("gjdhstda")}
		            required={true}
	          	/>
		    </div>
		    
		    </Row>)

})


export default CachLy;