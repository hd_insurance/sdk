import React, { useEffect, useState, createRef } from "react";
import router, { useRouter } from "next/router";
import StageSpinner from "../../common/loadingpage";
import randomstring from "randomstring";
import { animateScroll } from "react-scroll";
import { connect } from "react-redux";
import Wizard from "../../common/topwizard";
import Footer from "../../common/footer/footer";
import { confirmAlert } from "react-confirm-alert";
import { fetchPackages, setUserList } from "../../redux/actions/action-bhsk.js";
import { Tab } from "react-bootstrap";
import F1 from "./screen/f1.js";
import F2 from "./screen/f2.js";
import F3 from "./screen/f3.js";
import F4 from "./screen/f4.js";
import api from "../../services/Network.js";
import styles from '../../css/style.module.css';

const Main = (props) => {
  const footerRef = createRef();
  const F1Ref = createRef();
  const F2Ref = createRef();
  const F3Ref = createRef();

  const [step, setStep] = useState("0");
  const [disableNext, setDisableNext] = useState(false);

  const [topHead, setTopHead] = useState({
    title: "Thông tin nhân viên",
    subtitle:
      "Người thân chỉ có thể đăng ký nếu Nhân viên cũng tham gia BH Sức khỏe cho nhân viên và người thân HDBank",
  });
  const [loading, setLoading] = useState(true);
  const [isActivation, setActivation] = useState(false);

  const [emloyerInfo, setEmployerInfo] = useState({});
  const [defaultActive, setDefaultActive] = useState(null);
  const [voucherInfo, setVoucherInfo] = useState(null);
  const [org, setOrg] = useState(props.org);
  const [id, setId] = useState(null);
  const [slPack, setSlPack] = useState(null);
  // const [packages, setPackages] = useState([]);

  const wzdata = [
    {
      step: l.g("bhsk.form.lbbl_step_1"),
      label: l.g("bhsk.form.lbbl_title_1"),
      icon: "fas fa-user",
    },
    {
      step: l.g("bhsk.form.lbbl_step_2"),
      label: l.g("bhsk.form.lbbl_title_2"),
      icon: "fas fa-users",
    },
    {
      step: l.g("bhsk.form.lbbl_step_3"),
      label: l.g("bhsk.form.lbbl_title_3"),
      icon: "fas fa-list-ul",
    },
    {
      step: l.g("bhsk.form.lbbl_step_4"),
      label: l.g("bhsk.form.lbbl_title_4"),
      icon: "fas fa-check-circle",
    },
  ];

  useEffect(() => {
    try {
      setLoading(true);
      if (typeof window !== "undefined") {
        if (props.router.query.id) {
          setId(props.query.id.toString().toUpperCase());
          // getUserData(router.router.query.id.toString().toUpperCase())
        }
        setOrg(props.org);
        getInitData(props.org);
      }

      if (props.router.query.pack_code) {
        setSlPack(props.router.query.pack_code.toString().toUpperCase());
      }

      if (props.router.query.action && props.router.query.id) {
        setActivation(true);
        verifyRegister(props.router.query.id.toString().toUpperCase());
      }
    } catch (e) {
      // console.log(e)
    }
  }, []);

  const getInitData = async (orgx) => {
    try {
      const data = await api.get(
        "/api/bhsl/packages/" + orgx.toUpperCase() + "?lang=" + l.getLang()
      );
      console.log(data);
      // setPackages(data);
      props.fetchPackages(data);
      if (props.router.query.id) {
        await getUserData(props.router.query.id, data);
      } else {
        setLoading(false);
      }
    } catch (e) {
      console.log(e);
    }
  };

  const getUserData = async (usid, pkgs) => {
    return new Promise(async (resolved, rejected) => {
      try {
        if (usid) {
          const userdata = await api.get(
            "/api/bhsk/register/" + usid.toUpperCase()
          );
          if (userdata.Success) {
            if (userdata.Data[2]) {
              if (userdata.Data[2][0]) {
                setVoucherInfo(userdata.Data[2][0]);
              }
            }

            let packstaffindex = pkgs.findIndex(
              (x) => x.PACK_CODE === userdata.Data[1][0].PACK_CODE
            );

            setSlPack(userdata.Data[1][0].PACK_CODE);

            var rlt_list = [];

            const employer = userdata.Data[1].filter(function (item) {
              return item.RELATIONSHIP == "BAN_THAN";
            });

            const relatives = userdata.Data[1].filter(function (item) {
              return item.RELATIONSHIP != "BAN_THAN";
            });

            const listload = employer.concat(relatives);

            listload.map((r, index) => {
              let packindex = pkgs.findIndex(
                (x) => x.PACK_CODE === r.PACK_CODE
              );
              const pack_info = pkgs[packindex];
              if (packindex != -1) {
                const relative_user = {
                  ADDRESS: r.ADDRESS,
                  ADDRESS_LABEL: r.LOCATION_NAME,
                  TOTAL_AMOUNT: pack_info.FEES.split(".").join("") * 1,
                  CUS_NAME: r.CUS_NAME,
                  DISTRICT: r.DISTRICT,
                  DOB: r.DOB,
                  EMAIL: r.EMAIL,
                  EXCLUDE_COND1: r.EXCLUDE_COND1,
                  EXCLUDE_COND2: r.EXCLUDE_COND2,
                  GENDER: r.GENDER,
                  IDCARD: r.IDCARD,
                  IS_CONFIRM: "1",
                  ORG_CODE: r.ORG_CODE,
                  PACK_CODE: r.PACK_CODE,
                  PACK_POS: packindex,
                  PHONE: r.PHONE,
                  BANK_ACCOUNT_NUM: userdata.Data[0][0].BANK_ACCOUNT_NUM
                    ? userdata.Data[0][0].BANK_ACCOUNT_NUM
                    : "",
                  PROVINCE: r.PROVINCE,
                  RELATIONSHIP: r.RELATIONSHIP,
                  STAFF_CODE: r.STAFF_CODE,
                  WARDS: r.WARDS,
                  id: randomstring.generate(7),
                  ref: createRef(),
                  IS_EMPLOYEE: index == 0,
                  PAID_BY_SAL: 1,
                  IS_REGISTERED:
                    r.RELATIONSHIP == "BAN_THAN"
                      ? userdata.Data[0][0].IS_REGISTERED
                      : 0,
                };

                rlt_list.push(relative_user);
              }
            });

            props.setUserList([...rlt_list]);
            // setEmployerInfo(initStaff)
            // setRelatives(rlt_list)
            setLoading(false);
          }
        }

        // footerRef.current.setDisable(false)
      } catch (e) {
        console.log(e);
      }
    });
  };
  const verifyRegister = async (usid) => {
    try {
      const userdata = await api.get("/api/bhsk/verify/" + usid.toUpperCase());
    } catch (e) {
      console.log(e);
    }
  };

  const submitForm1 = (e) => {
    return F1Ref.current.handleSubmit();
  };
  const submitForm2 = (e) => {
    return F2Ref.current.handleSubmit();
  };
  const submitForm3 = (e) => {
    return F3Ref.current.dataCombined();
  };

  const onNextClick = async (e) => {
    animateScroll.scrollToTop();
    switch (step) {
      case "0":
        if (submitForm1()) {
          setEmployerInfo(submitForm1());
          F2Ref.current.update();
          setStep("1");
        }
        break;
      case "1":
        if (submitForm2()) {
          const rllist = submitForm2();
          if (rllist.length == 0) {
            confirmAlert({
              title: l.g("bhsk.form.lbl_confirm"),
              message: l.g("bhsk.form.popup_q1"),
              buttons: [
                {
                  label: l.g("bhsk.form.f3_title9"),
                  onClick: () => {},
                },
                {
                  label: l.g("bhsk.form.f3_title1x"),
                  onClick: () => {
                    setStep("2");
                    if (props.router.query.id) {
                      footerRef.current.setDisable(false);
                    } else {
                      footerRef.current.setDisable(true);
                    }
                    footerRef.current.setNextTitle(
                      l.g("bhsk.form.lbl_confirm")
                    );
                  },
                },
              ],
            });
          } else {
            setStep("2");
            if (props.router.query.id) {
              footerRef.current.setDisable(false);
            } else {
              footerRef.current.setDisable(true);
            }
            footerRef.current.setNextTitle(l.g("bhsk.form.lbl_confirm"));
          }
          // setRelatives(rllist)
          // F3Ref.current.resetRelatives(rllist)
        }
        break;
      case "2":
        const isSuccess = await submitForm3();
        if (isSuccess) {
          setStep("3");
        }
        break;
    }
  };
  const onPrevClick = (e) => {
    switch (step) {
      case "1":
        if (submitForm2()) {
          // setRelatives(submitForm2())
          setStep("0");
        }
        break;
      case "2":
        setStep("1");

        footerRef.current.setDisable(false);
        footerRef.current.setNextTitle(l.g("bhsk.form.lbl_next"));
        break;
        break;
      case "3":
        setStep("2");
        break;
    }
  };
  const handleSubmit = (info) => {};

  const onCbTermChange = (xxx) => {
    footerRef.current.setDisable(xxx);
  };

  const onEditClick = (id) => {
    if (id == "idstaff") {
      setStep("0");
      footerRef.current.setDisable(false);
    } else {
      setDefaultActive(id);
      setStep("1");
      footerRef.current.setDisable(false);
    }
  };

  const backtoEdit = () => {
    const urlx = props.router.pathname + "?id=" + props.router.query.id;
    setActivation(false);
    // router.push(urlx, undefined, { shallow: true });
    props.router.push(urlx, undefined, { shallow: true })
  };

  if (isActivation) {
    return (
      <div className={styles.main_container}>
        {loading ? (
          <div className={styles.main_loading}>
            <div className={styles.ff_loading}>
            <StageSpinner size={60} frontColor="#329945" loading={loading} />
            </div>
          </div>
        ) : (
          <div className={`container ${styles.kklm} ${styles.succsess_actiiivattiioonn}`}>
            {/* <img src="/img/bhsk/success.png" /> */}
            <img src={"/img/bhsk/success.png"} alt="success" />
            <h1>Xác nhận thông tin đăng ký thành công</h1>
            <p>
              Bạn vừa thực hiện xác nhận thông tin đăng ký. Nếu có bất kỳ thắc
              mắc, hãy liên lạc với chúng tôi qua hotline{" "}
              <span className={styles.text_green}>1900068898</span> hoặc qua mail{" "}
              <span className={styles.text_green}>info@hdinsurance.com.vn</span> để
              được hỗ trợ.
            </p>
            <p>Xin cám ơn!</p>
            <button onClick={(e) => backtoEdit()}>Xem thông tin đăng ký</button>
          </div>
        )}
      </div>
    );
  } else {
    return (
      <div className={styles.main_container}>
        {loading ? (
          <div className={styles.main_loading}>
            <div className={styles.ff_loading}>
              <StageSpinner size={60} frontColor="#329945" loading={loading} />
            </div>
          </div>
        ) : (
          <div className={`container ${styles.kklm}`}>
            <Wizard active={step} data={wzdata} />
            <Tab.Container id="xxxx" activeKey={step}>
              <Tab.Content>
                <Tab.Pane eventKey="0">
                  <F1
                    org={org}
                    slPack={slPack}
                    ref={F1Ref}
                    onCbTermChange={onCbTermChange}
                    handleSubmit={handleSubmit}
                  />
                </Tab.Pane>
                <Tab.Pane eventKey="1">
                  <F2
                    ref={F2Ref}
                    org={org}
                    slPack={slPack}
                    defaultActive={defaultActive}
                    handleSubmit={handleSubmit}
                  />
                </Tab.Pane>
                <Tab.Pane eventKey="2">
                  <F3
                    ref={F3Ref}
                    org={org}
                    slPack={slPack}
                    onEditClick={onEditClick}
                    onCbTermChange={onCbTermChange}
                    voucherInfo={voucherInfo}
                    handleSubmit={handleSubmit}
                    router={props.router}
                  />
                </Tab.Pane>
                <Tab.Pane eventKey="3">
                  <F4 handleSubmit={handleSubmit} router={props.router} />
                </Tab.Pane>
              </Tab.Content>
            </Tab.Container>

            <Footer
              ref={footerRef}
              isDisable={
                props.isruserlist[0]
                  ? props.isruserlist[0].PAID_BY_SAL != 1
                  : true
              }
              step={step}
              onPrev={onPrevClick}
              onNext={onNextClick}
              lastStep={3}
            />
          </div>
        )}
      </div>
    );
  }
};

const mapStateToProps = (state) => {
  return {
    packages: state.data,
    isruserlist: state.isruserlist,
  };
};

const mapDispatchToProps = (dispatch) => ({
  fetchPackages: (orgc) => dispatch(fetchPackages(orgc)),
  setUserList: (usrl) => dispatch(setUserList(usrl)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Main);
