import React, {
  useEffect,
  useState,
  createRef,
  forwardRef,
  useImperativeHandle,
} from 'react';
import { Button, Col, Row, Form } from 'react-bootstrap';
import currencyFormatter from 'currency-formatter';
import { animateScroll } from 'react-scroll';
import { connect } from 'react-redux';
import Popover from 'react-popover';
import { FLInput, FLDate, FLAddress, FLSelect, FLMultiSelect } from "hdi-uikit";
import cogoToast from 'cogo-toast';
import moment from 'moment';
import styles from "../../../css/style.module.css";

const UserInfo = forwardRef((props, ref) => {
  const formRef = createRef();
  const { info } = props;
  const [listPack, setListPack] = useState([]);
  const [validated, setValidated] = useState(false);
  const [registeredChecked, setRegisterChecked] = useState(props.regchecked);
  const [openBenefit, setOpenBenefit] = useState(false);
  const [openRelation, setShowRelation] = useState(false);
  const [relation, setRelation] = useState({
    title: props.info.RELATIONSHIP,
    value: props.info.RELATIONSHIP
      ? props.info.RELATIONSHIP + '-' + props.info.GENDER
      : null,
  });

  const [phone, setPhoneValue] = useState(props.info.PHONE);
  const [name, setNameValue] = useState(props.info.CUS_NAME);
  const [dob, setDobValue] = useState(props.info.DOB);
  const [email, setEmailValue] = useState(props.info.EMAIL);
  const [address, setAddressValue] = useState(props.info.ADDRESS);
  const [addressCode, setAddressCodeValue] = useState({
    label: props.info.ADDRESS_LABEL,
    prov: props.info.PROVINCE,
    dist: props.info.DISTRICT,
    ward: props.info.WARDS,
  });
  const [passport, setPassportValue] = useState(props.info.IDCARD);
  const [cr_package, setCR_Package] = useState(
    info.PACK_POS ? info.PACK_POS : 0
  );
  const [pkg, setPkg] = useState({ BENEFITS: [] });
  const [amount, setAmount] = useState('0 VNĐ');
  const [applyDate, setApplyDate] = useState(
    props.packages[0].APPLY_DATE
      ? moment(props.packages[0].APPLY_DATE, 'DD-MM-YYYY')
      : moment()
  );

  useEffect(() => {
    var l = props.packages;
    const newarr = l.filter(function (item) {
      return props.regchecked ? true : item.ORG_F * 1 <= props.employerFree * 1;
    });
    newarr.map((item, index) => {
      item.label = item.PACK_NAME;
      item.value = index;
      return item;
    });
    setListPack(newarr);
    if (info.PACK_POS > -1) {
      if (info.PACK_POS != -1) {
        setPkg(props.packages[info.PACK_POS]);
        setApplyDate(
          moment(props.packages[info.PACK_POS].APPLY_DATE, 'DD-MM-YYYY')
        );
        reCalcAmount();
      } else {
        setPkg(props.packages[0]);
        setApplyDate(moment(props.packages[0].APPLY_DATE, 'DD-MM-YYYY'));
        reCalcAmount();
      }
    }
  }, [props.packages, info.PACK_POS, props.employerFree, props.regchecked]);



  const openInNewTab = () => {
    if (pkg.URL_DETAIL) {
      if (typeof window !== 'undefined') {
        const newWindow = window.open(
          pkg.URL_DETAIL,
          '_blank',
          'noopener,noreferrer'
        );
        if (newWindow) {
          newWindow.opener = null;
        }
      }
    } else {
      cogoToast.error('Vui lòng chọn gói bảo hiểm.');
    }
  };
  const actionopenBenefit = () => {
    if (pkg.URL_DETAIL) {
      setOpenBenefit(true);
    } else {
      cogoToast.error('Vui lòng chọn gói bảo hiểm.');
    }
  };

  useEffect(() => {
    if (info.PACK_POS) {
      setPkg(props.packages[info.PACK_POS]);
    } else {
      setPkg(props.packages[0]);
    }
    reCalcAmount();
  }, []);

  useEffect(() => {
    reCalcAmount();
  }, [pkg]);

  useEffect(() => {
    setRegisterChecked(props.regchecked);
  }, [props.regchecked]);

  const reCalcAmount = (d = dob) => {
    var am = 0;
    if (props.org == 'hdbank_vn' || props.org == 'hdsaison') {
      if (d) {
        const age = getAge(d);

        if (age.y == 0) {
          if (pkg.ORG_F) {
            const fee = pkg.ORG_F * 1;
            am = fee + (fee * 30) / 100;
          }
        } else {
          if (pkg.ORG_F) {
            const fee = pkg.ORG_F * 1;
            am = fee;
          }
        }
      } else {
        if (pkg.ORG_F) {
          const fee = pkg.ORG_F * 1;
          am = fee;
        }
      }
    } else {
      if (pkg.ORG_F) {
        const fee = pkg.ORG_F * 1;

        am = fee;
      }
    }

    props.reCalcAmount();
    setAmount(
      currencyFormatter.format(am, {
        code: 'VNĐ',
        precision: 0,
        format: '%v %s',
        symbol: 'VNĐ',
      })
    );
  };

  const getAge = (indob) => {
    var a = moment(applyDate, 'DD-MM-YYYY');
    var b = moment(indob, 'DD-MM-YYYY');

    var age = moment.duration(a.diff(b));
    return { y: age.years(), d: age.days() };
  };

  const getRelationTitleByValue = (value) => {
    switch (value) {
      case 'BO_ME-M':
        return 'Bố';
      case 'BO_ME-F':
        return 'Mẹ';
      case 'CON_CAI-M':
        return l.g('bhsk.form.lb_title_son');
      case 'CON_CAI-F':
        return l.g('bhsk.form.lb_title_daughters');
      case 'VO_CHONG-M':
        return l.g('bhsk.form.lb_title_legal_husband');
      case 'VO_CHONG-F':
        return l.g('bhsk.form.lb_title_legal_wife');
      default:
        return value;
    }
  };

  const relationSelect = (value) => {
    setRelation(value);
    reCalcAmount();
    setShowRelation(false);
  };

  const selectedDay = () => {
    const tdate = moment(applyDate);
    tdate.subtract(14, 'days');
    return {
      year: tdate.year(),
      month: tdate.month() + 1,
      day: tdate.date(),
    };
  };

  const maximumDate = () => {
    const tdate = moment(applyDate);
    tdate.subtract(14, 'days');
    var now = moment();

    if (now >= tdate) {
      return {
        year: tdate.year(),
        month: tdate.month() + 1,
        day: tdate.date(),
      };
    } else {
      return {
        year: now.year(),
        month: now.month() + 1,
        day: now.date(),
      };
    }
  };
  const minimumDate = () => {
    const tdate = moment(applyDate);
    return {
      year: tdate.year() - 65,
      month: tdate.month() + 1,
      day: tdate.date(),
    };
  };

  const onDOBChange = (value) => {
    setDobValue(value);
    setTimeout(() => reCalcAmount(value), 1000);
    // props.reCalcAmount()
  };

  const relationList = [
    {
      title: l.g('bhsk.form.lb_title_children'),
      subtitle: l.g('bhsk.form.lb_title_1'),
      list: [
        { title: l.g('bhsk.form.lb_title_son'), value: 'CON_CAI-M' },
        { title: l.g('bhsk.form.lb_title_daughters'), value: 'CON_CAI-F' },
      ],
    },
    {
      title: l.g('bhsk.form.lb_title_legal_spouse'),
      subtitle: l.g('bhsk.form.lb_title_1'),
      list: [
        { title: l.g('bhsk.form.lb_title_legal_husband'), value: 'VO_CHONG-M' },
        { title: l.g('bhsk.form.lb_title_legal_wife'), value: 'VO_CHONG-F' },
      ],
    },
  ];
  const popoverPropsRelation = {
    isOpen: openRelation,
    place: 'below',
    preferPlace: 'right',
    onOuterAction: () => setShowRelation(false),
    body: [
      <FLMultiSelect
        title={l.g('bhsk.form.choose_relation_title')}
        relationList={relationList}
        selected={relationSelect}
      />,
    ],
  };

  const popoverBenefit = {
    isOpen: openBenefit,
    place: 'below',
    preferPlace: 'right',
    onOuterAction: () => setOpenBenefit(false),
    body: [
      <div className={styles.list_benefit_relative}>
        <div className={styles.h}>{pkg ? pkg.PACK_NAME : null}</div>
        {pkg ? (
          <div className={styles.list_xxx}>
            {(pkg.BENEFITS ? pkg.BENEFITS : []).map((item, index) => {
              return <div className={styles.ite}>{item.NAME}</div>;
            })}
            <Popover {...popoverBenefit}>
              <Button
                className={styles.view_detail_benefit}
                onClick={(e) => openInNewTab()}
              >
                {l.g('bhsk.form.lbll_detail')}
              </Button>
            </Popover>
          </div>
        ) : null}
      </div>,
    ],
  };

  const getData = (skipcheck = false) => {
    if (!skipcheck) {
      if (!relation.value) {
        animateScroll.scrollToTop();
        setShowRelation(true);
        cogoToast.error('Chưa chọn mối quan hệ với nhân viên!');
        return false;
      }

      const relationx = relation.value.split('-');
      const spdata = {
        ORG_CODE: props.org,
        STAFF_CODE: props.employer,
        CUS_NAME: name,
        RELATIONSHIP: relationx[0],
        DOB: dob,
        GENDER: relationx[1],
        IDCARD: passport,
        PROVINCE: addressCode.prov,
        DISTRICT: addressCode.dist,
        WARDS: addressCode.ward,
        ADDRESS_LABEL: addressCode.label,
        ADDRESS: address,
        EMAIL: email,
        PHONE: phone,
        TOTAL_AMOUNT: pkg.ORG_F ? pkg.ORG_F * 1 : 0,
        PACK_CODE: pkg.PACK_CODE ? pkg.PACK_CODE : props.info.PACK_CODE,
        EXCLUDE_COND1: props.info.EXCLUDE_COND1 == 0 ? 0 : 1,
        EXCLUDE_COND2: props.info.EXCLUDE_COND2 == 0 ? 0 : 1,
        IS_CONFIRM: '1',
        PACK_POS: cr_package,
        IS_EMPLOYEE: false,
      };
      return spdata;
    } else {
      const spdata = {
        ORG_CODE: props.org,
        DOB: dob,
        TOTAL_AMOUNT: pkg.ORG_F ? pkg.ORG_F * 1 : 0,
        PACK_CODE: pkg.PACK_CODE ? pkg.PACK_CODE : props.info.PACK_CODE,
        EXCLUDE_COND1: props.info.EXCLUDE_COND1 == 0 ? 0 : 1,
        EXCLUDE_COND2: props.info.EXCLUDE_COND2 == 0 ? 0 : 1,
        PACK_POS: cr_package,
        IS_EMPLOYEE: false,
      };
      return spdata;
    }
  };
  useImperativeHandle(ref, () => ({
    handleSubmit() {
      const form = formRef.current;
      if (form.checkValidity() === false) {
        setValidated(true);
        return false;
      }
      if (getData(false)) {
        return getData(false);
      } else {
        return false;
      }
    },
    getData() {
      return getData(true);
    },
  }));

  const onPackageChange = (index) => {
    setCR_Package(index);
    setPkg(props.packages[index]);
    setApplyDate(moment(props.packages[index].APPLY_DATE, 'DD-MM-YYYY'));
    setTimeout(() => {
      // reCalcAmount()
      props.onSelectPackage();
    }, 600);
  };

  return (
    <div className='bb-form f1'>
      <Form className={styles.f_orm} ref={formRef} noValidate validated={validated}>
        <Row className={styles.jjksx}>
          <Col md={6}>
            <Popover {...popoverPropsRelation}>
              <p className={styles.select_related}>
                <span>{l.g('bhsk.form.insured_person')} - </span>
                <a onClick={(e) => setShowRelation(true)}>
                  {relation.title
                    ? `${l.g('bhsk.form.choose_relation')}: ` +
                      getRelationTitleByValue(relation.value)
                    : l.g('bhsk.form.choose_relation_title')}{' '}
                  <i className={`${styles.fas} ${styles.fa_sort_down}`}></i>
                </a>
              </p>
            </Popover>
          </Col>
          <Col className={styles.delete_ngbd} md={6}>
            <a onClick={(e) => props.onDelete()}>
              {l.g('bhsk.form.delete_insured_person')}
            </a>
          </Col>
        </Row>

        <Row>
          <Col md={3} className={styles.mobile_mt}>
            <FLInput
              label={l.g('bhsk.form.lbl_fullname')}
              value={name}
              isUpperCase={true}
              changeEvent={setNameValue}
              required={true}
            />
          </Col>
          <Col md={3} className={styles.mobile_mt}>
            <FLInput
              label={l.g('bhsk.form.lbl_phone_number')}
              changeEvent={setPhoneValue}
              value={phone}
              required={true}
            />
          </Col>
          <Col md={3} className={styles.mobile_mt}>
            <FLInput
              changeEvent={setEmailValue}
              value={email}
              label={'Email'}
            />
          </Col>
          <Col md={3} className={styles.mobile_mt}>
            <FLInput
              label={l.g('bhsk.form.lbl_passport')}
              changeEvent={setPassportValue}
              value={passport}
              required={true}
            />
          </Col>
        </Row>

        <Row className='mt-15'>
          <Col md={3} className={styles.mobile_mt}>
            <FLDate
              disable={false}
              label={l.g('bhsk.form.lbl_dob')}
              value={dob}
              selectedDay={selectedDay()}
              changeEvent={onDOBChange}
              maximumDate={maximumDate()}
              minimumDate={minimumDate()}
              required={true}
            />
          </Col>

          <Col md={9}>
            <Row>
              <Col md={6} className={styles.mobile_mt}>
                <FLInput
                  required={true}
                  value={address}
                  changeEvent={setAddressValue}
                  label={l.g('bhsk.form.street')}
                />
              </Col>

              <Col md={6} className={styles.mobile_mt}>
                <FLAddress
                  disable={false}
                  changeEvent={setAddressCodeValue}
                  value={addressCode}
                  label={l.g('bhsk.form.address')}
                  required={true}
                />
              </Col>
            </Row>
          </Col>
        </Row>

        <div className={`${styles.p_line} mt-15`} />

        <Row className='mt-15'>
          <Col md={6} className={styles.mobile_mt}>
            <div className={styles.group_ipnv}>
              <FLSelect
                hideborder={true}
                label={l.g('bhsk.form.lbl_package_title')}
                value={cr_package}
                changeEvent={onPackageChange}
                dropdown={true}
                required={true}
                data={listPack}
              />
              <div className='ver-line'></div>
              <div className={styles.kkjs}>
                <Popover {...popoverBenefit}>
                  <Button
                    onClick={(e) => {
                      actionopenBenefit();
                    }}
                    className={styles.view_ql}
                  >
                    <i className={`${styles.fas} ${styles.fa_shield_alt}`}></i>{' '}
                    {l.g('bhsk.form.lbl_view_benefit')}
                  </Button>
                </Popover>
              </div>
            </div>
          </Col>

          <Col md={6} className={styles.mobile_mt}>
            <FLInput
              disable={true}
              value={amount}
              label={l.g('bhsk.form.lbl_fee')}
            />
          </Col>
        </Row>
      </Form>

      <div className={`mt-15 ${styles.f_note}`}>
        <p>{l.g('bhsk.form.f2_ui_t1')}:</p>
        {props.org == 'hdbank_vn' || props.org == 'hdsaison' ? (
          <p>- {l.g('bhsk.form.f2_ui_t2')}</p>
        ) : null}
        <p>- {l.g('bhsk.form.f2_ui_t3')}</p>
        {props.org == 'hdbank_vn' || props.org == 'hdsaison' ? (
          <p>
            - {l.g('bhsk.form.apply_date_note_q1')}
            {pkg.APPLY_DATE}
          </p>
        ) : null}
      </div>
    </div>
  );
});
const mapStateToProps = (state) => {
  return {
    isruserlist: state.isruserlist,
    packages: state.data,
    regchecked: state.regchecked,
  };
};

export default connect(mapStateToProps, null, null, { forwardRef: true })(
  UserInfo
);
