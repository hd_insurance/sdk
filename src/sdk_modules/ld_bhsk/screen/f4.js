import React, { useEffect, useState, createRef } from 'react';
// import Router from 'next/router';
import styles from "../../../css/style.module.css";


const Form4 = (props) => {
  return (
    <div className='bb-form f4'>
      <div className={styles.form_register_success}>
        <div className={styles.img_regster_suc}>
          <img src={"/img/img_register_success.png"} />
        </div>
        <div className={styles.content_regis_success}>
          <p>
            {props.router.query.id
              ? l.g('bhsk.form.f4_label2')
              : l.g('bhsk.form.f4_label1')}
          </p>
          <p>{l.g('bhsk.form.f4_label3')}</p>
        </div>
        <div>
          <button
            className={styles.btn_discover_insur}
            onClick={() => props.router.push('https://www.hdinsurance.com.vn/')}
          >
            {l.g('bhsk.form.f4_btn1')}
          </button>
          <button
            className={styles.btn_back_home}
            onClick={() =>
              props.router.push('/promotions-packages-for-employees.hdi')
            }
          >
            {l.g('bhsk.form.f4_btn2')}
          </button>
        </div>
      </div>
    </div>
  );
};

export default Form4;
