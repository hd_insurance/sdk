import React, {
  useEffect,
  useState,
  createRef,
  forwardRef,
  useImperativeHandle,
} from "react";
import TopTitle from "../../../common/toptitle";
import cogoToast from "cogo-toast";
import { connect } from "react-redux";
import {
  fetchPackages,
  setUserList,
  checkRegistered,
} from "../../../redux/actions/action-bhsk.js";
import { confirmAlert } from "react-confirm-alert";
import moment from "moment";
import Popover from "react-popover";
import { Button, Col, Row, Form } from "react-bootstrap";
import { FLInput, FLDate, FLAddress, FLSelect } from "hdi-uikit";
import styles from "../../../css/style.module.css";

const Form1 = forwardRef((props, ref) => {
  const [listPack, setListPack] = useState([]);

  const formRef = createRef();
  const [validated, setValidated] = useState(false);
  const [termChecked, setTermChecked] = useState(
    props.isruserlist[0] ? props.isruserlist[0].PAID_BY_SAL == 1 : false
  );
  const [openBenefit, setOpenBenefit] = useState(false);
  const [registeredChecked, setRegisterChecked] = useState(
    props.isruserlist[0] ? props.isruserlist[0].IS_REGISTERED == 1 : false
  );
  const [maxAmount, setMaxAmount] = useState(-1);

  const emloyerInfo = props.isruserlist[0] ? props.isruserlist[0] : {};

  const [phone, setPhoneValue] = useState(emloyerInfo.PHONE);
  const [gender, setGenderValue] = useState(emloyerInfo.GENDER);
  const [name, setNameValue] = useState(emloyerInfo.CUS_NAME);
  const [dob, setDobValue] = useState(emloyerInfo.DOB);
  const [email, setEmailValue] = useState(emloyerInfo.EMAIL);
  const [address, setAddressValue] = useState(emloyerInfo.ADDRESS);
  const [bankAccount, setBankAccount] = useState(emloyerInfo.BANK_ACCOUNT_NUM);
  const [employer, setEmployerValue] = useState(emloyerInfo.STAFF_CODE);
  const [addressCode, setAddressCodeValue] = useState({
    label: emloyerInfo.ADDRESS_LABEL,
    prov: emloyerInfo.PROVINCE,
    dist: emloyerInfo.DISTRICT,
    ward: emloyerInfo.WARDS,
  });
  const [passport, setPassportValue] = useState(emloyerInfo.IDCARD);

  const [emailtemp, setEmailTemp] = useState("vietjetair.com");

  // const [phone, setPhoneValue] = useState("0936081422");
  // const [gender, setGenderValue] = useState("M")
  // const [name, setNameValue] = useState("PHAM MINH HOANG");
  // const [dob, setDobValue] = useState("19/02/1996");
  // const [email, setEmailValue] = useState("hoang.1996.hp@hdbank.com.vn");
  // const [address, setAddressValue] = useState("Doan Xa 1, Kien Thuy");
  // const [bankAccount, setBankAccount] = useState("293607");
  // const [employer, setEmployerValue] = useState("A123");
  // const [addressCode, setAddressCodeValue] = useState({label:"Hai Phong, Viet Nam", prov:"00", dist: "001", ward:"0001"});
  // const [passport, setPassportValue] = useState("031937693");

  const [applyDate, setApplyDate] = useState(moment());

  const [cr_package, setCR_Package] = useState(
    props.isruserlist[0] ? props.isruserlist[0].PACK_POS : 0
  );
  const [pkg, setPkg] = useState({ BENEFITS: [] });

  useEffect(() => {
    if (props.isruserlist[0]) {
      setPkg(props.packages[props.isruserlist[0].PACK_POS]);
      setApplyDate(
        moment(
          props.packages[props.isruserlist[0].PACK_POS].APPLY_DATE,
          "DD-MM-YYYY"
        )
      );
    } else {
      setApplyDate(moment(props.packages[0].APPLY_DATE, "DD-MM-YYYY"));
      setPkg(props.packages[0]);
    }
    if (props.org == "HDBANK_VN") {
      setEmailTemp("hdbank.com.vn");
    }
    if (props.org == "VIETJET_VN") {
      setEmailTemp("vietjetair.com");
    }
    if (props.org == "HDSAISON_VN") {
      setEmailTemp("hdsaison.com.vn");
    }
  }, []);

  useEffect(() => {
    if (props.isruserlist.filter(conds).length > 0) {
      checkMaxAmount();
    }
  }, [props.isruserlist]);

  const conds = (e) => {
    return e.IS_EMPLOYEE == false;
  };
  const checkMaxAmount = () => {
    var lmax = -1;
    if (registeredChecked) {
      return -1;
    } else {
      props.isruserlist.filter(conds).map((i, id) => {
        if (i.TOTAL_AMOUNT >= lmax) {
          lmax = i.TOTAL_AMOUNT;
        }
      });
      setMaxAmount(lmax);
      return lmax;
    }
  };

  const getInfoMaxAmount = () => {
    var lmax = -1;
    var usr = null;
    props.isruserlist.filter(conds).map((i, id) => {
      if (i.TOTAL_AMOUNT >= lmax) {
        console.log(i);
        lmax = i.TOTAL_AMOUNT;
        usr = {
          PACK_CODE: i.PACK_CODE,
          PACK_POS: i.PACK_POS,
          CUS_NAME: i.CUS_NAME,
          AM: lmax,
        };
      }
    });

    return usr;
  };

  useEffect(() => {
    var l = props.packages;
    l.map((item, index) => {
      item.label = item.PACK_NAME;
      item.value = index;
      return item;
    });
    setListPack(l);
    if (props.isruserlist[0]) {
      setPkg(props.packages[props.isruserlist[0].PACK_POS]);
    }
  }, [props.packages]);

  useEffect(() => {
    if (props.slPack) {
      var index = props.packages.findIndex((x) => x.PACK_CODE === props.slPack);
      if (index != -1) {
        setCR_Package(index);
        setPkg(props.packages[index]);
      } else {
        setCR_Package(0);
        setPkg(props.packages[0]);
      }
    }
  }, [props.slPack]);

  const onCbChange = (e) => {
    const ck = e.target.checked;
    setTermChecked(ck);
    props.onCbTermChange(!ck);
  };

  const cbRegistedArlet = (usr_name, usr_packpos) => {
    confirmAlert({
      title: "Xác nhận",
      message: `Người thân của bạn (${usr_name}) đang đăng ký gói (${listPack[usr_packpos].PACK_NAME}) cao hơn gói bạn đang lựa chọn! Bạn có muốn lựa chọn gói cao nhất mà người thân của bạn đã đăng ký.`,
      buttons: [
        {
          label: "Yes",
          onClick: () => {
            setCR_Package(usr_packpos);
            setPkg({ ...props.packages[usr_packpos] });
            setRegisterChecked(false);
            props.checkRegistered(false);
          },
        },
        {
          label: "No",
          onClick: () => {},
        },
      ],
    });
  };

  const onCbChangeRegistered = (e) => {
    const ck = e.target.checked;
    if (ck == false) {
      const checkmax = getInfoMaxAmount();
      if (checkmax) {
        if (pkg.ORG_F * 1 < checkmax.AM) {
          cbRegistedArlet(checkmax.CUS_NAME, checkmax.PACK_POS);
          return e.stopPropagation();
        }
      }
    }

    props.checkRegistered(ck);
    setRegisterChecked(ck);
  };

  const selectedDay = () => {
    const tdate = moment(applyDate);
    tdate.subtract(14, "days");
    return {
      year: tdate.year(),
      month: tdate.month() + 1,
      day: tdate.date(),
    };
  };

  const maximumDate = () => {
    const tdate = moment(applyDate);
    tdate.subtract(14, "days");
    var now = moment();
    if (now >= tdate) {
      return {
        year: tdate.year(),
        month: tdate.month() + 1,
        day: tdate.date(),
      };
    } else {
      return {
        year: now.year(),
        month: now.month() + 1,
        day: now.date(),
      };
    }
  };
  const minimumDate = () => {
    const tdate = moment(applyDate);
    return {
      year: tdate.year() - 65,
      month: tdate.month() + 1,
      day: tdate.day(),
    };
  };

  const onPackageChange = (index) => {
    const pkfee = props.packages[index].ORG_F;
    if (pkfee >= maxAmount || maxAmount == -1) {
      setCR_Package(index);
      setPkg(props.packages[index]);
      setApplyDate(moment(props.packages[index].APPLY_DATE, "DD-MM-YYYY"));
      return false;
    } else {
      if (registeredChecked) {
        return false;
      } else {
        cogoToast.error(
          "Người thân được bảo hiểm chỉ được đăng ký gói thấp hơn hay bằng gói của nhân viên)"
        );
        return true;
      }
    }
  };
  const openInNewTab = () => {
    if (pkg.URL_DETAIL) {
      if (typeof window !== "undefined") {
        setOpenBenefit(false);
        const newWindow = window.open(
          pkg.URL_DETAIL,
          "_blank",
          "noopener,noreferrer"
        );
        if (newWindow) {
          newWindow.opener = null;
        }
      }
    } else {
      cogoToast.error("Vui lòng chọn gói bảo hiểm.");
    }
  };

  const actionopenBenefit = () => {
    if (pkg.URL_DETAIL) {
      setOpenBenefit(true);
    } else {
      cogoToast.error("Vui lòng chọn gói bảo hiểm.");
    }
  };

  const popoverBenefit = {
    isOpen: openBenefit,
    place: "below",
    preferPlace: "right",
    onOuterAction: () => setOpenBenefit(false),
    body: [
      <div className={styles.list_benefit_relative}>
        <div className={styles.h}>{pkg ? pkg.PACK_NAME : null}</div>
        <div className={styles.list_xxx}>
          {(pkg ? pkg.BENEFITS : []).map((item, index) => {
            return <div className={styles.ite}>{item.NAME}</div>;
          })}

          <Button
            className={styles.view_detail_benefit}
            onClick={(e) => openInNewTab()}
          >
            {l.g("bhsk.form.lbll_detail")}
          </Button>
        </div>
      </div>,
    ],
  };

  useImperativeHandle(ref, () => ({
    handleSubmit() {
      const form = formRef.current;
      if (form.checkValidity() === false) {
        setValidated(true);
        return false;
      }
      const data = {
        id: "idstaff",
        ORG_CODE: "VJ",
        STAFF_CODE: employer,
        CUS_NAME: name,
        DOB: dob,
        BANK_ACCOUNT_NUM: bankAccount,
        RELATIONSHIP: "BAN_THAN",
        GENDER: gender,
        IDCARD: passport,
        PROVINCE: addressCode.prov,
        DISTRICT: addressCode.dist,
        WARDS: addressCode.ward,
        ADDRESS_LABEL: addressCode.label,
        ADDRESS: address,
        EMAIL: email,
        PHONE: phone,
        TOTAL_AMOUNT: pkg.ORG_F * 1,
        PAID_BY_SAL: termChecked ? 1 : 0,
        PACK_CODE: pkg.PACK_CODE,
        PACK_POS: cr_package,
        EXCLUDE_COND1: 1,
        EXCLUDE_COND2: 1,
        IS_EMPLOYEE: true,
        IS_REGISTERED: registeredChecked,
      };
      let uslc = props.isruserlist;
      uslc[0] = data;
      props.setUserList([...uslc]);
      return data;
    },
  }));

  return (
    <div className="bb-form f1">
      <TopTitle
        title={l.g("bhsk.form.lbbl_title_1")}
        subtitle={l.g("bhsk.form.lbbl_subtitle_1")}
      />

      <Form
        className={styles.f_orm}
        ref={formRef}
        noValidate
        validated={validated}
      >
        <Row>
          <Col md={3} className={styles.mobile_mt}>
            <FLInput
              label={l.g("bhsk.form.lbl_staff")}
              value={employer}
              changeEvent={setEmployerValue}
              required={true}
            />
          </Col>
          <Col md={6}>
            <div className={`${styles.group_ipnv} ${styles.mobile_mt}`}>
              <FLSelect
                disable={false}
                label={l.g("bhsk.form.lbl_title")}
                value={gender}
                changeEvent={setGenderValue}
                hideborder={true}
                dropdown={true}
                required={true}
                data={[
                  { label: l.g("bhsk.form.f3_title15"), value: "M" },
                  { label: l.g("bhsk.form.f3_title16"), value: "F" },
                ]}
              />
              <div className="ver-line"></div>
              <div className={styles.kkjs}>
                <FLInput
                  disable={false}
                  hideborder={true}
                  value={name}
                  changeEvent={setNameValue}
                  isUpperCase={true}
                  label={l.g("bhsk.form.lbl_fullname")}
                  required={true}
                />
              </div>
            </div>
          </Col>
          <Col md={3} className={styles.mobile_mt}>
            <FLInput
              label={l.g("bhsk.form.lbl_bank_acc")}
              value={bankAccount}
              changeEvent={setBankAccount}
              required={true}
            />
          </Col>
        </Row>

        <Row className={styles.mobile_mt}>
          <Col md={3} className={styles.mobile_mt}>
            <FLInput
              changeEvent={setPhoneValue}
              value={phone}
              label={l.g("bhsk.form.lbl_phone_number")}
              pattern="[0-9]{9,12}"
              required={true}
            />
          </Col>
          <Col md={3} className={styles.mobile_mt}>
            <FLInput
              label={"Email"}
              pattern={`[a-z0-9._%+-]+@${emailtemp}$`}
              changeEvent={setEmailValue}
              value={email}
              required={true}
            />
          </Col>
          <Col md={3} className={styles.mobile_mt}>
            <FLInput
              label={l.g("bhsk.form.lbl_passport")}
              // pattern="[0-9]{9,22}"
              changeEvent={setPassportValue}
              value={passport}
              required={true}
            />
          </Col>
          <Col md={3} className={styles.mobile_mt}>
            <FLDate
              disable={false}
              value={dob}
              changeEvent={setDobValue}
              selectedDay={selectedDay()}
              maximumDate={maximumDate()}
              minimumDate={minimumDate()}
              label={l.g("bhsk.form.lbl_dob")}
              required={true}
            />
          </Col>
        </Row>
        <Row className={styles.mobile_mt}>
          <Col md={6} className={styles.mobile_mt}>
            <FLInput
              required={true}
              value={address}
              changeEvent={setAddressValue}
              label={l.g("bhsk.form.street")}
            />
          </Col>
          <Col md={6} className={styles.mobile_mt}>
            <FLAddress
              disable={false}
              changeEvent={setAddressCodeValue}
              value={addressCode}
              label={l.g("bhsk.form.address")}
              required={true}
            />
          </Col>
        </Row>

        <div className={`${styles.p_line} ${styles.mobile_mt}`} />

        <Row className={styles.mobile_mt}>
          <Col md={6}>
            <div className={styles.group_ipnv}>
              <FLSelect
                disable={registeredChecked}
                hideborder={true}
                label={l.g("bhsk.form.lbl_package_title")}
                value={cr_package}
                changeEvent={onPackageChange}
                dropdown={true}
                required={true}
                data={listPack}
              />
              <div className="ver-line"></div>
              <div className={styles.kkjs}>
                <Popover {...popoverBenefit}>
                  <Button
                    className={styles.view_ql}
                    onClick={(e) => {
                      actionopenBenefit();
                    }}
                  >
                    <i
                      className={`${styles.fa} ${styles.fas} ${styles.fa_shield_alt}`}
                    ></i>{" "}
                    {l.g("bhsk.form.lbl_view_benefit")}
                  </Button>
                </Popover>
              </div>
            </div>
          </Col>

          <Col md={6}>
            <FLInput
              disable={true}
              line={registeredChecked}
              value={pkg ? `${pkg.FEES} ${pkg.CURRENCY}` : "0 VNĐ"}
              label={l.g("bhsk.form.lbl_fee")}
            />
          </Col>
        </Row>
      </Form>
      <div className={styles.mobile_mt}>
        <input
          className={styles.checkbox}
          type="checkbox"
          id="checkbox-1x"
          onChange={onCbChangeRegistered}
          checked={registeredChecked}
        />
        <label for="checkbox-1x">
          <p>{l.g("bhsk.form.insured")}</p>
        </label>
      </div>

      <div>
        <input
          className={styles.checkbox}
          type="checkbox"
          id="checkbox-1c"
          onChange={onCbChange}
          checked={termChecked}
        />
        <label for="checkbox-1c">
          <p>{l.g("bhsk.form.lbl_noted_f1")}</p>
        </label>
      </div>
    </div>
  );
});

const mapStateToProps = (state) => {
  return {
    packages: state.data,
    isruserlist: state.isruserlist,
  };
};
const mapDispatchToProps = (dispatch) => ({
  setUserList: (usrl) => dispatch(setUserList(usrl)),
  checkRegistered: (isChecked) => dispatch(checkRegistered(isChecked)),
});

export default connect(mapStateToProps, mapDispatchToProps, null, {
  forwardRef: true,
})(Form1);
