import React, {
  useEffect,
  useState,
  createRef,
  forwardRef,
  useImperativeHandle,
} from "react";
import TopTitle from "../../../common/toptitle";
import { connect } from "react-redux";
import {
  fetchPackages,
  setUserList,
} from "../../../redux/actions/action-bhsk.js";
import { confirmAlert } from "react-confirm-alert";
import currencyFormatter from "currency-formatter";
import Avatar from "react-avatar";
import { Button, Col, Row, Tabs, Tab, Nav, Dropdown } from "react-bootstrap";
import UserInfo from "./userinfo";
import randomstring from "randomstring";
import Popover from "react-popover";
import moment from "moment";
import { isBrowser, isMobile } from "react-device-detect";
import styles from "../../../css/style.module.css";

const background = [
  "#EC884D",
  "#3EA1FE",
  "#CCBFB0",
  "#6c5ce7",
  "#fdcb6e",
  "#16a085",
];

const Form2 = forwardRef((props, ref) => {
  var default_transform = -(isMobile ? 45 : 60);
  const [active, setActive] = useState(null);
  const [openRelation, setOpenRelation] = useState(false);
  const [totalAmount, setTotalAmount] = useState("0 VNĐ");
  const [applyDate, setApplyDate] = useState(
    props.packages[0].APPLY_DATE
      ? moment(props.packages[0].APPLY_DATE, "DD-MM-YYYY")
      : moment()
  );

  const spdata = {
    ORG_CODE: "VJ",
    STAFF_CODE: "11232",
    CUS_NAME: "NGUYEN MINH THU",
    RELATIONSHIP: "CON_CAI",
    DOB: "19/02/1996",
    GENDER: "F",
    IDCARD: "031937693",
    PROVINCE: "01",
    DISTRICT: "001",
    WARDS: "0001",
    ADDRESS_LABEL: "Kien Thuy - Hai Phong",
    ADDRESS: "Thon Doan Xa 1",
    EMAIL: "hoang.1996.hp@gmail.com",
    PHONE: "033608422",
    TOTAL_AMOUNT: "500000",
    PACK_CODE: "A",
    EXCLUDE_COND1: 1,
    EXCLUDE_COND2: 1,
    IS_CONFIRM: "1",
    IS_EMPLOYEE: false,
  };

  useEffect(() => {
    calcTotalAmount();
    setApplyDate(moment(props.packages[0].APPLY_DATE, "DD-MM-YYYY"));
  }, []);

  useEffect(() => {}, [active]);

  useEffect(() => {
    calcTotalAmount();
  }, [props.isruserlist]);

  const actionReCalcAmount = (usersll) => {
    var am = 0;
    usersll.map((user, index) => {
      if (props.packages[user.PACK_POS]) {
        const pkg_fee = user.IS_REGISTERED
          ? 0
          : props.packages[user.PACK_POS].ORG_F * 1;
        if (props.org == "hdbank_vn" || props.org == "hdsaison") {
          const age = getAge(user.DOB);
          if (age.y == 0) {
            am += pkg_fee + (pkg_fee * 30) / 100;
          } else {
            am += pkg_fee;
          }
        } else {
          am += pkg_fee;
        }
      }
    });

    return am;
  };

  const calcTotalAmount = () => {
    var am = 0;
    const relatives_am = actionReCalcAmount(props.isruserlist);
    var total_am = am + relatives_am;
    var str_total = currencyFormatter.format(total_am, {
      code: l.g("bhsk.currency"),
      precision: 0,
      format: "%v %s",
      symbol: l.g("bhsk.currency"),
    });

    setTotalAmount(str_total);
  };

  const onSelectPackage = () => {
    setTimeout(() => calcTotalAmount(), 300);
    const position = getPrositionbyId(active);
    if (props.isruserlist[position]) {
      let userdata = props.isruserlist[position].ref.current.handleSubmit();
      if (userdata) {
        var l = props.isruserlist;
        l[position] = { ...l[position], ...userdata };
        setUserList([...l]);
      } else {
      }
    } else {
    }
  };

  const openTab = (id) => {
    const position = getPrositionbyId(active);
    if (props.isruserlist[position].ref) {
      let userdata = props.isruserlist[position].ref.current.handleSubmit();
      if (userdata) {
        var l = props.isruserlist;
        l[position] = { ...l[position], ...userdata };
        setUserList([...l]);
        setActive(id);
        return l;
      } else {
        return false;
      }
    } else {
      return false;
    }
  };

  const getPrositionbyId = (id) => {
    return props.isruserlist.findIndex((x) => x.id === id);
  };

  const addUser = (isNew) => {
    if (props.isruserlist.length > 1) {
      const position = getPrositionbyId(active);
      let uslc = props.isruserlist;
      let userdata = props.isruserlist[position].ref.current.handleSubmit();
      if (userdata) {
        uslc[position] = { ...uslc[position], ...userdata };
        var newID = randomstring.generate(7);
        const newarr = insert(uslc, 1, {
          id: newID,
          ref: createRef(),
          IS_EMPLOYEE: false,
        });
        setActive(newID);
        props.setUserList(newarr);
      }
    } else {
      let uslc = props.isruserlist;
      var newID = randomstring.generate(7);
      const newarr = insert(uslc, 1, {
        id: newID,
        ref: createRef(),
        IS_EMPLOYEE: false,
      });
      setActive(newID);
      props.setUserList(newarr);
    }
  };

  const insert = (arr, index, newItem) => [
    ...arr.slice(0, index),

    newItem,

    ...arr.slice(index),
  ];

  const conds = (e) => {
    return e.IS_EMPLOYEE == false;
  };

  const popoverUserRelation = {
    isOpen: openRelation,
    place: "below",
    preferPlace: "right",
    onOuterAction: () => setOpenRelation(false),
    body: [
      <div className={styles.list_user_relative}>
        <div className={styles.h}>Chọn người được bảo hiểm</div>
        <div className={styles.list_user}>
          {props.isruserlist.filter(conds).map((item, index) => {
            return (
              <div
                className={styles.user_item}
                onClick={(e) => {
                  openTab(item.id);
                }}
              >
                <Avatar
                  name={item.CUS_NAME ? item.CUS_NAME : index}
                  round={true}
                  color={background[index]}
                  size={32}
                />
                <div className={styles.if}>
                  <div className={styles.name}>
                    {item.CUS_NAME
                      ? item.CUS_NAME
                      : `Người được bảo hiểm ${index}`}
                  </div>
                  <div className={styles.package}>
                    Gói{" "}
                    {props.packages[item.PACK_POS]
                      ? props.packages[item.PACK_POS].PACK_NAME
                      : null}
                  </div>
                </div>
                {item.id == active ? (
                  <i className={`${styles.fas} ${styles.fa_check_circle}`}></i>
                ) : null}
              </div>
            );
          })}
        </div>
      </div>,
    ],
  };

  const openUserList = () => {
    setOpenRelation(true);
  };

  const submitRecalc = () => {
    try {
      if (props.isruserlist.length > 1) {
        const position = getPrositionbyId(active);
        if (position != -1) {
          let userdata = props.isruserlist[position].ref.current.getData();
          if (userdata) {
            var l = props.isruserlist;
            l[position] = { ...l[position], ...userdata };
            props.setUserList([...l]);
            setTimeout(() => calcTotalAmount(), 500);
            return l;
          } else {
            return false;
          }
        } else {
          return false;
        }
      } else {
        return [];
      }
    } catch (e) {
      console.log(e);
      return false;
    }
  };

  useImperativeHandle(ref, () => ({
    handleSubmit() {
      if (props.isruserlist.length > 1) {
        const position = getPrositionbyId(active);
        if (position != -1) {
          let userdata = props.isruserlist[position].ref.current.handleSubmit();
          if (userdata) {
            var l = props.isruserlist;
            l[position] = { ...l[position], ...userdata };
            props.setUserList([...l]);
            return l;
          } else {
            return false;
          }
        } else {
          return false;
        }
      } else {
        return [];
      }
    },

    update() {
      if (props.isruserlist.length > 1) {
        if (props.defaultActive == null) {
          setActive(props.isruserlist[1].id);
        } else {
          setActive(props.defaultActive);
        }
      }
    },
  }));

  const deleteUser = () => {
    confirmAlert({
      title: "Xác nhận",
      message: "Bạn có chắc chắn muốn xóa người được bảo hiểm này không?",
      buttons: [
        {
          label: "Không xóa",
          onClick: () => {},
        },
        {
          label: "Xóa NĐBH",
          onClick: () => {
            var l = props.isruserlist;
            var pos_del = getPrositionbyId(active);
            l.splice(pos_del, 1);
            setUserList([...l]);
            if (l.length >= 2) {
              setActive(l[1].id);
            } else {
              setActive(null);
            }

            setTimeout(() => {
              console.log("recalc");
              calcTotalAmount();
            }, 300);
          },
        },
      ],
    });
  };

  const getAge = (indob) => {
    var a = moment(applyDate, "DD-MM-YYYY");
    var b = moment(indob, "DD-MM-YYYY");
    var age = moment.duration(a.diff(b));
    return { y: age.years(), d: age.days() };
  };

  return (
    <div className="bb-form f2">
      <TopTitle
        title={l.g("bhsk.form.lbbl_title_2")}
        subtitle={l.g("bhsk.form.lbbl_subtitle_2")}
      />

      <div className={styles.user_related_tabbed}>
        <Tab.Container id="left-tabs-example" activeKey={active}>
          <div className={styles.tab_header}>
            <div className={styles.xx_left}>
              <div className={styles.btn_add_user_container}>
                <Button
                  className={styles.btn_add_user}
                  onClick={(e) => addUser(false)}
                >
                  <span>{l.g("bhsk.form.btn_add_relatives")}</span>{" "}
                  <i className={`${styles.fas} ${styles.fa_plus}`}></i>
                </Button>
              </div>

              <div className={styles.list_usr_hor}>
                {(props.isruserlist.filter(conds).length > 3
                  ? props.isruserlist.filter(conds).slice(0, 3)
                  : props.isruserlist.filter(conds)
                ).map((useritem, index) => {
                  default_transform = (isMobile ? 45 : 60) + default_transform;
                  if (index < 2) {
                    return (
                      <div
                        onClick={() => openTab(useritem.id)}
                        className={
                          active == useritem.id
                            ? `${styles.usr_item} ${styles.active}`
                            : styles.usr_item
                        }
                        style={{
                          transform: "translateX(" + default_transform + "px)",
                        }}
                      >
                        <Avatar
                          className={styles.custom_avatar_kitin}
                          name={
                            useritem.CUS_NAME
                              ? useritem.CUS_NAME
                              : (index + 1).toString()
                          }
                          size={42}
                          round={true}
                          color={background[index]}
                        />
                        <div className={styles.arrow} />
                      </div>
                    );
                  } else {
                    return (
                      <div
                        onClick={() => openUserList()}
                        className={styles.usr_item}
                        style={{
                          background: background[index],
                          transform: "translateX(" + default_transform + "px)",
                        }}
                      >
                        <Popover {...popoverUserRelation}>
                          <Avatar
                            className={styles.custom_avatar_kitin}
                            name={
                              props.isruserlist.filter(conds).length == 3
                                ? useritem.CUS_NAME
                                  ? useritem.CUS_NAME
                                  : 3
                                : "+ " +
                                  (props.isruserlist.filter(conds).length - 2)
                            }
                            size={42}
                            round={true}
                            color={background[index]}
                          />
                        </Popover>
                        <div className={styles.arrow} />
                      </div>
                    );
                  }
                })}
              </div>
            </div>

            <div className={styles.xx_right}>
              <div className={styles.right_info}>
                <p>
                  {l.g("bhsk.form.lbl_total_am")}{" "}
                  <span className={styles.price}>{totalAmount}</span>
                </p>
              </div>
            </div>
          </div>

          <div className={styles.tab_content_x}>
            {props.isruserlist.length == 1 ? (
              <div className={styles.empty_relatives}>
                <div className={styles.contain}>
                  <p>{l.g("bhsk.form.btn_add_relatives_question")}</p>
                  <Button onClick={(e) => addUser(true)}>
                    {l.g("bhsk.form.btn_add_dp")}
                  </Button>
                </div>
              </div>
            ) : null}

            <Tab.Content>
              {props.isruserlist.filter(conds).map((useritem, index) => {
                return (
                  <Tab.Pane eventKey={useritem.id}>
                    <UserInfo
                      ref={useritem.ref}
                      id={useritem.id}
                      key={useritem.id}
                      info={useritem}
                      org={props.org}
                      onSelectPackage={onSelectPackage}
                      packages={props.packages}
                      employerFree={props.isruserlist[0].TOTAL_AMOUNT}
                      employer={props.isruserlist[0].STAFF_CODE}
                      onDelete={deleteUser}
                      reCalcAmount={submitRecalc}
                    />
                  </Tab.Pane>
                );
              })}
            </Tab.Content>
          </div>
        </Tab.Container>
      </div>
    </div>
  );
});

const mapStateToProps = (state) => {
  return {
    isruserlist: state.isruserlist,
    packages: state.data,
  };
};
const mapDispatchToProps = (dispatch) => ({
  setUserList: (usrl) => dispatch(setUserList(usrl)),
});
export default connect(mapStateToProps, mapDispatchToProps, null, {
  forwardRef: true,
})(Form2);
