import React, { useEffect, useState, createRef } from 'react';
import styles from "../../../css/style.module.css";

const Success = (props) => {
  return (
    <div className='Success-class'>
      <div className={styles.form_register_success}>Success</div>
    </div>
  );
};

export default Success;
