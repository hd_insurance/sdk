import React, { useEffect, useState, createRef, forwardRef, useImperativeHandle } from "react";
// import router from "next/router";
import { Button, Col, Row, Popover, Form } from 'react-bootstrap';
import randomstring from "randomstring";
import moment from 'moment'
import { connect } from 'react-redux'
import Overlay from 'react-bootstrap/Overlay'
import TopTitle from "../../../common/toptitle";
import cogoToast from 'cogo-toast';
import api from "../../../services/Network.js";
import { fetchPackages, setUserList } from '../../../redux/actions/action-bhsk.js';
import currencyFormatter from 'currency-formatter';
import StageSpinner from "../../../common/loadingpage";
import { confirmAlert } from 'react-confirm-alert';
import styles from "../../../css/style.module.css";

const Form3 = forwardRef((props, ref) => {
	const {emloyerInfo, org} = props
	const refPreview = createRef(null);
	const refNGKDBH = createRef(null);
	const [loading , setLoading] = useState(false);
	const [relatives , setRelatives] = useState([]);
	const refConditions = createRef(null);
	const [rd1 , onRdChangeR1] = useState(false);
	const [rd2 , onRdChangeR2] = useState(false);
	const [validatedVoucher, setValidatedVoucher] = useState(false);
	const [showPreview, setShowPreview] = useState(false);
	const [showListChangeNDBH, setShowListChangeNDBH] = useState(false);
	const [showListCondition, setShowListCondition] = useState(false);
	const [isVoucher, setIsVoucher] = useState(props.voucherInfo!=null);
	const [popConditionTitle, setPopConditionTitle] = useState("EXCLUDE_COND1");
	const [currentUserPreview, setCurrentUserPreview] = useState({});
	const [target, setTarget] = useState(null);
	const [rm_exclude, setListExclude] = useState([]);
	const [applyDate, setApplyDate] = useState(props.packages[0].APPLY_DATE?moment(props.packages[0].APPLY_DATE, "DD-MM-YYYY"):moment());
	const [totalAmount, setTotalAmount] = useState(0)
	const [discountAmount, setDiscountAmount] = useState(0)

	const [voucherLoading, setVoucherLoading] =  useState(false)
	const [voucher, setVoucher] =  useState(props.voucherInfo?props.voucherInfo.ACTIVATION_CODE:"")
	const [voucherInfo, setVoucherInfo] =  useState(props.voucherInfo)


const actionReCalcAmount = (usersll)=>{
	var am = 0;
	props.isruserlist.map((user, index)=>{
		// console.log(user.CUS_NAME, user.TOTAL_AMOUNT)
		if(user.DOB){
			if(props.packages[user.PACK_POS]){
				let pkg_fee = props.packages[user.PACK_POS].ORG_F*1
				if(user.IS_REGISTERED){
					pkg_fee = 0;
				}
				if(user.EXCLUDE_COND1 == 1 && user.EXCLUDE_COND2 == 1){
					if(props.org == "hdbank_vn" || props.org == "hdsaison"){
						const age = getAge(user.DOB)
						if((age.y==0)){
							am += pkg_fee + ((pkg_fee*30)/100)
						}else{
							am += pkg_fee
						}
					}else{
						am += pkg_fee
					}
				}
			}
		}
		
	})

	return am;
}


const userAmount = (user)=>{
	let am = 0;
		if(user.DOB){
			if(props.packages[user.PACK_POS]){
				const pkg_fee = props.packages[user.PACK_POS].ORG_F*1
				
				if(user.EXCLUDE_COND1 == 1 && user.EXCLUDE_COND2 == 1){
					if(props.org == "hdbank_vn" || props.org == "hdsaison"){
					const age = getAge(user.DOB)
						if((age.y==0)){
							am += pkg_fee + ((pkg_fee*30)/100)
						}else{
							am += pkg_fee
						}
					}else{
						am += pkg_fee
					}
				}
			}
		}
	return currencyFormatter.format(am, { code: l.g("bhsk.currency"), precision:0, format: '%v %s', symbol: l.g("bhsk.currency") })
}
const calcTotalAmount=()=>{
	var am = 0;
	const relatives_am = actionReCalcAmount(props.isruserlist)

	var total_am = am + relatives_am
	if(voucherInfo){
		const discount_am = ((total_am*voucherInfo.DISCOUNT)/100)
		setDiscountAmount(discount_am)
	}else{
		setDiscountAmount(0)
	}
	setTotalAmount(total_am)
}

const calcDiscountAmount = ()=>{
	total_am
}
const getAge = (indob) => {
	var a = moment(applyDate, 'DD-MM-YYYY');
	var b = moment(indob, 'DD-MM-YYYY');
	var age = moment.duration(a.diff(b));
	return {y:age.years(), d:age.days()}
}

const formatCurrentcy = (am)=>{
	return new Intl.NumberFormat('vi-VN', { style: 'currency', currency: 'VND' }).format(am)
}

	const handClickPreview = (event, index) => {
		setCurrentUserPreview(props.isruserlist[index])
		setShowPreview(!showPreview);
		setTarget(event.target);
	}
	const handClickChangeNDBH = (event) => {
		setShowListChangeNDBH(!showListChangeNDBH);
		setTarget(event.target);
		const newarr = props.isruserlist.filter(function(item) {
			return ((item.EXCLUDE_COND2 == 0)||(item.EXCLUDE_COND1 == 0))
		})
		setListExclude(newarr)
	}
	const confirmNKDBH = () =>{
		setShowListChangeNDBH(false);
		props.isruserlist.map((item, index)=>{
			if(rm_exclude.includes(item.id)){
				item.EXCLUDE_COND1 = 1
				item.EXCLUDE_COND2 = 1
			}
			return item
		})
		props.setUserList(props.isruserlist)
		// setUserList(userList)
		const c1 = props.isruserlist.filter(function(item) {
						return (item.EXCLUDE_COND1 == 0)
					})
		const c2 = props.isruserlist.filter(function(item) {
						return (item.EXCLUDE_COND2 == 0)
					})
		onRdChangeR1(c1.length>0)
		onRdChangeR2(c2.length>0)
		calcTotalAmount();

	}
	const handClickConditions = (event) =>{

		setShowListCondition(!showListChangeNDBH);
		setTarget(event.target);
	}

	const handleOnChangeR1 = (event) =>{
		onRdChangeR1(event.target.checked);
		if(event.target.checked){
			setPopConditionTitle("EXCLUDE_COND1")
			setShowListCondition(true);
			setTarget(event.target);
		}
	}
	const handleOnChangeR2 = (event) =>{
		onRdChangeR2(event.target.checked);
		if(event.target.checked){
			setPopConditionTitle("EXCLUDE_COND2")
			setShowListCondition(true);
			setTarget(event.target);
		}
	}
	const confirmConditions = (x) =>{
		setShowListCondition(false);
		const carr = props.isruserlist.filter(function(item) {
			return (item[x] == 0)
		})
		if(x=="EXCLUDE_COND1" && carr.length == 0){
			onRdChangeR1(false);
		}
		if(x=="EXCLUDE_COND2" && carr.length == 0){
			onRdChangeR2(false);
		}

		const newarr = props.isruserlist.filter(function(item) {
			return ((item.EXCLUDE_COND2 == 0)||(item.EXCLUDE_COND1 == 0))
		})
		setListExclude(newarr)
	}

	const onEditClick = (userid)=>{
		props.onEditClick(userid)
	}

	const exclude_checked = (isChecked, index, condition)=>{
		props.isruserlist[index][condition] = (isChecked?0:1)
		props.setUserList(props.isruserlist)
		// setUserList(userList)
	}
	const remove_exclude_checked = (isChecked, id)=>{
		if(!isChecked){
			rm_exclude.push(id)
		}else{
			const index = rm_exclude.indexOf(id);
			if (index > -1) {
			  rm_exclude.splice(index, 1);
			  setListExclude(rm_exclude)
			}
		}
	}
const condExclude = (item)=>{

	return ((item.EXCLUDE_COND2 == 0)||(item.EXCLUDE_COND1 == 0))
}

const condExclude2 = (item)=>{
	return ((item.EXCLUDE_COND2 == 0)||(item.EXCLUDE_COND1 == 0))
}

const getListUserExclude = ()=>{
	return props.isruserlist.filter(function(item) {
		return ((item.EXCLUDE_COND2 == 0)||(item.EXCLUDE_COND1 == 0))
	})
}

const removeVoucher = ()=>{
	 confirmAlert({
          title: 'Xác nhận',
          message: 'Bạn có chắc chắn muốn bỏ áp dụng mã giảm giá?',
          buttons: [
            {
              label: 'Không',
              onClick: () => {
              	setValidatedVoucher(false)
              }
            },
            {
              label: 'Bỏ áp dụng',
              onClick: () => {
              	setValidatedVoucher(false)
              	setVoucherInfo(null)
              	setVoucher("")
              	rmvcl()
              }
            }
          ]
        })
}

const handleSubmitVoucher = async (event) => {
	try{
		const form = event.currentTarget;
	    
	    event.preventDefault();
	    event.stopPropagation();
	    setValidatedVoucher(true);
	    if (form.checkValidity() === false) {
	    	return false
	    }
	    setVoucherLoading(true)

	    var browser_code = randomstring.generate(9)
	    if (typeof window !== 'undefined') {
	        if(localStorage.getItem('browser_code')){
	          browser_code = localStorage.getItem('browser_code')
	        }else{
	          browser_code = randomstring.generate(9)
	        }
	    }
		const response = await api.post("/api/bhsk/vouchervalid", {code: voucher, org: props.org=="VIETJET_VN"?"REGIS_STAFF_VJ":"REGIS_STAFF_HDBANK", browser_code: browser_code})
		setVoucherLoading(false)
		if(response.success){
			if (typeof window !== 'undefined') {
	         localStorage.setItem('browser_code', browser_code);
	        }

			setVoucherInfo(response.data[0][0])
		}else{
			cogoToast.error(response.error_message);
		}


	}catch(e){
		setVoucherLoading(false)
		console.log(e)
	}
    

};

const rmvcl = async () => {
	try{
	    var browser_code = randomstring.generate(9)
	    if (typeof window !== 'undefined') {
	        if(localStorage.getItem('browser_code')){
	          browser_code = localStorage.getItem('browser_code')
	        }else{
	          browser_code = randomstring.generate(9)
	        }
	    }
		const response = await api.post("/api/bhsk/vcrml", {code: voucherInfo.ACTIVATION_CODE, browser_code: browser_code})
		calcTotalAmount();
		if(response.success){
			 localStorage.removeItem('browser_code', browser_code);
		}
	}catch(e){
		console.log(e)
	}
    

};





useImperativeHandle(ref, () => ({
	resetRelatives(list){
		setRelatives(list)
	},
	setPackg(inlist){
		setPackages(inlist)
	},
    dataCombined() {
    	return new Promise(async (resolved, rejected)=>{
    		var GH_ORDER_INSUR = []
	    	props.isruserlist.map((item, index)=>{
	    		const user_obj = {
			        "ORG_CODE": org.toUpperCase(),
			        "STAFF_CODE": item.STAFF_CODE,
			        "CUS_NAME": item.CUS_NAME?item.CUS_NAME:item.STAFF_NAME,
			        "RELATIONSHIP": item.RELATIONSHIP?item.RELATIONSHIP:"BAN_THAN",
			        "DOB": item.DOB,
			        "GENDER": item.GENDER,
			        "IDCARD": item.IDCARD,
			        "PROVINCE": item.PROVINCE,
			        "DISTRICT": item.DISTRICT,
			        "WARDS": item.WARDS,
			        "ADDRESS": item.ADDRESS,
			        "EMAIL": item.EMAIL,
			        "PHONE": item.PHONE,
			        "AMOUNT": item.TOTAL_AMOUNT.toString(),
			        "PACK_CODE": item.PACK_CODE,
			        "EXCLUDE_COND1": item.EXCLUDE_COND1.toString(),
			        "EXCLUDE_COND2": item.EXCLUDE_COND2.toString(),
			        "IS_CONFIRM": "1",
			        "IS_REGISTERED": item.IS_REGISTERED?1:0
			      }
			      GH_ORDER_INSUR.push(user_obj)
	    	})

	    	const emloyerInfo = props.isruserlist[0]
	    	let staff_info = {
			    "ORG_CODE": org.toUpperCase(),
			    "STAFF_CODE": emloyerInfo.STAFF_CODE,
			    "STAFF_NAME": emloyerInfo.CUS_NAME,
			    "DOB": emloyerInfo.DOB,
			    "BANK_ACCOUNT_NUM": emloyerInfo.BANK_ACCOUNT_NUM,
			    "GENDER": emloyerInfo.GENDER,
			    "IDCARD": emloyerInfo.IDCARD,
			    "PROVINCE": emloyerInfo.PROVINCE,
			    "DISTRICT": emloyerInfo.DISTRICT,
			    "WARDS": emloyerInfo.WARDS,
			    "ADDRESS": emloyerInfo.ADDRESS,
			    "EMAIL": emloyerInfo.EMAIL,
			    "PHONE": emloyerInfo.PHONE,
			    "TOTAL_AMOUNT": emloyerInfo.TOTAL_AMOUNT.toString(),
			    "PAID_BY_SAL": "1",
			    "ACTIVATION_CODE": voucher,
			    GH_ORDER_INSUR: GH_ORDER_INSUR
			}

			try {
			 var data = {};

	
			
			  if(props.router.query.id){
			  	staff_info.CUS_CODE = props.router.query.id.toUpperCase()
			  	data = await api.post("/api/bhsk/updateregister", staff_info)
			  }else{
			  	data = await api.post("/api/bhsk/register", staff_info)
			  }

		      if(data.Success){
		      	return resolved(true)
		      }else{
		      	if(data.Error){
		      		cogoToast.error(data.ErrorMessage);
		      		resolved(false)
		      	}else{
		      		resolved(false)
		      	}
		      }
		      
		    } catch (e) {
		    	resolved(false)
		    }
    	})    	
    	
    }

  }));


// useEffect(() => {
// 	if(relatives[0]){
// 		props.emloyerInfo.EXCLUDE_COND1 = "1"
// 		props.emloyerInfo.EXCLUDE_COND2 = "1"
// 		props.emloyerInfo.id = "idstaff"
// 		var l = [props.emloyerInfo]
// 	    l = l.concat(relatives);
// 	    setUserList(l)
// 	    calcTotalAmount();
// 	}
	
// }, [relatives]);

useEffect(() => {
	setApplyDate(moment(props.packages[0].APPLY_DATE, 'DD-MM-YYYY'))
   
    const c1 = props.isruserlist.filter(function(item) {
						return (item.EXCLUDE_COND1 == 0)
					})
	const c2 = props.isruserlist.filter(function(item) {
					return (item.EXCLUDE_COND2 == 0)
				})
	
	onRdChangeR1(c1.length>0)
	onRdChangeR2(c2.length>0)
	calcTotalAmount();

}, []);

useEffect(() => {
	calcTotalAmount();
});


useEffect(() => {
    calcTotalAmount();
  

}, [props.isruserlist, rm_exclude]);



if(props.packages.length == 0){
	return (<div></div>)
}else{
 return (<div className="bb-form f3">
			<TopTitle title={l.g('bhsk.form.lbbl_title_3')}/>
			<Row>
				<Col md={6}>
					<p className={styles.title_content_ndbh}>
					{l.g('bhsk.form.f3_title1')}
					</p>
					<div className={styles.form_list_ndbh}>

						{props.isruserlist.map((user, index)=>{
							return(
								<div className={styles.item_list_ndbh}>
								<div className="row">

									<div className="col-md-8">

										<div className={styles.title_item_ndbh}>
											<div className={styles.mname}>
												<label>{index+1}. {user.GENDER=="M"?"Mr.":"Ms."}</label>
												<label>{user.CUS_NAME}</label>
											</div>

											<label className={styles.am}>{(user.EXCLUDE_COND1 == 0 || user.EXCLUDE_COND2 == 0 || user.IS_REGISTERED == 1)?`0 ${l.g("bhsk.currency")}`:`${userAmount(user)}`}</label>
										</div>
										<div className={styles.name_package_insurance}>
											{props.packages[user.PACK_POS]?props.packages[user.PACK_POS].PACK_NAME:""} (1 {l.g('bhsk.form.year')})
										</div>
									</div>
									<div className={`col-md-4 ${styles.btn_action_item}`}>
										<button className={styles.preview_item_ndbh} onClick={(e)=>handClickPreview(e, index)}>
											<i className={`${styles.fas} ${styles.fa_eye}`}></i>
											<label>{l.g('bhsk.form.label_view')}</label>
										</button>
										<button className={styles.edit_item_ndbh} onClick={(e)=>{onEditClick(user.id)}}>
											<i className={`${styles.fas} ${styles.fa_edit}`}></i>
											<label>{l.g('bhsk.form.label_edit')}</label> 
										</button>
									</div>
								</div>
						</div>
								)
						})}
						

					


					</div>

					{(rd1 || rd2)?<div className={`row ${styles.form_content_ngkdbh}`}>
						<div className={`col-9 ${styles.ngkdbh_name}`}>Người không được bảo hiểm ({`${props.isruserlist.filter(condExclude2).length}`})</div>
						<div className={`col-3 ${styles.change_ngdbh}`} onClick={handClickChangeNDBH}>Thay đổi</div>
						<div className={`col-12 ${styles.content_ngkdbh}`}>
							Những người trong danh sách này sẽ không được tính bảo hiểm
						</div>
					</div>:null}

				</Col>
				<Col md={6}>
					<div className={`${styles.form_voucher} ${styles.mobile_mt}`}>
					{voucherInfo?<div className={`${styles.vc_info} ${styles.animate__animated} ${styles.animate__fadeIn}`}>
						<div className={styles.gift_icon}>
							<i className={`${styles.fas} ${styles.fa_gift}`}></i>
						</div>	
						<div className={styles.g_info}>
							<h4>{voucherInfo.PRODUCT_NAME}</h4>
							<div className={styles.g_pack}>{voucherInfo.PACK_NAME}</div>
							<div className={styles.g_discount}>DISCOUNT <span>-{voucherInfo.DISCOUNT}{voucherInfo.DISCOUNT_UNIT == "P"?"%":l.g("bhsk.currency")}</span></div>
						</div>
						{(props.router.query.id&&isVoucher)?null:<div className={styles.remove_voucher}>
							<i onClick={(e)=>removeVoucher()} className={`${styles.far} ${styles.fa_times_circle}`}></i>
						</div>}
					</div>:<Form noValidate validated={validatedVoucher} onSubmit={handleSubmitVoucher}>
							<div className={styles.vcvc}>
								 <Form.Control
								 	  className={styles.hqk_custom_input}
						              type="text"
						              placeholder={l.g('bhsk.form.f3_title7')}
						              aria-describedby="inputGroupPrepend"
						              value={voucher}
						              onChange={(e)=>setVoucher(e.target.value)}
						              required
						            />
						         <Button type="submit" disabled={voucherLoading}>{voucherLoading?<div className={styles.loading_voucher}><StageSpinner/></div>:l.g('bhsk.form.f3_title8')}</Button>
							</div>

						</Form>}
						
					</div>


					<div className={styles.form_total_payment}>
						<div className={styles.total_cost_insur}>
								<label>{l.g('bhsk.form.f3_title2')}:</label>
								<label>{currencyFormatter.format(totalAmount, { code: l.g("bhsk.currency"), precision:0, format: '%v %s', symbol: l.g("bhsk.currency") })}</label>
						</div>
						<div className={styles.total_cost_insur}>
								<label>{l.g('bhsk.form.f3_title3')}:</label>
								<label>-{currencyFormatter.format(discountAmount, { code:l.g("bhsk.currency"), precision:0, format: '%v %s', symbol: l.g("bhsk.currency") })}</label>
						</div>
						<div className={styles.total_payment}>
								<label>{l.g('bhsk.form.f3_title4')}:</label>
								<label>{currencyFormatter.format((totalAmount - discountAmount), { code: l.g("bhsk.currency"), precision:0, format: '%v %s', symbol: l.g("bhsk.currency") })}</label>
						</div>
					</div>
					{/*<div className="mt-15">
						<p className="content-">
							{l.g('bhsk.form.f3_title5')}
						</p>
						<div className={styles.rd_group}>
						  <input className={styles.radio} type="radio" name="rd1" value={false} id="radio-1" onChange={handleOnChangeR1} checked={rd1} />
                          <label for="radio-1">{l.g('bhsk.form.f3_title9')}</label>
						  <div className={styles.sp_hoz} />
						  <input className={styles.radio} type="radio" name="rd1" value={true} id="radio-2" onChange={handleOnChangeR1} checked={!rd1} />
          				  <label for="radio-2">{l.g('bhsk.form.f3_title10')}</label>
					  </div>
					</div>*/}
					<div className="mt-15">
						<p>
							{l.g('bhsk.form.f3_title6')}
						</p>
						<div className={styles.rd_group}>
						  <input className={styles.radio} type="radio" name="rd2" value={false} id="radio-3" onChange={handleOnChangeR2} checked={rd2} />
                          <label for="radio-3">{l.g('bhsk.form.f3_title9')}</label>

						  <div className={styles.sp_hoz} />
						  <input className={styles.radio} type="radio" name="rd2" value={true} id="radio-4"onChange={handleOnChangeR2} checked={!rd2} />
          				  <label for="radio-4">{l.g('bhsk.form.f3_title10')}</label>
					  </div>
					</div>
					<div className={`mt-15 ${styles.confirm_terms}`}>
						<input className="checkbox" type="checkbox" id="checkbox-1cx" defaultChecked={props.router.query.id!=null} onChange={(e)=>{props.onCbTermChange(!e.target.checked)}}/>
						<label for="checkbox-1cx">
							<p>{l.g('bhsk.form.f3_title11')} <a href="https://hdimedia.hdinsurance.com.vn/f/e9b68e9745785b0df9b933b71a64963b" target="_blank">{l.g('bhsk.form.f3_title12')}</a> {l.g('bhsk.form.f3_title13')}</p></label>
					</div>
				</Col>
				<div ref={refPreview}>
					<Overlay
						show={showPreview}
						rootClose={true}
						onHide={() => setShowPreview(false)}
						target={target}
						placement="bottom"
						container={refPreview.current}
					>
						<Popover id={styles.popover_contained_preview}>
							<Popover.Title>
								<label className={styles.title_popover}>
								{l.g('bhsk.form.f3_title14')}
								</label>
								<div className={styles.icon_close_popover} onClick={() =>setShowPreview(!showPreview)}>
									<i className={`${styles.fas} ${styles.fa_times}`}></i>
								</div>
							</Popover.Title>
						<Popover.Content>
							<div className="">
								<div className={`row ${styles.detail_item_ndbh}`}>
									<label className="col-4">{currentUserPreview.GENDER=="M"?l.g('bhsk.form.f3_title15'):l.g('bhsk.form.f3_title16')}:</label>
									<label className="col-8">{currentUserPreview.CUS_NAME?currentUserPreview.CUS_NAME:currentUserPreview.STAFF_NAME}</label>
								</div>

								<div className={`row ${styles.detail_item_ndbh}`}>
									<label className="col-4">{l.g('bhsk.form.lbl_passport')}:</label>
									<label className="col-8">{currentUserPreview.IDCARD}</label>
								</div>
								<div className={`row ${styles.detail_item_ndbh}`}>
									<label className="col-4">{l.g('bhsk.form.lbl_dob')}:</label>
									<label className="col-8">{currentUserPreview.DOB}</label>
								</div>
								<div className={`row ${styles.detail_item_ndbh}`}>
									<label className="col-4">{l.g('bhsk.form.lbl_phone_number')}:</label>
									<label className="col-8">{currentUserPreview.PHONE}</label>
								</div>
								<div className={`row ${styles.detail_item_ndbh}`}>
									<label className="col-4">Email:</label>
									<label className="col-8">{currentUserPreview.EMAIL}</label>
								</div>
								<div className={`row ${styles.detail_item_ndbh}`}>
									<label className="col-4">{l.g('bhsk.form.addd')}:</label>
									<label className="col-8">{currentUserPreview.ADDRESS}</label>
								</div>
								<div className={`row ${styles.detail_item_ndbh}`}>
									<label className="col-4">{l.g('bhsk.form.effect_llbl')}:</label>
									<label className="col-8">1 {l.g('bhsk.form.year')}</label>
								</div>
							</div>
							<div className={styles.total_payment_item_ndbh}>
								<div className={`row ${styles.row_kutin}`}>
									<label className="col-4">{l.g('bhsk.form.lbl_total_am2')}</label>
									<label className="col-8">{currentUserPreview.TOTAL_AMOUNT?userAmount(currentUserPreview):`0 ${l.g("bhsk.currency")}`}</label>
								</div>
							</div>
						</Popover.Content>
						</Popover>
					</Overlay>
				</div>
				<div ref={refNGKDBH}>
					<Overlay
						show={showListChangeNDBH}
						target={target}
						// rootClose={true}
						// onHide={() => setShowListChangeNDBH(false)}
						placement="top"
						container={refNGKDBH.current}
					>
						<Popover id={styles.popover_contained_nkdbh}>
							<Popover.Title>
								<label className={styles.title_popover}>
								Chọn người không bị tâm thần, thần kinh, bệnh phong, ung thư...?.
								Người không điều trị bệnh hoặc thương tật
								</label>
							</Popover.Title>
						<Popover.Content className={styles.content_popover_custom}>
							<div className="list-ndbh">
								{props.isruserlist.filter(condExclude).map((user, index)=>{
								return(
									<div className={styles.itenm_list_ndbh}>
										<input className={styles.checkbox} type="checkbox" defaultChecked id={"xba"+user.id} onChange={(e)=>{remove_exclude_checked(e.target.checked, user.id)}}/>
										<label for={"xba"+user.id}>{user.CUS_NAME} ( {props.packages[user.PACK_POS].PACK_NAME} )</label>
									</div>)
								})}

								<div className={styles.form_btn_confirm_nkdbh}>
									<button className={styles.btn_confirm_nkdbh} onClick={()=>confirmNKDBH()}>Xác nhận</button>
								</div>
							</div>
						</Popover.Content>
						</Popover>
					</Overlay>
				</div>
				<div ref={refConditions}>
				<Overlay
						show={showListCondition}
						target={target}
						placement="bottom"
						container={refConditions.current}
					>
						<Popover id={styles.popover_contained_nkdbh}>
							<Popover.Title>
								<label className={styles.title_popover}>
								{popConditionTitle=="EXCLUDE_COND1"?l.g('bhsk.form.f3_title6'):l.g('bhsk.form.f3_title5')}
								</label>
							</Popover.Title>
						<Popover.Content className={styles.content_popover_custom}>
							<div className="list-ndbh">
							{props.isruserlist.map((user, index)=>{
							return(
								<div className={styles.itenm_list_ndbh}>
									<input className={styles.checkbox} type="checkbox" id={"xba"+user.id} defaultChecked={user[popConditionTitle]==0} onChange={(e)=>{exclude_checked(e.target.checked, index, popConditionTitle)}}/>
									<label for={"xba"+user.id}>{user.CUS_NAME} ( {props.packages[user.PACK_POS]?props.packages[user.PACK_POS].PACK_NAME:""} )</label>
								</div>)
							})}
								<div className={styles.form_btn_confirm_nkdbh}>
									<button className={styles.btn_confirm_nkdbh} onClick={(e)=>confirmConditions(popConditionTitle)}>Xác nhận</button>
								</div>
							</div>
						</Popover.Content>
						</Popover>
					</Overlay>
				</div>
			</Row>
  		</div>)}

})

const mapStateToProps = (state) => {
  return({
  	isruserlist: state.isruserlist?state.isruserlist:[],
    packages: state.data
})};
const mapDispatchToProps = dispatch => ({
    setUserList: (usrl) => dispatch(setUserList(usrl))
});


export default connect(mapStateToProps, mapDispatchToProps, null, { forwardRef: true })(Form3);