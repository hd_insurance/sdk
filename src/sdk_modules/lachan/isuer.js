import React, {
  useEffect,
  useState,
  forwardRef,
  createRef,
  useImperativeHandle,
} from "react";
import styles from "../../css/style.module.css";
import { animateScroll } from "react-scroll";
import DynamicRender from "../../common/render/DnmRender";
import StageSpinner from "../../common/loadingpage";
import util from "../../util";
import moment from "moment";
import cogoToast from "cogo-toast";

import LoadingForm from "../../common/loadingform";
import api from "../../services/Network.js";

const Isuer = forwardRef((props, ref) => {
  const [define, setDefine] = useState({});
  const [listmaped, setMaped] = useState({});
  const [loading, setLoading] = useState(true);
  const [isLoadingCalcFee, setIsLoadingCalcFee] = useState(false);
  const [issBenefit, setIssBenefit] = useState({});
  const [isspackage, setIssPackage] = useState({});
  const [isspackageFormat, setIssPackageFormat] = useState([]);
  const [benefit, setBenefit] = useState({
    label: issBenefit.PACK_NAME,
    link: issBenefit.URL_BEN,
    description: issBenefit.DESCRIPTION
      ? JSON.parse(issBenefit.DESCRIPTION)
      : [],
  });

  const [state, setState] = useState({
    name: "",
    phone: "",
    email: "",
    gender: "",
    passport: "",
    dob: "",
    address: "",
    addressCode: {
      dist: "",
      label: "",
      prov: "",
      ward: "",
    },
    cr_package: props.selectedPackage?props.selectedPackage:null,
    amount: "",
    registeredChecked: true, //derfault,
    affectday: moment().add(1, "days").format("DD/MM/YYYY"),
    expiration: moment().add(1, "days").add(1, "y").format("DD/MM/YYYY"),
    relation: {
      title: "",
      value: null,
    },
    render: true,
    isspackage: {},
  });


  const setVal = (key, val) => {
    setState((prevState) => ({
      ...prevState,
      [key]: val,
    }));
  };
  const maximumDate = () => {
    const tdate = moment(state.affectday, "DD/MM/YYYY");
    if (props.config_define.PRODUCT_CODE == "TAINAN365") {
      tdate.subtract(18, "years");
    }
    if (props.config_define.PRODUCT_CODE == "SUCKHOE365") {
      tdate.subtract(15, "days");
    }

    var now = moment();

    if (now >= tdate) {
      return {
        year: tdate.year(),
        month: tdate.month() + 1,
        day: tdate.date(),
      };
    } else {
      return {
        year: now.year(),
        month: now.month() + 1,
        day: now.date(),
      };
    }
  };
  const minimumDate = () => {
    const tdate = moment(state.affectday, "DD/MM/YYYY");
    return {
      year: tdate.year() - 65,
      month: tdate.month() + 1,
      day: tdate.date(),
    };
  };
  const defaultDate = () => {
    const tdate = moment(state.affectday, "DD/MM/YYYY");
    if (props.config_define.PRODUCT_CODE == "TAINAN365") {
      tdate.subtract(18, "years");
    }
    if (props.config_define.PRODUCT_CODE == "SUCKHOE365") {
      tdate.subtract(15, "days");
    }

    return {
      year: tdate.year(),
      month: tdate.month() + 1,
      day: tdate.date(),
    };
  };

  const defaultDateEff = () => {
    const tdate = moment(state.affectday, "DD/MM/YYYY");
    return {
      year: tdate.year(),
      month: tdate.month() + 1,
      day: tdate.date(),
    };
  };

  const setDefaultScreenValue = (map) => {
    map["dob"].define.minimumDate = minimumDate();
    map["dob"].define.maximumDate = maximumDate();
    map["dob"].define.defaultvalue = defaultDate();
    map["affectday"].define.minimumDate = defaultDateEff();
  };
  useEffect(() => {
    if (listmaped["dob"]) {
      listmaped["dob"].define.minimumDate = minimumDate();
      listmaped["dob"].define.maximumDate = maximumDate();
      listmaped["dob"].define.defaultvalue = defaultDate();
      setVal("render", !state.render);
    }
  }, [state.affectday]);

  useEffect(() => {
    const { default_info } = props;
    if (props.default_info.forwardStep1) {
      setState((prevState) => ({
        ...prevState,
        name: default_info.name,
        phone: default_info.phone,
        email: default_info.email,
        gender: default_info.gender,
        dob: default_info.dob,
        passport: default_info.passport,
        address: default_info.address,
        addressCode: default_info.addressCode,
        affectday: default_info.affectday
          ? default_info.affectday
          : moment().add(1, "days").format("DD/MM/YYYY"),
        cr_package: default_info.cr_package ? default_info.cr_package : prevState.cr_package,
      }));
    }
  }, [props.default_info]);

  const handleCalcFee = async () => {
    let data = {
      a1: props.config_define.ORG_CODE,
      a2: props.config_define.PRODUCT_CODE,
      a3: state.cr_package,
      a4: state.dob,
      a5: state.gender,
      a6: state.affectday,
    };
    try {
      setIsLoadingCalcFee(true);
      const response = await api.post("/api/bhsk/calc/fee", data);
      if (response) {
        setIsLoadingCalcFee(false);
        if (response?.[0]?.[0]?.MESSAGE) {
          setVal("amount", response[0][0].MESSAGE);
          props.updateTotalAmount(
            response[0][0].MESSAGE,
            props.default_info.id
          );
        }
      }
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    // console.log(
    //   "handleCalcFee",
    //   state.dob,
    //   state.affectday,
    //   state.gender,
    //   state.cr_package
    // );
    if (state.dob && state.affectday && state.gender && state.cr_package) {
      handleCalcFee();
    }
  }, [state.dob, state.affectday, state.gender, state.cr_package]);

  const convertRelation = (define) => {
    const result = define
      .filter(
        (e) => e.DEFINE_CODE == "MOI_QUAN_HE" && e.TYPE_CODE != "BAN_THAN"
      )
      .map((e) => {
        const list = define
          .filter((item) => item.DEFINE_CODE == e.TYPE_CODE)
          .map((item) => {
            return {
              title: item.TYPE_NAME,
              value: item.DEFINE_CODE,
              gender: item.TYPE_CODE,
            };
          });
        return {
          title: e.TYPE_NAME,
          list: list.length != 0 ? list : null,
        };
      });
    return result;
  };
  useEffect(() => {
    if (props.define) {
      const formatGender = props.define[0].map((item) => {
        return { label: item.TYPE_NAME, value: item.TYPE_CODE };
      });

      const formatRelation = convertRelation(props.define[1]);
      // console.log(formatRelation)

      const formatPackage = props.define[2].map((item) => {
        return { label: item.PACK_NAME, value: item.PACK_CODE };
      });
      const lsd = props.define[2].filter(x => x.PACK_CODE === props.default_info.cr_package).map((item) =>{
        return { label: item.PACK_NAME, value: item.PACK_CODE}
      })
      setIssPackageFormat(formatPackage);
      //cr_package

      if (!util.isEmptyObj(listmaped)) {
        listmaped["delete_is"].onClick = () => {
          props.deleteUser(props.default_info.id);
          props.default_info.forwardStep1 && props.setEntryFirst("");
        };

        listmaped["cr_package"].setData(formatPackage);
        listmaped["relation"].setData(formatRelation);
        //set default package

         setVal("isspackage", props.define[2][0]);

        if(props.selectedPackage){
          setVal("cr_package", props.selectedPackage);
          const index = props.define[2].map(e => e.PACK_CODE).indexOf(props.selectedPackage);
          setVal("isspackage", props.define[2][index]);
          setIssPackage(props.define[2][index]);
          setBenefit(formatBenefit(props.define[2][index]));
        }else if(lsd && lsd.length > 0){
          setVal("cr_package", lsd[0]?.value);
          const index = props.define[2].map(e => e.PACK_CODE).indexOf(lsd[0]?.value);
          setVal("isspackage", props.define[2][index]);
          setIssPackage(props.define[2][index]);
          setBenefit(formatBenefit(props.define[2][index]));
        }else{
          setVal("cr_package", formatPackage[0].value);
          setIssPackage(props.define[2][0]);
          setBenefit(formatBenefit(props.define[2][0]));
        }
        
       
        // disable relation
        if (props.default_info.forwardStep1) {
          listmaped["relation"].setValue({
            title: "Bản thân",
            value: "BAN_THAN",
          });
          setVal("relation", { title: "Bản thân", value: "BAN_THAN" }); // set cứng bản thân cho người 1
          listmaped["relation"].setDisable(true);
        } else {
          listmaped["relation"].setValue({
            title: "",
            value: null,
          });
          setVal("relation", {
            title: "",
            value: null,
          });
          listmaped["relation"].setDisable(false);
        }
      }
    }
  }, [listmaped]);
  useEffect(() => {
    if (!util.isEmptyObj(listmaped)) {
      const { default_info } = props;
      // xoa NDBH
      if (default_info.forwardStep1) {
        listmaped["delete_is"].toggle(true);
        listmaped["name"].setDisable(true);
        listmaped["phone"].setDisable(true);
        // listmaped["email"].setDisable(true);
        listmaped["passport"].setDisable(true);
        listmaped["dob"].setDisable(true);
        listmaped["address"].setDisable(true);
        listmaped["addressCode"].setDisable(true);
      } else {
        listmaped["delete_is"].toggle(false);
        listmaped["name"].setDisable(false);
        listmaped["phone"].setDisable(false);
        // listmaped["email"].setDisable(false);
        listmaped["passport"].setDisable(false);
        listmaped["dob"].setDisable(false);
        listmaped["address"].setDisable(false);
        listmaped["addressCode"].setDisable(false);
      }
    }
  });

  useEffect(() => {
    const { obj_config, map } = util.rootObjectComponent(
      props.defineFormIssuerJSON
    );
    setMaped(map);
    hanleAmountInit(map);
    setDefaultScreenValue(map);
    setDefine(obj_config);
    setTimeout(() => {
      setLoading(false);
    }, 200);
  }, []);
  const hanleAmountInit = (map) => {
    map["amount"].setValue(0);
  };

  const checkDOB = (dobValue, effValue) => {
    // validate khi typing
    const dobDate = moment(dobValue, "DD/MM/YYYY");
    let maxDate = moment(effValue, "DD/MM/YYYY");
    let minDate = moment(effValue, "DD/MM/YYYY").subtract(65, "years");
    let mess = "";
    let ageMess = "65";

    if (props.config_define.PRODUCT_CODE == "TAINAN365") {
      // tai nan
      maxDate = maxDate.subtract(18, "years");
      mess = "Người được bảo hiểm phải từ 18 đến 65 tuổi";
    }
    if (props.config_define.PRODUCT_CODE == "SUCKHOE365") {
      // suc khoe
      maxDate = maxDate.subtract(15, "days");
      mess = "Người được bảo hiểm phải từ 15 ngày đến 65 tuổi";
    }
    if (props.config_define.PRODUCT_CODE == "SUCKHOE_TWD"){
      // thanh niên đoàn
      ageMess = "50";
      minDate = moment(effValue, "DD/MM/YYYY").subtract(50, "years");
      maxDate = maxDate.subtract(14, "years");
      mess = "Người được bảo hiểm phải từ 14 đến 50 tuổi";
    }
    if (dobDate > maxDate) {
      return {
        mess: mess,
        isCheck: false,
      };
    }
    if (dobDate < minDate) {
      return {
        mess: `Người được bảo hiểm không vượt quá ${ageMess} tuổi`,
        isCheck: false,
      };
    }
    return {
      mess: "",
      isCheck: true,
    };
  };

  useImperativeHandle(ref, () => ({
    handleSubmit(action) {

      if (!state.relation.value) {
        cogoToast.error("Chưa chọn mối quan hệ!");
        listmaped["relation"].openRelation(true);
        return false;
      }
      const checkForm = listmaped["form"].ref.current.handleSubmit();
      if (!checkForm) {
        return false;
      }
      // check validate date
      const checkDate = checkDOB(state.dob, state.affectday);
      if (
        !checkDate.isCheck &&
        (action === "next_step" || !props.default_info.forwardStep1)
      ) {
        cogoToast.error(checkDate.mess);
        return false;
      }
      return { ...state }; // obj data
    },
    getId() {
      return props.default_info.id;
    },
  }));

  const formatBenefit = (pkinfo) => {
    let url_ben = null
    if(pkinfo.URL_BEN){
      url_ben = pkinfo.URL_BEN
    }else if(pkinfo.DESCRIPTION){
      const des = pkinfo.DESCRIPTION?JSON.parse(pkinfo.DESCRIPTION):null
      if(des[0]){
        url_ben = des[0].URL_DETAIL
      }
    }
    return {
      label: pkinfo.PACK_NAME,
      link: url_ben,
      description: JSON.parse(pkinfo.DESCRIPTION),
    };
  };

  return (
    <React.Fragment>
      {loading ? (
        <div className={styles.main_loading}>
          <div className={styles.ff_loading}>
            <StageSpinner
              size={60}
              frontColor="#329945"
              loading={loading}
              from={"isuser"}
            />
          </div>
        </div>
      ) : (
        <>
          {isLoadingCalcFee ? <LoadingForm /> : null}
          <DynamicRender
            layout={define}
            name={state.name}
            phone={state.phone}
            email={state.email}
            passport={state.passport}
            dob={state.dob}
            address={state.address}
            addressCode={state.addressCode}
            affectday={state.affectday}
            amount={state.amount}
            cr_package={state.cr_package}
            relation={state.relation}
            benefit_popup={benefit}
            onInputChange={(name, value) => {
              // console.log(name, value);
              if (name == "affectday") {
                setVal(
                  "expiration",
                  moment(value, "DD/MM/YYYY").add(1, "y").format("DD/MM/YYYY")
                );
              }
              setVal(name, value);
            }}
            onInputChangePackage={(name, value, index) => {
              setVal("cr_package", value);
              setVal("isspackage", props.define[2][index]);
              setIssPackage(props.define[2][index]);
              setBenefit(formatBenefit(props.define[2][index]));
            }}
            onRelationChange={(name, value) => {
              setVal("relation", value);
              setVal("gender", value.gender);
            }}
          />
        </>
      )}
    </React.Fragment>
  );
});
export default Isuer;
