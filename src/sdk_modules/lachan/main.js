import React, { useEffect, useState, createRef, useRef } from "react";
import Footer from "../../common/footer/footer";
import styles from "../../css/style.module.css";
import { animateScroll } from "react-scroll";
import DynamicRender from "../../common/render/DnmRender";
import StageSpinner from "../../common/loadingpage";
import Isuer from "./isuer";
import util from "../../util";
import { connect } from "react-redux";
import { setOBJState } from "../../redux/actions/action-lachan.js";
import { confirmAlert } from "react-confirm-alert";
import api from "../../services/Network.js";
import moment, { now } from "moment";
import LoadingForm from "../../common/loadingform";
import cogoToast from "cogo-toast";

function Main(props) {
  const footerRef = useRef();
  const [entryFirst, setEntryFirst] = useState("");
  const [step, setStep] = useState(0);
  const [isDisableFooter, setDisableFooter] = useState(false);
  const [loading, setLoading] = useState(true);
  const [define, setDefine] = useState({});
  const [listmaped, setMaped] = useState({});
  const [defineConfig, setDefineConfig] = useState([]);
  const [listIsuer, setListIsuer] = useState([]);
  const [loadingForm, setLoadingForm] = useState(false);
  const [deleteId, setDeleteId] = useState("");
  const [selectedPackage, setSelectedPackage] = useState(null);

  const [initFormObject, setInitFormObject] = useState(null);


  const getLangCode = () => {
    if (lng) {
      return lng.getLang() === "vi" ? "VN" : "EN";
    }
    return "VN";
  };
  const config_define = {
    ORG_CODE: props.productConfig.ORG_CODE,
    ORG_SELLER: props.productConfig.ORG_SELLER,
    PRODUCT_CODE: props.productConfig.PRODUCT,
    CHANNEL: props.productConfig.CHANNEL,
    MAX_AGE: 65,
    MIN_AGE: 1,
  };

  const config_create_order = {
    CATEGORY: props.productConfig.CATEGORY,
    PRODUCT_CODE: config_define.PRODUCT_CODE,
    CHANNEL: config_define.CHANNEL,
    ACTION: "BH_M",
    USERNAME: props.productConfig.ORG_SELLER,
  };
  const [stateStep1, setStateStep1] = useState({
    gender: "",
    name: "",
    phone: "",
    email: "",
    passport: "",
    dob: "",
    address: "",
    addressCode: {
      dist: "",
      label: "",
      prov: "",
      ward: "",
    },
    amount: "",
    vat: "k",
    mst: "",
    company_name: "",
    address_dn: "",
    addressCode_dn: "",
    cr_package: null,
    registeredChecked: true, //derfault,
  });

  const [checkPayment, setCheckPayment] = useState(false);
  const [totalAmount, setTotalAmount] = useState(0);

  const mapDataStep1 = () => {
    let BUYER = {
      CUS_ID: "",
      TYPE: "CN", // default ???
      NATIONALITY: "",
      NAME: stateStep1.name,
      DOB: stateStep1.dob,
      GENDER: stateStep1.gender,
      PROV: stateStep1.addressCode.prov,
      DIST: stateStep1.addressCode.dist,
      WARDS: stateStep1.addressCode.ward,
      ADDRESS: stateStep1.address,
      IDCARD: stateStep1.passport,
      EMAIL: stateStep1.email,
      PHONE: stateStep1.phone,
      FAX: "",
      TAXCODE: "",
    };
    return BUYER;
  };
  const mapDataStep2 = () => {
    const HEALTH_INSUR = listIsuer
      .filter((item) => {
        return item.EXCLUDE_COND1 !== true && item.EXCLUDE_COND2 !== true && item.EXCLUDE_COND3 !== true;
      })
      .map((item) => {
        return {
          CUS_ID: "",
          TYPE: "CN",
          NATIONALITY: "",
          NAME: item.name,
          DOB: item.dob,
          GENDER: item.gender, // gender chua co trong form
          PROV: item.addressCode.prov,
          DIST: item.addressCode.dist,
          WARDS: item.addressCode.ward,
          ADDRESS: item.address,
          IDCARD: item.passport,
          EMAIL: item.email,
          PHONE: item.phone,
          FAX: "",
          TAXCODE: "",
          RELATIONSHIP: item.relation ? item.relation.value : "BAN_THAN",
          PRODUCT_CODE: config_create_order.PRODUCT_CODE,
          PACK_CODE: item.cr_package,
          REGION: "VN",
          EFFECTIVE_DATE: item.affectday,
          EXPIRATION_DATE: item.expiration, // mac dinh + 1 nam
          FEES: 0, // dang chua co fees
          AMOUNT: item.amount,
          TOTAL_DISCOUNT: 0,
          TOTAL_ADD: 0,
          VAT: 0,
          TOTAL_AMOUNT: totalAmount,
        };
      });
    // console.log(HEALTH_INSUR);
    return HEALTH_INSUR;
  };

  const setValStep1 = (key, val) => {
    setStateStep1((prevState) => ({
      ...prevState,
      [key]: val,
    }));
  };
  const getActiveTab = () => {
    return listmaped["tabs_item"].ref.current.getActiveTab(); // lay active tab
  };
  const getPrositionbyId = (id) => {
    return listIsuer.findIndex((x) => x.id === id);
  };
  const setActiveTab = (id, position) => {
    if (listmaped["tab_header"]) {
      listmaped["tab_header"].setActive(id);
      listmaped["tabs_item"].setActive(id);
      // console.log("setActivetab", id);
      if (position != undefined) {
        // console.log("position", listIsuer[position]);
      }
    }
  };

  const addUserIssuer = () => {
    if (listIsuer.length > 0) {
      const isuser = handleSubmitIsuser();
      if (isuser) {
        const { data, position } = isuser;
        // gán data cũ và thêm 1 user mới...
        let l = listIsuer;
        l[position] = { ...l[position], ...data };
        setListIsuer([...l]);
        const default_info = {
          id: util.randomID(),
          ref: createRef(),
        };

        addIss(default_info);
      } else {
        return 1;
      }
    } else {
      // chua co nguoi duoc bao hiem
      const default_info = {
        id: util.randomID(),
        ref: createRef(),
      };
      addIss(default_info);
    }
  };

  const addIss = (default_info) => {
    listIsuer.push(default_info);
    setListIsuer([...listIsuer]);
    setActiveTab(default_info.id);
  };
  useEffect(() => {
    if (deleteId) {
      const position = getPrositionbyId(deleteId);
      const lxl = listIsuer;
      lxl.splice(position, 1); // xoá cả ở líst child...
      setListIsuer([...lxl]);
      listmaped["tabs_item"].removeItem(position);
      listmaped["tab_header"].setData([...lxl]);
      if (lxl.length >= 1) {
        setActiveTab(lxl[lxl.length - 1].id);
      }
      setTimeout(() => {
        calcTotalAmount();
        // console.log("tinh lai total amount");
      }, 300);
      setDeleteId("");
    }
  }, [deleteId]);
  const deleteUser = (id) => {
    confirmAlert({
      title: "Xác nhận",
      message: "Bạn có chắc chắn muốn xóa người được bảo hiểm này không?",
      buttons: [
        {
          label: "Không xóa",
          onClick: () => { },
        },
        {
          label: "Xóa NĐBH",
          onClick: () => {
            setDeleteId(id);
            // console.log("vao xoa", id);
          },
        },
      ],
    });
  };

  const calcTotalAmount = () => {
    let amt = 0;
    listIsuer.forEach((item, index) => {
      if (item.amount && item.registeredChecked) {
        if (item.EXCLUDE_COND1 !== true && item.EXCLUDE_COND2 !== true && item.EXCLUDE_COND3 !== true) {
          amt += item.amount * 1;
        }
      }
    });
    listmaped["tab_header"].setTotalAmount(amt);
    setTotalAmount(amt);
  };

  const updateTotalAmount = (monney, id) => {
    // chỉ xử lý để hiện thị số tiền trên tab header
    let amt = 0;
    if (listIsuer.length === 1) {
      listmaped["tab_header"].setTotalAmount(monney);
      setTotalAmount(monney * 1);
      return;
    }
    listIsuer.forEach((item, index) => {
      if (item.amount && item.id != id) {
        if (item.EXCLUDE_COND1 !== true && item.EXCLUDE_COND2 !== true && item.EXCLUDE_COND3 !== true) {
          amt += item.amount * 1;
        }
      }
    });
    listmaped["tab_header"].setTotalAmount(amt + monney);
    setTotalAmount(amt + monney);
    // console.log('updateTotalAmount', totalAmount)
  };

  const handleSubmitIsuser = (action = "next_step") => {
    const activeTab = getActiveTab(); // lay active tab
    const position = util.getPrositionbyId(listIsuer, activeTab);
    // console.log("tab_header position ", listIsuer, position);
    if (position == -1) return false;
    const data = listIsuer[position]?.ref?.current?.handleSubmit(action);
    if (data) {
      return { data, position };
    } else {
      return false;
    }
  };
  useEffect(() => {
    // payment done
    if (props.payment && !loading && !util.isEmptyObj(listmaped)) {
      listmaped["wizard"].ref.current.setStep(3);
      listmaped["step_layout"].ref.current.setStep(3);
      setStep(3);
    }
  }, [loading]);
  useEffect(() => {
    if (!util.isEmptyObj(listmaped)) {
      listmaped["tab_header"].onItemSelect = (index) => {// header user duoc bao hiem clicked
        const isuser = handleSubmitIsuser();
        if (isuser) {
          const { data, position } = isuser;
          let l = listIsuer;
          l[position] = { ...l[position], ...data };
          if (config_create_order.PRODUCT_CODE === 'SUCKHOE365' || config_create_order.PRODUCT_CODE === 'TAINAN365') {
            //
          } else {
            setListIsuer([...l]);
          }
          setActiveTab(index, position);
        }
      };

      // /addUserIssuer
      listmaped["tab_header"].onAddItemClick(addUserIssuer);
      listmaped["tabs_item"].onAddItemClick(addUserIssuer);

      handleScreen3();
      listmaped["flcheckbox_ck"].onValueChange = (val) => {
        if (val) {
          listmaped["flcheckbox_ck"].setValue(true);
          footerRef.current.setDisable(false);
        } else {
          listmaped["flcheckbox_ck"].setValue(false);
          footerRef.current.setDisable(true);
        }
      };
    }
  }, [listmaped, define, listIsuer]);

  useEffect(() => {
    if (config_create_order.PRODUCT_CODE === 'SUCKHOE365' || config_create_order.PRODUCT_CODE === 'TAINAN365') {
      displayUserNotExcluded_suckhoe365_tainan365();
    } else {
      const listUserItem = [];
      listIsuer.forEach((item, index) => {
        const default_info = {};
        const object_user_component = {
          id: item.id,
          component: Isuer,
          props: {
            selectedPackage: selectedPackage,
            default_info: item,
            config_define: config_define,
            define: defineConfig,
            defineFormIssuerJSON: JSON.parse(
              JSON.stringify(listmaped["tabs_item"].component)
            ),
            ref: item.ref,
            deleteUser: deleteUser,
            setEntryFirst: setEntryFirst,
            updateTotalAmount: updateTotalAmount,
          },
        };
        listUserItem.push(object_user_component);
      });
      if (listmaped["tabs_item"]) {
        listmaped["tabs_item"].setItem(listUserItem);
        listmaped["tab_header"].setData(listIsuer);
        listmaped["tab_header"].onAddItemClick(addUserIssuer);
        listmaped["tabs_item"].onAddItemClick(addUserIssuer);
      }
    }

    // console.log("List issuer : ", listIsuer);
  }, [listIsuer]);

  const handleCalcTotalMoney = (data = []) => {
    let total = 0;
    // console.log('handleCalcTotalMoney1111', data)
    data.forEach((item, index) => {
      if (item.EXCLUDE_COND1 !== true && item.EXCLUDE_COND2 !== true && item.EXCLUDE_COND3 !== true) {
        total += item.amount * 1;
      }
    });
    listmaped["totalpayment"].setData(total);
    listmaped["tab_header"].setTotalAmount(total);
    setTotalAmount(total);
  };

  const handleScreen3 = () => {
    if (listmaped["lockoutlist"]) {
      listmaped["lockoutlist"].setDataEvent((issur_list) => {
        handleCalcTotalMoney(issur_list);
        setListIsuer(issur_list);
        listmaped["listbh_1"].ref.current.setData(issur_list);
      });
      listmaped["listbh_1"].setDataEvent((issur_list) => {
        if (listmaped["lockoutlist"].ref.current) {
          handleCalcTotalMoney(issur_list);
          setListIsuer(issur_list);
          listmaped["lockoutlist"].ref.current.setData(issur_list);
        }
      });

      listmaped["listbh_1"].onClickItemEdit = (idEditor) => {
        setStep(1);
        footerRef.current.setDisable(false);
        footerRef.current.setNextTitle(l.g("bhsk.form.lbl_next"));

        listmaped["wizard"].ref.current.setStep(1);

        listmaped["step_layout"].ref.current.setStep(1);
        listmaped["tab_header"].setData(listIsuer.filter(p => p.id == idEditor));
        listmaped["tab_header"].setActive(idEditor);
        listmaped["tabs_item"].setActive(idEditor);
      };

    } else {
      console.log("handleScreen3 not found");
    }
  };

  const checkConfirmMinAge = (id, closeOnly) => {
    confirmAlert({
      title: "Xác nhận",
      message: "Trẻ em dưới 18 tuổi phải mua kèm với cha/mẹ hoặc người giám hộ. Yêu cầu nhập thêm người tham gia bảo hiểm là cha/mẹ hoặc người giám hộ.",
      buttons: closeOnly ? [{
        label: "Đóng",
        onClick: () => {
        },
      }] : [
        {
          label: "Đồng ý",
          onClick: () => addUserIssuer(),
        },
        {
          label: "Huỷ",
          onClick: () => {
          },
        },
      ],
    });
  };

  const checkConfirmAge = (lst) => {
    confirmAlert({
      title: "Xác nhận",
      message: "Trẻ em dưới 18 tuổi phải mua kèm với cha/mẹ hoặc người giám hộ. Xác nhận thông tin này trước khi chuyển sang bước tiếp",
      buttons: [
        {
          label: "Đồng ý",
          onClick: () => onNextStepConfirm(lst),
        },
        {
          label: "Huỷ",
          onClick: () => {
          },
        },
      ],
    });
  };

  function isAgeLess18(obj) {
    if (obj.dob == undefined || obj.affectday == undefined) return false;

    const dobDate = moment(obj.dob, "DD/MM/YYYY");
    let affectday = moment(obj.affectday, "DD/MM/YYYY");
    let rs = util.getAge(dobDate, affectday);
    if (rs.y < 18) {
      return true;
    }
    return false;
  }

  function checkShowAgeConfirm_SUCKHOE365(lstAllUserRegister) {
    let lstNguoiDBH = lstAllUserRegister.filter(p => !p.EXCLUDE_COND1 && !p.EXCLUDE_COND2 && !p.EXCLUDE_COND3);
    // Trong danh sách người được bảo hiểm, chỉ cẩn có 1 người >=18t là được.
    for (let i = 0; i < lstNguoiDBH.length; i++) {
      const element = lstNguoiDBH[i];
      const _dobDate = moment(element.dob, "DD/MM/YYYY");
      let _affectday = moment(element.affectday, "DD/MM/YYYY");
      let _rs = util.getAge(_dobDate, _affectday);
      if (_rs.y >= 18) { // co 1 nguoi du 18t thi pass.
        return 'PASS';
      }
    }
    let _lstUserBanThan = lstNguoiDBH.filter(p => p.relation.value == 'BAN_THAN') // User mua cho bản thân.
    if (_lstUserBanThan.length > 0) {
      if (isAgeLess18(_lstUserBanThan[0])) {
        return 'BAN_THAN_FAILS';
      }
    }
    let _lstUserRelation = lstNguoiDBH.filter(p => p.relation.value != 'BAN_THAN') // User mua hộ
    if (_lstUserRelation.length > 0) {
      for (let i = 0; i < _lstUserRelation.length; i++) {
        const e = _lstUserRelation[i];
        if (isAgeLess18(e)) {
          return 'RELATION_FAILS';
        }
      }
    }
    return 'PASS';
  }

  function displayUserNotExcluded_suckhoe365_tainan365() {
    if (config_create_order.PRODUCT_CODE === 'SUCKHOE365' || config_create_order.PRODUCT_CODE === 'TAINAN365') {
      const listUserItem = [];
      let _lst = listIsuer.filter(p => !p.EXCLUDE_COND1 && !p.EXCLUDE_COND2 && !p.EXCLUDE_COND3);
      listIsuer.forEach((item, index) => {
        const default_info = {};
        const object_user_component = {
          id: item.id,
          component: Isuer,
          props: {
            selectedPackage: selectedPackage,
            default_info: item,
            config_define: config_define,
            define: defineConfig,
            defineFormIssuerJSON: JSON.parse(
              JSON.stringify(listmaped["tabs_item"].component)
            ),
            ref: item.ref,
            deleteUser: deleteUser,
            setEntryFirst: setEntryFirst,
            updateTotalAmount: updateTotalAmount,
          },
        };
        listUserItem.push(object_user_component);
      });
      if (listmaped["tabs_item"]) {
        listmaped["tabs_item"].setItem(listUserItem);
        listmaped["tab_header"].setData(_lst);
        // listmaped["tab_header"].onAddItemClick(addUserIssuer);
        // listmaped["tabs_item"].onAddItemClick(addUserIssuer);

        if (_lst.length > 0) {
          const _id = _lst[_lst.length - 1].id;
          listmaped["tab_header"].setActive(_id);
          listmaped["tabs_item"].setActive(_id);
        }
      }
    }
  }

  const onNextClick = async (e) => {
    // animateScroll.scrollToTop();
    switch (step) {
      case 0:
        stateStep1.cr_package = localStorage.getItem('pack_code') ? localStorage.getItem('pack_code') : null;

        if (listmaped["form_step1"].ref.current.handleSubmit()) {
          //validate form step 1
          if (stateStep1.registeredChecked && stateStep1.name) {
            // neu check nguoi mua bao hiem la nguoi duoc bao hiem = true
            if (entryFirst) {
              // update lai user
              let l = listIsuer;
              const position = getPrositionbyId(entryFirst);
              l[position] = {
                ...l[position],
                ...stateStep1,
                amount: l[position]?.amount || "",
              };
              let _lst = l.filter(p => !p.EXCLUDE_COND1 && !p.EXCLUDE_COND2 && !p.EXCLUDE_COND3);
              listmaped["tab_header"].setData(_lst);
              setListIsuer([...l]);
            } else {
              // tao user moi
              const default_info = {
                id: util.randomID(),
                ref: createRef(),
                forwardStep1: true,
                ...stateStep1,
              };
              addIss(default_info);
              setEntryFirst(default_info.id);
              // console.log("onNextClick 2", default_info);
            }
          }

          calcTotalAmount();
          setStep(1);
          listmaped["wizard"].ref.current.setStep(1);
          listmaped["step_layout"].ref.current.setStep(1);
        } else {
        }

        break;
      case 1:
        const isuser = handleSubmitIsuser();
        if (isuser) {
          const { data, position } = isuser;
          let lst = listIsuer;
          lst[position] = { ...lst[position], ...data };

          if (listmaped["lockoutlist"].ref.current) {
            listmaped["lockoutlist"].ref.current.setData(listIsuer);
          }

          if (config_create_order.PRODUCT_CODE === 'SUCKHOE365') {
            let _skCheck = checkShowAgeConfirm_SUCKHOE365(lst);
            if (_skCheck === 'BAN_THAN_FAILS') {
              checkConfirmAge();
              break;
            } else if (_skCheck === 'RELATION_FAILS') {
              checkConfirmMinAge();
              break;
            } else {
              onNextStepConfirm(lst)
            }
          }

          onNextStepConfirm(lst);
        }

        break;
      case 2:
        if (config_create_order.PRODUCT_CODE === 'SUCKHOE365') {
          let _skCheck = checkShowAgeConfirm_SUCKHOE365(listIsuer);
          if (_skCheck === 'BAN_THAN_FAILS' || _skCheck === 'RELATION_FAILS') {
            checkConfirmMinAge(null, true);
            break;
          }
        }
        const SELLER = {
          SELLER_CODE: config_define?.ORG_SELLER,
          ORG_CODE: config_define?.ORG_CODE,
          ORG_TRAFFIC: "",
          TRAFFIC_LINK: "",
          ENVIROMENT: "WEB",
        };
        const PAY_INFO = {
          PAYMENT_TYPE: "CTT",
        };
        const BUYER = mapDataStep1();
        const HEALTH_INSUR = mapDataStep2();
        const data = {
          SELLER: SELLER,
          BUYER: BUYER,
          HEALTH_INSUR: HEALTH_INSUR,
          PAY_INFO: PAY_INFO,
        };
        const result = await createOrder(data);
        if (result) {
          localStorage.removeItem('pack_code');
          window.open(
            `${result.url_redirect}&callback=${window.location.origin + window.location.pathname
            }?payment=done`,
            "_self"
          );
        }
        break;
    }
  };

  const onNextStepConfirm = (lst) => {
    setListIsuer([...lst]);
    listmaped["listbh_1"].setData(lst);
    // console.log('onNextStepConfirm', lst)
    // console.log('onNextStepConfirm', totalAmount, listmaped["totalpayment"])
    // listmaped["totalpayment"].setData(totalAmount);
    handleCalcTotalMoney(lst);
    if (checkPayment) {
      footerRef.current.setDisable(false);
    } else {
      footerRef.current.setDisable(true);
    }
    footerRef.current.setNextTitle("Thanh toán");
    listmaped["flcheckbox_ck"].setValue(false);
    //set step in wizard
    listmaped["wizard"].ref.current.setStep(2);
    //set step screen
    listmaped["step_layout"].ref.current.setStep(2);
    setStep(2);
  }

  const onPrevClick = (e) => {
    switch (step) {
      case 1:
        const isuser = handleSubmitIsuser("prev_step");
        if (isuser) {
          const { data, position } = isuser;
          let l = listIsuer;
          l[position] = { ...l[position], ...data };
          setListIsuer([...l]);
          listmaped["wizard"].ref.current.setStep(step - 1);
          listmaped["step_layout"].ref.current.setStep(step - 1);
          setStep(0);
        }
        if (listIsuer.length == 0) {
          listmaped["wizard"].ref.current.setStep(step - 1);
          listmaped["step_layout"].ref.current.setStep(step - 1);
          setStep(0);
        }
        break;
      case 2:
        // console.log("listIsuer ", listIsuer)
        if (config_create_order.PRODUCT_CODE === 'SUCKHOE365' || config_create_order.PRODUCT_CODE === 'TAINAN365') {
          let _lst = listIsuer.filter(p => !p.EXCLUDE_COND1 && !p.EXCLUDE_COND2 && !p.EXCLUDE_COND3);
          if (_lst.length == 0) {
            confirmAlert({
              title: "Xác nhận",
              message: "Danh sách người tham gia bảo hiểm trống!",
              buttons: [
                {
                  label: "Đóng",
                  onClick: () => {
                  },
                },
              ],
            });
            break;
          }

          displayUserNotExcluded_suckhoe365_tainan365();
          handleCalcTotalMoney(_lst);
        }
        listmaped["wizard"].ref.current.setStep(step - 1);
        listmaped["step_layout"].ref.current.setStep(step - 1);
        footerRef.current.setDisable(false);
        footerRef.current.setNextTitle(l.g("bhsk.form.lbl_next"));
        setStep(1);
        break;
      case 3:
        listmaped["wizard"].ref.current.setStep(step - 1);
        listmaped["step_layout"].ref.current.setStep(step - 1);
        setStep(2);
        break;
    }
  };

  const setDefaultScreenValue = (map) => {
    let now = moment();
    //setup option
    // map["dob"].define.minimumDate = {
    //   year: now.year() - 65,
    //   month: now.month() + 1,
    //   day: now.date(),
    // };
    map["dob"].define.maximumDate = {
      year: now.year() - 18,
      month: now.month() + 1,
      day: now.date(),
    };

    map["dob"].define.defaultvalue = {
      year: now.year() - 18,
      month: now.month() + 1,
      day: now.date(),
    };

    map["button_1"].onClick = () => {
      window.location.href = "https://hdinsurance.com.vn";
    };
    map["button_2"].onClick = () => {
      window.location.href = "https://hdinsurance.com.vn";
    };

    //initFormObject
    //if has default values, set value
  };
  const createOrder = async (data) => {
    try {
      setLoadingForm(true);
      var url_req = `/api/sdk-form/createOrder/${config_create_order.ACTION}/${config_create_order.PRODUCT_CODE}/${config_create_order.CHANNEL}/${config_create_order.CATEGORY}/${config_create_order.USERNAME}`;
      if (props.prod_define) {
        url_req = url_req + "?ref_id=" + props.prod_define.ref_id;
      }
      const res = await api.post(url_req, data);
      if (res.Success) {
        // setLoadingForm(false);
        return res.Data;
      } else {
        setLoadingForm(false);
        cogoToast.error(res.ErrorMessage);
        return false;
      }
    } catch (error) {
      console.log("Error create order", error);
      setLoadingForm(false);
      cogoToast.error(error.ErrorMessage);
      return false;
    }
  };
  const getDefineData = async (callback) => {
    try {
      var url_df = `/api/sdk-form/define/${config_define.ORG_CODE}/${config_define.PRODUCT_CODE}/${config_define.CHANNEL}/${getLangCode()}`;
      if (props.prod_define) {
        if (props.prod_define.sku) {
          url_df = url_df + `?sku=${props.prod_define.sku}`;
        }
        if (props.prod_define.product_package) {
          setSelectedPackage(props.prod_define.product_package)
        }
      }
      const data = await api.get(url_df);

      if (data.data) {
        setDefineConfig(data.data);
        callback(data.data);
        // console.log(data.data);
      } else {
      }
      // setPackages(data);
    } catch (e) {
      console.log(e);
    }
  };
  const getInitData = async (ref_id, callback) => {
    try {
      const data = await api.get(`/api/sdk-form/cusinfor?ref_id=${ref_id}`);
      if (data[0]) {
        if (data[0][0].OBJ_PRODUCT) {
          const obj_user = JSON.parse(data[0][0].OBJ_PRODUCT);
          setInitFormObject(obj_user);
          if (obj_user) {
            const buyer_info = obj_user.BUYER;
            const insur_info = obj_user.HEALTH_INSUR;
            stateStep1.name = buyer_info.NAME;
            stateStep1.dob = buyer_info.DOB;
            stateStep1.phone = buyer_info.PHONE
              ? buyer_info.PHONE.trim().replace("+", "")
              : "";
            stateStep1.email = buyer_info.EMAIL
              ? buyer_info.EMAIL.toLowerCase()
              : "";
            stateStep1.address = buyer_info.ADDRESS;

            setStateStep1({ ...stateStep1 });

            const listUserDefault = [];
            insur_info.forEach((user, index) => {
              const obj_user = {
                ref: createRef(),
                id: util.randomID(),
                gender: "",
                name: user.NAME,
                phone: "",
                email: user.EMAIL.toLowerCase(),
                passport: user.IDCARD,
                dob: user.DOB,
                address: user.ADDRESS,
                addressCode: {
                  dist: "",
                  label: "",
                  prov: "",
                  ward: "",
                },
                vat: "k",
                mst: "",
                company_name: "",
                address_dn: "",
                addressCode_dn: "",
                cr_package: null,
                registeredChecked: true, //derfault,
              };
              listUserDefault.push(obj_user);
            });
            setListIsuer(...[listUserDefault]);
            callback(true);
          }
        }
      }

      // console.log("aaaaa ", data[0][0].OBJ_PRODUCT)
      // // console.log(data);
    } catch (e) {
      console.log(e);
    }
  };
  useEffect(() => {
    //call api get define
    getDefineData((dfn_cfg) => {
      const { obj_config, map } = util.rootObjectComponent(props.page_layout);
      props.setOBJState(obj_config);
      setMaped(map);
      setDefine(obj_config);
      setDefaultScreenValue(map);
      const label_checkbox = `Tôi cam kết các thông tin khai báo là chính xác, trung thực và hoàn toàn chịu trách nhiệm về các thông tin đã khai báo. Đồng thời tôi đã đọc, hiểu và đồng ý với <a href="${dfn_cfg[5][0]?.URL_RULE}" target="_blank">điều kiện, điều khoản, quy tắc</a> của HDI.`;
      const listGender = dfn_cfg[0];
      if (listGender) {
        const formatGender = listGender.map((item) => {
          return { label: item.TYPE_NAME, value: item.TYPE_CODE };
        });
        map["gender"].setData(formatGender);
        map["flcheckbox_ck"].setLabel(label_checkbox);
      }
      if (props.prod_define) {
        getInitData(props.prod_define.ref_id, (load_success) => { });
      }

      setLoading(false);
    });
  }, []);

  return (
    <div className={styles.main_container}>
      {loadingForm && <LoadingForm />}
      {loading ? (
        <div className={styles.main_loading}>
          <div className={styles.ff_loading}>
            <StageSpinner size={60} frontColor="#329945" loading={loading} />
          </div>
        </div>
      ) : (
        <div className={`container ${styles.kklm}`}>
          <DynamicRender
            layout={define}
            is_clone={stateStep1.registeredChecked}
            gender={stateStep1.gender}
            name={stateStep1.name}
            phone={stateStep1.phone}
            email={stateStep1.email}
            passport={stateStep1.passport}
            dob={stateStep1.dob}
            address={stateStep1.address}
            addressCode={stateStep1.addressCode}
            onInputChange={(name, value) => {
              setValStep1(name, value);
            }}
            onCloneOptionChange={(name, value) => {
              setValStep1(name, value);
              if (value) {
                listmaped["passport"].setRequire(true);
                listmaped["dob"].setRequire(true);

                listmaped["address"].setRequire(true);
                listmaped["addressCode"].setRequire(true);
              } else {
                listmaped["passport"].setRequire(false);
                listmaped["dob"].setRequire(false);

                listmaped["address"].setRequire(false);
                listmaped["addressCode"].setRequire(false);
                // xoa nguoi dc bao hiem dau tien
                const position = getPrositionbyId(entryFirst);
                const l = listIsuer;
                l.splice(position, 1); // xoá cả ở líst child...
                setListIsuer([...l]);
                listmaped["tabs_item"].removeItem(position);
                let _lstUserDuocBH = listIsuer.filter(p => !p.EXCLUDE_COND1 && !p.EXCLUDE_COND2 && !p.EXCLUDE_COND3);
                listmaped["tab_header"].setData(_lstUserDuocBH);
                if (l.length >= 1) {
                  setActiveTab(l[l.length - 1].id);
                }
                setEntryFirst("");
              }
            }}
          />

          <Footer
            ref={footerRef}
            isDisable={isDisableFooter}
            step={step}
            onPrev={onPrevClick}
            onNext={onNextClick}
            lastStep={3}
          />
        </div>
      )}
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    objState: state.lachan,
  };
};

const mapDispatchToProps = (dispatch) => ({
  setOBJState: (obj) => dispatch(setOBJState(obj)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Main);
