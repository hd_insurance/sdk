import React, {
  useEffect,
  useState,
  forwardRef,
  createRef,
  useImperativeHandle,
} from "react";
import styles from "../../css/style.module.css";
import { animateScroll } from "react-scroll";
import DynamicRender from "../../common/render/DnmRender";
import StageSpinner from "../../common/loadingpage";
import util from "../../util";
import moment from "moment";
import cogoToast from "cogo-toast";

import LoadingForm from "../../common/loadingform";
import api from "../../services/Network.js";
import { confirmAlert } from "react-confirm-alert";

const listOrg = ["HDBANK_VN", "HDSAISON", "PHULONG", "VIETJET_VN"];

const Isuer = forwardRef((props, ref) => {
  const UserContext = React.createContext({});
  const UserProvider = UserContext.Provider;
  const UserConsumer = UserContext.Consumer;

  const [define, setDefine] = useState({});
  const [listmaped, setMaped] = useState({});
  const [loading, setLoading] = useState(true);
  const [isLoadingCalcFee, setIsLoadingCalcFee] = useState(false);
  const [issBenefit, setIssBenefit] = useState({});
  const [isspackage, setIssPackage] = useState({}); // dang k dung
  const [isspackageFormat, setIssPackageFormat] = useState([]); // dang k thay dung ?
  const [benefit, setBenefit] = useState({
    label: issBenefit.PACK_NAME,
    link: `${issBenefit.URL_BEN}`,
    description: issBenefit.DESCRIPTION
      ? JSON.parse(issBenefit.DESCRIPTION)
      : [],
  });
  const [isRender, setRender] = useState(false);
  const [state, setState] = useState({
    name: "",
    phone: "",
    email: "",
    gender: "",
    passport: "",
    dob: "",
    address: "",
    addressCode: {
      dist: "",
      label: "",
      prov: "",
      ward: "",
    },
    cr_package: null,
    amount: "",
    registeredChecked: true, //derfault,
    affectday: moment().add(1, "days").format("DD/MM/YYYY"),
    expiration: moment().add(1, "days").add(1, "y").format("DD/MM/YYYY"),
    relation: {
      title: "",
      value: null,
    },
    render: true,
    isspackage: {
      CATEGORY: "",
      PACK_CODE: "",
      PACK_NAME: "",
      PACK_NAME_EN: null,
      URL_BEN: "",
      DESCRIPTION: "",
    },
  });
  const [listpackFormated, setListpackFormated] = useState([]);
  const setVal = (key, val) => {
    setState((prevState) => ({
      ...prevState,
      [key]: val,
    }));
  };
  const maximumDate = () => {
    const tdate = moment(state.affectday, "DD/MM/YYYY");
    tdate.subtract(15, "days");
    var now = moment();

    if (now >= tdate) {
      return {
        year: tdate.year(),
        month: tdate.month() + 1,
        day: tdate.date(),
      };
    } else {
      return {
        year: now.year(),
        month: now.month() + 1,
        day: now.date(),
      };
    }
  };
  const minimumDate = () => {
    const tdate = moment(state.affectday, "DD/MM/YYYY");
    return {
      year: tdate.year() - 65,
      month: tdate.month() + 1,
      day: tdate.date(),
    };
  };
  const defaultDate = () => {
    const tdate = moment(state.affectday, "DD/MM/YYYY");
    tdate.subtract(15, "days");
    return {
      year: tdate.year(),
      month: tdate.month() + 1,
      day: tdate.date(),
    };
  };

  useEffect(() => {
    const { default_info } = props;

    setState((prevState) => ({
      ...prevState,
      name: default_info.name,
      phone: default_info.phone,
      email: default_info.email,
      gender: default_info.gender,
      dob: default_info.dob,
      passport: default_info.passport,
      address: default_info.address,
      addressCode: default_info.addressCode,
      affectday: default_info.affectday
        ? default_info.affectday
        : moment().add(1, "days").format("DD/MM/YYYY"),
    }));

    // }
  }, [props.default_info]);

  const handleCalcFee = async () => {
    let data = {
      a1: props.config_define.ORG_CODE,
      a2: props.config_define.PRODUCT_CODE,
      a3: state.cr_package,
      a4: state.dob,
      a5: state.gender,
      a6: state.affectday,
    };

    try {
      setIsLoadingCalcFee(true);
      const response = await api.post("/api/bhsk/calc/fee", data);
      if (response) {
        setIsLoadingCalcFee(false);
        if (response?.[0]?.[0]?.MESSAGE) {
          setVal("amount", response[0][0].MESSAGE);
          props.updateTotalAmount(
            response[0][0].MESSAGE,
            props.default_info.id
          );
        }
      }
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    if (state.dob && state.affectday && state.gender && state.cr_package) {
      handleCalcFee();
    }
  }, [state.dob, state.affectday, state.gender, state.cr_package]);

  const convertRelation = (define) => {
    const result = define
      .filter(
        (e) => e.DEFINE_CODE == "MOI_QUAN_HE" && e.TYPE_CODE != "BAN_THAN"
      )
      .map((e) => {
        const list = define
          .filter((item) => item.DEFINE_CODE == e.TYPE_CODE)
          .map((item) => {
            return {
              title: item.TYPE_NAME,
              value: item.DEFINE_CODE,
              gender: item.TYPE_CODE,
            };
          });
        return {
          title: e.TYPE_NAME,
          list: list.length != 0 ? list : null,
        };
      });
    return result;
  };

  const filterPack = (listpack, max_pack) => {
    return listpack.filter((e) => {
      let fee = 0;
      if (e.description.length > 0) {
        fee = e.description[0].ORG_F;
      }
      return fee * 1 <= max_pack?.description[0].ORG_F * 1;
    });
  };

  useEffect(() => {
    const formatPackage = props.define[2].map((item) => {
      return {
        label: item.PACK_NAME,
        value: item.PACK_CODE,
        description: item.DESCRIPTION ? JSON.parse(item.DESCRIPTION) : [],
      };
    });
    setListpackFormated(formatPackage);
    let indexPack = 0;
    if (props.max_pack) {
      indexPack = formatPackage.findIndex((e) => e.value == props.max_pack);
    }
    const filPack = filterPack(formatPackage, formatPackage[indexPack]);
    if (!util.isEmptyObj(listmaped)) {
      !props.default_info.forwardStep1 &&
        listmaped["cr_package"].setData(filPack);
    }
  }, [props.max_pack]);


  useEffect(() => {
    if (props.define) {
      // console.log("render===>", props.default_info);
      let listPack = props.define[2];
      const formatRelation = convertRelation(props.define[1]);
      if(props.config_define.ORG_CODE === 'HDSAISON'){
        if(props?.default_info?.registeredChecked){
          listPack = listPack.filter((e) => e.PACK_TYPE === 'BAN_THAN');
        }else{
          listPack = listPack.filter((e) => e.PACK_TYPE !== 'BAN_THAN');
        }
      }
      const formatPackage = listPack.map((item) => {
        return {
          label: item.PACK_NAME,
          value: item.PACK_CODE,
          description: item.DESCRIPTION ? JSON.parse(item.DESCRIPTION) : [],
        };
      });
      setListpackFormated(formatPackage);
      let indexPack = 0;

      // Phan nay comment roi sau nay check lai logic
      let filPack = formatPackage
      
      if (props.max_pack) {

        if(props.pack_code){
          const max_pack_check = formatPackage.filter((e) => {
            return e.value == props.max_pack
          });
          const select_pack_check = formatPackage.filter((e) => {
            return e.value == props.pack_code
          });
          const fee_max = max_pack_check[0].description[0].ORG_F*1
          const fee_select = select_pack_check[0].description[0].ORG_F*1

          console.log("maxpack ",props.max_pack)
          if(fee_select>=fee_max){

            indexPack = formatPackage.findIndex((e) => e.value == props.max_pack);
            filPack = filterPack(formatPackage, formatPackage[indexPack]);
            console.log("filPack x", filPack.length, props.max_pack)
          }else{
            indexPack = formatPackage.findIndex((e) => e.value == props.pack_code);
            const indexMaxPack = formatPackage.findIndex((e) => e.value == props.max_pack);
            filPack = filterPack(formatPackage, formatPackage[indexMaxPack]);

          }
          // console.log("filPack FN", filPack, props.max_pack)
          
        }else{
          indexPack = formatPackage.findIndex((e) => e.value == props.max_pack);
          filPack = filterPack(formatPackage, formatPackage[indexPack]);
          

          
        }
        
      }



      setIssPackageFormat(formatPackage); // dang k thay dung

      if (!util.isEmptyObj(listmaped)) {
        listmaped["delete_is"].onClick = () => {
          props.deleteUser(props.default_info.id);
          props.default_info.forwardStep1 && props.setEntryFirst("");
        };

        listmaped["cr_package"].setData(formatPackage);

        !props.default_info.forwardStep1 &&
          listmaped["cr_package"].setData(filPack);
        // console.log("====>", props.max_pack, props.default_info);
        listmaped["relation"].setData(formatRelation);
        //set default package
        setIssPackage(listPack[indexPack]); // dang k dung ?
        setVal("isspackage", listPack[indexPack]);
        setVal("cr_package", formatPackage[indexPack].value);
        

        setBenefit(formatBenefit(listPack[indexPack]));
        // disable relation
        if (props.default_info.forwardStep1) {
          listmaped["relation"].setValue({
            title: props.define[1][0].TYPE_NAME,
            value: props.define[1][0].TYPE_CODE,
          });
          setVal("relation", {
            title: props.define[1][0].TYPE_NAME,
            value: props.define[1][0].TYPE_CODE,
          }); // set cứng bản thân cho người 1
          listmaped["relation"].setDisable(true);
        } else {
          if (props.default_info.relation?.value) {
            listmaped["relation"].setValue(props.default_info.relation);
            setVal("relation", props.default_info.relation);
          } else {
            listmaped["relation"].setValue({
              title: "",
              value: null,
            });
            setVal("relation", {
              title: "",
              value: null,
            }); // set cứng bản thân cho người 1
          }

          listmaped["relation"].setDisable(false);
        }
      }
    }
  }, [listmaped]);

  const setDefaultScreenValue = (map) => {
    if (listOrg.includes(props.config_define.ORG_CODE.trim())) {
      map["affectday"].setDisable(true);
    }
    const formatPackage = props.define[2].map((item) => {
      return {
        label: item.PACK_NAME,
        value: item.PACK_CODE,
        description: item.DESCRIPTION ? JSON.parse(item.DESCRIPTION) : [],
      };
    });

    map["affectday"].setValue(formatPackage[0].description[0].EFF_DATE);
    setVal("affectday", formatPackage[0].description[0].EFF_DATE);

    map["dob"].define.minimumDate = minimumDate();
    map["dob"].define.maximumDate = maximumDate();
    map["dob"].define.defaultvalue = defaultDate();
  };

  const handleDisplayNote = (map) => {
    if (props.config_define.ORG_CODE == "HDBANK_VN") {
      map["rowKitin"].toggle(false);
      return;
    }
    if (props.config_define.ORG_CODE == "VIETJET_VN") {
      map["rowKitin"].toggle(true);
      return;
    }
    if (props.config_define.ORG_CODE == "HDSAISON") {
      map["rowKitin"].toggle(false);
      return;
    }
    if (props.config_define.ORG_CODE == "PHULONG") {
      map["rowKitin"].toggle(false);
      return;
    }
  };

  useEffect(() => {
    if (listmaped["dob"]) {
      listmaped["dob"].define.minimumDate = minimumDate();
      listmaped["dob"].define.maximumDate = maximumDate();
      listmaped["dob"].define.defaultvalue = defaultDate();
      setVal("render", !state.render);
    }
    const expData = moment(state.affectday, "DD/MM/YYYY")
      .add(1, "y")
      .format("DD/MM/YYYY");
    setVal("expiration", expData);
    // console.log("expData", expData);
  }, [state.affectday]);
  useEffect(() => {
    const { obj_config, map } = util.rootObjectComponent(
      props.defineFormIssuerJSON
    );
    setMaped(map);
    setDefaultScreenValue(map);
    handleDisplayNote(map);
    setDefine(obj_config);
    handleDisableNDBHFirst(map); // xử lý disable NDBH lần đầu // khanh
    setTimeout(() => {
      setLoading(false);
      // props.setRender(); // tam thoi cmt
    }, 200);
  }, []);

  const handleDisableNDBHFirst = (map) => {
    // console.log("props.default_info", props.default_info);
    if (props.default_info.forwardStep1) {
      map["delete_is"].toggle(true);
      map["name"].setDisable(true);
      map["phone"].setDisable(true);
      map["email"].setDisable(true);
      map["passport"].setDisable(true);
      map["dob"].setDisable(true);
      map["address"].setDisable(true);
      map["addressCode"].setDisable(true);
    } else {
      map["delete_is"].toggle(false);
      map["name"].setDisable(false);
      map["phone"].setDisable(false);
      map["email"].setDisable(false);
      map["passport"].setDisable(false);
      map["dob"].setDisable(false);
      map["address"].setDisable(false);
      map["addressCode"].setDisable(false);
    }
  };
  const checkDOB = (dobValue, effValue) => {
    // validate khi typing
    const dobDate = moment(dobValue, "DD/MM/YYYY");
    let maxDate = moment(effValue, "DD/MM/YYYY");
    let minDate = moment(effValue, "DD/MM/YYYY").subtract(65, "years");
    let mess = "";
    let ageMess = "65";

    maxDate = maxDate.subtract(15, "days");
    mess = "Người được bảo hiểm phải từ 15 ngày đến 65 tuổi";

    if (dobDate > maxDate) {
      return {
        mess: mess,
        isCheck: false,
      };
    }
    if (dobDate < minDate) {
      return {
        mess: `Người được bảo hiểm không vượt quá ${ageMess} tuổi`,
        isCheck: false,
      };
    }
    return {
      mess: "",
      isCheck: true,
    };
  };
  useImperativeHandle(ref, () => ({
    handleSubmit() {
      if (!state.relation.value) {
        cogoToast.error("Chưa chọn mối quan hệ!");
        listmaped["relation"].openRelation(true);
        return false;
      }
      const checkForm = listmaped["form"].ref.current.handleSubmit();
      if (!checkForm) {
        return false;
      }
      const checkDate = checkDOB(state.dob, state.affectday);
      if (
        !checkDate.isCheck &&
        (action === "next_step" || !props.default_info.forwardStep1)
      ) {
        cogoToast.error(checkDate.mess);
        return false;
      }

      return { ...state }; // obj data
    },
    getId() {
      return props.default_info.id;
    },
  }));

  const formatBenefit = (pkinfo) => {
    return {
      label: pkinfo.PACK_NAME,
      link: `${pkinfo.URL_BEN}`,
      description: JSON.parse(pkinfo.DESCRIPTION),
    };
  };

  return (
    <React.Fragment>
      {loading ? (
        <div className={styles.main_loading}>
          <div className={styles.ff_loading}>
            <StageSpinner
              size={60}
              frontColor="#329945"
              loading={loading}
              from={"isuser"}
            />
          </div>
        </div>
      ) : (
        <>
          {isLoadingCalcFee ? <LoadingForm /> : null}
          <DynamicRender
            layout={define}
            name={state.name}
            phone={state.phone}
            email={state.email}
            passport={state.passport}
            dob={state.dob}
            address={state.address}
            addressCode={state.addressCode}
            affectday={state.affectday}
            amount={state.amount}
            cr_package={state.cr_package}
            relation={state.relation}
            benefit_popup={benefit}
            onInputChange={(name, value) => {
              // console.log(name, value);
              if (name == "affectday") {
                setVal(
                  "expiration",
                  moment(value, "DD/MM/YYYY").add(1, "y").format("DD/MM/YYYY")
                );
              }
              setVal(name, value);
            }}
            onInputChangePackage={(name, value, index) => {
              const pack = listpackFormated.find((e) => e.value == value);
              if (props.default_info.forwardStep1) {
                const userMax = props.getMaxPackNT();
                // console.log("userMax", userMax);
                if (userMax && pack?.description[0]?.ORG_F * 1 < userMax.AM) {
                  confirmAlert({
                    title: lng.trans("<lang>confirm_delete</lang>"),
                    message: `Người thân của bạn (${userMax.CUS_NAME}) đang đăng ký gói (${userMax.PACK_NAME}) cao hơn gói bạn đang lựa chọn! Bạn có muốn lựa chọn gói cao nhất mà người thân của bạn đã đăng ký.`,
                    buttons: [
                      {
                        label: "Ok",
                      },
                    ],
                  });
                  return "stopPropagation";
                }
                props.setMax_pack(value);
              }


              setVal("affectday", pack.description[0].EFF_DATE);
              setVal("cr_package", value);
              setVal("isspackage", props.define[2][index]);
              setIssPackage(props.define[2][index]);
              setBenefit(formatBenefit(props.define[2][index]));
            }}
            onRelationChange={(name, value) => {
              setVal("relation", value);
              setVal("gender", value.gender);
            }}
          />
        </>
      )}
    </React.Fragment>
  );
});
export default Isuer;
