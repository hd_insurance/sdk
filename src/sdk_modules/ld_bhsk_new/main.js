import React, { useEffect, useState, createRef, useRef } from "react";
import Footer from "../../common/footer/footer";
import styles from "../../css/style.module.css";
import { animateScroll } from "react-scroll";
import DynamicRender from "../../common/render/DnmRender";
import StageSpinner from "../../common/loadingpage";
import Isuer from "./isuer";
import util from "../../util";
import defineJSON from "./define_layout.json";
import defineFormIssuerJSON from "./form_isuer.json";
import { connect } from "react-redux";
import { setOBJState } from "../../redux/actions/action-lachan.js";
import { confirmAlert } from "react-confirm-alert";
import api from "../../services/Network.js";
import moment from "moment";
import LoadingForm from "../../common/loadingform";
import cogoToast from "cogo-toast";

function Main(props) {
  const footerRef = useRef();
  const [entryFirst, setEntryFirst] = useState("");
  const [step, setStep] = useState(0);
  const [isDisableFooter, setDisableFooter] = useState(false);
  const [loading, setLoading] = useState(true);
  const [define, setDefine] = useState({});
  const [listmaped, setMaped] = useState({});
  const [defineConfig, setDefineConfig] = useState([]);
  const [listIsuer, setListIsuer] = useState([]);
  const [checkPayment, setCheckPayment] = useState(false);
  const [totalAmount, setTotalAmount] = useState(0);
  const [loadingForm, setLoadingForm] = useState(false);
  const [deleteId, setDeleteId] = useState("");
  const [max_pack, setMax_pack] = useState(
    props.pack_code ? props.pack_code : null
  );
  const [render, setRender] = useState(true);
  const config_create_order = {
    CATEGORY: props.productConfig.CATEGORY,
    PRODUCT_CODE: props.productConfig.PRODUCT,
    CHANNEL: props.productConfig.CHANNEL,
    ACTION: props.productConfig.ACTION,
    USERNAME: props.productConfig.USERNAME,
  };

  const config_define = {
    ORG_CODE: props.org,
    PRODUCT_CODE: props.productConfig.PRODUCT,
    CHANNEL: props.productConfig.CHANNEL,
    MAX_AGE: props.productConfig.MAX_AGE,
    MIN_AGE: props.productConfig.MIN_AGE,
  };
 
  // const [stateStep1, setStateStep1] = useState({
  //   gender: "M", //M
  //   name: "KHANH1", //tuvm
  //   phone: "0936081444",
  //   email: "a@vietjetair.com", //a@vietjetair.com
  //   passport: "036085000222", //001084019998
  //   dob: "01/07/2003", //01/07/2003
  //   address: "512 hoang hoa tham", //512 hoang hoa tham
  //   addressCode: {
  //     dist: "02031", //02031
  //     label: "Tỉnh Hà Giang - Huyện Bắc Mê - Xã Phiêng Luông", //Tỉnh Hà Giang - Huyện Bắc Mê - Xã Phiêng Luông
  //     prov: "02", //02
  //     ward: "0203101018", //0203101018
  //   },
  //   // amount: "",
  //   // cr_package: null,
  //   registeredChecked: true, //derfault,
  // });
  const [stateStep1, setStateStep1] = useState({
    gender: "", //M
    name: "", //tuvm
    phone: "",
    email: "", //a@vietjetair.com
    passport: "", //1631864423
    dob: "", //01/07/2003
    address: "", //512 hoang hoa tham
    addressCode: {
      dist: "", //02031
      label: "", //Tỉnh Hà Giang - Huyện Bắc Mê - Xã Phiêng Luông
      prov: "", //02
      ward: "", //0203101018
    },
    // amount: "",
    // cr_package: null,
    registeredChecked: true, //derfault,
  });

  const mapDataStep1 = () => {
    let BUYER = {
      CUS_ID: "",
      TYPE: "CN",
      NATIONALITY: "",
      NAME: stateStep1.name,
      DOB: stateStep1.dob,
      GENDER: stateStep1.gender,
      PROV: stateStep1.addressCode.prov,
      DIST: stateStep1.addressCode.dist,
      WARDS: stateStep1.addressCode.ward,
      ADDRESS: stateStep1.address,
      IDCARD: stateStep1.passport,
      EMAIL: stateStep1.email,
      PHONE: stateStep1.phone,
      FAX: "",
      TAXCODE: "",
    };
    return BUYER;
  };
  const mapDataStep2 = () => {
    const HEALTH_INSUR = listIsuer
      .filter((item) => {
        return item.EXCLUDE_COND1 !== true && item.EXCLUDE_COND2 !== true;
      })
      .map((item) => {
        // console.log(item);
        return {
          CUS_ID: "",
          TYPE: "CN",
          NATIONALITY: "",
          NAME: item.name,
          DOB: item.dob,
          GENDER: item.gender, // gender chua co trong form
          PROV: item.addressCode?.prov,
          DIST: item.addressCode?.dist,
          WARDS: item.addressCode?.ward,
          ADDRESS: item.address,
          IDCARD: item.passport,
          EMAIL: item.email,
          PHONE: item.phone,
          FAX: "",
          TAXCODE: "",
          RELATIONSHIP: item.relation ? item.relation.value : "BAN_THAN",
          PRODUCT_CODE: config_create_order.PRODUCT_CODE,
          PACK_CODE: item.cr_package,
          REGION: "VN",
          EFFECTIVE_DATE: item.affectday,
          EXPIRATION_DATE: item.expiration, // mac dinh + 1 nam
          FEES: 0, // dang chua co fees
          AMOUNT: item.amount,
          TOTAL_DISCOUNT: 0,
          TOTAL_ADD: 0,
          VAT: 0,
          TOTAL_AMOUNT: totalAmount,
        };
      });
    return HEALTH_INSUR;
  };

  const setValStep1 = (key, val) => {
    setStateStep1((prevState) => ({
      ...prevState,
      [key]: val,
    }));
  };
  const getActiveTab = () => {
    return listmaped["tabs_item"].ref.current.getActiveTab(); // lay active tab
  };
  const getPrositionbyId = (id) => {
    return listIsuer.findIndex((x) => x.id === id);
  };

  const setActiveTab = ({
    id,
    position,
    map = listmaped,
    setActive = true,
  }) => {
    if (map["tab_header"] && setActive) {
      map["tab_header"].setActive(id);
      map["tabs_item"].setActive(id);
      if (position != undefined) {
      }
    }
  };

  const addUserIssuer = async () => {
    if (listIsuer.length > 0) {
      const isuser = await handleSubmitIsuser();
      if (isuser) {
        const { data, position } = isuser;
        // gán data cũ và thêm 1 user mới...
        let l = listIsuer;
        l[position] = { ...l[position], ...data };
        setListIsuer([...l]);
        const default_info = {
          id: util.randomID(),
          ref: createRef(),
        };

        addIss(default_info);
      } else {
        return 1;
      }
    } else {
      // chua co nguoi duoc bao hiem
      const default_info = {
        id: util.randomID(),
        ref: createRef(),
      };
      addIss(default_info);
    }
  };

  const addIss = (default_info, isFirst) => {
    let lxl = listIsuer;
    if (isFirst) {
      lxl = [default_info, ...lxl];
      // lxl.unshift(default_info);
    } else {
      lxl.push(default_info);
    }
    setListIsuer([...lxl]);
    setActiveTab({ id: default_info.id });
  };
  useEffect(() => {
    if (deleteId) {
      const position = getPrositionbyId(deleteId);
      const lxl = listIsuer;
      lxl.splice(position, 1); // xoá cả ở list child...
      setListIsuer([...lxl]);
      listmaped["tabs_item"].removeItem(position);
      listmaped["tab_header"].setData([...lxl]);
      if (lxl.length >= 1) {
        setActiveTab({ id: lxl[lxl.length - 1].id, position: lxl.length - 1 });
      }
      calcTotalAmount();
      setDeleteId("");
    }
  }, [deleteId]);
  const deleteUser = (id) => {
    confirmAlert({
      title: lng.trans("<lang>confirm_delete</lang>"),
      message: lng.trans("<lang>86hcacg0</lang>"),
      buttons: [
        {
          label: lng.trans("<lang>acudsxd7</lang>"),
          onClick: () => {},
        },
        {
          label: lng.trans("<lang>j1a6fpbl</lang>"),
          onClick: () => {
            setDeleteId(id);
            // setMax_pack(null);
            // console.log("vao xoa", id);
          },
        },
      ],
    });
  };

  const calcTotalAmount = () => {
    let amt = 0;
    // console.log("call total am", listIsuer);
    listIsuer.forEach((item, index) => {
      if (item.amount && item.registeredChecked) {
        amt += item.amount * 1;
      }
    });
    // console.log(amt);
    listmaped["tab_header"].setTotalAmount(amt);
    setTotalAmount(amt);
  };

  const updateTotalAmount = (monney, id) => {
    // chỉ xử lý để hiện thị số tiền trên tab header
    let amt = 0;
    if (listIsuer.length === 1) {
      listmaped["tab_header"].setTotalAmount(monney);
      setTotalAmount(monney * 1);
      return;
    }
    listIsuer.forEach((item, index) => {
      if (item.amount && item.id != id) {
        amt += item.amount * 1;
      }
    });
    listmaped["tab_header"].setTotalAmount(amt + monney);
    setTotalAmount(amt + monney);
  };

  const handleSubmitIsuser = async (action = "next_step") => {
    const activeTab = getActiveTab(); // lay active tab
    const position = util.getPrositionbyId(listIsuer, activeTab);
    if (position == -1) return false;
    const data = listIsuer[position]?.ref?.current?.handleSubmit(action);
    if (data) {
      // if (props.org) {
      //   const check = await checkCMDN([data]);
      //   if (check) {
      //     cogoToast.success("Nhân viên đã tham gia bảo hiểm!");
      //     return false;
      //   }
      // }
      return { data, position };
    } else {
      return false;
    }
  };
  useEffect(() => {
    // payment done
    if (props.payment && !loading && !util.isEmptyObj(listmaped)) {
      listmaped["wizard"].ref.current.setStep(3);
      listmaped["step_layout"].ref.current.setStep(3);
      setStep(3);
    }
  }, [loading]);
  useEffect(() => {
    if (!util.isEmptyObj(listmaped)) {
      listmaped["tab_header"].onItemSelect = async (id) => {
        const isuser = await handleSubmitIsuser();
        if (isuser) {
          const { data, position } = isuser;
          let l = listIsuer;
          l[position] = { ...l[position], ...data };
          setListIsuer([...l]);
          // console.log(getPrositionbyId(id));
          setActiveTab({ id: id, position: getPrositionbyId(id) });
        }
      };

      // /addUserIssuer
      listmaped["tab_header"].onAddItemClick(addUserIssuer);
      listmaped["tabs_item"].onAddItemClick(addUserIssuer);
      handleScreen3();
      listmaped["flcheckbox_ck"].onValueChange = (val) => {
        if (val) {
          listmaped["flcheckbox_ck"].setValue(true);
          footerRef.current.setDisable(false);
        } else {
          listmaped["flcheckbox_ck"].setValue(false);
          footerRef.current.setDisable(true);
        }
      };
    }
  }, [define, listIsuer]);
  const getMaxPackNT = () => {
    // console.log(listIsuer);
    let max_am = 0;
    let userMax = null;
    listIsuer.forEach((is) => {
      if (!is.forwardStep1 && is.isspackage) {
        const description = is?.isspackage?.DESCRIPTION
          ? JSON.parse(is?.isspackage?.DESCRIPTION)
          : [];
        if (description[0].ORG_F * 1 > max_am) {
          max_am = description[0].ORG_F * 1;
          userMax = {
            PACK_NAME: is.isspackage.PACK_NAME,
            PACK_CODE: is.isspackage.PACK_CODE,
            CUS_NAME: is.name,
            AM: max_am,
          };
        }
      }
    });
    return userMax;
  };
  const reRender = () => {
    setRender(!render);
  };
  useEffect(() => {
    // console.log("vao dayyyy", listIsuer);
    const listUserItem = [];
    listIsuer.forEach((item, index) => {
      const default_info = {};
      const object_user_component = {
        id: item.id,
        component: Isuer,
        props: {
          default_info: item,
          config_define: config_define,
          define: defineConfig,
          defineFormIssuerJSON: JSON.parse(
            JSON.stringify(defineFormIssuerJSON)
          ),
          ref: item.ref,
          deleteUser: deleteUser,
          setEntryFirst: setEntryFirst,
          updateTotalAmount: updateTotalAmount,
          pack_code: props.pack_code,
          max_pack: max_pack,
          setMax_pack: setMax_pack,
          getMaxPackNT: getMaxPackNT,
          setRender: reRender,
        },
      };
      listUserItem.push(object_user_component);
    });
    if (listmaped["tabs_item"]) {
      listmaped["tabs_item"].setItem(listUserItem);
      listmaped["tab_header"].setData(listIsuer);
      listmaped["tab_header"].onAddItemClick(addUserIssuer);
      listmaped["tabs_item"].onAddItemClick(addUserIssuer);
    }
  }, [listIsuer, max_pack]);

  const handleCalcTotalMoney = (data = []) => {
    let total = 0;
    data.forEach((item, index) => {
      if (item.EXCLUDE_COND1 !== true && item.EXCLUDE_COND2 !== true) {
        total += item.amount * 1;
      }
    });
    listmaped["totalpayment"].setData(total);
    setTotalAmount(total);
  };

  const handleScreen3 = () => {
    if (listmaped["lockoutlist"]) {
      listmaped["lockoutlist"].setDataEvent((issur_list) => {
        handleCalcTotalMoney(issur_list);
        setListIsuer(issur_list);
        listmaped["listbh_1"].ref.current.setData(issur_list);
      });
      listmaped["listbh_1"].setDataEvent((issur_list) => {
        if (listmaped["lockoutlist"].ref.current) {
          handleCalcTotalMoney(issur_list);
          setListIsuer(issur_list);
          listmaped["lockoutlist"].ref.current.setData(issur_list);
        }
      });

      listmaped["listbh_1"].onClickItemEdit = (index) => {
        setStep(1);
        footerRef.current.setDisable(false);
        footerRef.current.setNextTitle(l.g("bhsk.form.lbl_next"));
        listmaped["wizard"].ref.current.setStep(1);
        listmaped["step_layout"].ref.current.setStep(1);
        const idEditor = listIsuer[index].id;

        listmaped["tab_header"].setActive(idEditor);
        listmaped["tabs_item"].setActive(idEditor);
      };
    } else {
      console.log("handleScreen3 not found");
    }
  };
  const getMaxPack = (fee) => {
    const formatPackage = defineConfig[2].map((item) => {
      return {
        label: item.PACK_NAME,
        value: item.PACK_CODE,
        description: item.DESCRIPTION ? JSON.parse(item.DESCRIPTION) : [],
      };
    });
    // console.log(formatPackage);
    const indexPack = formatPackage.findIndex((e) => e.value == max_pack);
    const filterPack = formatPackage.filter((e) => {
      let feeCheck = 0;
      if (e.description.length > 0) {
        feeCheck = e.description[0].ORG_F;
      }
      console.log(fee, feeCheck, (feeCheck * 1) <= (fee * 1))
      return (feeCheck * 1) <= (fee * 1);
    });
    if (
      fee * 1 < formatPackage[indexPack].description[0].ORG_F * 1 &&
      filterPack.length > 0
    ) {
      setMax_pack(filterPack[(filterPack.length-1)].value);
      console.log("setMax_pack 2",filterPack[(filterPack.length-1)].value)
    }else{
      const rs =[...filterPack].pop();
      console.log("setMax_pack 1",rs?.value)
      setMax_pack(rs?.value);
    }
    if (filterPack.length == 0) {
      cogoToast.error("Không có gói bảo hiểm nào phù hợp!");
      footerRef.current.setDisable(true);
    }
  };
  const checkCMDN = async (arrNDBH) => {
    try {
      setLoadingForm(true);
      const arr_check = arrNDBH.map((e) => {
        return {
          textsearch: `${e.name.replace(/\s/g, "")}${e.passport.replace(
            /\s/g,
            ""
          )}`.toLowerCase(),
        };
      });
      const res = await api.post(
        //${config_create_order.PRODUCT_CODE}
        `/api/sdk-form/checkCMND/${config_create_order.PRODUCT_CODE}/${config_define.ORG_CODE}/${config_create_order.USERNAME}`,
        { arr_check }
      );

      if (res.data[0].length > 0) {
        // console.log(res.data[0]);
        // console.log(res.data[0][0].FEE);
        setLoadingForm(false);
        getMaxPack(res.data[0][0].FEE);
        if (res.data[0][0]?.TYPE == "SUCCESS") {
          return false;
        } // Product code khac, k phai ban cho nhan vien
        return true;
      }
      setLoadingForm(false);
      return false;
    } catch (error) {
      console.log("err check cmnd", error);
      setLoadingForm(false);
      return true;
    }
  };

  const onNextClick = async (e) => {
    // animateScroll.scrollToTop();
    switch (step) {
      case 0:
        if (listmaped["form_step1"].ref.current.handleSubmit()) {
          const check = await checkCMDN([
            { name: stateStep1.name, passport: stateStep1.passport },
          ]);
          if (check && props.org && stateStep1.registeredChecked) {
            cogoToast.success("Nhân viên đã tham gia bảo hiểm!");
            // setValStep1("registeredChecked", false);
            return 1;
          }
          if (!check && props.org && !stateStep1.registeredChecked) {
            cogoToast.success("Nhân viên chưa tham gia bảo hiểm!");
            // setValStep1("registeredChecked", true);
            props.pack_code && setMax_pack(props.pack_code);
            return 1;
          }
          // const getLastPack = [...defineConfig[2]].pop();
          // props.pack_code && setMax_pack(props.pack_code ? props.pack_code : getLastPack?.PACK_CODE);

          // props.pack_code && setMax_pack(props.pack_code);
          //validate form step 1
          // console.log("MAIN MAX PACK",max_pack)

          const firstUser = listIsuer.findIndex((e) => e.forwardStep1 == true);
          let lxl = listIsuer;
          if (firstUser != -1) {
            //update
            lxl[firstUser] = { ...lxl[firstUser], ...stateStep1 };
            setListIsuer([...lxl]);
            // setDeleteId(firstUser.id);
          } else {
            if (stateStep1.registeredChecked) {
              // tao user moi
              const default_info = {
                id: util.randomID(),
                ref: createRef(),
                forwardStep1: true,
                ...stateStep1,
              };
              addIss(default_info, true);
            }
          }

          calcTotalAmount(); // tinh lại tiền khi nó tắt không mua bản thân nữa
          setStep(1);
          listmaped["wizard"].ref.current.setStep(1);
          listmaped["step_layout"].ref.current.setStep(1);
        }
        break;
      case 1:
        const isuser = await handleSubmitIsuser();
        if (isuser) {
          const { data, position } = isuser;
          let lst = listIsuer;
          lst[position] = { ...lst[position], ...data };
          setListIsuer([...lst]);
          //set step in wizard
          listmaped["wizard"].ref.current.setStep(2);
          //set step screen
          listmaped["step_layout"].ref.current.setStep(2);
          listmaped["listbh_1"].setData(lst);
          listmaped["totalpayment"].setData(totalAmount);
          if (checkPayment) {
            footerRef.current.setDisable(false);
          } else {
            footerRef.current.setDisable(true);
          }
          footerRef.current.setNextTitle(lng.trans("<lang>mnn76qtj</lang>"));
          listmaped["flcheckbox_ck"].setValue(false);
          setStep(2);
        }
        if (listmaped["lockoutlist"].ref.current) {
          listmaped["lockoutlist"].ref.current.setData(listIsuer);
        }
        break;
      case 2:
        const SELLER = {
          SELLER_CODE: "Seller_TN",
          ORG_CODE: config_define.ORG_CODE,
          ORG_TRAFFIC: "",
          TRAFFIC_LINK: "",
          ENVIROMENT: "WEB",
        };
        const PAY_INFO = {
          PAYMENT_TYPE: "CTT",
        };
        const BUYER = mapDataStep1();
        const HEALTH_INSUR = mapDataStep2();
        const data = {
          SELLER: SELLER,
          BUYER: BUYER,
          HEALTH_INSUR: HEALTH_INSUR,
          PAY_INFO: PAY_INFO,
        };
        const result = await createOrder(data);
        if (result) {
          console.log("====>", result);
          window.open(
            `${result.url_redirect}&callback=${
              window.location.origin + window.location.pathname
            }?payment=done`,
            "_self"
          );
        }

        break;
    }
  };
  const onPrevClick = async (e) => {
    switch (step) {
      case 1:
        const isuser = await handleSubmitIsuser("prev_step");
        if (!isuser && listIsuer.length != 0) return;
        // setLoadingForm(true)
        if (isuser) {
          const { data, position } = isuser;
          let l = listIsuer;
          l[position] = { ...l[position], ...data };
          setListIsuer([...l]);
        }

        listmaped["wizard"].ref.current.setStep(step - 1);
        listmaped["step_layout"].ref.current.setStep(step - 1);
        setStep(0);
        break;
      case 2:
        listmaped["wizard"].ref.current.setStep(step - 1);
        listmaped["step_layout"].ref.current.setStep(step - 1);
        footerRef.current.setDisable(false);
        footerRef.current.setNextTitle(l.g("bhsk.form.lbl_next"));
        setStep(1);
        break;
      case 3:
        listmaped["wizard"].ref.current.setStep(step - 1);
        listmaped["step_layout"].ref.current.setStep(step - 1);
        setStep(2);
        break;
    }
  };

  const setDefaultScreenValue = (map) => {
    let now = moment();
    //setup option
    map["dob"].define.minimumDate = {
      year: now.year() - 65,
      month: now.month() + 1,
      day: now.date(),
    };
    map["dob"].define.maximumDate = {
      year: now.year() - 18,
      month: now.month() + 1,
      day: now.date(),
    };

    map["dob"].define.defaultvalue = {
      year: now.year() - 18,
      month: now.month() + 1,
      day: now.date(),
    };

    map["button_1"].onClick = () => {
      window.location.href = "https://hdinsurance.com.vn";
    };
    map["button_2"].onClick = () => {
      window.location.href = "https://hdinsurance.com.vn";
    };
  };
  const createOrder = async (data) => {
    try {
      setLoadingForm(true);
      const res = await api.post(
        `/api/sdk-form/createOrder/${config_create_order.ACTION}/${config_create_order.PRODUCT_CODE}/${config_create_order.CHANNEL}/${config_create_order.CATEGORY}/${config_create_order.USERNAME}`,
        data
      );

      if (res.Success) {
        // setLoadingForm(false);
        return res.Data;
      }else{
        setLoadingForm(false);
        cogoToast.error(res.ErrorMessage);
        return false;
      }
      // setLoadingForm(false);
      // cogoToast.error("Có lỗi xảy ra, vui lòng thử lại!");
      //
      // return false;
    } catch (error) {
      console.log("errr create order", error);
      setLoadingForm(false);
      cogoToast.error(error.ErrorMessage);
      return false;
    }
  };
  const getLangCode = () => {
    if (lng) {
      const langCode = lng.getLang() == "vi" ? "VN" : "EN";
      return langCode;
    }
    return "VN";
  };
  const getDefineData = async (callback) => {
    try {
      const data = await api.get(
        `/api/sdk-form/define/${config_define.ORG_CODE}/${
          config_define.PRODUCT_CODE
        }/${config_define.CHANNEL}/${getLangCode()}`
      );
      if (data.data) {
        setDefineConfig(data.data);
        callback(data.data);
        // console.log(data.data);
      } else {
      }
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    //call api get define
    getDefineData((dfn_cfg) => {
      const { obj_config, map } = util.rootObjectComponent(defineJSON);
      props.setOBJState(obj_config);
      setMaped(map);
      handlePattern(map);
      setDefine(obj_config);
      setDefaultScreenValue(map);
      const label_checkbox = `${lng.trans(
        "<lang>f3_title11</lang>"
      )} <a href="${dfn_cfg[5][0]?.URL_RULE}" target="_blank">${lng.trans(
        "<lang>f3_title12</lang>"
      )}</a> ${lng.trans("<lang>f3_title13</lang>")}`;
      const listGender = dfn_cfg[0];
      if (!max_pack) {
        setMax_pack(dfn_cfg[2][0]?.PACK_CODE);
        console.log("setMax_pack 3")

      }

      if (listGender) {
        const formatGender = listGender.map((item) => {
          return { label: item.TYPE_NAME, value: item.TYPE_CODE };
        });
        map["gender"].setData(formatGender);
        map["flcheckbox_ck"].setLabel(label_checkbox);
      }
      setLoading(false);
    });
  }, []);

  const handlePattern = (map) => {
    if (props.org == "HDBANK_VN") {
      map["email"].setPattern(`[a-zA-Z0-9._]+@hdbank.com.vn$`);
      // map["affectday"].setValue("15/08/2021")
      return;
    }
    if (props.org == "VIETJET_VN") {
      map["email"].setPattern(`[a-zA-Z0-9._]+@vietjetair.com$`);
      // map["affectday"].setValue("01/08/2021")
      return;
    }
    if (props.org == "HDSAISON") {
      map["email"].setPattern(`[a-zA-Z0-9._]+@hdsaison.com.vn$`);
      return;
    }
    // if (props.org == "PHULONG") {
    //   map["email"].setPattern(`[a-zA-Z0-9._%+-]+@phulong.com$`);
    //   return;
    // }
  };

  return (
    <div className={styles.main_container}>
      {loadingForm && <LoadingForm />}
      {loading ? (
        <div className={styles.main_loading}>
          <div className={styles.ff_loading}>
            <StageSpinner size={60} frontColor="#329945" loading={loading} />
          </div>
        </div>
      ) : (
        <div className={`container ${styles.kklm}`}>
          <DynamicRender
            layout={define}
            is_clone={stateStep1.registeredChecked}
            gender={stateStep1.gender}
            name={stateStep1.name}
            phone={stateStep1.phone}
            email={stateStep1.email}
            passport={stateStep1.passport}
            dob={stateStep1.dob}
            address={stateStep1.address}
            addressCode={stateStep1.addressCode}
            onInputChange={(name, value) => {
              setValStep1(name, value);
            }}
            onCloneOptionChange={(name, value) => {
              setValStep1(name, value);
              if (value) {
                listmaped["passport"].setRequire(true);
                listmaped["dob"].setRequire(true);

                listmaped["address"].setRequire(true);
                listmaped["addressCode"].setRequire(true);
              } else {
                listmaped["passport"].setRequire(false);
                listmaped["dob"].setRequire(false);

                listmaped["address"].setRequire(false);
                listmaped["addressCode"].setRequire(false);
                // xoa nguoi dc bao hiem dau tien
                const firstUser = listIsuer.find((e) => e.forwardStep1 == true);

                if (firstUser) {
                  setDeleteId(firstUser.id);
                }

                // const position = getPrositionbyId(entryFirst);
                // const l = listIsuer;
                // l.splice(position, 1); // xoá cả ở líst child...
                // setListIsuer([...l]);
                // listmaped["tabs_item"].removeItem(position);
                // listmaped["tab_header"].setData([...l]);
                // if (l.length >= 1) {
                //   setActiveTab({ id: l[l.length - 1].id });
                // }
                // setEntryFirst("");

              }
            }}
          />
          <Footer
            ref={footerRef}
            isDisable={isDisableFooter}
            step={step}
            onPrev={onPrevClick}
            onNext={onNextClick}
            lastStep={3}
          />
        </div>
      )}
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    objState: state.lachan,
  };
};

const mapDispatchToProps = (dispatch) => ({
  setOBJState: (obj) => dispatch(setOBJState(obj)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Main);
