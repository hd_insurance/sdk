import React from "react";
import Main from "./main";
import { Provider } from "react-redux";
import store from "../../redux/product";

const Index = (props) => {
  return (
    <Provider store={store}>
      <Main {...props} />
    </Provider>
  );
};

export default Index;
