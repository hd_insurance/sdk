import React, { useEffect, useState, createRef, useRef } from "react";
import Footer from "../../common/footer/footer";
import styles from "../../css/style.module.css";
import { animateScroll } from "react-scroll";
import DynamicRender from "../../common/render/DnmRender";
import StageSpinner from "../../common/loadingpage";
import Isuer from "./isuer";
import util from "../../util";
import { connect } from "react-redux";
import { setBuyerInfo, setListInsurer } from "../../redux/actions/product";
import { confirmAlert } from "react-confirm-alert";
import api from "../../services/Network.js";
import moment, { now } from "moment";
import LoadingForm from "../../common/loadingform";
import cogoToast from "cogo-toast";
import currencyFormatter from "currency-formatter";
import HandleEvent from "./handle";

function Main(props) {
  const footerRef = useRef();
  const [entryFirst, setEntryFirst] = useState("");
  const [step, setStep] = useState(0);
  const [isDisableFooter, setDisableFooter] = useState(false);
  const [loading, setLoading] = useState(true);
  const [define, setDefine] = useState({});
  const [listmaped, setMaped] = useState({});
  const [defineConfig, setDefineConfig] = useState([]);

  const [loadingForm, setLoadingForm] = useState(false);
  const [deleteId, setDeleteId] = useState("");
  const [listDefineCar, setListDefineCar] = useState([]);
  const [schemaIssuer, setSchemaIssuer] = useState(null);
  const [initFormObject, setInitFormObject] = useState(null);
  const [paymentMethod, setPaymentMethod] = useState("CTT");
  const [ordPartner, setOrdPartner] = useState(null);

  const config_define = {
    ...props.productConfig,
    vehicleType: props.productConfig.TYPE,
  };


  const [listIsuer, setListIsuer] = useState([]);
  const [buyer_info, setBuyerInfo] = useState({
    GENDER: "",
    NAME: "",
    PHONE: "",
    EMAIL: "",
    IDCARD: "",
    DOB: "",
    ADDRESS: "",
    addressCode: {
      dist: "",
      label: "",
      prov: "",
      ward: "",
    },
    amount: "",
    vat: "k",
    mst: "",
    company_name: "",
    address_dn: "",
    addressCode_dn: {
      dist: "",
      label: "",
      prov: "",
      ward: "",
    },
    cr_package: null,
    registeredChecked: true, //derfault,
  });
  const [register_vcx, setRegister_vcx] = useState(false);
  const [checkPayment, setCheckPayment] = useState(false);
  const [totalAmount, setTotalAmount] = useState(0);

  const setValBuyerInfo = (key, val) => {
    setBuyerInfo((prevState) => ({
      ...prevState,
      [key]: val,
    }));
  };
  const getActiveTab = () => {
    return listmaped["tabs_item"].ref.current.getActiveTab(); // lay active tab
  };
  const getPrositionbyId = (id) => {
    return listIsuer.findIndex((x) => x.id === id);
  };
  const setActiveTab = ({ id, position, map = listmaped }) => {
    if (map["tab_header"]) {
      map["tab_header"].setActive(id);
      map["tabs_item"].setActive(id);

      // if (position != undefined) {
      //   const data_submit = listIsuer[position].ref.current.getRecalcData()
      //   const recalData = data_submit.recalData
      //     listIsuer[position].ref.current.recalData(
      //       data_submit.recalData
      //     );
      //   }
    } else {
    }
  };

  const addUserIssuer = () => {
    if (props.ref_id) {
      return cogoToast.error("Thêm xe được bảo hiểm không khả dụng");
    }
    // console.log(listIsuer);
    if (listIsuer.length > 0) {
      const isuser = handleSubmitIsuser();
      if (isuser) {
        const { data, position } = isuser;
        // gán data cũ và thêm 1 user mới...
        let l = listIsuer;
        l[position] = { ...l[position], ...data };
        setListIsuer([...l]);
        const default_info = {
          id: util.randomID(),
          ref: createRef(),
        };

        addIss(default_info);
      } else {
        return 1;
      }
    } else {
      // chua co nguoi duoc bao hiem
      const default_info = {
        id: util.randomID(),
        ref: createRef(),
      };
      addIss(default_info);
    }
  };

  const addIss = (default_info, map) => {
    listIsuer.push(default_info);
    setListIsuer([...listIsuer]);
    setActiveTab({ id: default_info.id, map: map });
  };
  useEffect(() => {
    if (deleteId) {
      const position = getPrositionbyId(deleteId);
      const lxl = listIsuer;
      lxl.splice(position, 1); // xoá cả ở list child...
      setListIsuer([...lxl]);
      listmaped["tabs_item"].removeItem(position);
      listmaped["tab_header"].setData([...lxl]);
      if (lxl.length >= 1) {
        setActiveTab({ id: lxl[lxl.length - 1].id, position: lxl.length - 1 });
      }
      handleCalcTotalMoney();
      setTimeout(() => {
        // console.log("tinh lai total amount");
      }, 300);
      setDeleteId("");
    }
  }, [deleteId]);

  const deleteUser = (id) => {
    confirmAlert({
      title: "Xác nhận",
      message: "Bạn có chắc chắn muốn xóa người được bảo hiểm này không?",
      buttons: [
        {
          label: "Không xóa",
          onClick: () => { },
        },
        {
          label: "Xóa NĐBH",
          onClick: () => {
            setDeleteId(id);
            // console.log("vao xoa", id);
          },
        },
      ],
    });
  };

  const handleSubmitIsuser = () => {
    var valid = true;
    listIsuer.forEach((item, index) => {
      if (valid) {
        const check = item.ref.current.handleSubmit();
        if (!check) {
          setActiveTab({ id: item.id, position: index });
          valid = false;
        }
      }
    });
    return valid;
  };

  const validSubmitIsuser = () => {
    var valid = true;
    listIsuer.forEach((item, index) => {
      if (valid) {
        const data_issuer = item.ref.current.handleSubmit();
        if (!data_issuer) {
          setActiveTab({ id: item.id, position: index });
          valid = false;
        } else {
          listIsuer[index] = { ...item, ...data_issuer };
        }
      }
    });
    return valid;
  };

  useEffect(() => {
    // payment done
    if (props.payment && !loading && !util.isEmptyObj(listmaped)) {
      listmaped["wizard"].ref.current.setStep(3);
      listmaped["step_layout"].ref.current.setStep(3);
      setStep(3);
    }
  }, [loading]);

  useEffect(() => {
    if (!util.isEmptyObj(listmaped)) {
      listmaped["tab_header"].onItemSelect = (id) => {
        const current_active_tab = getActiveTab();
        const position = getPrositionbyId(current_active_tab);
        const data = listIsuer[position]?.ref?.current?.handleSubmit();
        if (data) {
          setActiveTab({ id: id, position: getPrositionbyId(id) });
        }
      };

      // /addUserIssuer
      listmaped["tab_header"].onAddItemClick(() => {
        if (props.ref_id) {
          cogoToast.error("Thêm xe được bảo hiểm không khả dụng");
        } else {
          addUserIssuer();
        }

        //
      });
      listmaped["tabs_item"].onAddItemClick(addUserIssuer);

      handleScreen3();
      listmaped["flcheckbox_ck"].onValueChange = (val) => {
        if (val) {
          listmaped["flcheckbox_ck"].setValue(true);
          footerRef.current.setDisable(false);
        } else {
          listmaped["flcheckbox_ck"].setValue(false);
          footerRef.current.setDisable(true);
        }
      };
    }
  }, [listmaped, define, listIsuer]);
  const createBuyer = () => {
    return {
      TYPE: "CN", // mac dinh
      NAME: buyer_info.NAME, // bat buoc
      DOB: buyer_info.DOB,
      GENDER: buyer_info.GENDER,
      ADDRESS: buyer_info.ADDRESS,
      IDCARD: buyer_info.IDCARD,
      EMAIL: buyer_info.EMAIL,
      PHONE: buyer_info.PHONE,
      PROV: buyer_info.addressCode.prov,
      DIST: buyer_info.addressCode.dist,
      WARDS: buyer_info.addressCode.ward,
      IDCARD_D: "",
      IDCARD_P: "",
      FAX: "",
      TAXCODE: buyer_info.mst,
      ADDRESS_CODE: buyer_info.addressCode,
    };
  };
  useEffect(() => {
    const listUserItem = [];
    if (!util.isEmptyObj(listmaped)) {
      listIsuer.forEach((item, index) => {
        const object_user_component = {
          id: item.id,
          component: Isuer,
          props: {
            schemaIssuer: schemaIssuer,
            default_info: item,
            listDefineCar: listDefineCar,
            vehicleType: config_define.TYPE, // fix tam
            define: defineConfig,
            config_define: config_define,
            defineFormIssuerJSON: JSON.parse(
              JSON.stringify(listmaped["tabs_item"].component)
            ),
            ref: item.ref,
            buyerInfor: createBuyer(),
            deleteUser: deleteUser,
            handleCalcTotalMoney: handleCalcTotalMoney,
            // updateTotalAmount: updateTotalAmount,
          },
        };
        listUserItem.push(object_user_component);
      });
      if (listmaped["tabs_item"]) {
        listmaped["tabs_item"].setItem(listUserItem);
        listmaped["tab_header"].setData(listIsuer);
        listmaped["tab_header"].onAddItemClick(addUserIssuer);
        listmaped["tabs_item"].onAddItemClick(addUserIssuer);
      }
    }
  }, [listIsuer, buyer_info]);

  const handleCalcTotalMoney = () => {
    let total = 0;
    listIsuer.map((car, i) => {
      total += car.ref.current.calcTotalAmountCar();
    });
    listmaped["totalpayment"].setData(total);
    listmaped["tab_header"].setTotalAmount(total);
    setTotalAmount(total);
  };
  const formatNumber = (value) => {
    return currencyFormatter.format(value, {
      thousand: ".",
      precision: 0,
    });
  };

  const handleScreen3 = () => {
    if (listmaped["listbh_1"]) {
      listmaped["listbh_1"].setPreviewGCN((curentPrev) => {
        HandleEvent.previewGCN(curentPrev, setLoadingForm, cogoToast, api, formatNumber, moment, listDefineCar[11])
      });

      listmaped["listbh_1"].onClickItemEdit = (index) => {
        setStep(1);
        footerRef.current.setDisable(false);
        footerRef.current.setNextTitle(l.g("bhsk.form.lbl_next"));
        listmaped["wizard"].ref.current.setStep(1);
        listmaped["step_layout"].ref.current.setStep(1);
        const idEditor = listIsuer[index].id;
        listmaped["tab_header"].setActive(idEditor);
        listmaped["tabs_item"].setActive(idEditor);
      };
    } else {
      // console.log("handleScreen3 not found");
    }
  };
  const createOrder = async () => {
    let arrCar = [];
    let submitArr = JSON.parse(JSON.stringify(listIsuer));
    submitArr.map((item_car, i) => {
      delete item_car.ref;
      delete item_car.id;
      delete item_car.PAID;
      delete item_car.VEHICLE_TYPE_LABEL;
      delete item_car.VEHICLE_USE_LABEL;
      delete item_car.ADDRESS_LABEL;
      arrCar.push(item_car);
    });
    let BILL_INFO = null;
    if (buyer_info.vat != "k") {
      BILL_INFO = {
        TYPE: buyer_info.vat == "cn" ? "CN" : "CQ",
        NAME:
          buyer_info.vat == "cn" ? buyer_info.NAME : buyer_info.company_name,
        DOB: buyer_info.vat == "cn" ? buyer_info.DOB : "",
        GENDER: buyer_info.vat == "cn" ? buyer_info.GENDER : "",
        PROV:
          buyer_info.vat == "cn"
            ? buyer_info.addressCode.prov
            : buyer_info.addressCode_dn.prov,
        DIST:
          buyer_info.vat == "cn"
            ? buyer_info.addressCode.dist
            : buyer_info.addressCode_dn.dist,
        WARDS:
          buyer_info.vat == "cn"
            ? buyer_info.addressCode.ward
            : buyer_info.addressCode_dn.ward,
        ADDRESS:
          buyer_info.vat == "cn" ? buyer_info.ADDRESS : buyer_info.address_dn,
        IDCARD: buyer_info.vat == "cn" ? buyer_info.IDCARD : "",
        EMAIL: buyer_info.EMAIL,
        PHONE: buyer_info.vat == "cn" ? buyer_info.PHONE : "",
        FAX: "",
        TAXCODE: buyer_info.vat == "dn" ? buyer_info.mst : "",
      };
    }

    let data = {
      ORG_CODE: config_define.ORG_CODE || config_define.ORG_CODE_CREATE,
      CHANNEL: config_define.CHANNEL,
      USERNAME: config_define.USERNAME,
      ACTION: config_define.ACTION,
      VEHICLE_INSUR: arrCar, //arrCar
      BUYER: createBuyer(),
      BILL_INFO: BILL_INFO,
      PAY_INFO: {
        PAYMENT_TYPE: paymentMethod,
      },
      ORD_PARTNER: ordPartner
    };
    if (props.prod_define && props.prod_define.ref_id && !ordPartner) {
      const _rsp = await api.get(`/api/sdk-form/cusinfor?ref_id=${props.prod_define.ref_id}`);
      if (_rsp[0]) {
        if (_rsp[0][0].REF_ID && _rsp[0][0].TRANSACTION_ID) {
          data.ORD_PARTNER = {
            REF_ID: _rsp[0][0].REF_ID, 
            TRANS_ID: _rsp[0][0].TRANSACTION_ID,
          }
        }
      }
    } else {
      data.ORD_PARTNER = {
        TRANS_ID: 'testconnectkhanh29'
      }
    }
    try {
      setLoadingForm(true);
      const response = await api.post("/api/car/create-order", data);
      // console.log("data create order: ", data);
      // console.log(response);
      if (response.Success) {
        setLoadingForm(false);
        // console.log(response);
        return response.Data.url_redirect;
      } else {
        if (response.Error) {
          setLoadingForm(false);
          cogoToast.error(response.ErrorMessage);
          return false;
        } else {
          setLoadingForm(false);
          cogoToast.error("Có lỗi xảy ra, vui lòng thử lại");
          return false;
        }
      }
    } catch (error) {
      console.log(error);
      setLoadingForm(false);
      cogoToast.error("Có lỗi xảy ra, vui lòng thử lại!");
      return false;
    }
  };
  const registerVCX = async () => {
    const data = {
      org_code: "HDI",
      name: buyer_info.NAME,
      province: buyer_info.addressCode.prov,
      district: buyer_info.addressCode.dist,
      wards: buyer_info.addressCode.ward,
      address: buyer_info.ADDRESS,
      email: buyer_info.EMAIL,
      phone: buyer_info.PHONE,
      note: "",
      channel: "",
      attact_files: [],
    };
    // console.log("data vcx", data);
    try {
      const org = config_define.ORG_CODE ? config_define.ORG_CODE : "HDI";
      const response = await api.post(`/api/sdk-form/registerVCX/${org}`, data);
      // console.log("response register vcx", response);
      if (response.success) {
      }
    } catch (error) {
      console.log(error);
    }
  };
  const onNextClick = async (e) => {
    // animateScroll.scrollToTop();
    switch (step) {
      case 0:
        if (listmaped["form_step1"].ref.current.handleSubmit()) {
          setStep(1);
          props.setBuyerInfo(buyer_info);
          listmaped["wizard"].ref.current.setStep(1);
          listmaped["step_layout"].ref.current.setStep(1);
        }

        break;
      case 1:
        const valid_isuser = validSubmitIsuser();

        if (valid_isuser) {
          listmaped["listbh_1"].setData(listIsuer);
          listmaped["totalpayment"].setData(totalAmount);

          if (checkPayment) {
            footerRef.current.setDisable(false);
          } else {
            footerRef.current.setDisable(true);
          }
          footerRef.current.setNextTitle("Thanh toán");
          listmaped["flcheckbox_ck"].setValue(false);
          // //set step in wizard
          listmaped["wizard"].ref.current.setStep(2);
          // //set step screen
          listmaped["step_layout"].ref.current.setStep(2);
          setStep(2);
        }

        break;
      case 2:
        const result = await createOrder();
        if (register_vcx) {
          const resultVCX = await registerVCX();
        }
        if (result) {
          if (paymentMethod == "CTT") {
            window.open(
              `${result}&callback=${window.location.origin + window.location.pathname
              }?payment=done`,
              "_self"
            );
          } else {
            setStep(3);
            listmaped["wizard"].ref.current.setStep(3);
            listmaped["step_layout"].ref.current.setStep(3);
          }
        }

        break;
    }
  };
  const onPrevClick = (e) => {
    switch (step) {
      case 1:
        const isuser = handleSubmitIsuser();
        if (isuser) {
          const { data, position } = isuser;
          let l = listIsuer;
          l[position] = { ...l[position], ...data };
          setListIsuer([...l]);
          listmaped["wizard"].ref.current.setStep(step - 1);
          listmaped["step_layout"].ref.current.setStep(step - 1);
          setStep(0);
        }
        if (listIsuer.length == 0) {
          listmaped["wizard"].ref.current.setStep(step - 1);
          listmaped["step_layout"].ref.current.setStep(step - 1);
          setStep(0);
        }
        break;
      case 2:
        // console.log("listIsuer ", listIsuer)
        listmaped["wizard"].ref.current.setStep(step - 1);
        listmaped["step_layout"].ref.current.setStep(step - 1);
        footerRef.current.setDisable(false);
        footerRef.current.setNextTitle(l.g("bhsk.form.lbl_next"));
        setStep(1);
        break;
      case 3:
        listmaped["wizard"].ref.current.setStep(step - 1);
        listmaped["step_layout"].ref.current.setStep(step - 1);
        setStep(2);
        break;
    }
  };

  const setDefaultScreenValue = (map) => {
    let now = moment();
    //setup option
    map["DOB"].define.minimumDate = {
      year: now.year() - 65,
      month: now.month() + 1,
      day: now.date(),
    };
    map["DOB"].define.maximumDate = {
      year: now.year() - 18,
      month: now.month() + 1,
      day: now.date(),
    };

    map["DOB"].define.defaultvalue = {
      year: now.year() - 18,
      month: now.month() + 1,
      day: now.date(),
    };

    map["button_1"].onClick = () => {
      window.location.href = "https://hdinsurance.com.vn";
    };
    map["button_2"].onClick = () => {
      window.location.href = "https://hdinsurance.com.vn";
    };

    //initFormObject
    //if has default values, set value
  };

  const getDefineData = async (callback) => {
    try {
      const data = await api.get(`/api/car/define`);
      if (data) {
        setDefineConfig(data);
        setListDefineCar(data);
        callback(data);
        // console.log(data);
      }
      // setPackages(data);
    } catch (e) {
      console.log(e);
    }
  };

  const getInitData = async (ref_id, callback) => {
    try {
      const data = await api.get(`/api/sdk-form/cusinfor?ref_id=${ref_id}`);
      if (data[0]) {
        if (data[0][0].REF_ID && data[0][0].TRANSACTION_ID) {
          setOrdPartner({ REF_ID: data[0][0].REF_ID, TRANSACTION_ID: data[0][0].TRANSACTION_ID });
        }
        if (data[0][0].OBJ_PRODUCT) {
          const obj_user = JSON.parse(data[0][0].OBJ_PRODUCT);
          if (obj_user) {
            callback(obj_user);
          }
        }
      }

    } catch (e) {
      cogoToast.error("Có lỗi xảy ra");
      console.log("ERROR INIT ", e);
    }
  };

  const formSchemaInit = () => { };

  const mapSetupState = (key, id) => {
    const _parse = util.getParseSchema(jsonschema, id);
    setValBuyerInfo(key, _parse.default);
  };

  const formInitData = async (map) => {
    setListIsuer([]);

    var listNewUser = [];
    var fistId = null;
    getInitData(props.ref_id, (data) => {
      setValBuyerInfo("NAME", data?.BUYER?.NAME);
      setValBuyerInfo("GENDER", data?.BUYER?.GENDER);
      setValBuyerInfo("ADDRESS", data?.BUYER?.ADDRESS);
      setValBuyerInfo("DOB", data?.BUYER?.DOB);
      setValBuyerInfo("EMAIL", data?.BUYER?.EMAIL);
      setValBuyerInfo("PHONE", data?.BUYER?.PHONE);
      setValBuyerInfo("IDCARD", data?.BUYER?.IDCARD);
      setValBuyerInfo("PROV", data?.BUYER?.PROV);
      setValBuyerInfo("WARDS", data?.BUYER?.WARDS);
      setValBuyerInfo("DIST", data?.BUYER?.DIST);
      setValBuyerInfo("ADDRESS_LABEL", data?.BUYER?.ADDRESS_LABEL);
      if (data?.BUYER?.ADDRESS_LABEL) {
        setValBuyerInfo("addressCode", {
          dist: data?.BUYER?.DIST,
          label: data?.BUYER?.ADDRESS_LABEL,
          prov: data?.BUYER?.PROV,
          ward: data?.BUYER?.WARDS,
        });
      }

      const _parse_payment_method = data?.PAY_INFO?.PAYMENT_TYPE;
      if (_parse_payment_method === "CTT" || _parse_payment_method === "TH") {
        setPaymentMethod(_parse_payment_method);
      }

      const insurdata = data?.INSUR;
      insurdata.map((block_product, index) => {
        if (block_product.PRODUCT_CODE == "XCG_TNDSBB_NEW") {
          const _parse_vehicle_insur = block_product.DATA;
          if (_parse_vehicle_insur) {
            _parse_vehicle_insur.map((item, index) => {
              const id = util.randomID();
              const default_info = {
                id: id,
                ref: createRef(),
                PAID: true,
                ...item,
              };
              if (index == 0) {
                fistId = id;
              }

              // addIss(default_info);
              listNewUser.push(default_info);
              // setListIsuer([...listIsuer]);
              // setActiveTab({ id: default_info.id, map: map });
            });
          }
        }
      });

      setListIsuer([...listNewUser]);
      setActiveTab({ id: fistId, map: map });
    });
  };

  useEffect(() => {
    getDefineData((dfn_cfg) => {
      const { obj_config, map } = util.rootObjectComponent(props.page_layout);

      setMaped(map);
      setDefine(obj_config);

      setDefaultScreenValue(map);

      map["GENDER"].setData(dfn_cfg[0]);
      formSchemaInit();

      setTimeout(() => {
        setLoading(false);
      }, 200);
    });
  }, []);

  useEffect(() => {
    if (props.ref_id) {
      if (listmaped.wizard) {
        formInitData();
      }
    }
  }, [listmaped]);

  return (
    <div className={styles.main_container}>
      {loadingForm && <LoadingForm />}
      {loading ? (
        <div className={styles.main_loading}>
          <div className={styles.ff_loading}>
            <StageSpinner size={60} frontColor="#329945" loading={loading} />
          </div>
        </div>
      ) : (
        <div className={`container ${styles.kklm}`}>
          <DynamicRender
            layout={define}
            gender={buyer_info.GENDER}
            name={buyer_info.NAME}
            phone={buyer_info.PHONE}
            email={buyer_info.EMAIL}
            passport={buyer_info.IDCARD}
            dob={buyer_info.DOB}
            address={buyer_info.ADDRESS}
            addressCode={buyer_info.addressCode}
            vat={buyer_info.vat}
            company_name={buyer_info.company_name}
            mst={buyer_info.mst}
            address_dn={buyer_info.address_dn}
            addressCode_dn={buyer_info.addressCode_dn}
            register_vcx={register_vcx}
            onInputChange={(name, value) => {
              // console.log(name, value);
              if (name == "register_vcx") {
                setRegister_vcx(value);
                return;
              }
              setValBuyerInfo(name, value);
            }}
            onChangeVat={(name, value) => {
              setValBuyerInfo(name, value);
              switch (value) {
                case "cn":
                  listmaped["row_cn"].toggle(false);
                  listmaped["row_dn"].toggle(true);
                  listmaped["EMAIL"].setRequire(true);
                  listmaped["IDCARD"].setRequire(true);
                  listmaped["ADDRESS"].setRequire(true);
                  listmaped["addressCode"].setRequire(true);
                  break;
                case "dn":
                  listmaped["row_dn"].toggle(false);
                  listmaped["row_cn"].toggle(true);
                  listmaped["EMAIL"].setRequire(false);
                  listmaped["IDCARD"].setRequire(false);
                  listmaped["ADDRESS"].setRequire(false);
                  listmaped["addressCode"].setRequire(false);
                  break;
                default:
                  listmaped["row_dn"].toggle(true);
                  listmaped["row_cn"].toggle(true);
                  listmaped["EMAIL"].setRequire(false);
                  listmaped["IDCARD"].setRequire(false);
                  listmaped["ADDRESS"].setRequire(false);
                  listmaped["addressCode"].setRequire(false);
                  break;
              }
            }}
          />

          <Footer
            ref={footerRef}
            isDisable={isDisableFooter}
            step={step}
            onPrev={onPrevClick}
            onNext={onNextClick}
            lastStep={3}
          />
        </div>
      )}
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    objState: state.lachan,
  };
};

const mapDispatchToProps = (dispatch) => ({
  setBuyerInfo: (obj) => dispatch(setBuyerInfo(obj)),
  setListInsurer: (obj) => dispatch(setListInsurer(obj)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Main);
