import React, {
    useEffect,
    useState,
    forwardRef,
    createRef,
    useImperativeHandle,
    useRef,
} from "react";
import styles from "../../css/style.module.css";
import { animateScroll } from "react-scroll";
import DynamicRender from "../../common/render/DnmRender";
import StageSpinner from "../../common/loadingpage";
import util from "../../util";
import moment from "moment";
import cogoToast from "cogo-toast";
import { config } from "./const";
import { confirmAlert } from "react-confirm-alert";

import LoadingForm from "../../common/loadingform";
import api from "../../services/Network.js";
import FormValid from "../../services/FormValid.js";
import { connect } from "react-redux";
import { setListInsurer } from "../../redux/actions/product";

let sdk_caculate_fees = [];
let sdk_caculate_fees_tndstn = [];

function usePrevious(value) {
    // custom hook lay gia tri truoc khi thay doi
    const ref = useRef();
    useEffect(() => {
        ref.current = value;
    });
    return ref.current;
}

const Isuer = forwardRef((props, ref) => {
    const [define, setDefine] = useState({});
    const [listmaped, setMaped] = useState({});
    const [loading, setLoading] = useState(true);
    const [isLoadingCalcFee, setIsLoadingCalcFee] = useState(false);
    const [render, setRender] = useState(false);
    const [isLoading, setIsLoading] = useState(false);
    const [messErrSeat, setMessErrSeat] = useState('');

    const [buyforData, setBuyforData] = useState({
        "for": "CA_NHAN",
        "type": "CN",
        "data_value": {
            "gender": "M",
            "name": "",
            "email": "",
            "phone": "",
            "name_dn": "",
            "email_dn": "",
            "phone_dn": "",
            "mst_dn": ""
        }
    });

    const [init_buyforData, setInitBuyforData] = useState(null);

    // bat buoc
    const [vehicle_group, setVehicle_group] = useState(""); // nhom xe
    const [vehicle_type, setVehicle_type] = useState(""); // loai xe
    const [purpose, setPurpose] = useState(""); // muc dich su dung
    const [seat, setSeat] = useState(""); // so cho ngoi
    const [payload, setPayload] = useState(""); // trong tai
    const [newCar, setNewCar] = useState(false);
    const [duration, setDuration] = useState("1"); // thoi han HD
    const [effective_date, setEffective_date] = useState(
        moment().add(1, "days").format("DD/MM/YYYY")
    );
    const [effective_time, setEffective_time] = useState("00:00");
    const [end_date, setEnd_date] = useState(
        moment().add(1, "days").add(1, "years").format("DD/MM/YYYY")
    );
    const [end_time, setEnd_time] = useState("00:00");
    const [tndsfee, setTndsfee] = useState(""); // phi bh tnds bat buoc , cong cai nay
    const [plate, setPlate] = useState(""); // bien so xe
    const [frame_number, setFrame_number] = useState(""); // so khung
    const [vehicle_number, setVehicle_number] = useState(""); // so may

    // TNDS tu nguyen
    const [damage_nt3, setDamage_nt3] = useState(""); // thiet hai than the nguoi thu 3
    const [property_damage_nt3, setProperty_damage_nt3] = useState(""); // thiet hai tai san nguoi thu 3
    const [client_damage, setClient_damage] = useState(""); // thiet hai than the hanh khach
    const [voluntary_tnds_fee, setVoluntary_tnds_fee] = useState(""); //phi tu nguyen, voi cai nay

    // Bao hiem tai nan lai xe, phu xe, ng ngoi tren xe
    const [ins_money, setIns_money] = useState(""); // so tien bao hiem
    const [ins_fee_lx, setIns_fee_lx] = useState(""); // phi bh tai nan lai xe, phu xe, nguoi ngoi tren xe

    // Bao hiem tnds doi voi hang hoa
    const [goods, setGoods] = useState(""); // hang hoa duoc bao hiem
    const [responsibility_level, setResponsibility_level] = useState(""); // muc tranh nhiem
    const [ins_goods_fee, setIns_gooods_fee] = useState(""); // Phí BH TNDS của chủ xe với hàng hóa

    // nguoi khac
    const [gender, setGender] = useState(""); // danh xung
    const [owner_name, setOwner_name] = useState(""); // ten chu xe
    const [owner_phone, setOwner_phone] = useState(""); // so dien thoai
    const [owner_email, setOwner_email] = useState(""); // email

    // doanh nghiep
    const [company_name, setCompany_name] = useState(""); // ten DN
    const [mst, setMst] = useState(""); // ma so thuetên tổ
    const [compan_phone, setCompany_phone] = useState(""); // so dien thoai
    const [companr_email, setCompany_email] = useState(""); // email

    const [isShowtnds, setIsShowtnds] = useState(false);
    const [isShowAccident, setIsShowAccident] = useState(false);
    const [isShowtndsForGood, setIsShowtndsForGood] = useState(false);
    const [buyFor, setBuyFor] = useState("CN"); // type

    const [totalFee, setTotalFee] = useState(0);

    const prevState = usePrevious({
        // luu gia tri truoc thay doi
        vehicle_type,
        purpose,
        seat,
        payload,
        duration,
        damage_nt3,
        property_damage_nt3,
        client_damage,
        ins_money,
        goods,
        responsibility_level,
    });

    let timeoutCalc = null;

    useImperativeHandle(ref, () => ({
        // getRecalcData(){
        //   return getData()
        // },
        handleSubmit() {
            if (messErrSeat) {
                confirmAlert({
                    title: "Thông báo",
                    message: messErrSeat,
                    buttons: [
                        {
                            label: "Đóng",
                            onClick: () => {
                            },
                        },
                    ],
                });
                return;
            }
            if (listmaped["form"].ref.current.handleSubmit()) {
                const data = getData();

                return getData();
            }
            return false;
        },
        getId() {
            return props.default_info.id;
        },
        calcTotalAmountCar() {
            return calcTotalAmount();
        },
        recalData(data) {
            recalData(data);
        },
    }));

    const setDefaultBuyer = (buyerInfor) => {
        // if (buyFor == "CN") {
        //   // if (buyerInfor.PHONE) {
        //   //   setOwner_phone(buyerInfor.PHONE);
        //   //   console.log("props.default_info ", buyerInfor.PHONE);
        //   // }
        //   // if (buyerInfor.NAME) {
        //   //   setOwner_name(buyerInfor.NAME);
        //   // }
        //   // if (props.buyerInfor.EMAIL) {
        //   //   setOwner_email(buyerInfor.EMAIL);
        //   // }
        //   // if (props.buyerInfor.GENDER) {
        //   //   setGender(buyerInfor.GENDER);
        //   // }
        // }
    };
    useEffect(() => {
        const duration_df = props?.config_define?.DURATION;
        if (duration_df) {
            setEnd_date(moment(effective_date, "DD/MM/YYYY")
                .add(duration_df, "years")
                .format("DD/MM/YYYY"))
        }
    }, [props.config_define])

    useEffect(() => {
        if (props.buyerInfor) {
            let default_data = {
                "for": "CA_NHAN",
                "type": "CN",
                "data_value": {
                    "gender": "M",
                    "name": props?.buyerInfor.NAME,
                    "email": props?.buyerInfor.EMAIL,
                    "phone": props?.buyerInfor.PHONE,
                    "name_dn": "",
                    "email_dn": "",
                    "phone_dn": props?.buyerInfor.PHONE,
                    "mst_dn": "",
                    "address": props?.buyerInfor.ADDRESS,
                    "addressCode": props?.buyerInfor?.addressCode,
                }
            }
            setInitBuyforData(default_data)
            setBuyforData(default_data)
        }
    }, [props.buyerInfor]);


    useEffect(() => {
        // console.log("default_info ", props.default_info.DISABLE)

        if (!util.isEmptyObj(listmaped)) {
            listmaped["delete_is"].onClick = () => {
                props.deleteUser(props.default_info.id);
            };

            const default_info = props.default_info;
            if (default_info.PAID) {
                mapDefaultData(default_info);
            }
            FormValid.setDefault(props.form_valid, {
                seat: listmaped["seat"],
                vehicle_group: listmaped["vehicle_group"],
                vehicle_type: listmaped["vehicle_type"],
                duration: listmaped["duration"],
                purpose: listmaped["purpose"],
                isShowtnds: listmaped["isShowtnds"],
                isShowAccident: listmaped["isShowAccident"],
                damage_nt3: listmaped["damage_nt3"],
                property_damage_nt3: listmaped["property_damage_nt3"],
                ins_money: listmaped["ins_money"]
            })

            if (props.isDisable) {
                listmaped["isShowAccident"].setDisableEditor(true);  //bhlx- phu xe, nguoi nguoi tren xe disable
            }
            listmaped["isShowtndsForGood"].setDisableEditor(true);

            // listmaped["isShowtnds"].setDisableEditor(true); //bh tnds




        }
    }, [listmaped]);

    useEffect(() => {
        if (!util.isEmptyObj(listmaped)) {
            if (
                purpose &&
                vehicle_group &&
                config.MUC_DICH_USE.includes(purpose) &&
                config.THIET_HAI_THAN_THE_HK.includes(vehicle_group)
            ) {
                listmaped["client_damage"].toggle(false);
                listmaped["col_client_damage"].toggle(false);
                setClient_damage(props.listDefineCar[8][0].value);
                setRender(!render);
            } else {
                listmaped["client_damage"].toggle(true);
                listmaped["col_client_damage"].toggle(true);
                setClient_damage("");
                setRender(!render);
            }
            if (vehicle_group && config.NGUOIHANG.includes(vehicle_group)) {
                listmaped["center_line_pay"].toggle(false);
                listmaped["payload"].toggle(false);
                listmaped["row_tndsForGood_checkbox"].toggle(false);
                isShowtndsForGood && listmaped["row_isShowtndsForGood"].toggle(false);
                listmaped["isShowtndsForGood"].setDisableEditor(false);
                setRender(!render);

            } else {
                listmaped["center_line_pay"].toggle(true);
                listmaped["payload"].toggle(true);

                listmaped["row_tndsForGood_checkbox"].toggle(true);
                listmaped["row_isShowtndsForGood"].toggle(true);
                listmaped["isShowtndsForGood"].setDisableEditor(true);
                setRender(!render);
            }


        }
    }, [purpose, vehicle_group]);


    const disabledForm = (disabled_fields) => {
        // disabled_fields.forEach((field_name, index)=>{

        // })
        listmaped["vehicle_group"].setDisableEditor(true);
        listmaped["vehicle_type"].setDisableEditor(true);
        listmaped["duration"].setDisableEditor(true);
        // listmaped["seat"].setDisableEditor(true);
        listmaped["purpose"].setDisableEditor(true);

    }

    const mapDefaultData = (default_info) => {
        if (default_info.PAID) {
            listmaped["delete_is"].toggle(true);
        }

        if (default_info.VEHICLE_GROUP) {


            setVehicle_group(default_info.VEHICLE_GROUP);
        }
        if (default_info.VEHICLE_GROUP) {
            listmaped["vehicle_type"].setData(filterCar(default_info.VEHICLE_GROUP));
            setVehicle_type(default_info.VEHICLE_TYPE);
        }
        if (default_info.VEHICLE_USE) {
            setPurpose(default_info.VEHICLE_USE);
        }
        if (default_info.SEAT_NO) {
            setSeat(default_info.SEAT_NO.toString());
        }
        let duration_default = props?.config_define?.DURATION;
        if (duration_default) {
            setDuration(duration_default)
        } else {
            setDuration("1");
        }

        if (default_info.NONE_NUMBER_PLATE) {
            const isNonePlate = default_info.NONE_NUMBER_PLATE == "1";
            setNewCar(isNonePlate);
            listmaped["plate"].setRequire(!isNonePlate);
            listmaped["frame_number"].setRequire(isNonePlate);
            listmaped["vehicle_number"].setRequire(isNonePlate);
        }
        if (default_info.CHASSIS_NO) {
            setFrame_number(default_info.CHASSIS_NO);
        }
        if (default_info.ENGINE_NO) {
            setVehicle_number(default_info.ENGINE_NO);
        }
        if (default_info.NUMBER_PLATE) {
            setPlate(default_info.NUMBER_PLATE);
        }


        if (default_info.IS_VOLUNTARY == "1") {
            setIsShowtnds(true);
            listmaped["row_isShowtnds"].toggle(false);

            if (default_info.VOLUNTARY_CIVIL) {
                if (default_info.VOLUNTARY_CIVIL.INSUR_3RD) {
                    setDamage_nt3(default_info.VOLUNTARY_CIVIL.INSUR_3RD);
                    listmaped["damage_nt3"].setDisableEditor(true);
                }
                if (default_info.VOLUNTARY_CIVIL.INSUR_3RD_ASSETS) {
                    setProperty_damage_nt3(default_info.VOLUNTARY_CIVIL.INSUR_3RD_ASSETS);
                    listmaped["property_damage_nt3"].setDisableEditor(true);
                }
            }
        }


        if (default_info.IS_DRIVER == "1") {
            setIsShowAccident(true);
            listmaped["row_isShowAccident"].toggle(false);

            if (default_info.VOLUNTARY_CIVIL) {
                if (default_info.VOLUNTARY_CIVIL.DRIVER_INSUR) {
                    setIns_money(default_info.VOLUNTARY_CIVIL.DRIVER_INSUR);
                    listmaped["ins_money"].setDisableEditor(true);
                }
            }
        }


        if (default_info.IS_CARGO == "1") {
            setIsShowtndsForGood(true);
            listmaped["row_isShowtndsForGood"].toggle(false);
            listmaped["isShowtndsForGood"].setDisableEditor(true);
            if (default_info.VOLUNTARY_CIVIL) {
                if (default_info.VOLUNTARY_CIVIL.WEIGHT_CARGO) {
                    setGoods(default_info.VOLUNTARY_CIVIL.WEIGHT_CARGO);
                    listmaped["goods"].setDisableEditor(true);
                }
                if (default_info.VOLUNTARY_CIVIL.CARGO_INSUR) {
                    setResponsibility_level(default_info.VOLUNTARY_CIVIL.CARGO_INSUR);
                    listmaped["responsibility_level"].setDisableEditor(true);
                }
            }
        }

    };
    const filterVehicle = (arrVehicleGroup) => {
        // chi lay GROUP o to hoac xe may voi moi page rieng
        if (props.vehicleType == "motor") {
            return arrVehicleGroup.filter((e) => e.VH_GROUP == "XE_MAY");
        }
        if (props.vehicleType == "car") {
            return arrVehicleGroup.filter((e) => e.VH_GROUP == "XE_OTO");
        } else {
            return arrVehicleGroup;
        }
    };

    const filterCar = (value_code) => {
        // filter loai xe chi tiet voi moi nhom xe
        if (props.listDefineCar.length > 0) {
            return props.listDefineCar[3].filter((l) => l.VH_GROUP_DT == value_code);
        } else {
            return [];
        }
    };
    const minimumDate = () => {
        const now = moment();
        return {
            year: now.year(),
            month: now.month() + 1,
            day: now.date() + 1,
        };
    };

    const setDefaultScreenValue = (map) => {
        // map["gender"].setData(
        //   props.listDefineCar.length > 0 ? props.listDefineCar[0] : []
        // );


        map["vehicle_group"].setData(
            props.listDefineCar.length > 0
                ? filterVehicle(props.listDefineCar[2])
                : []
        );
        map["purpose"].setData(
            props.listDefineCar.length > 0 ? props.listDefineCar[4] : []
        );
        map["duration"].setData([
            { label: "1 năm", value: "1" },
            { label: "2 năm", value: "2" },
            { label: "3 năm", value: "3" },
        ]);

        map["damage_nt3"].setData(
            props.listDefineCar.length > 0 ? props.listDefineCar[7] : []
        );
        map["property_damage_nt3"].setData(
            props.listDefineCar.length > 0 ? props.listDefineCar[9] : []
        );
        map["client_damage"].setData(
            props.listDefineCar.length > 0 ? props.listDefineCar[8] : []
        );
        map["ins_money"].setData(
            props.listDefineCar.length > 0 ? props.listDefineCar[11] : []
        );
        map["responsibility_level"].setData(
            props.listDefineCar.length > 0 ? props.listDefineCar[12] : []
        );
        map["effective_date"].define.minimumDate = minimumDate();
    };

    useEffect(() => {
        const { obj_config, map } = util.rootObjectComponent(
            props.defineFormIssuerJSON
        );
        setMaped(map);
        setDefaultScreenValue(map);
        setDefine(obj_config);
        setTimeout(() => {
            setLoading(false);
        }, 200);
    }, []);

    const filterCarWeight = (weigh) => {
        return props.listDefineCar[22].find((o) => {
            return (
                parseFloat(o.MIN_VALUE) <= parseFloat(weigh) &&
                parseFloat(weigh) <= parseFloat(o.MAX_VALUE)
            );
        }).value;
    };

    const filterCarSeat = (numberSeats) => {
        return props.listDefineCar[23].find((vl) => vl.NUM_OF_SEAT == numberSeats)
            .value;
    };

    const createObjCalcFees = (
        vehicle_type,
        purpose,
        seat,
        payload,
        duration,
        damage_nt3,
        property_damage_nt3,
        client_damage,
        ins_money,
        responsibility_level,
        goods) => {
        if (!vehicle_type) vehicle_type = "VH_TYPE";
        if (!purpose) purpose = "CAR_PURPOSE";
        if (!seat) {
            seat = "CAR_SEAT";
        } else {
            seat = filterCarSeat(seat);
        }
        if (!payload) {
            payload = "CAR_WEIGHT";
        } else {
            payload = filterCarWeight(payload);
        }
        if (!duration) {
            duration = "NUM_OF_DAY";
        } else {
            duration = parseInt(duration) * 365 + "";
        }
        if (
            // check truong hop chon chua du tnds tu nguyen
            !isShowtnds
        ) {
            damage_nt3 = "";
            property_damage_nt3 = "";
            client_damage = "";
        }
        if (!isShowAccident) {
            ins_money = "";
        }
        if (!isShowtndsForGood) {
            responsibility_level = "";
            goods = "";
        }
        if (!ins_money) ins_money = "BH_NTX";
        if (!vehicle_type) vehicle_type = "VH_TYPE";
        if (!purpose) purpose = "CAR_PURPOSE";
        if (!client_damage) client_damage = "TH_HK";
        if (!property_damage_nt3) property_damage_nt3 = "TH_TSN3";
        if (!damage_nt3) damage_nt3 = "TH_N3";
        if (!responsibility_level) responsibility_level = "BH_HH";
        if (!goods) goods = "TTAI_TAN";
        return [
            {
                KEY_CODE: "BBPLA_CNGOI",
                VAL_CODE: `${vehicle_type}@${seat}`,
                TYPE_CODE: "CACHE",
            },
            {
                KEY_CODE: "BBPLA_LXE",
                VAL_CODE: `${vehicle_type}`,
                TYPE_CODE: "CACHE",
            },
            {
                KEY_CODE: "BBPLA_TTAI",
                VAL_CODE: `${vehicle_type}@${payload}`,
                TYPE_CODE: "CACHE",
            },
            {
                KEY_CODE: "BBPLA_MDKD",
                VAL_CODE: `${vehicle_type}@${purpose}`,
                TYPE_CODE: "CACHE",
            },
            {
                KEY_CODE: "BBPLA_MDKD_CNGOI",
                VAL_CODE: `${vehicle_type}@${purpose}@${seat}`,
                TYPE_CODE: "CACHE",
            },
            {
                KEY_CODE: "HS_T30D",
                VAL_CODE: `${duration}`,
                TYPE_CODE: "CONSTANT",
            },
            {
                KEY_CODE: "HS_D30D",
                VAL_CODE: "0",
                TYPE_CODE: "CONSTANT",
            },

            {
                KEY_CODE: "XCGTN_BH_HH",
                VAL_CODE: `${responsibility_level}`,
                TYPE_CODE: "CACHE"
            },
            {
                KEY_CODE: "XCGTN_CNGOI",
                VAL_CODE: `${seat}`,
                TYPE_CODE: "CACHE"
            },
            {
                KEY_CODE: "XCGTN_N3_CNGOI",
                VAL_CODE: `${vehicle_type}@${seat}@${damage_nt3}`,
                TYPE_CODE: "CACHE"
            },
            {
                KEY_CODE: "XCGTN_N3_LXE",
                VAL_CODE: `${vehicle_type}@${damage_nt3}`,
                TYPE_CODE: "CACHE"
            },
            {
                KEY_CODE: "XCGTN_N3_TTAI",
                VAL_CODE: `${vehicle_type}@${payload}@${damage_nt3}`,
                TYPE_CODE: "CACHE"
            },

            {
                KEY_CODE: "XCGTN_NTX",
                VAL_CODE: `${ins_money}`,
                TYPE_CODE: "CACHE"
            },
            {
                KEY_CODE: "XCGTN_TH_HK",
                VAL_CODE: `${vehicle_type}@${purpose}@${client_damage}`,
                TYPE_CODE: "CACHE"
            },
            {
                KEY_CODE: "XCGTN_TSN3_CNGOI",
                VAL_CODE: `${vehicle_type}@${seat}@${property_damage_nt3}`,
                TYPE_CODE: "CACHE"
            },
            {
                KEY_CODE: "XCGTN_TSN3_LXE",
                VAL_CODE: `${vehicle_type}@${property_damage_nt3}`,
                TYPE_CODE: "CACHE"
            },
            {
                KEY_CODE: "XCGTN_TSN3_TTAI",
                VAL_CODE: `${vehicle_type}@${payload}@${property_damage_nt3}`,
                TYPE_CODE: "CACHE"
            },
            {
                KEY_CODE: "XCGTN_TTAI_HH",
                VAL_CODE: `${goods}`,
                TYPE_CODE: `CONSTANT`,
            },
            {
                KEY_CODE: "XCG_TGBH",
                VAL_CODE: `${duration}`,
                TYPE_CODE: `CONSTANT`,
            },

        ];
    };

    // const createObjCalcFeesTndstn = (
    //     vehicle_type,
    //     purpose,
    //     seat,
    //     payload,
    //     damage_nt3,
    //     property_damage_nt3,
    //     client_damage,
    //     ins_money,
    //     responsibility_level,
    //     goods,
    //     duration
    // ) => {
    //     if (
    //         // check truong hop chon chua du tnds tu nguyen
    //         !isShowtnds
    //     ) {
    //         damage_nt3 = "";
    //         property_damage_nt3 = "";
    //         client_damage = "";
    //     }
    //     if (!isShowAccident) {
    //         ins_money = "";
    //     }
    //     if (!isShowtndsForGood) {
    //         responsibility_level = "";
    //         goods = "";
    //     }
    //     if (!ins_money) ins_money = "BH_NTX";
    //     if (!vehicle_type) vehicle_type = "VH_TYPE";
    //     if (!purpose) purpose = "CAR_PURPOSE";
    //     if (!client_damage) client_damage = "TH_HK";
    //     if (!payload) {
    //         payload = "CAR_WEIGHT";
    //     } else {
    //         payload = filterCarWeight(payload);
    //     }
    //     if (!property_damage_nt3) property_damage_nt3 = "TH_TSN3";
    //     if (!damage_nt3) damage_nt3 = "TH_N3";
    //     if (!seat) {
    //         seat = "CAR_SEAT";
    //     } else {
    //         seat = filterCarSeat(seat);
    //     }
    //     if (!responsibility_level) responsibility_level = "BH_HH";
    //     if (!goods) goods = "TTAI_TAN";
    //     if (!duration) {
    //         duration = "NUM_OF_DAY";
    //     } else {
    //         duration = parseInt(duration) * 365 + "";
    //     }
    //     return [
    //         {
    //             KEY_CODE: "TN_NTX",
    //             VAL_CODE: `${ins_money}`,
    //         },
    //         {
    //             KEY_CODE: "TN_TH_HK",
    //             VAL_CODE: `${vehicle_type}@${purpose}@${client_damage}`,
    //         },
    //         {
    //             KEY_CODE: "TN_TSN3_TTAI",
    //             VAL_CODE: `${vehicle_type}@${payload}@${property_damage_nt3}`,
    //         },
    //         {
    //             KEY_CODE: "TN_N3_TTAI",
    //             VAL_CODE: `${vehicle_type}@${payload}@${damage_nt3}`,
    //         },
    //         {
    //             KEY_CODE: "TN_N3_CNGOI",
    //             VAL_CODE: `${vehicle_type}@${seat}@${damage_nt3}`,
    //         },
    //         {
    //             KEY_CODE: "TN_TSN3_CNGOI",
    //             VAL_CODE: `${vehicle_type}@${seat}@${property_damage_nt3}`,
    //         },
    //         {
    //             KEY_CODE: "TN_TSN3_LXE",
    //             VAL_CODE: `${vehicle_type}@${property_damage_nt3}`,
    //         },
    //         {
    //             KEY_CODE: "TN_N3_LXE",
    //             VAL_CODE: `${vehicle_type}@${damage_nt3}`,
    //         },
    //         {
    //             KEY_CODE: "TN_BH_HH",
    //             VAL_CODE: `${responsibility_level}`,
    //         },
    //         {
    //             KEY_CODE: "TN_TTAI_HH",
    //             VAL_CODE: `${goods}`,
    //             TYPE_CODE: `CONSTANT`,
    //         },
    //         {
    //             KEY_CODE: "TN_TGBH",
    //             VAL_CODE: `${duration}`,
    //             TYPE_CODE: `CONSTANT`,
    //         },
    //         {
    //             KEY_CODE: "TN_CNGOI",
    //             VAL_CODE: `${seat}`,
    //         },
    //     ];
    // };

    const onChangeVehicleGroup = (value) => {
        setVehicle_group(value);
        setPurpose("");
        setSeat("");
        setPayload("");
        setVehicle_type("");
    };

    const flagStateChanged = (state = {}, prevState) => {
        if (prevState) {
            for (const property in state) {
                if (state[property] != prevState[property]) return true;
            }
            return false;
        } else return true;
    };
    const getFee = () => {
        if (timeoutCalc) {
            clearTimeout(timeoutCalc);
        }
        setIsLoading(true);
        // props.onCbTermChange(true);
        timeoutCalc = setTimeout(() => {
            calcFee(sdk_caculate_fees, sdk_caculate_fees_tndstn);
        }, 2000);
    };

    useEffect(() => {
        // uncheck tnds tu nguyen
        if (
            config.XE_MAY.includes(vehicle_group) &&
            duration &&
            vehicle_group &&
            vehicle_type &&
            seat
        ) {
            // xe may
            if (
                !isShowtnds &&
                prevState.damage_nt3 &&
                prevState.property_damage_nt3
            ) {
                getFee();
            }
            if (!isShowAccident && prevState.ins_money) {
                getFee();
            }
        }
        if (
            (vehicle_group &&
                vehicle_type &&
                purpose &&
                seat &&
                payload &&
                duration) ||
            (vehicle_group &&
                vehicle_type &&
                purpose &&
                seat &&
                ![...config.NGUOIHANG, ...config.XE_MAY].includes(vehicle_group) &&
                duration)
        ) {
            if (
                !isShowtnds &&
                ((prevState.damage_nt3 && prevState.property_damage_nt3) ||
                    (prevState.damage_nt3 &&
                        prevState.property_damage_nt3 &&
                        prevState.client_damage))
            ) {
                getFee();
            }
            if (!isShowAccident && prevState.ins_money) {
                getFee();
            }
            if (
                !isShowtndsForGood &&
                prevState.goods &&
                prevState.responsibility_level
            ) {
                getFee();
            }
        }
    }, [isShowtnds, isShowAccident, isShowtndsForGood]);

    useEffect(() => {
        const form_valid_result = FormValid.valid(props.form_valid, { seat: seat })
        if (form_valid_result) {
            //  confirmAlert({
            //   title: "Thông báo",
            //   message: form_valid_result,
            //   buttons: [
            //     {
            //       label: "Đóng",
            //       onClick: () => {},
            //     },
            //   ],
            // });
            setMessErrSeat(form_valid_result);
            return;
        }

        if (seat > 60) {
            //validate seat
            setMessErrSeat('Không hỗ trợ với các loại xe lớn hơn 60 chỗ');
            // confirmAlert({
            //   title: "Thông báo",
            //   message: "Không hỗ trợ với các loại xe lớn hơn 60 chỗ",
            //   buttons: [
            //     {
            //       label: "Đóng",
            //       onClick: () => {},
            //     },
            //   ],
            // });
        } else {
            setMessErrSeat('');
            if (
                config.XE_MAY.includes(vehicle_group) &&
                duration &&
                vehicle_group &&
                vehicle_type &&
                seat
            ) {
                // truong hop xe may
                getFee();
            }
            // 2 truong hop: cho nguoi va hang, cac loai oto con lai
            if (
                (vehicle_group &&
                    vehicle_type &&
                    purpose &&
                    seat &&
                    payload &&
                    duration) ||
                (vehicle_group &&
                    vehicle_type &&
                    purpose &&
                    seat &&
                    ![...config.NGUOIHANG, ...config.XE_MAY].includes(vehicle_group) &&
                    duration)
            ) {
                getFee();
            }
        }
    }, [vehicle_group, vehicle_type, purpose, seat, payload, duration]);


    useEffect(() => {
        if (
            config.XE_MAY.includes(vehicle_group) &&
            duration &&
            vehicle_group &&
            vehicle_type &&
            seat
        ) {
            // xe may
            if (isShowtnds || isShowAccident) {
                if (
                    ((damage_nt3 && property_damage_nt3) ||
                        (damage_nt3 && property_damage_nt3 && client_damage)) &&
                    flagStateChanged(
                        { damage_nt3, property_damage_nt3, client_damage },
                        prevState
                    )
                ) {
                    getFee();
                }
                if (ins_money && flagStateChanged({ ins_money }, prevState)) {
                    getFee();
                }
            }
        }
        if (
            (vehicle_group &&
                vehicle_type &&
                purpose &&
                seat &&
                payload &&
                duration) ||
            (vehicle_group &&
                vehicle_type &&
                purpose &&
                seat &&
                ![...config.NGUOIHANG, ...config.XE_MAY].includes(vehicle_group) &&
                duration)
        ) {
            if (isShowtnds || isShowAccident || isShowtndsForGood) {
                if (
                    ((damage_nt3 && property_damage_nt3) ||
                        (damage_nt3 && property_damage_nt3 && client_damage)) &&
                    flagStateChanged(
                        { damage_nt3, property_damage_nt3, client_damage },
                        prevState
                    )
                ) {
                    getFee();
                }
                if (ins_money && flagStateChanged({ ins_money }, prevState)) {
                    getFee();
                }
                if (
                    goods &&
                    responsibility_level &&
                    flagStateChanged({ goods, responsibility_level }, prevState)
                ) {
                    getFee();
                }
            }
        }
    }, [
        damage_nt3,
        property_damage_nt3,
        client_damage,
        ins_money,
        goods,
        responsibility_level,
    ]);


    const calcFee = async () => {
        const default_info = props.default_info;
        sdk_caculate_fees = createObjCalcFees(
            vehicle_type,
            purpose,
            seat,
            payload,
            duration,
            damage_nt3,
            property_damage_nt3,
            client_damage,
            ins_money,
            responsibility_level,
            goods,
        );

        try {
            const productInfor = [
                {
                    INX: "0",
                    CATEGORY: "XE",
                    PRODUCT_CODE: default_info.COMPULSORY_CIVIL ? default_info.COMPULSORY_CIVIL.PRODUCT_CODE : props.config_define.PRODUCT,
                    PACK_CODE: default_info.COMPULSORY_CIVIL ? default_info.COMPULSORY_CIVIL.PACK_CODE : props.config_define.PRODUCT,
                    DISCOUNT: "0",
                    DISCOUNT_UNIT: "",
                    EFF: effective_date,
                    EXP: getExpDateByDurationAndEffDate(duration, effective_date),
                    DYNAMIC_FEES: sdk_caculate_fees,
                }
            ];

            const responseCalc = await api.post("/api/car/fee", productInfor);
            if (responseCalc) {
                // console.log("result", responseCalc);
                if (responseCalc.PRODUCT_DETAIL.length > 0) {
                    const rs = responseCalc.PRODUCT_DETAIL[0];
                    const tnds_fee = rs?.OUT_DETAIL.length > 0 ? rs.OUT_DETAIL.filter(vl => vl.CODE === "TNDSBB_F") : "";
                    const vol_tnds_fee = rs?.OUT_DETAIL.length > 0 ? rs.OUT_DETAIL.filter(vl => vl.CODE === "XCGTN_BS") : "";

                    // listmaped["tndsfee"].setValue(tnds_fee);
                    // listmaped["voluntary_tnds_fee"].setValue(vol_tnds_fee);
                    setTndsfee(tnds_fee[0]?.FEES_DATA);
                    setVoluntary_tnds_fee(vol_tnds_fee[0]?.FEES_DATA);
                    setTotalFee(rs?.TOTAL_AMOUNT);
                    // console.log("tnds_fee", tnds_fee);
                    // console.log("vol_tnds_fee", vol_tnds_fee);
                    props.handleCalcTotalMoney();
                    // props.onCbTermChange(false);
                    setIsLoading(false);
                }

            }
        } catch (e) {
            cogoToast.error("Không thể tính phí, vui lòng thử lại sau.");
            console.log(e);
            setIsLoading(false);
        }
    };

    const getLabelInput = (arrData, value) => {
        let result = arrData.filter((obj) => {
            return obj.value === value;
        });
        if (result[0]) {
            return result[0].label;
        } else {
            return ""; // truong hop value truyen vao k co trong list data thi set mac dinh
        }
    };
    const getTypeNameInput = (arrData, value) => {
        let result = arrData.filter((obj) => {
            return obj.value === value;
        });
        if (result[0]) {
            return result[0].TYPE_NAME;
        } else {
            return ""; // truong hop value truyen vao k co trong list data thi set mac dinh
        }
    };
    const calcTotalAmount = () => {
        return (
            parseInt(tndsfee ? tndsfee : "0") +
            parseInt(voluntary_tnds_fee ? voluntary_tnds_fee : "0")
        );
    };
    const getLabelAddress = (label, address = "") => {
        let provideFormat = address || "";
        if (label) {
            let provideVl = label.split("-");
            provideFormat && (provideFormat += ",");
            provideFormat += provideVl[2] + "," + provideVl[1] + "," + provideVl[0];
        }
        return provideFormat;
    };

    const getBuyerData = () => {
        var b = {
            RELATIONSHIP: "KHAC", //moi_them
            DOB: buyforData.type == "CN" ? props.buyerInfor.DOB : "", // dob cua nguoi khac

            PROV: buyforData?.data_value?.addressCode?.prov,
            DIST: buyforData?.data_value?.addressCode?.dist,
            WARDS: buyforData?.data_value?.addressCode?.ward,
            ADDRESS_LABEL: getLabelAddress(buyforData?.data_value?.addressCode?.label, buyforData?.data_value?.ADDRESS),
            ADDRESS_FORM: getLabelAddress(buyforData?.data_value?.addressCode?.label, buyforData?.data_value?.ADDRESS),
            ADDRESS: buyforData?.data_value?.address,
            IDCARD: buyforData.type == "CN" ? props.buyerInfor.IDCARD : "",
            IDCARD_D: "",
            IDCARD_P: "",
            EMAIL: "",
            PHONE: "",
            TAXCODE: ""
        }

        buyforData.type == "CN" ? b.TYPE = "CN" : b.TYPE = "CQ"
        if (buyforData.type == "CN") {
            b.RELATIONSHIP = buyforData.for
        } else {
            b.RELATIONSHIP = "KHAC"
        }

        if (buyforData.type == "CN") {
            if (buyforData.for == "BAN_THAN") {
                b.NAME = props.buyerInfor.NAME
            } else {
                b.NAME = buyforData.data_value.name
            }
        } else {
            b.NAME = buyforData.data_value.name_dn
        }

        if (buyforData.type == "CN") {
            if (buyforData.for == "BAN_THAN") {
                b.ADDRESS = props.buyerInfor.ADDRESS
                b.GENDER = props.buyerInfor.GENDER
            } else {
                b.GENDER = buyforData.data_value.gender
            }
        } else {
            b.GENDER = ""
        }

        if (buyforData.type == "CN") {
            if (buyforData.for == "BAN_THAN") {
                b.EMAIL = props.buyerInfor.EMAIL
            } else {
                b.EMAIL = buyforData.data_value.email
            }
        } else {
            b.EMAIL = buyforData.data_value.email_dn
        }

        if (buyforData.type == "CN") {
            if (buyforData.for == "BAN_THAN") {
                b.PHONE = props.buyerInfor.PHONE
            } else {
                b.PHONE = buyforData.data_value.phone
            }
        } else {
            b.PHONE = buyforData.data_value.phone_dn
        }


        if (buyforData.type == "DN") {
            b.TAXCODE = buyforData.data_value.mst_dn
        }
        if (buyforData.type == "CN") {
            if (props.buyerInfor?.addressCode?.prov) {
                b.PROV = props.buyerInfor.addressCode.prov || ""
                b.DIST = props.buyerInfor.addressCode.dist || ""
                b.WARDS = props.buyerInfor.addressCode.ward || ""
            }
            if (buyforData.for == "BAN_THAN") {
                b.ADDRESS_LABEL = getLabelAddress(props.buyerInfor?.addressCode?.label, props.buyerInfor?.ADDRESS);
                b.ADDRESS_FORM = getLabelAddress(props.buyerInfor?.addressCode?.label, props.buyerInfor?.ADDRESS);
            }
        } else {
            b.GENDER = ""
        }
        return b;
    }
    const getData = () => {

        const buyerdata = getBuyerData()

        return {
            // data create order
            ...buyerdata,
            IS_SPLIT_ORDER: "0", //Tách đơn hay gộp đơn
            VEHICLE_GROUP: vehicle_group, //Nhóm xe
            VEHICLE_TYPE: vehicle_type, //Loại xe
            VEHICLE_TYPE_LABEL: getLabelInput(
                vehicle_group ? filterCar(vehicle_group) : [],
                vehicle_type
            ),
            VEHICLE_USE: purpose, //Mục dích sử dụng
            VEHICLE_USE_LABEL: getLabelInput(
                props.listDefineCar.length > 0 ? props.listDefineCar[4] : [],
                purpose
            ),
            NONE_NUMBER_PLATE: newCar ? "1" : "0", // 1: chưa có biển số --- 0: có biển số
            NUMBER_PLATE: plate, // bien so xe
            CHASSIS_NO: frame_number, //Số khung
            ENGINE_NO: vehicle_number, // Số máy
            SEAT_NO: seat * 1, //Số chỗ ngồi
            SEAT_CODE: filterCarSeat(seat), //KEY VALUE SEAT khi filter
            WEIGH: payload, //Trọng tải
            WEIGH_CODE: payload ? filterCarWeight(payload) : "", //KEY VALUE WEIGH khi filter
            BRAND: "", //Hãng xe
            MODEL: "", //Hiệu xe
            MFG: "", //Năm sản xuất
            CAPACITY: "", //Dung tích
            REF_VEHICLE_VALUE: "", //Giá trị xe tham khao
            VEHICLE_REGIS: "", //Ngày đăng ký xe,

            FAX: "",
            IS_VOLUNTARY: isShowtnds ? "1" : "0",
            IS_DRIVER: isShowAccident ? "1" : "0",
            IS_CARGO: isShowtndsForGood ? "1" : "0",
            IS_COMPULSORY: "1",
            IS_VOLUNTARY_ALL:
                isShowtnds || isShowAccident || isShowtndsForGood ? "1" : "0",
            IS_PHYSICAL: "",
            FEES_DATA: 0,
            FEES: 0,
            AMOUNT: tndsfee,
            VAT: 0,
            TOTAL_DISCOUNT: 0,
            TOTAL_AMOUNT: calcTotalAmount(),
            //Trách nhiệm dân sự bắt buộc
            COMPULSORY_CIVIL: {
                PRODUCT_CODE: props.default_info.COMPULSORY_CIVIL ? props.default_info.COMPULSORY_CIVIL.PRODUCT_CODE : props.config_define.PRODUCT_BB,
                PACK_CODE: props.config_define.PACK_CODE_BB,
                IS_SERIAL_NUM: "", // cấp giấy chứng nhận bản cứng
                SERIAL_NUM: "",
                EFF: effective_date,
                TIME_EFF: effective_time,
                EXP: end_date,
                TIME_EXP: end_time,
                AMOUNT: tndsfee,
                VAT: 0,
                TOTAL_DISCOUNT: 0,
                TOTAL_AMOUNT: 0,
                FEES_DATA: 0, //them sau
                FEES: 0, //them sau
            },

            //Trách nhiệm dân sự tự nguyện
            VOLUNTARY_CIVIL: {
                PRODUCT_CODE: props.default_info.VOLUNTARY_CIVIL ? props.default_info.VOLUNTARY_CIVIL.PRODUCT_CODE : props.config_define.PRODUCT_TN,
                PACK_CODE: props.config_define.PACK_CODE_TN,
                INSUR_3RD: damage_nt3, //Thiệt hại thân thể người thứ 3
                INSUR_3RD_ASSETS: property_damage_nt3, // Thiệt hại tài sản người thứ 3
                INSUR_PASSENGER: client_damage, // Thiệt hại thân thể hành khách
                TOTAL_LIABILITY: "", //Tổng hạn mức trách nhiệm
                DRIVER_NUM: "0", //Số người TG(Bảo hiểm lái, phụ xe và người ngồi trên xe)
                DRIVER_INSUR: ins_money ? ins_money : "", //Số tiền BH (Bảo hiểm lái, phụ xe và người ngồi trên xe)
                WEIGHT_CARGO: goods, //Trọng lượng hàng hóa (Bảo hiểm TNDS của chủ xe đối với hàng hóa)
                CARGO_INSUR: responsibility_level, // Số tiền bảo hiểm hàng hóa - mức trách nhiệm (Bảo hiểm TNDS của chủ xe đối với hàng hóa),
                FEES_DATA: 0,
                FEES: 0,
                AMOUNT: voluntary_tnds_fee,
                VAT: 0,
                TOTAL_DISCOUNT: 0,
                TOTAL_AMOUNT: voluntary_tnds_fee,
            },
        };
    };

    // onChange duration
    useEffect(() => {
        if (duration && !util.isEmptyObj(listmaped)) {
            switch (duration) {
                case "1": {
                    const date = moment(effective_date, "DD/MM/YYYY")
                        .add(1, "years")
                        .format("DD/MM/YYYY");
                    listmaped["end_date"].setValue(date.toString());
                    setEnd_date(date);
                    setEnd_time(effective_time);
                    break;
                }
                case "2": {
                    const date = moment(effective_date, "DD/MM/YYYY")
                        .add(2, "years")
                        .format("DD/MM/YYYY");
                    listmaped["end_date"].setValue(date.toString());
                    setEnd_date(date);
                    setEnd_time(effective_time);
                    break;
                }
                case "3": {
                    const date = moment(effective_date, "DD/MM/YYYY")
                        .add(3, "years")
                        .format("DD/MM/YYYY");
                    listmaped["end_date"].setValue(date.toString());
                    setEnd_date(date);
                    setEnd_time(effective_time);
                    break;
                }
                default:
                    break;
            }
            getFee();
        }
    }, [duration, effective_date, effective_time]);

    const getExpDateByDurationAndEffDate = (_duration, _effDate) => {
        try {
            let resExpDate = moment().format('DD/MM/YYYY');
            switch (_duration) {
                case "1": {
                    resExpDate = moment(_effDate, "DD/MM/YYYY")
                        .add(1, "years")
                        .format("DD/MM/YYYY");
                    break;
                }
                case "2": {
                    resExpDate = moment(_effDate, "DD/MM/YYYY")
                        .add(2, "years")
                        .format("DD/MM/YYYY");
                    break;
                }
                case "3": {
                    resExpDate = moment(_effDate, "DD/MM/YYYY")
                        .add(3, "years")
                        .format("DD/MM/YYYY");
                    break;
                }
                default:
                    break;
            }
            return resExpDate;
        } catch (error) {
            console.log(error);
        }
    }
    return (
        <React.Fragment>
            {loading ? (
                <div className={styles.main_loading}>
                    <div className={styles.ff_loading}>
                        <StageSpinner
                            size={60}
                            frontColor="#329945"
                            loading={loading}
                            from={"isuser"}
                        />
                    </div>
                </div>
            ) : (
                <>
                    {isLoading ? <LoadingForm /> : null}
                    <DynamicRender
                        layout={define}
                        gender_define={props.listDefineCar.length > 0 ? props.listDefineCar[0] : []}
                        vehicle_group={vehicle_group}
                        vehicle_type={vehicle_type}
                        purpose={purpose}
                        seat={seat}
                        payload={payload}
                        newCar={newCar}
                        duration={duration}
                        effective_date={effective_date}
                        effective_time={effective_time}
                        end_date={end_date}
                        end_time={end_time}
                        tndsfee={tndsfee}
                        totalFee={totalFee}
                        plate={plate}
                        frame_number={frame_number}
                        vehicle_number={vehicle_number}
                        damage_nt3={damage_nt3}
                        property_damage_nt3={property_damage_nt3}
                        client_damage={client_damage}
                        voluntary_tnds_fee={voluntary_tnds_fee}
                        ins_money={ins_money}
                        goods={goods}
                        responsibility_level={responsibility_level}
                        gender={gender}
                        owner_name={owner_name}
                        owner_phone={owner_phone}
                        owner_email={owner_email}
                        company_name={company_name}
                        mst={mst}
                        compan_phone={compan_phone}
                        companr_email={companr_email}
                        isShowtnds={isShowtnds}
                        isShowAccident={isShowAccident}
                        isShowtndsForGood={isShowtndsForGood}
                        buyFor={init_buyforData}

                        onChangeBuyForData={(name, value) => {
                            setBuyforData(value);
                        }}
                        onInputChange={(name, val) => {
                            if (name == "vehicle_group") {
                                onChangeVehicleGroup(val);
                                listmaped["vehicle_type"].setData(filterCar(val));
                            }
                            name == "vehicle_type" && setVehicle_type(val);
                            name == "purpose" && setPurpose(val);
                            name == "seat" && setSeat(parseFloat(val));
                            name == "payload" && setPayload(val);
                            if (name == "newCar") {
                                setNewCar(val);
                                listmaped["plate"].setRequire(!val);
                                listmaped["frame_number"].setRequire(val);
                                listmaped["vehicle_number"].setRequire(val);
                            }
                            name == 'duration' && setDuration(val);
                            name == 'effective_date' && setEffective_date(val);
                            name == "effective_time" && setEffective_time(val);
                            name == "end_date" && setEnd_date(val);
                            name == "end_time" && setEnd_time(val);
                            name == "tndsfee" && setTndsfee(val);
                            name == "totalFee" && setTotalFee(val);
                            name == "plate" && setPlate(val);
                            name == "frame_number" && setFrame_number(val);
                            name == "vehicle_number" && setVehicle_number(val);
                            name == "damage_nt3" && setDamage_nt3(val);
                            name == "property_damage_nt3" && setProperty_damage_nt3(val);
                            name == "client_damage" && setClient_damage(val);
                            name == "voluntary_tnds_fee" && setVoluntary_tnds_fee(val);
                            name == "ins_money" && setIns_money(val);
                            name == "goods" && setGoods(val);
                            name == "responsibility_level" && setResponsibility_level(val);
                            name == "gender" && setGender(val);
                            name == "owner_name" && setOwner_name(val);
                            name == "owner_phone" && setOwner_phone(val);
                            name == "owner_email" && setOwner_email(val);
                            name == "company_name" && setCompany_name(val);
                            name == "mst" && setMst(val);
                            name == "compan_phone" && setCompany_phone(val);
                            name == "companr_email" && setCompany_email(val);

                            if (name == "isShowtnds") {
                                if (val) {
                                    setDamage_nt3(
                                        props.listDefineCar[7].length > 0
                                            ? props.listDefineCar[7][0].value
                                            : ""
                                    );
                                    setProperty_damage_nt3(
                                        props.listDefineCar[9].length > 0
                                            ? props.listDefineCar[9][0].value
                                            : ""
                                    );
                                    if (
                                        purpose &&
                                        vehicle_group &&
                                        config.MUC_DICH_USE.includes(purpose) &&
                                        config.THIET_HAI_THAN_THE_HK.includes(vehicle_group)
                                    ) {
                                        setClient_damage(
                                            props.listDefineCar[8].length > 0
                                                ? props.listDefineCar[8][0].value
                                                : ""
                                        );
                                    } else {
                                        setClient_damage("");
                                    }
                                    // setClient_damage(
                                    //   props.listDefineCar[8].length > 0
                                    //     ? props.listDefineCar[8][0].value
                                    //     : ""
                                    // );
                                } else {
                                    setDamage_nt3("");
                                    setProperty_damage_nt3("");
                                    setClient_damage("");
                                }
                                setIsShowtnds(val);
                                listmaped["row_isShowtnds"].toggle(!val);
                            }
                            if (name == "isShowAccident") {
                                if (val) {
                                    setIns_money(
                                        props.listDefineCar[11].length > 0
                                            ? props.listDefineCar[11][0].value
                                            : ""
                                    );
                                } else {
                                    setIns_money("");
                                }
                                setIsShowAccident(val);
                                listmaped["row_isShowAccident"].toggle(!val);
                            }
                            if (name == "isShowtndsForGood") {
                                if (val) {
                                    setResponsibility_level(
                                        props.listDefineCar[12].length > 0
                                            ? props.listDefineCar[12][0].value
                                            : ""
                                    );
                                } else {
                                    setResponsibility_level("");
                                }
                                setGoods("");
                                setIsShowtndsForGood(val);

                                listmaped["row_isShowtndsForGood"].toggle(!val);
                            }
                        }}
                        onChangeBuyFor={(name, val) => {
                            setBuyFor(val);
                            switch (val) {
                                // case "CN":
                                //   listmaped["row_nk"].toggle(true);
                                //   listmaped["row_dn"].toggle(true);
                                //   listmaped["row_cn"].toggle(true);
                                //   break;
                                // case "NK":
                                //   listmaped["row_nk"].toggle(false);
                                //   listmaped["row_dn"].toggle(true);
                                //   listmaped["row_cn"].toggle(true);
                                //   break;
                                // case "DN":
                                //   listmaped["row_cn"].toggle(true);
                                //   listmaped["row_nk"].toggle(true);
                                //   listmaped["row_dn"].toggle(false);
                                //   break;
                                case "CN":
                                    listmaped["row_nk"].toggle(false);
                                    listmaped["row_dn"].toggle(true);
                                    break;
                                case "DN":
                                    listmaped["row_nk"].toggle(true);
                                    listmaped["row_dn"].toggle(false);
                                    break;
                            }
                        }}
                    />
                </>
            )}
        </React.Fragment>
    );
});

const mapStateToProps = (state) => {
    return {
        buyerInfor: state.buyerInfor,
        listInsurer: state.listInsurer,
    };
};

const mapDispatchToProps = (dispatch) => ({
    setListInsurer: (obj) => dispatch(setListInsurer(obj)),
});

export default connect(mapStateToProps, mapDispatchToProps, null, {
    forwardRef: true,
})(Isuer);
