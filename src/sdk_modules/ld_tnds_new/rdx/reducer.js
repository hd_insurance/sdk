const SET_DATA_INPUT = "SET_DATA_INPUT"

export default function reducer(state = {}, action = {}) {
  switch (action.type) {
    case SET_DATA_INPUT: {
      return { ...state, ...action.data };
    }
    default:
      return state;
  }
}
