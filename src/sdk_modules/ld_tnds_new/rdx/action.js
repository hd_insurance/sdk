const SET_BUYER_INFO = "SET_BUYER_INFO"
const SET_INSURER_INFO = "SET_INSURER_INFO"

export const setBuyerInfo = (data) => ({
   type: SET_BUYER_INFO,
   data: data
})

export const setListInsurer = (data) => ({
   type: SET_INSURER_INFO,
   data: data
})
