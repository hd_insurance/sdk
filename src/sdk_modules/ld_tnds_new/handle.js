import util from "../../util";
import React, { useEffect, useState, createRef, useRef } from "react";


const initListIssured = (_parse_vehicle_insur)=>{
	var listNewUser = [];
	var fistId = null
    _parse_vehicle_insur.map((item, index) => {
      const id = util.randomID();
      const default_info = {
        id: id,
        ref: createRef(),
        PAID: true,
        ...item,
      };
      if (index == 0) {
        fistId = id;
      }
      listNewUser.push(default_info);
    });

    return {listNewUser, fistId}
            
}


const previewGCN = async (curentPrev, setLoadingForm, cogoToast, api, formatNumber, moment, arrBH_NTX = []) => {
    // console.log("previewGCN ", curentPrev);
    let driver_insr = curentPrev.VOLUNTARY_CIVIL.DRIVER_INSUR;
    if(arrBH_NTX.length>0){
      arrBH_NTX.forEach((item) => {
        if(item.value === driver_insr){
          driver_insr = item.TYPE_NAME
        } 
      })
    }
    try {
      const data = {
        PRODUCT_CODE: "XCG_TNDSBB_NEW",
        TEMP_CODE: "XCG_TNDSBB_VIEW",
        PACK_CODE: "TNDSBB",
        DATA_TYPE: "JSON",
        JSON_DATA: {
          NAME: curentPrev.NAME,
          ADDRESS: curentPrev.ADDRESS_LABEL,
          PHONE: curentPrev.PHONE,
          EMAIL: curentPrev.EMAIL,
          CERTIFICATE_NO: "", // k co
          DOB: curentPrev.DOB,
          IDCARD: curentPrev.IDCARD,
          PRINCIPAL: "", // k co
          EFFECTIVE_DATE: curentPrev.COMPULSORY_CIVIL.EFF,
          EXPIRATION_DATE: curentPrev.COMPULSORY_CIVIL.EXP,
          PRODUCT_CODE: "XCG_TNDSBB_NEW",
          AMOUNT: formatNumber(curentPrev.AMOUNT),
          TOTAL_AMOUNT: formatNumber(curentPrev.TOTAL_AMOUNT),
          MONEY: "", //  k co
          NAME_CTY: "", // k co
          ADDRESS_CTY: "", // k co
          REGION: "", // k co
          BOI_THUONG: "",
          NGAY_CAP: moment().format("DD/MM/YYYY"),
          EFFECTIVE_DATE_EN: "", // k co
          EXPIRATION_DATE_EN: "", // k co
          REGION_VI: "",
          REGION_EN: "",
          DOI_TUONG: "",
          HH_TO: curentPrev.COMPULSORY_CIVIL.TIME_EFF.split(":")[0],
          SS_TO: curentPrev.COMPULSORY_CIVIL.TIME_EFF.split(":")[1],
          DAY_TO: "",
          MM_TO: "",
          YY_TO: "",
          HH_F: curentPrev.COMPULSORY_CIVIL.TIME_EXP.split(":")[0],
          SS_F: curentPrev.COMPULSORY_CIVIL.TIME_EXP.split(":")[1],
          DAY_F: "",
          MM_F: "",
          YY_F: "",
          NUMBER_PLATE: curentPrev.NUMBER_PLATE,
          CHASSIS_NO: curentPrev.CHASSIS_NO,
          ENGINE_NO: curentPrev.ENGINE_NO,
          MFG: "",
          VEHICLE_TYPE: curentPrev.VEHICLE_TYPE_LABEL,
          SEAT_NO: curentPrev.SEAT_NO,
          WEIGH: curentPrev.WEIGH,
          VEHICLE_USE: curentPrev.VEHICLE_USE_LABEL,
          INSUR_3RD: "",
          INSUR_PASSENGER: "",
          INSUR_3RD_ASSETS: "",
          TOTAL_LIABILITY: "",
          DRIVER_NUM: "",
          DRIVER_INSUR: formatNumber(driver_insr),
          VEHICLE_GROUP: "",
          AMOUNT_VOLUNTARY_CIVIL: "",
          TOTAL_VOLUNTARY_CIVIL: formatNumber(
            curentPrev.VOLUNTARY_CIVIL.TOTAL_AMOUNT
          ),
        },
      };

      setLoadingForm(true);
      const response = await api.post("/api/car/preview-gcn", data);
      if (response.Success) {
        setLoadingForm(false);
        const data = response.Data.FILE_BASE64;
        const pdfWindow = window.open("");
        pdfWindow.document.write(
          "<iframe frameborder='0' style='border:0; top:0px; left:0px; bottom:0px; right:0px; position: absolute;' width='100%' height='100%' src='data:application/pdf;base64, " +
            encodeURI(data) +
            "'></iframe>"
        );
        // console.log(response);
      } else {
        if (response.Error) {
          setLoadingForm(false);
          cogoToast.error(response.ErrorMessage);
        } else {
          setLoadingForm(false);
          cogoToast.error("Có lỗi xảy ra, vui lòng thử lại!");
        }
      }
    } catch (error) {
      console.log(error);
      setLoadingForm(false);
      cogoToast.error("Có lỗi xảy ra, vui lòng thử lại!");
    }
  };





const createOrder = async (listIsuer, buyer_info, paymentMethod, config_define, ord_sku, api, cogoToast, setLoadingForm) => {
    return new Promise(async (resolve, reject)=>{
        let arrCar = [];
        let submitArr = JSON.parse(JSON.stringify(listIsuer));
        submitArr.map((item_car, i) => {
          delete item_car.ref;
          delete item_car.id;
          delete item_car.PAID;
          delete item_car.VEHICLE_TYPE_LABEL;
          delete item_car.VEHICLE_USE_LABEL;
          delete item_car.ADDRESS_LABEL;
          delete item_car.DISABLE;
          arrCar.push(item_car);
        });
        let BILL_INFO = null;
        if (buyer_info.vat != "k") {
          BILL_INFO = {
            TYPE: buyer_info.vat == "cn" ? "CN" : "CQ",
            NAME:
              buyer_info.vat == "cn" ? buyer_info.NAME : buyer_info.company_name,
            DOB: buyer_info.vat == "cn" ? buyer_info.DOB : "",
            GENDER: buyer_info.vat == "cn" ? buyer_info.GENDER : "",
            PROV:
              buyer_info.vat == "cn"
                ? buyer_info.addressCode.prov
                : buyer_info.addressCode_dn.prov,
            DIST:
              buyer_info.vat == "cn"
                ? buyer_info.addressCode.dist
                : buyer_info.addressCode_dn.dist,
            WARDS:
              buyer_info.vat == "cn"
                ? buyer_info.addressCode.ward
                : buyer_info.addressCode_dn.ward,
            ADDRESS:
              buyer_info.vat == "cn" ? buyer_info.ADDRESS : buyer_info.address_dn,
            IDCARD: buyer_info.vat == "cn" ? buyer_info.IDCARD : "",
            EMAIL: buyer_info.EMAIL,
            PHONE: buyer_info.vat == "cn" ? buyer_info.PHONE : "",
            FAX: "",
            TAXCODE: buyer_info.vat == "dn" ? buyer_info.mst : "",
          };
        }

        const data = {
          ORG_CODE: config_define.ORG_CODE_CREATE,
          CHANNEL: config_define.CHANNEL,
          USERNAME: config_define.USERNAME,
          ACTION: config_define.ACTION,

          VEHICLE_INSUR: arrCar, //arrCar
          BUYER: createBuyer(buyer_info),
          BILL_INFO: BILL_INFO,
          PAY_INFO: {
            PAYMENT_TYPE: paymentMethod,
          },
          ORD_SKU:{
             OSKU_CODE: ord_sku.OSKU_CODE,
             DETAIL_CODE: ord_sku.DETAIL_CODE,
             MAR_CODE: ord_sku.MAR_CODE
          }
        };

        console.log('data =====>', data);
       
        try {
          setLoadingForm(true);
          const response = await api.post("/api/car/create-order", data);
          
          if (response.Success) {
            setLoadingForm(false);
            // console.log(response);
            return resolve(response.Data)
          } else {
            if (response.Error) {
              setLoadingForm(false);
              cogoToast.error(response.ErrorMessage);
              return resolve(false)
            } else {
              setLoadingForm(false);
              cogoToast.error("Có lỗi xảy ra, vui lòng thử lại");
              return resolve(false)
            }
          }
        } catch (error) {
          console.log(error);
          setLoadingForm(false);
          cogoToast.error("Có lỗi xảy ra, vui lòng thử lại!");
          return resolve(false)
        }
    })
    
  };


const createBuyer = (buyer_info) => {
    // console.log("buyer_info = ", buyer_info)
  return {
      TYPE: "CN", // mac dinh
      NAME: buyer_info?.NAME, // bat buoc
      DOB: buyer_info?.DOB,
      GENDER: buyer_info?.GENDER,
      ADDRESS: buyer_info?.ADDRESS,
      IDCARD: buyer_info?.IDCARD,
      EMAIL: buyer_info?.EMAIL,
      PHONE: buyer_info?.PHONE,
      PROV: buyer_info?.addressCode?.prov,
      DIST: buyer_info?.addressCode?.dist,
      WARDS: buyer_info?.addressCode?.ward,
      IDCARD_D: "",
      IDCARD_P: "",
      FAX: "",
      TAXCODE: buyer_info?.mst,
  };
};

export default {
  initListIssured,
  previewGCN,
  createBuyer,
  createOrder
};