import React, { useEffect, useState, createRef, useRef, forwardRef } from "react";
import api from "../../services/Network.js";
import LoadingForm from "../../common/loadingpage";
import VNAT from "./vnat"




const Main = forwardRef((props, ref) => {
  const [loading, setLoading] = useState(true);
  const [layout, setLayout] = useState(null);
  useEffect(() => {
    loadLayout("615c0b68242c430775e52b35") 
     
  },[]);

  const loadLayout = async (layout_id)=>{
    try{
      const response = await api.get(`/api/layout/${layout_id}`)
      if(response.data.layout_component){
        setLayout(response.data.layout_component)
      }
    }catch(e){
      console.error(e)
    }
  }


  return (
   layout?<VNAT {...props} layout={layout}/>:<div className="center-loading-page"><LoadingForm/></div>

  );
})


export default Main;
