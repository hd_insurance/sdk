import React, { useEffect, useState, createRef, useRef, forwardRef } from "react";
import { animateScroll } from "react-scroll";
import DynamicRender from "../../../common/render/DnmRender";
import { confirmAlert } from "react-confirm-alert";
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css
import api from "../../../services/Network.js";
import moment, { now } from "moment";
import cogoToast from "cogo-toast";
import currencyFormatter from "currency-formatter";
import util from "../../../util";
import n2w from "../../../util/n2w";
import Footer from "../../../common/footer/footer";
import {LoadingComponent} from "hdi-uikit";

var _ = require("lodash");

const Main = forwardRef((props, ref) => {
  const footerRef = useRef();

  const mm_date = moment()
  const mm_last_date = moment().subtract(6, 'months')
  const current_date = {
    year: mm_date.year(),
    month: mm_date.month()+1,
    day: mm_date.date()
  };
  const last_date_effect = {
    year: mm_last_date.year(),
    month: mm_last_date.month()+1,
    day: mm_last_date.date()
  };
  
  const current_product_name = props.current_product_name;
  const current_product = props.current_product;
  const [currentLang, setCurrentLang] = useState(util.getLanguage()=="vi"?"VN":"EN");
  const [loading, setLoading] = useState(true);
  const [loadingSubmit, setLoadinSubmit] = useState(false);
  const [step, setStep] = useState(0);
  const [isDisableFooter, setDisableFooter] = useState(false);
  const [define, setDefine] = useState({});
  const [listmaped, setMaped] = useState({});
  const [showClaimModal, setShowClaimModal] = useState(true);
  const [current_confirm_accordinate, setCurrentConfirmAcc] = useState("0");

  //define

  const [internal_lang, setInternalLang] = useState([]);


  const [claim_for, setClaimFor] = useState("TTCN");
  const [damage_for, setDamageFor] = useState("TAI_NAN");

  const [ben1, setBen1] = useState(false);
  const [ben2, setBen2] = useState(false);
  const [ben2_tainan, setBen2TN] = useState(false);
  const [ben3, setBen3] = useState(false);


  //form 1
  const [gcn_no, setGCNNo] = useState("");
  const [contact_phone, setContactPhone] = useState("");
  const [contact_email, setContactEmail] = useState("");
  const [issuer_name, setIssuerName] = useState("");
  const [issuer_passport, setIssuerPassport] = useState("");
  const [issuser_beneficiary, setIssuerBene] = useState({});
  const [issuser_bank, setIssuerBank] = useState({});

  //form 2
  
  const [accident_description, setAccidentDescription] = useState("");
  const [accident_date, setAccidentDate] = useState("");
  const [accident_time, setAccidentTime] = useState("");
  const [accident_address, setAccidentAddress] = useState("");
  const [accident_addcode, setAccidentAddCode] = useState({label: "", prov: "", dist: "", ward: ""});

  const [accident_hauqua, setAccidentHauqua] = useState("");

  //form 2 tai nan y te
  const [accident_conse, setAccidentConse] = useState(""); //hau qua tai nan medical
  const [medical_in, setMedicalIn] = useState("");
  const [medical_out, setMedicalOut] = useState("");
  const [medical_addcode, setMedicalAddCode] = useState({label: "", prov: "", dist: "", ward: ""});
  const [medical_address, setMedicalAddress] = useState("");
  //form 2 kham chua benh
  const [diagnostic_conse, setDiagnosticConse] = useState(""); //chuan doan benh 
  const [healthcare_medical_in, setHealthcareMedicalIn] = useState("");
  const [healthcare_medical_out, setHealthcareMedicalOut] = useState("");
  const [healthcare_addcode, setHealthcareMedicalAddCode] = useState({label: "", prov: "", dist: "", ward: ""});
  const [healthcare_address, setHealthcareMedicalAddress] = useState("");

  const [dead_date, setDeadDate] = useState("");
  const [dead_time, setDeadTime] = useState("");

  const [dead_date_tainan, setDeadDateTN] = useState("");
  const [dead_time_tainan, setDeadTimeTN] = useState("");


  const [is_eme_transport, setIsEmeTransport] = useState(false);

  const [total_claim_amount, setTotalClaimAmount] = useState(null);
  const [total_claim_amount_text, setTotalClaimAmountText] = useState("");

  //form 3
  const [layoutUpload, setLayoutUpload] = useState([]);
  const [file_attach, setFileAttach] = useState([]);

  //form 4
  const [is_commit, setIsCommit] = useState(false);


  const [state, setState] = useState({
    view_col_ndbh_name:{hide: claim_for=="GCN"},
    view_col_gnc_no:{hide: claim_for=="TTCN"},
    view_col_passport_hc:{hide: claim_for=="GCN"},
    view_row_tainan_yte: {hide: true},
    view_row_tainan_tuvong: {hide: true},
    view_row_tainan_tuvong: {hide: true},
    view_khambenh_detail: {hide: true},
    view_row_is_tuvong: {hide: damage_for=="TAI_NAN"},
    view_row_is_tainantuvong: {hide: damage_for=="KHAM_BENH"},
    view_row_tuvong:{hide: true},
    view_dead_date_tainan: {hide: true},
    view_dead_time_tainan: {hide: true}
  });
  
  useEffect(() => {


      if(props.layout.internal_language){
        setInternalLang(props.layout.internal_language)
      }
      const { obj_config, map } = util.rootObjectComponent(props.layout, state);

     
      

      setTimeout(
        () => setMaped({...map}), 
        1000
      );



     


      setDefine({...obj_config});
       getTermPrivacy()

      if(!util.isEmptyObj(listmaped)){

       
      }
      
      setTimeout(() => {
        
        setLoading(false);
      }, 200);

  }, []);

  useEffect(() => {
    if (!util.isEmptyObj(listmaped)) {     
      defaultData()  
      eventHandle()
      setClaimFor("TTCN")
      setDamageFor("TAI_NAN")
      listmaped["accident_date"].define.minimumDate = last_date_effect
      listmaped["accident_date"].define.maximumDate = current_date
      listmaped["medical_in"].define.minimumDate = last_date_effect
      listmaped["medical_in"].define.maximumDate = current_date
      listmaped["medical_out"].define.minimumDate = last_date_effect
      listmaped["medical_out"].define.maximumDate = current_date
      listmaped["healthcare_medical_in"].define.minimumDate = last_date_effect
      listmaped["healthcare_medical_in"].define.maximumDate = current_date
      listmaped["healthcare_medical_out"].define.minimumDate = last_date_effect
      listmaped["healthcare_medical_out"].define.maximumDate = current_date
      //tu vong
      listmaped["ipn_dead_date_tainan"].define.maximumDate = current_date
      listmaped["dead_date"].define.maximumDate = current_date

      listmaped["button_1"].onClick = () => {
         window.open('https://hdinsurance.com.vn');
      };

      listmaped["button_2"].onClick = () => {
        window.open('https://hdinsurance.com.vn');
      };
    }
  },[listmaped]);


  


  useEffect(() => {
    if (!util.isEmptyObj(listmaped)) { 
      // console.log("change ben 1", ben1)
      
    }
  },[ben1]);


  useEffect(() => {
    if(total_claim_amount){
      var text = n2w.to_vietnamese(total_claim_amount)
      text = text.charAt(0).toUpperCase() + text.slice(1);
      setTotalClaimAmountText(text+ ' đồng')
    }else{
      setTotalClaimAmountText('')
    }
     
  },[total_claim_amount]);


  const defaultData = ()=>{
      
  }

  const eventHandle = ()=>{

  }



  const getListDocument =  (listbenefit)=>{
    return new Promise(async (resolved, rejected)=>{
      try {
        const response = await api.post(`/api/eclaim/list-document/${current_product}`,{ben: listbenefit});
        if (response.data) {
          const cars = response.data[0];
          var grouped = _.mapValues(_.groupBy(cars, "GROUP_ID"), (clist) =>
            clist.map((car) => _.omit(car, "GROUP_ID"))
          );

         
          var layout_docs = []
          for (const [key, value] of Object.entries(grouped)) {
            let ite = {
              "group_name": value[0].GROUP_NAME,
              "GROUP_ID": key,
              "files":[]
            }
            value.forEach((frame, index) => {
              ite.files.push({
                "id": index+"-"+key.toString(),
                "label": frame.FILE_NAME,
                "key": frame.FILE_KEY,
                "description": frame.GROUP_DESC,
                "required": true,
                "max": 10,
                "list":[]
              });
            });
            layout_docs.push(ite);
          }
        }
        setLayoutUpload(layout_docs);
        setLoadinSubmit(false)
        return resolved()
    }catch(e){
      setLoadinSubmit(false)
      return resolved()
    }
    })
     

  }


   const getTermPrivacy = ()=>{
    return new Promise(async (resolved, rejected)=>{
      try {
        const response = await api.get(`/api/eclaim/product-list/define-key/CLAIM_CONFIRM`);
       
        const result = response.data[0].filter(term => {
          return (term.TYPE_CODE == current_product) && (term.LANGUAGE == currentLang.toUpperCase())
        });
        if(result[0]){
          setState((prevState) => {
            prevState.view_cb_confirm = {label: result[0].DESCRIPTION}
            return prevState
          })
        }
        
      }catch(e){
      }
    })
  }



  const showArlet = (title, value)=>{
     confirmAlert({
      title: title,
      message: value,
      buttons: [
        {
          label: 'Close',
          onClick: () => {}
        }
      ]
    });
  }
   const checkOrderInfo = ()=>{
    return new Promise(async (resolved, rejected)=>{
      try {
        const response = await api.post(`/api/eclaim/get-order-info/${props.current_category}/${current_product}?claim_for=${claim_for}`, {
          CERTIFICATE_NO: claim_for == "GCN"?gcn_no:"",
          ID_CARD: claim_for == "TTCN"?issuer_passport:"",
          NAME: claim_for == "TTCN"?issuer_name:""
        });
        if(response.data[0][0].IS_EXISTS == 1){
          return resolved(true)
        }else{
          return resolved(false)
        }
        
      }catch(e){
        console.log("co loi xay ra ",e)
        return resolved(false)
      }
    })
     

  }



  const onBen1Change = (value)=>{ //Co dieu tri y te


      if(value){
        setState((prevState) => {
          prevState.view_row_tainan_yte = {hide: false}
          return prevState
        })
      }else{
        setState((prevState) => {
          prevState.view_row_tainan_yte = {hide: true}
          return prevState
        })

      }

      setBen1(value)
  }

  const onBen2Change = (value, key)=>{ //Tu vong

        if(damage_for=="TAI_NAN"){
           if(value){
            setState((prevState) => {
                prevState.view_row_tainan_tuvong = {hide: false}
                prevState.view_row_tuvong = {hide: true}
              return prevState
            })
          }else{
            setState((prevState) => {
              prevState.view_row_tainan_tuvong = {hide: true}
              prevState.view_row_tuvong = {hide: true}
            return prevState
            })
          }
           
         }else{
          if(value){
           setState((prevState) => {
            prevState.view_row_tuvong = {hide: false}
            prevState.view_row_tainan_tuvong = {hide: true}
            return prevState
          })
         }else{
          setState((prevState) => {
            prevState.view_row_tuvong = {hide: true}
            prevState.view_row_tainan_tuvong = {hide: true}
            return prevState
          })
         }
         }
       
      setBen2(value)
  }

  const onHauquaTNChange = (value)=>{
    
    if(value == "tuvong"){
        setState((prevState) => {
          prevState.view_dead_date_tainan = {hide: false}
          prevState.view_dead_time_tainan = {hide: false}
          return prevState
        })
    }else{
       setState((prevState) => {
          prevState.view_dead_date_tainan = {hide: true}
          prevState.view_dead_time_tainan = {hide: true}
          return prevState
       })
    }
    setAccidentHauqua(value)
  }

  const onClaimForChange = (value)=>{
      if(value == "TTCN"){
        setState((prevState) => {
          prevState.view_col_ndbh_name = {hide: false}
          prevState.view_col_gnc_no = {hide: true}
          prevState.view_col_passport_hc = {hide: false}
          return prevState
        })
      }else{
        setState((prevState) => {
          prevState.view_col_ndbh_name = {hide: true}
          prevState.view_col_gnc_no = {hide: false}
          prevState.view_col_passport_hc = {hide: true}
          return prevState
        })
      }
      setClaimFor(value)
  }
  const onDamageForChange = (value)=>{

      setAccidentHauqua("")
      setBen2(false)
      setBen1(false)
      setState((prevState) => {
        prevState.view_row_tainan_tuvong = {hide: true}
        prevState.view_row_tuvong = {hide: true}
      return prevState
      })

      if(value == "KHAM_BENH"){
        setState((prevState) => {
          prevState.view_row_tainan = {hide: true}
          prevState.view_row_tainan_yte = {hide: true}
          prevState.view_khambenh_detail = {hide: false}
          prevState.view_row_tainan_yte_cb = {hide: true}
          prevState.view_row_is_tuvong = {hide: false}
          prevState.view_row_is_tainantuvong = {hide: true}
          
          return prevState
        })

      }else{
        setState((prevState) => {
          prevState.view_row_tainan = {hide: false}
          prevState.view_row_tainan_yte = {hide: true}
          prevState.view_khambenh_detail = {hide: true}
          prevState.view_row_tainan_yte_cb = {hide: false}
          prevState.view_row_is_tuvong = {hide: true}
          prevState.view_row_is_tainantuvong = {hide: false}
          return prevState
        })
      }
      setDamageFor(value)
  }


  const setStep4Value = ()=>{
    footerRef.current.setDisable(true);
    setIsCommit(false)
    setState((_state) => {

        if(issuser_beneficiary.bene_type != "INSURER"){
          _state["view_txt_nguoithuhuong"] =  {value: `${issuser_beneficiary.gender=="ONG"?internal_lang.key_gender_m:internal_lang.key_gender_f} ${issuser_beneficiary.name?issuser_beneficiary.name.toUpperCase():""}`}
        }else{
          _state["view_txt_nguoithuhuong"] =  {value: `(${issuser_beneficiary.bene_label})`}
        }
          _state["view_txt_bene_type"] = {value: issuser_beneficiary.bene_label}
          
          _state["view_txt_bene_name"] = {value: issuser_beneficiary.name}
          _state["view_txt_bene_passport"] = {value: issuser_beneficiary.passport} 
          _state["view_txt_bene_phone"] = {value: issuser_beneficiary.phone}
          _state["view_bene_bank_name"] = {value: issuser_bank.bank_name}
          _state["view_txt_bene_bank_number"] = {value: issuser_bank.bank_number}
          _state["view_txt_bene_bank_account"] = {value: issuser_bank.bank_account.toUpperCase()}
          _state["view_txt_insuer_email"] = {value: contact_email}
          _state["view_txt_insuer_phone"] = {value: contact_phone}
          _state["view_txt_insurance_name"] = {value: current_product_name}
          if(issuser_beneficiary.bene_type == "INSURER"){
            _state["view_div_bene_passport"] = {hide: true}
            _state["view_div_bene_phone"] = {hide: true}
            _state["view_div_beneciary_name"] = {hide: true}
          }else{
            _state["view_div_bene_passport"] = {hide: false}
            _state["view_div_bene_phone"] = {hide: false}
            _state["view_div_beneciary_name"] = {hide: false}
          }

          if(issuser_bank.bank_type == "ND"){
            _state["view_div_swiffcode"] = {hide: true}
          }else{
            _state["view_div_swiffcode"] = {hide: false, value: issuser_bank.bank_swiffcode?issuser_bank.bank_swiffcode:"--"}
          }


          if(claim_for == "TTCN"){ //Claim theo thong tin ca nhan
            _state["view_txt_issuer_note"] = {value: `(${issuer_name})`}
            _state["view_txt_insuer_name"] = {value: issuer_name}
            _state["view_txt_insuer_passport"] = {value: issuer_passport} 
            _state["view_div_gnc_number"] = {hide: true}
            _state["view_div_issuer_name"] = {hide: false}
            _state["view_div_issuer_passport_number"] = {hide: false}
           
          }else{ //Claim theo thong so gcn

            _state["view_txt_issuer_note"] = {value: ``}
            _state["view_txt_gnc_no"] = {value: gcn_no}
            _state["view_div_gnc_number"] = {hide: false}
            _state["view_div_issuer_name"] = {hide: true}
            _state["view_div_issuer_passport_number"] = {hide: true}
            
          }

          _state["view_txt_chiphivc"] = {value: is_eme_transport?internal_lang.key_yes:internal_lang.key_no}
        if(damage_for == "TAI_NAN"){
            //txt_tinhtpkham
            listmaped["txt_damage_for"].setValue(internal_lang.key_txt_tainan)
            listmaped["txt_tencsyte"].setValue(medical_address)
            listmaped["txt_tinhtpkham"].setValue(medical_addcode.label)
            
            _state["view_txt_accident_address"] = {value: `${accident_address} (${accident_addcode.label})`}
            _state["view_txt_tinhtpkham"] = {value: medical_addcode.label}
            _state["view_txt_tencsyte"] = {value: medical_address}
            _state["view_div_motatonthat"] = {hide: false}
            _state["view_div_time_tainan"] = {hide: false}
            _state["view_div_address_tainan"] = {hide: false}
            _state["view_div_chuandoanbenh"] = {hide: true}
            if(ben1){
                _state["view_div_hauquatainan"] = {hide: false}
                _state["view_div_ngayxuatnhapvien"] = {hide: false}
                _state["view_div_tinhtp_kham"] = {hide: false}
                _state["view_div_tencsyte"] = {hide: false}
                _state["view_txt_med_in"] = {value: medical_in}
                _state["view_txt_med_out"] = {value: medical_out}
            }else{
                _state["view_div_hauquatainan"] = {hide: true}
                _state["view_div_ngayxuatnhapvien"] = {hide: true}
                _state["view_div_tinhtp_kham"] = {hide: true}
                _state["view_div_tencsyte"] = {hide: true}
            }
            if(ben2){
              if(accident_hauqua == "ttvv"){
                _state["view_txt_date_time_tv"] = {value: internal_lang.key_hauqua}
                _state["view_txt_dead_time_tv_value"] = {value: internal_lang.key_ttvv}
                _state["view_div_ndbh_tuvong"] = {hide: true}
              }else{
                _state["view_txt_date_time_tv"] = {value: internal_lang.key_date_time_tv}
                _state["view_txt_dead_time_tv_value"] = {value: `${dead_date_tainan} - ${dead_time_tainan}`}
                _state["view_div_ndbh_tuvong"] = {hide: false}
              }
            }else{
              _state["view_div_ndbh_tuvong"] = {hide: true}
            }
          }else if(damage_for == "KHAM_BENH"){
            _state["view_div_time_tainan"] = {hide: true}
            _state["view_div_hauquatainan"] = {hide: true}
            _state["view_txt_tencsyte"] = {value: healthcare_address}
            listmaped["txt_tinhtpkham"].setValue(healthcare_addcode.label)
            _state["view_txt_tinhtpkham"] = {value: healthcare_addcode.label}
            listmaped["txt_damage_for"].setValue(internal_lang.key_txt_khambenh)
            _state["view_txt_med_in"] = {value: healthcare_medical_in}
            _state["view_txt_med_out"] = {value: healthcare_medical_out}
            _state["view_div_motatonthat"] = {hide: true}
            _state["view_div_chuandoanbenh"] = {hide: false}
            _state["view_div_ngayxuatnhapvien"] = {hide: false}
            _state["view_div_tinhtp_kham"] = {hide: false}
            _state["view_div_tencsyte"] = {hide: false}
            _state["view_div_address_tainan"] = {hide: true}
            _state["view_txt_date_time_tv"] = {value: internal_lang.key_date_time_tv}
            _state["view_txt_dead_time_tv_value"] = {value: `${dead_date} - ${dead_time}`}
            if(ben2){
              _state["view_div_ndbh_tuvong"] = {hide: false}
            }else{
              _state["view_div_ndbh_tuvong"] = {hide: true}
            }
            
          }

          if(ben2){
            _state["view_div_timetuvong"] = {hide: false}
          }else{
            _state["view_div_timetuvong"] = {hide: true}
          }
      // console.log("set step 4 value ", _state.view_div_chuandoanbenh)
      // setState(JSON.parse(JSON.stringify(_state)))
       return _state
  })



  }


  const validationData = ()=>{
    if(damage_for=="TAI_NAN"){
          
      if(ben2){ //accident_date
          if(accident_hauqua == "tuvong"){
              const dhappened = moment(`${accident_date} ${accident_time}`, 'DD/MM/YYYY HH:mm')
              const dtv = moment(`${dead_date_tainan} ${dead_time_tainan}`, 'DD/MM/YYYY HH:mm')
              if (dtv <= dhappened) {
                cogoToast.error(internal_lang.key_error_date_2,{hideAfter:2});
                return false
              }
              
          }
        }

      if(ben1){
        const d1 = moment(medical_in, 'DD/MM/YYYY')
        const d2 = moment(medical_out, 'DD/MM/YYYY')

        if (d2 >= d1) {
          return true
        } else {
           cogoToast.error(internal_lang.key_error_date_1,{hideAfter:2});
          return false
        }

      }
    }else if(damage_for=="KHAM_BENH"){
      const d1 = moment(healthcare_medical_in, 'DD/MM/YYYY')
      const d2 = moment(healthcare_medical_out, 'DD/MM/YYYY')
      if (d2 >= d1) {
          if(ben2){
            const dtv = moment(dead_date, 'DD/MM/YYYY')

            if(d1 > dtv){
              cogoToast.error(internal_lang.key_error_date_2,{hideAfter:2});
              return false
            }
          }
          return true
        } else {
           cogoToast.error(internal_lang.key_error_date_1,{hideAfter:2});
          return false
        }
       
    }
    return true;
  }

  const getBenList = ()=>{
    let listbenefit = ""
    if(damage_for == "TAI_NAN"){
      listbenefit = "BEN1" //tai nan
      if(ben1){
        listbenefit+=",BEN2" //kham chua benh tai nan
      }
      if(ben2){
        if(accident_hauqua == "tuvong"){
          listbenefit+=",BEN3" //tu vong tai nan
        }else if(accident_hauqua=="ttvv"){
          listbenefit+=",BEN6" //thuong tat vv tai nan
        }
      }
      

    }else if(damage_for == "KHAM_BENH"){
      listbenefit = "BEN5" //tai nan
      if(ben2){
        listbenefit+=",BEN7" //tu vong do dich benh
      }
    }
    if(is_eme_transport){
        listbenefit+=",BEN4" //chi phi van chuyen cap cuu
    }
    return listbenefit;
  }
  const submitClaimForm = async () => {

    setLoadinSubmit(true)
    var declare_ext = []
    
    if(damage_for == "TAI_NAN"){
        declare_ext.push({
          "KEY": "BEN1",
          "COL_KEY": "SHORT_DESC",
          "VAL": accident_description
        })
        declare_ext.push({
          "KEY": "BEN1",
          "COL_KEY": "ADDRESS",
          "VAL": accident_address
        })
        declare_ext.push({
          "KEY": "BEN1",
          "COL_KEY": "WARDS",
          "VAL": accident_addcode.ward
        })
        declare_ext.push({
          "KEY": "BEN1",
          "COL_KEY": "DISTRICT",
          "VAL": accident_addcode.dist
        })
        declare_ext.push({
          "KEY": "BEN1",
          "COL_KEY": "PROVINCE",
          "VAL": accident_addcode.prov
        })
        declare_ext.push({
          "KEY": "BEN1",
          "COL_KEY": "HAPPENED_DATE",
          "VAL": accident_date
        })
        declare_ext.push({
          "KEY": "BEN1",
          "COL_KEY": "HAPPENED_TIME",
          "VAL": accident_time
        })

        if(ben1){
          declare_ext.push({
            "KEY": "BEN2",
            "COL_KEY": "CONSEQUENCE",
            "VAL": accident_conse
          })
          declare_ext.push({
            "KEY": "BEN2",
            "COL_KEY": "HOSPITAL_DATE",
            "VAL": medical_in
          })
          declare_ext.push({
            "KEY": "BEN2",
            "COL_KEY": "DISCHARGE_DATE",
            "VAL": medical_out
          })
          declare_ext.push({
            "KEY": "BEN2",
            "COL_KEY": "HOSPITAL_PROVINCE",
            "VAL": medical_addcode.prov
          })
          declare_ext.push({
            "KEY": "BEN2",
            "COL_KEY": "HOSPITAL_NAME",
            "VAL": medical_address
          })
        }

        if(accident_hauqua == "tuvong"){

          if(ben2){
            declare_ext.push({
              "KEY": "BEN3", //KHAMBENH = BEN 3
              "COL_KEY": "DATE_OF_DEATH",
              "VAL": dead_date_tainan
            })
            declare_ext.push({
              "KEY": "BEN3",
              "COL_KEY": "TIME_OF_DEATH",
              "VAL": dead_time_tainan
            })
          }

          if(accident_hauqua=="ttvv"){
            declare_ext.push({
              "KEY": "BEN4",
              "COL_KEY": "PERMANENT_INJURY",
              "VAL": "1"
            })
          }
        }
      }else if(damage_for == "KHAM_BENH"){ //NEU LA KHAM CHUA BENH
          declare_ext.push({
            "KEY": "BEN5",
            "COL_KEY": "REASON",
            "VAL": diagnostic_conse
          })
          declare_ext.push({
            "KEY": "BEN5",
            "COL_KEY": "HOSPITAL_DATE",
            "VAL": healthcare_medical_in
          })
          declare_ext.push({
            "KEY": "BEN5",
            "COL_KEY": "DISCHARGE_DATE",
            "VAL": healthcare_medical_out
          })
          declare_ext.push({
            "KEY": "BEN5",
            "COL_KEY": "HOSPITAL_PROVINCE",
            "VAL": healthcare_addcode.prov
          })
          declare_ext.push({
            "KEY": "BEN5",
            "COL_KEY": "HOSPITAL_NAME",
            "VAL": healthcare_address
          })
          if(ben2){
            declare_ext.push({
              "KEY": "BEN7", //KHAMBENH = BEN 3
              "COL_KEY": "DATE_OF_DEATH",
              "VAL": dead_date
            })
            declare_ext.push({
              "KEY": "BEN7",
              "COL_KEY": "TIME_OF_DEATH",
              "VAL": dead_time
            })
          }
      }
        
          declare_ext.push({
            "KEY": "BEN4",
            "COL_KEY": "EMERGENCY_TRANSPORT",
            "VAL": is_eme_transport?"1":"0"
          })


    var claim_declare = {
      required_amount: total_claim_amount,
      payment_type: issuser_bank.transfer_type=="STK"?"CK":"",
      account_no: issuser_bank.bank_number||"",
      account_name: issuser_bank.bank_account,
      account_bank: issuser_bank.bank_name?issuser_bank.bank_name:"",
      branch_bank: issuser_bank.bank_branch?issuser_bank.bank_branch:"",
      swift_code: issuser_bank.bank_swiffcode?issuser_bank.bank_swiffcode:"",
      intermediate_bank: issuser_bank.isMiddle?issuser_bank.middle_bank_name:"",
      inter_swift_code: issuser_bank.isMiddle?issuser_bank.middle_swiff:"",
      bank_address: issuser_bank.bank_address?issuser_bank.bank_address:"",
      beneficiary_type: issuser_beneficiary.bene_type,
      beneficiary_gender: issuser_beneficiary.gender?issuser_beneficiary.gender:"",
      beneficiary_name: issuser_beneficiary.name?issuser_beneficiary.name:"",
      beneficiary_phone: issuser_beneficiary.phone?issuser_beneficiary.phone:"",
      beneficiary_idcard: issuser_beneficiary.passport?issuser_beneficiary.passport:"",
      beneficiary_address: issuser_bank.account_address?issuser_bank.account_address:"",
      is_commit: "1",
      declare_ext: declare_ext

    }
    var data_claim = {
        "channel": "E_CLAIM",
        "username": "WEB_HDI",
        "orgcode": "HDI",
        "product_code": current_product,
        "package_code": "",
        "detail_code": "",
        "type": "BAN_THAN",
        "certificate_no": claim_for=="GCN"?gcn_no:"",
        "claim": {
          "claim_declare": claim_declare,
          "insured_detail": {
            "phone": contact_phone,
            "email": contact_email,
            "idcard": claim_for=="TTCN"?issuer_passport:"",
            "dob": "",
            "name": claim_for=="TTCN"?issuer_name:""
          },
          "file_attach": file_attach
        }
      }
      return new Promise(async (resolved, rejected)=>{
        try{
          const response = await api.post(`/api/eclaim/submit`, data_claim);
          if(response.success){
            setLoadinSubmit(false)
            resolved(true)
          }else{
            setLoadinSubmit(false)
            resolved(false)
            showArlet('Gửi yêu cầu bồi thường thất bại',response.message?response.message:'Có lỗi xảy ra, vui lòng thử lại hoặc liên hệ bộ phận cskh để được trợ giúp')
          }
        }catch(e){
          setLoadinSubmit(false)
          showArlet('Gửi yêu cầu bồi thường thất bại', 'Có lỗi xảy ra, vui lòng thử lại hoặc liên hệ bộ phận cskh để được trợ giúp')
          resolved(false)
        }
      })
  }
  const nextStep1 = ()=>{
    setLoadinSubmit(true)
    return new Promise(async (resolved, rejected)=>{
      if(listmaped["form_step1"].ref.current.handleSubmit()){
            const check = await checkOrderInfo()
            if(!check){
              showArlet('Thông tin yêu cầu không tồn tại', 'Vui lòng kiểm tra lại thông tin và thử lại')
              setLoadinSubmit(false)
              return resolved(false)
            }else{
              setLoadinSubmit(false)
              return resolved(true)
            }
            
          }
          setLoadinSubmit(false)
       return resolved(false)
    })
    
  }
  const nextStep2 = async ()=>{
    
    if(listmaped["form_step2"].ref.current.handleSubmit()){
        setLoadinSubmit(true)
        if(damage_for=="TAI_NAN"){
          if(!ben1 && !ben2 && !is_eme_transport){
             setLoadinSubmit(false)
             cogoToast.error(internal_lang.key_msg_ben)
            return false
          }
        }
        const listBen = getBenList()
        await getListDocument(listBen)
        return validationData()
      setLoadinSubmit(false)
      return true
    }
    setLoadinSubmit(false)
    return false
  }
  const nextStep3 = ()=>{
    setLoadinSubmit(true)
    setStep4Value()
    
    
    return true
  }
  const nextStep4 = ()=>{
    return submitClaimForm()
    
  }
  const nextStep5 = ()=>{
    if(listmaped["form_step1"].ref.current.handleSubmit()){
      return true
    }
    return true
  }

  const nextStepForm = ()=>{
    const user_beneficiary = listmaped["user_beneficiary"].ref.current.getData();
    const bank_beneficiary = listmaped["bank_beneficiary"].ref.current.getData();
    setIssuerBene(user_beneficiary)
    setIssuerBank(bank_beneficiary)
    listmaped["wizard"].ref.current.setStep(step+1);
    listmaped["step_layout"].ref.current.setStep(step+1);
    setStep(step+1)
  }

  const onNextClick = async ()=>{
    switch(step){
      case 0:
        const check_valid = await nextStep1()
        if(check_valid){
          nextStepForm()
        }else{
          console.log("chua valid")
        }
      break;
      case 1:
        if(await nextStep2()){
          nextStepForm()
        }
      break;
      case 2:
        if(nextStep3()){
          setTimeout(
            () => {
              setLoadinSubmit(false)
              nextStepForm()
            }, 
            1000
          );
        }
      break;
      case 3:
        if(await nextStep4()){
          nextStepForm()
        }
      break;
      case 4:
        if(nextStep5()){
          nextStepForm()
        }
      break;
    }
   

  }
  const onPrevClick = ()=>{
    listmaped["wizard"].ref.current.setStep(step-1);
    //set step screen
    footerRef.current.setDisable(false);
    listmaped["step_layout"].ref.current.setStep(step-1);
    setStep(step-1)
  }

  const onFileUpdate = (file_attachment)=>{
    if(file_attachment){
      setFileAttach(file_attachment)
    }else{
      cogoToast.error("Upload file error!")
    }
  }
  return (
    <LoadingComponent loading={loadingSubmit}>

       {loading ? (
        <div >
          <div>
            
          </div>
        </div>
      ) : (
      <div>

        <DynamicRender
          layout={define}
          productConfig={props.product_config}
          current_product_name={current_product_name}
          current_confirm_position={current_confirm_accordinate}
          //define
          //form value
          claim_for={claim_for}
          damage_for={damage_for}
          ben1={ben1}
          ben2={ben2}
          ben3={ben3}
          gcn_no={gcn_no}
          contact_phone={contact_phone}
          contact_email={contact_email}
          issuer_name={issuer_name}
          issuer_passport={issuer_passport}
          //form 4
          isCommit={is_commit}
          onCommitmentChange={(name, value)=>{
            setIsCommit(value)
            if(value){
              footerRef.current.setDisable(false);
            }else{
              footerRef.current.setDisable(true);
            }
          }}
          //form 2
          accident_description={accident_description}
          accident_date={accident_date}
          accident_time={accident_time}
          accident_address={accident_address}
          accident_addcode={accident_addcode}
          accident_conse={accident_conse}
          medical_in={medical_in}
          medical_out={medical_out}
          medical_addcode={medical_addcode}
          medical_address={medical_address}
          accident_hauqua={accident_hauqua}
          dead_date={dead_date}
          dead_time={dead_time}
          dead_date_tainan={dead_date_tainan}
          dead_time_tainan={dead_time_tainan}
          total_claim_amount={total_claim_amount}
          total_claim_amount_text={total_claim_amount_text}
          txt_amount_required={"(Số tiền yêu cầu: "+currencyFormatter.format(total_claim_amount, {
            code: l.g("bhsk.currency"),
            precision: 0,
            format: "%v %s",
            symbol: l.g("bhsk.currency"),
          })+")"}
          is_eme_transport={is_eme_transport}
          diagnostic_conse={diagnostic_conse}
          healthcare_medical_in={healthcare_medical_in}
          healthcare_medical_out={healthcare_medical_out}
          healthcare_addcode={healthcare_addcode}
          healthcare_address={healthcare_address}
          onValueChange={(name, value)=>{
              switch(name){
                case "rd_claim_for":
                  onClaimForChange(value)
                break;
                case "rd_damage_for":
                  onDamageForChange(value)
                  break;
                case "cb_ben1":
                  onBen1Change(value)
                  break;
                case "cb_is_tuvong":
                  onBen2Change(value, "kb")
                  break;
                case "cb_is_tainantuvong":
                  onBen2Change(value, "tn")
                  break
                case "hauqua_tainan":
                  onHauquaTNChange(value)
                  break;
                default:
                break;
              }
          }}
          onConfirmTabChange={(position)=>{
            setCurrentConfirmAcc(position)
          }}
          onInputChange={(name, val)=>{
              name == "gcn_no" && setGCNNo(val);
              name == "contact_phone" && setContactPhone(val);
              name == "contact_email" && setContactEmail(val);
              name == "issuer_name" && setIssuerName(val);
              name == "issuer_passport" && setIssuerPassport(val);
              name == "accident_description" && setAccidentDescription(val);
              name == "accident_date" && setAccidentDate(val);
              name == "accident_time" && setAccidentTime(val);
              name == "accident_address" && setAccidentAddress(val);
              name == "accident_addcode" && setAccidentAddCode(val);
              name == "accident_conse" && setAccidentConse(val);
              name == "medical_in" && setMedicalIn(val);
              name == "medical_out" && setMedicalOut(val);
              name == "medical_addcode" && setMedicalAddCode(val);
              name == "medical_address" && setMedicalAddress(val);
              name == "diagnostic_conse" && setDiagnosticConse(val);
              name == "healthcare_medical_in" && setHealthcareMedicalIn(val);
              name == "healthcare_medical_out" && setHealthcareMedicalOut(val);
              name == "healthcare_addcode" && setHealthcareMedicalAddCode(val);
              name == "healthcare_address" && setHealthcareMedicalAddress(val);
              name == "dead_date" && setDeadDate(val);
              name == "dead_time" && setDeadTime(val);
              name == "is_eme_transport" && setIsEmeTransport(val);
              name == "total_claim_amount" && setTotalClaimAmount(val);
              name == "ipn_dead_date_tainan" && setDeadDateTN(val);
              name == "ipn_dead_time_tainan" && setDeadTimeTN(val);

          }}
          upload_layout={layoutUpload}
          onFileUpdate={onFileUpdate}
        />
       

          <Footer
            ref={footerRef}
            isDisable={isDisableFooter}
            step={step}
            onPrev={onPrevClick}
            onNext={onNextClick}
            lastStep={4}
          />


    </div>)}

     



    </LoadingComponent>
  );
})


export default Main;
