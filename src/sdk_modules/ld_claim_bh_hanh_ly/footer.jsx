import React, {
  useEffect,
  useState,
  createRef,
  useImperativeHandle,
  forwardRef,
} from "react";
import { Button } from "react-bootstrap";
const Footer = forwardRef((props, ref) => {
  const [disableNext, setDisableNext] = useState(props.isDisable);
  const [nextTile, setNextTitle] = useState(l.g("bhsk.form.lbl_next"));
  useImperativeHandle(ref, () => ({
    setDisable(isDisable) {
      setDisableNext(isDisable);
    },

    setNextTitle(v) {
      setNextTitle(v);
    },
  }));

  return (
    <div className="footer-form-bhsk">
      <div className="f-info">
        <p></p>
        <a></a>
      </div>

      {props.step != 4 ? (
        <Button
          className="footer-button f-button"
          onClick={props.onNext}
          disabled={disableNext}
        >
          {nextTile}
        </Button>
      ) : null}
      {props.step > 0 && props.step != 4 ? (
        <Button className="footer-button-prev j-button" onClick={props.onPrev}>
          {l.g("bhsk.form.lbl_previous")}
        </Button>
      ) : null}
    </div>
  );
});

export default Footer;
