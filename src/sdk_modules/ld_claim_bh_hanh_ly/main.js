import React, { useEffect, useState, createRef } from "react";
import cogoToast from "cogo-toast";
import router, { useRouter } from "next/router";
// import {
//   BrowserRouter as Router,
//   Link,
//   useLocation
// } from "react-router-dom";
import StageSpinner from "../../common/loadingpage";
import randomstring from "randomstring";
import { animateScroll } from "react-scroll";
import { connect } from "react-redux";
import Wizard from "../../common/topwizard";
import Footer from "../../common/footer/footer";
import { confirmAlert } from "react-confirm-alert";
import {
  fetchPackages,
  setUserList,
} from "../../redux/actions/action-bhsk";
import { Tab } from "react-bootstrap";
import F1 from "./screen/f1";
import F2 from "./screen/f2";
import F3 from "./screen/f3";
import F4 from "./screen/f4";
import F5 from "./screen/f5";
import api from "../../services/Network.js";
import styles from '../../css/style.module.css';


const Main = (props) => {
  const footerRef = createRef();
  const F1Ref = createRef();
  const F2Ref = createRef();
  const F3Ref = createRef();
  const F4Ref = createRef();
  const F5Ref = createRef();

  const wzdata = [
    {
      step: `${lng.get("u3nifi1w")} 1`,
      label: lng.get("o2bjchek"),
      icon: "fas fa-plus-circle",
    },
    {
      step: `${lng.get("u3nifi1w")} 2`,
      label: lng.get("h82oqgru"),
      icon: "fas fa-paste",
    },
    {
      step: `${lng.get("u3nifi1w")} 3`,
      label: lng.get("zbuqgo7r"),
      icon: "fas fa-file-alt",
    },
    {
      step: `${lng.get("u3nifi1w")} 4`,
      label: lng.get("0lgdflcg"),
      icon: "fas fa-hand-holding-usd",
    },
    {
      step: `${lng.get("u3nifi1w")} 5`,
      label: lng.get("2stxdrp2"),
      icon: "fas fa-check-circle",
    },
  ];

  const [step, setStep] = useState("0");
  const [disableNext, setDisableNext] = useState(false);

  const [topHead, setTopHead] = useState({
    title: "Thông tin nhân viên",
    subtitle:
      "Người thân chỉ có thể đăng ký nếu Nhân viên cũng tham gia BH Sức khỏe cho nhân viên và người thân HDBank",
  });
  const [loading, setLoading] = useState(false);
  const [isActivation, setActivation] = useState(false);

  const [emloyerInfo, setEmployerInfo] = useState({});
  const [defaultActive, setDefaultActive] = useState(null);
  const [voucherInfo, setVoucherInfo] = useState(null);
  const [org, setOrg] = useState(props.org);
  const [id, setId] = useState(null);
  const [slPack, setSlPack] = useState(null);

  const [dataF1, setDataF1] = useState(null);
  const [dataF2, setDataF2] = useState(null);
  const [dataF3, setDataF3] = useState(null);

  const [isuData, setIsuData] = useState(null);

  // const [packages, setPackages] = useState([]);

  useEffect(() => {
    if (props.outerRouter.query.prod && props.outerRouter.query.id) {
      initGetData();
    }
  }, []);

  const initGetData = async () => {
    try {
      setLoading(true);
      const response = await api.post(
        `/api/vj/get-search-insur-info/${props.outerRouter.query.prod.toUpperCase()}`,
        { a13: props.outerRouter.query.id, a14: "1" }
      );
      setLoading(false);
      if (response.data[0]) {
        setIsuData(response.data[0][0]);
      }
    } catch (e) {
      setLoading(false);
      console.log(e);
    }
  };

  const genarateDeclareExt = () =>{
    let declare_ext = [
      {
        "KEY": "BEN1",
        "COL_KEY": "HAPPENED_DATE",
        "VAL": dataF2?.infoLoss?.dateAccident
      },
      {
        "KEY": "BEN1",
        "COL_KEY": "HAPPENED_TIME",
        "VAL": dataF2?.infoLoss?.timeAccident
      },
      {
        "KEY": "BEN1",
        "COL_KEY": "ADDRESS",
        "VAL": dataF2?.infoLoss?.addressAccident
      },
      {
        "KEY": "BEN1",
        "COL_KEY": "SHORT_DESC",
        "VAL": dataF2?.infoLoss?.reason
      },
    ];
    if(dataF2.ben1){
      declare_ext.push(
          {
            "KEY": "BEN2",
            "COL_KEY": "BAGGAGE_DEMAGE",
            "VAL": "Hành lý hư hỏng"
          },
      )
    }
    if(dataF2.ben2){
      declare_ext.push(
          {
            "KEY": "BEN3",
            "COL_KEY": "ARR_DATE_R",
            "VAL": dataF2?.infoBagDelay?.dateReceived
          },
          {
            "KEY": "BEN3",
            "COL_KEY": "ARR_TIME_R",
            "VAL": dataF2?.infoBagDelay?.timeReceived
          },
          {
            "KEY": "BEN3",
            "COL_KEY": "BAGGAGE_PLACE",
            "VAL": dataF2?.infoBagDelay?.addReceived
          }
      )
    }
    return declare_ext;
  }

  const submitData = async () => {
    return new Promise(async (resolved, rejected) => {
      try {
        setLoading(true);
        const mergeData = {
          channel: "WEB",
          username: "VIETJET_VN",
          orgcode: "VIETJET_VN",
          product_code: isuData.PRODUCT_CODE,
          package_code: "",
          detail_code: props.outerRouter.query.id,
          type: dataF1.atn ? "KHAI_HO" : "BAN_THAN",
          claim: {
            claim_declare: {
              name: dataF1.atn ? dataF1.atn.atn_name.toUpperCase() : "",
              relation: dataF1.atn
                ? dataF1.atn.atn_relation.value.split("-")[0]
                : "",
              type: dataF1.atn ? "KHAI_HO" : "BAN_THAN",
              gender: dataF1.atn
                ? dataF1.atn.atn_relation.value.split("-")[1]
                : "",
              idcard: dataF1.atn ? dataF1.atn.atn_passport : "",
              idcard_d: "",
              idcard_p: "",
              email: dataF1.atn ? dataF1.atn.atn_email : "",
              phone: dataF1.atn ? dataF1.atn.atn_phonenumber : "",
              dob: dataF1.atn ? dataF1.atn.atn_dob : "",
              address: dataF1.atn ? dataF1.atn.atn_address : "",
              wards: dataF1.atn ? dataF1.atn.ant_addressCode.ward : "",
              district: dataF1.atn ? dataF1.atn.ant_addressCode.dist : "",
              province: dataF1.atn ? dataF1.atn.ant_addressCode.prov : "",
              required_amount: dataF2.bankInfo.amountClaim,
              payment_type: dataF2.bankInfo.payMethod,
              account_no: dataF2.bankInfo.bankNum,
              account_name: dataF2.bankInfo.bankAccount.toUpperCase(),
              account_bank: dataF2.bankInfo.bank.label,
              is_commit: "1",
              declare_ext: genarateDeclareExt(),
            },
            insured_detail: {
              phone: dataF1.isuer ? dataF1.isuer.isu_phonenumber : "",
              email: dataF1.isuer ? dataF1.isuer.isu_email : "",
              idcard: dataF1.isuer ? dataF1.isuer.isu_passport : "",
              dob: dataF1.isuer ? dataF1.isuer.isu_dob : "",
              change: {
                flight_no: dataF1?.infoChange?.numFlight,
                dep: dataF1?.infoChange?.departure?.label,
                arr: dataF1?.infoChange?.destination?.label,
                flight_date: dataF1?.infoChange?.dateFlight,
                flight_time: dataF1?.infoChange?.timeFlight
              }
            },
            file_attach: dataF3,
          },
        };
        const sumitResult = await api.post(
          `/api/claim/${isuData.PRODUCT_CODE}`,
          mergeData
        );
        console.log('sumitResult', sumitResult);
        setLoading(false);
        if (sumitResult.data.TYPE == "SUCCESS") {
          return resolved(true);
        } else {
          cogoToast.error(lng.get("error_claim_msg"));
          return resolved(false);
        }
      } catch (e) {
        setLoading(false);
        cogoToast.error(lng.get("error_claim_msg"));
        return resolved(false);
      }
    });
  };

  const onNextClick = async (e) => {
    animateScroll.scrollToTop();
    switch (step) {
      case "0":
        const dataf1 = F1Ref.current.handleSubmit();
        if (dataf1) {
          console.log('dataF1 >>>>>', dataf1);
          setDataF1(dataf1);
          setStep("1");
        } else {
        }

        break;
      case "1":
        const dataf2 = F2Ref.current.handleSubmit();
        if (dataf2) {
          console.log('dataF2 >>>>>', dataf2);
          if (dataf2.ben1 || dataf2.ben2) {
            setDataF2(dataf2);
            setStep("2");
          }else {
            cogoToast.error(lng.get("err_ben_null"));
          }
        } else {
        }
        break;
      case "2":
        console.log('2');
        const dataf3 = F3Ref.current.handleSubmit();
        if (dataf3) {
          footerRef.current.setDisable(true);
          setDataF3(dataf3);
          setStep("3");
        } else {
        }

        break;
      case "3":
        const result = await submitData();
        if (result) {
          setStep("4");
        }
        break;
    }
  };
  const onPrevClick = (e) => {
    footerRef.current.setDisable(false);
    switch (step) {
      case "1":
        setStep("0");
        break;
      case "2":
        setStep("1");
        break;
        break;
      case "3":
        setStep("2");
        break;
      case "4":
        setStep("3");
        break;
      case "5":
        setStep("4");
        break;
    }
  };

  const onEnableBtn = (isCheck) =>{
    footerRef.current.setDisable(isCheck);
  }

  const handleSubmit = (info) => {};

  const onCbTermChange = (xxx) => {
    footerRef.current.setDisable(xxx);
  };

  const onEditClick = (id) => {
    if (id == "idstaff") {
      setStep("0");
      footerRef.current.setDisable(false);
    } else {
      setDefaultActive(id);
      setStep("1");
      footerRef.current.setDisable(false);
    }
  };

  const backtoEdit = () => {
    const urlx =
      props.outerRouter.pathname + "?id=" + props.outerRouter.query.id;
    setActivation(false);
    props.outerRouter.push(urlx, undefined, { shallow: true });
  };

  return (
    <div className={styles.main_container}>
    {loading ? (
          <div className={styles.main_loading}>
            <div className={styles.ff_loading}>
            <StageSpinner size={60} frontColor="#329945" loading={loading} />
          </div>
        </div>
      ) : (
        <div className={`container ${styles.kklm}`}>
        <Wizard active={step} data={wzdata} />

          <Tab.Container id="xxxx" activeKey={step}>
            <Tab.Content>
              <Tab.Pane eventKey="0">
                <F1 org={org} ref={F1Ref} isuData={isuData} />
              </Tab.Pane>
              <Tab.Pane eventKey="1">
                <F2 org={org} ref={F2Ref} />
              </Tab.Pane>
              <Tab.Pane eventKey="2">
                <F3 org={org} ref={F3Ref} />
              </Tab.Pane>
              <Tab.Pane eventKey="3">
                <F4
                  org={org}
                  ref={F4Ref}
                  isuData={isuData}
                  dataF1={dataF1}
                  dataF2={dataF2}
                  onEnable={onEnableBtn}
                />
              </Tab.Pane>
              <Tab.Pane eventKey="4">
                <F5 org={org} ref={F5Ref} outerRouter={router} />
              </Tab.Pane>
            </Tab.Content>
          </Tab.Container>

          <Footer
            loading={loading}
            ref={footerRef}
            isDisable={false}
            step={step}
            onPrev={onPrevClick}
            onNext={onNextClick}
            lastStep={4}
          />
        </div>
      )}
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    packages: state.data,
    isruserlist: state.isruserlist,
  };
};

const mapDispatchToProps = (dispatch) => ({
  fetchPackages: (orgc) => dispatch(fetchPackages(orgc)),
  setUserList: (usrl) => dispatch(setUserList(usrl)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Main);
