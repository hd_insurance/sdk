import React, { useEffect, useState, createRef, forwardRef, useImperativeHandle } from "react";
import { Button, Col, Row, Form} from 'react-bootstrap';

import {FLTextArea, FLInput, FLDate, FLTime, FLAddress} from "hdi-uikit";
// import FLInput from "../../../libs/flinput";
// import FLDate from "../../../libs/flinput/date.js";
// import FLTime from "../../../libs/flinput/timepicker.jsx";
// import FLAddress from "../../../libs/flinput/address.js";
// import FLSelect from "../../../libs/flinput/listselect.js";
// import FLSelectMultiple from "../../../libs/flinput/listselectmultiple.js";
// import FLAmount from "../../../libs/flinput/amount.js";


const BaggageDelay = forwardRef((props, ref) => {

    const [dateReceived, setDateReceived] = useState(null); // Ngay nhan hanh ly
    const [timeReceived, setTimeReceived] = useState(null); // Gio nhan hanh ly
    const [addReceived, setAddReceived]= useState(null); // Dia chi nhan hanh ly

    useImperativeHandle(ref, () => ({
        getData() {
            let outdata = {
                dateReceived,
                timeReceived,
                addReceived
            }
            return outdata
        }
    }));

    return (<div>
        <Row className="declaration-form">
            <div className="col-md-3 mt-15">
                <FLDate
                    disable={false}
                    changeEvent={setDateReceived}
                    value={dateReceived}
                    label={lng.get("zaimdjsr")}
                    required={true}
                />
            </div>
            <div className="col-md-3 mt-15">
                <FLTime
                    disable={false}
                    changeEvent={setTimeReceived}
                    value={timeReceived}
                    label={lng.get("kf4qcgfu")}
                    required={true}
                />
            </div>
            <div className="col-md-6 mt-15">
                <FLInput
                    required={true}
                    changeEvent={setAddReceived}
                    value={addReceived}
                    label={lng.get("y6vqiakd")}
                />
            </div>
        </Row>
    </div>)

})


export default BaggageDelay;