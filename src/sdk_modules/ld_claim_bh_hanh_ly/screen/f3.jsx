import React, {
  useEffect,
  useState,
  useContext,
  createRef,
  forwardRef,
  useImperativeHandle,
} from "react";
import TopTitle from "../../../common/toptitle";
import cogoToast from "cogo-toast";
import { connect } from "react-redux";
import {
  fetchPackages,
  setUserList,
  checkRegistered,
} from "../../../redux/actions/action-bhsk.js";

import {
  Form,
  Accordion,
  AccordionContext,
  useAccordionToggle,
} from "react-bootstrap";
import Upload1 from "../../../common/upload/upload1";

import api from "../../../services/Network.js";
import styles from "../../../css/style.module.css";
import ReactHtmlParser from "react-html-parser";

var _ = require("lodash");
const Form3 = forwardRef((props, ref) => {
  const formRef = createRef();
  const [validated, setValidated] = useState(false);
  const [layoutConfig, setLayoutConfig] = useState([]);
  const [fileList, setFileList] = useState([]);

  useImperativeHandle(ref, () => ({
    handleSubmit() {
      return fileList;
    },
  }));

  useEffect(() => {
    initLayout();
  }, []);

  const initLayout = async () => {
    try {
      const response = await api.get(`/api/vj/listdocument/lost_baggage`);
      if (response.data) {
        const cars = response.data[0];
        var grouped = _.mapValues(_.groupBy(cars, "GROUP_ID"), (clist) =>
          clist.map((car) => _.omit(car, "GROUP_ID"))
        );
        for (const [key, value] of Object.entries(grouped)) {
          let ite = {
            title: value[0].GROUP_NAME,
            frame: [],
          };
          value.forEach((frame, index) => {
            ite.frame.push({
              title: frame.FILE_NAME,
              key: frame.FILE_KEY,
            });
          });
          layoutConfig.push(ite);
        }
      }
      setLayoutConfig([...layoutConfig]);
    } catch (e) {
      console.log(e);
    }
  };

  const onFileUpload = (info, filekey) => {
    fileList.push({
      FILE_KEY: info.key,
      FILE_NAME: `${info.key}_${filekey}`,
      FILE_ID: filekey,
    });
    setFileList([...fileList]);
  };
  const onFileDelete = (fileId) => {
    console.log(fileId, fileList);
    const index = fileList.findIndex((item) => item.FILE_ID === fileId);
    fileList.splice(index, 1);
    setFileList([...fileList]);
  };

  const ContextAwareToggle = ({ children, eventKey, callback }) => {
    const currentEventKey = useContext(AccordionContext);

    const decoratedOnClick = useAccordionToggle(
      eventKey,
      () => callback && callback(eventKey)
    );

    const isCurrentEventKey = currentEventKey === eventKey;

    return (
      <div
        className={
          isCurrentEventKey
            ? `${styles.qna_item_cl} ${styles.active}`
            : `${styles.qna_item_cl}`
        }
        onClick={decoratedOnClick}
      >
        <div className={styles.title_item_clapse}>{children}</div>

        <div className={styles.cl_item_icon_document}>
          {isCurrentEventKey ? (
            <i className="fas fa-angle-up"></i>
          ) : (
            <i className="fas fa-angle-down"></i>
          )}
        </div>
      </div>
    );
  };

  return (
    <div className={`${styles.indemnify_form} f2`}>
      <TopTitle title={lng.get("v9vpkvgg")} subtitle={""} />
      <div style={{paddingLeft: 20, paddingRight: 20}}>
        {ReactHtmlParser(lng.get("h30bxuy0"))}
      </div>
      <Form className="f-orm" ref={formRef} noValidate validated={validated}>
        <Accordion defaultActiveKey="item_0" className={styles.accor_upload}>
          {layoutConfig.map((uploadpart, index) => {
            return (
              <div className={styles.ac_upload_document} key={index}>
                <ContextAwareToggle eventKey={`item_${index}`}>
                  {lng.getTextFromCode(uploadpart.title)}
                </ContextAwareToggle>

                <Accordion.Collapse
                  eventKey={`item_${index}`}
                  className={styles.bodies}
                >
                  <div className={`${styles.row} ${styles.flex} row`}>
                    {uploadpart.frame.map((frame, index) => {
                      return (
                        <div
                          className={`col-md-3 ${styles.flex_col} ${styles.qna_content_item}`}
                        >
                          <p>{lng.getTextFromCode(frame.title)}</p>
                          <Upload1
                            onFileUpload={onFileUpload}
                            onFileDelete={onFileDelete}
                            config={frame}
                            key={index}
                          />
                        </div>
                      );
                    })}
                  </div>
                </Accordion.Collapse>
              </div>
            );
          })}
        </Accordion>
      </Form>
    </div>
  );
});

const mapStateToProps = (state) => {
  return {
    packages: state.data,
    isruserlist: state.isruserlist,
  };
};
const mapDispatchToProps = (dispatch) => ({
  setUserList: (usrl) => dispatch(setUserList(usrl)),
  checkRegistered: (isChecked) => dispatch(checkRegistered(isChecked)),
});

export default connect(mapStateToProps, mapDispatchToProps, null, {
  forwardRef: true,
})(Form3);
