import React, {
  useEffect,
  useState,
  createRef,
  forwardRef,
  useImperativeHandle,
} from "react";
import TopTitle from "../../../common/toptitle";
// import cogoToast from "cogo-toast";
import { connect } from "react-redux";
import {
  fetchPackages,
  setUserList,
  checkRegistered,
} from "../../../redux/actions/action-bhsk.js";
import { Row, Form } from "react-bootstrap";
import {FLInput, FLBankList, FLSelect, FLAmount, FLDate, FLTime} from "hdi-uikit";

import BaggageDelay from "../benefit/cham_hanh_ly";

import styles from "../../../css/style.module.css";

const Form2 = forwardRef((props, ref) => {
  const formRef = createRef();
  const form1Ref = createRef();
  const form2Ref = createRef();

  const [dateAccident, setDateAccident] = useState(null); //Ngày xảy ra
  const [timeAccident, setTimeAccident] = useState(null); // Giờ xảy ra
  const [addressAccident, setAddressAccident] = useState(null); // Địa điểm xảy ra tổn thất
  const [reason, setReason] = useState(null); // Mô tả ngắn gọn về sự cố

  const [validated, setValidated] = useState(false);
  const [ben1, setBen1] = useState(true);
  const [ben2, setBen2] = useState(false);

  const [amountClaim, setAmoutClaim] = useState(null);
  const [payMethod, setPaymethod] = useState("CK");
  const [bankNum, setBankNum] = useState();
  const [bank, setBank] = useState({ label: null, id: null });
  const [bankAccount, setBankAccount] = useState(null);


  useImperativeHandle(ref, () => ({
    handleSubmit() {
      const form = formRef.current;
      if (form.checkValidity() === false) {
        setValidated(true);
        return false;
      }
      let infoBagDelay = {};
      // if (ben1) {
      //   dataTainan = form1Ref.current.getData();
      // }
      let infoLoss = {dateAccident, timeAccident, addressAccident, reason};
      if (ben2) {
        infoBagDelay = form2Ref.current.getData();
      }

      const bankInfo = {
        bank,
        bankAccount,
        bankNum,
        payMethod,
        amountClaim,
      };
      return {
        infoLoss,
        ben1,
        ben2,
        infoBagDelay,
        bankInfo,
      };
    },
  }));

  const cbModeChange = (e) => {
    if (e.target.name == "BEN1") {
      setBen1(e.target.checked);
    } else if (e.target.name == "BEN2") {
      setBen2(e.target.checked);
    }
  };


  return (
    <div className={`${styles.indemnify_form} f2`}>
      <TopTitle title={lng.get("uyituyq0")} subtitle={""} />

      <Form className="f-orm" ref={formRef} noValidate validated={validated}>

        <Row style={{marginBottom: 16}}>
          <div className="col-md-3 mt-15">
            <FLDate
                required={true}
                changeEvent={setDateAccident}
                value={dateAccident}
                label={lng.get("3gcxnaen")}
            />
          </div>
          <div className="col-md-3 mt-15">
            <FLTime
                required={true}
                changeEvent={setTimeAccident}
                value={timeAccident}
                label={lng.get("bk6alezm")}
            />
          </div>
          <div className="col-md-6 mt-15">
            <FLInput
                required={true}
                changeEvent={setAddressAccident}
                value={addressAccident}
                label={lng.get("outpth0e")}
            />
          </div>
          <div className="col-md-12 mt-15">
            <FLInput
                label={lng.get("jmnklciv")}
                value={reason}
                changeEvent={setReason}
                required={true}
            />
          </div>
        </Row>


        <label className="form-switch">
          <input
            type="checkbox"
            name={"BEN1"}
            checked={ben1}
            onChange={(e) => cbModeChange(e)}
          />
          <i/> {lng.get("6hp2if8d")}
        </label>

        <div style={{marginTop: 8} }>

          <label className="form-switch">
            <input
                type="checkbox"
                name={"BEN2"}
                onChange={(e) => cbModeChange(e)}
            />
            <i></i> {lng.get("16jw6lii")}
          </label>

          {ben2 && <BaggageDelay ref={form2Ref} />}

          <div className={styles.bank_info}>
            <Row>
              <div className="col-md-3 mt-15">
                <FLAmount
                  required={true}
                  value={amountClaim}
                  changeEvent={setAmoutClaim}
                  label={lng.get("pso6fxqw")}
                />
              </div>
              <div className="col-md-3 mt-15">
                <FLSelect
                  disable={false}
                  label={lng.get("yooywyv9")}
                  value={payMethod}
                  changeEvent={setPaymethod}
                  dropdown={true}
                  required={true}
                  data={[{ label: lng.get("24sdbgn6"), value: "CK" }]}
                />
              </div>
            </Row>

            {payMethod == "CK" && (
              <Row>
                <div className="col-md-3 mt-15">
                  <FLInput
                    required={true}
                    changeEvent={setBankNum}
                    value={bankNum}
                    label={lng.get("fjvajqcl")}
                  />
                </div>
                <div className="col-md-3 mt-15">
                  <FLBankList
                    disable={false}
                    changeEvent={setBank}
                    value={bank}
                    label={lng.get("8flfvfio")}
                    required={true}
                  />
                </div>
                <div className="col-md-3 mt-15">
                  <FLInput
                    required={true}
                    value={bankAccount}
                    changeEvent={setBankAccount}
                    isUpperCase={true}
                    label={lng.get("b7peygbc")}
                  />
                </div>
              </Row>
            )}
          </div>
        </div>
      </Form>
    </div>
  );
});

const mapStateToProps = (state) => {
  return {
    packages: state.data,
    isruserlist: state.isruserlist,
  };
};
const mapDispatchToProps = (dispatch) => ({
  setUserList: (usrl) => dispatch(setUserList(usrl)),
  checkRegistered: (isChecked) => dispatch(checkRegistered(isChecked)),
});

export default connect(mapStateToProps, mapDispatchToProps, null, {
  forwardRef: true,
})(Form2);
