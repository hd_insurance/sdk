import React, { useEffect, useState, createRef } from "react";
import ReactHtmlParser from "react-html-parser";
// import Router from 'next/router';
import styles from "../../../css/style.module.css";

const Form5 = (props) => {
  return (
    <div className="bb-form f5">
      <div className={styles.form_register_success}>
        <div className={styles.img_regster_suc}>
          <img src={"/img/img_register_success.png"} />
        </div>
        <div className={styles.content_regis_success}>
          {ReactHtmlParser(lng.get("xw6tyom0"))}
        </div>
        <div>
          <button
            className={styles.btn_discover_insur}
            onClick={() =>
              props.outerRouter.push("https://www.hdinsurance.com.vn/")
            }
          >
            {lng.get("bx63gbxx")}
          </button>
          <button
            className={styles.btn_back_home}
            onClick={() => {
              if(process.env.NODE_ENV != "production"){
                props.outerRouter.push("https://beta-vietjetair.hdinsurance.com.vn/search-insurance-policy.hdi")
              } else {
                props.outerRouter.push("https://vietjetair.hdinsurance.com.vn/search-insurance-policy.hdi")
              }
            }
            }
          >
            {lng.get("gua78xva")}
          </button>
        </div>
      </div>
    </div>
  );
};

export default Form5;
