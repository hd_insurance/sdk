import React, {useEffect, useRef, useState} from "react";
import Footer from "../../common/footer/footer";
import styles from "../../css/style.module.css";
import DynamicRender from "../../common/render/DnmRender";
import StageSpinner from "../../common/loadingpage";
import util from "../../util";
// import defineJSON from "./define_layout.json";
import {connect} from "react-redux";
import {setOBJState} from "../../redux/actions/action-nhatunhan";
import LoadingForm from "../../common/loadingform";
import moment from "moment";
import api from "../../services/Network.js";
import cogoToast from "cogo-toast";

function Main(props) {
  const footerRef = useRef();
  const typingTimerRef = useRef(null);

  const [step, setStep] = useState(0);
  const [isDisableFooter, setDisableFooter] = useState(false);
  const [loading, setLoading] = useState(true);
  const [isLoadingForm, setIsLoadingForm] = useState(false);
  const [define, setDefine] = useState({});
  const [listmaped, setMaped] = useState({});
  const [selectedPackage, setSelectedPackage] = useState(null);
  const [benefit, setBenefit] = useState({
    label: "",
    link: "",
    description: [],
  });
  const [defineConfig, setDefineConfig] = useState([]);

  const config_define = {
    ORG_CODE: props.productConfig.ORG_CODE,
    PRODUCT_CODE: props.productConfig.PRODUCT,
    CHANNEL: props.productConfig.CHANNEL,
    CATEGORY: props.productConfig.CATEGORY,
    IS_HIDE_VALUE_HOUSE: props.productConfig.IS_INPUT_VALUE_HOUSE,
    IS_DISABLE_PACKAGE: props.productConfig.IS_DISABLE_PACKAGE, // true thi disable gói bảo hiểm, false thi nguoc lai
  };

  const config_create_order = {
    ORG_SELLER: props.productConfig.ORG_SELLER,
    PRODUCT_CODE: config_define.PRODUCT_CODE,
    CHANNEL: props.productConfig.CHANNEL,
    CATEGORY: props.productConfig.CATEGORY,
    ACTION: props.productConfig.ACTION,
    USERNAME: props.productConfig.USERNAME,
  };

  const [stateData, setStateData] = useState({
    gender: "",
    name: "",
    phone: "",
    email: "",
    passport: "",
    dob: "",
    address: "",
    addressCode: {
      dist: "",
      label: "",
      prov: "",
      ward: "",
    },
    vat: "N",
    insBuyer: "true",
    relationship: "BAN_THAN",
    // data step 2
    genderOther: "",
    nameOther: "",
    phoneOther: "",
    emailOther: "",
    passportOther: "", // nếu bản thân thì passport = passportOther
    addressOther: "",
    addressCodeOther: {
      dist: "",
      label: "",
      prov: "",
      ward: "",
    },
    typeHouse: "AD",
    dientich: "",
    yearUse: "",
    sotang: "",
    valueHouse: "",
    deductible: "", // muc mien thuong
    addressHouse: "",
    addressCodeHouse: {
      dist: "",
      label: "",
      prov: "",
      ward: "",
    },
    cr_package: null,
    yearIsr: 1,
    effectiveDate: moment().add(1, "days").format("DD/MM/YYYY"),
    amount: "",
    flcheckbox_ck: false,
    checkedHouse: false,
    isspackage: {},
  });

  const mapDataBuyer = () => {
    let BUYER = {
      TYPE: "CN",
      NAME: stateData.name,
      DOB: stateData.dob,
      GENDER: stateData.gender,
      PROV: stateData.addressCode.prov,
      DIST: stateData.addressCode.dist,
      WARDS: stateData.addressCode.ward,
      ADDRESS: stateData.address,
      IDCARD: stateData.passport,
      EMAIL: stateData.email,
      PHONE: stateData.phone,
      FAX: "",
      TAXCODE: "",
      TYPE_VAT: stateData.vat,
    };
    return BUYER;
  };

  const mapInfoBill = () => {
    let BILL_INFO = {
      TYPE: "CN",
      NAME: stateData.name,
      DOB: stateData.dob,
      GENDER: stateData.gender,
      PROV: stateData.addressCode.prov,
      DIST: stateData.addressCode.dist,
      WARDS: stateData.addressCode.ward,
      ADDRESS: stateData.address,
      IDCARD: stateData.passport,
      EMAIL: stateData.email,
      PHONE: stateData.phone,
      FAX: "",
      TAXCODE: "",
    };
    return BILL_INFO;
  };

  const mapHouseInfo = () => {
    let HOUSE_INFO = {
      HOUSE_TYPE: stateData.typeHouse,
      HOUSE_AREA: stateData.dientich,
      YEAR_COMPLETE: stateData.yearUse, // Năm hoàn thành xây dựng
      NUM_FLOOR: stateData.sotang,
      PROV: stateData.addressCodeHouse.prov,
      DIST: stateData.addressCodeHouse.dist,
      WARDS: stateData.addressCodeHouse.ward,
      ADDRESS: stateData.addressHouse,
    };
    return HOUSE_INFO;
  };

  const mapHouseProduct = () => {

    let yearAffect = moment(stateData.effectiveDate, "DD/MM/YYYY").year();
    let yearOfUse = handleYearOfUse(stateData.yearUse, yearAffect);

    let HOUSE_PRODUCT = {
      PRODUCT_CODE: config_define.PRODUCT_CODE,
      PACK_CODE: stateData.cr_package,
      EFF: stateData.effectiveDate,
      TIME_EFF: "",
      EXP: moment(stateData.effectiveDate, "DD/MM/YYYY").add(
          "years", stateData.yearIsr * 1
      ).format("DD/MM/YYYY"),
      TIME_EXP: "",
      HOUSE_USE_CODE: `${yearOfUse}`, // Mã số năm sử dụng
      INSUR_HOUSE_VALUE: stateData.valueHouse * 1, // gia tri ngoi nhà
      INSUR_HOUSE_ASSETS: 0,
      INSUR_DEDUCTIBLE: stateData.deductible,
    };
    return HOUSE_PRODUCT;
  };

  const mapInfoNDBH = () => {
    if (stateData.insBuyer) {
      return {
        TYPE: "CN",
        NAME: stateData.name,
        DOB: stateData.dob,
        GENDER: stateData.gender,
        PROV: stateData.addressCode.prov,
        DIST: stateData.addressCode.dist,
        WARDS: stateData.addressCode.ward,
        ADDRESS: stateData.address,
        IDCARD: stateData.passport,
        EMAIL: stateData.email,
        PHONE: stateData.phone,
        FAX: "",
        TAXCODE: "",
        RELATIONSHIP: stateData.relationship, // Mối quan hệ
        CONTRACT_CODE: "", // mã HĐ với TH sửa
        DETAIL_CODE: "", // Mã chi tiết HĐ với TH sửa
        LAND_USE_RIGHTS: "", // Quyền sử dụng đất
      };
    }
    return {
      TYPE: "CN",
      NAME: stateData.nameOther,
      DOB: "",
      GENDER: stateData.genderOther,
      PROV: stateData.addressCodeOther.prov,
      DIST: stateData.addressCodeOther.dist,
      WARDS: stateData.addressCodeOther.ward,
      ADDRESS: stateData.addressOther,
      IDCARD: stateData.passportOther,
      EMAIL: stateData.emailOther,
      PHONE: stateData.phoneOther,
      FAX: "",
      TAXCODE: "",
      RELATIONSHIP: stateData.relationship, // Mối quan hệ
      CONTRACT_CODE: "", // mã HĐ với TH sửa
      DETAIL_CODE: "", // Mã chi tiết HĐ với TH sửa
      LAND_USE_RIGHTS: "", // Quyền sử dụng đất
    };
  }

  const mapHouseInsur = () => {
    let HOUSE_INFO = mapHouseInfo();
    let HOUSE_PRODUCT = mapHouseProduct();
    let NDBH = mapInfoNDBH();
    return [
      {
        ...NDBH,
        HOUSE_INFO,
        HOUSE_PRODUCT,
        // BILL_INFO,
      }
    ];
  };

  const setValData = (key, val) => {
    setStateData((prevState) => ({
      ...prevState,
      [key]: val,
    }));
  };

  const calcFee = async (dynamicFees) => {
    try {
      const data = {
        channel: "SDK_HOUSE",
        userName: "HDI",
        ORG_CODE: config_define.ORG_CODE,
        PRODUCT_INFO: [
          {
            INX: "0",
            CATEGORY: config_define.CATEGORY,
            PRODUCT_CODE: config_define.PRODUCT_CODE,
            PACK_CODE: stateData.cr_package,
            DISCOUNT: "0",
            DISCOUNT_UNIT: "",
            DYNAMIC_FEES: dynamicFees,
          },
        ],
      };
      // console.log("======> data ne", data);
      setIsLoadingForm(true);
      const response = await api.post("/api/nha-tu-nhan/calc/fee", data);
      setTimeout(() => {
        if (response?.Data) {
          setIsLoadingForm(false);
          setValData("amount", response.Data.TOTAL_AMOUNT);
        }
      }, 1000);
    } catch (err) {
      console.log(err);
    }
  };

  const handleCalcFee = async (arrDynamic) => {
    if (typingTimerRef) {
      clearTimeout(typingTimerRef.current);
    }
    typingTimerRef.current = setTimeout(() => {
      calcFee(arrDynamic);
    }, 800);
  };

  const createOrder = async (data) => {
    try {
      setIsLoadingForm(true);
      var url_req = `/api/sdk-form/createOrder/${config_create_order.ACTION}/${config_create_order.PRODUCT_CODE}/${config_create_order.CHANNEL}/${config_create_order.CATEGORY}/${config_create_order.USERNAME}`;
      if (props.prod_define) {
        url_req = url_req + "?ref_id=" + props.prod_define.ref_id
        if (props.prod_define.product_package) {
          setSelectedPackage(props.prod_define.product_package)
        }
      }

      const res = await api.post(url_req, data);
      if (res.Success) {
        // setLoadingForm(false);
        return res.Data;
      } else {
        setIsLoadingForm(false);
        console.log("Error 1")
        cogoToast.error(res.ErrorMessage);
        return false;
      }
    } catch (error) {
      // console.log("errr create order", error);
      setIsLoadingForm(false);
      console.log("Error 2 ", error)
      cogoToast.error(error.ErrorMessage);
      return false;
    }
  };

  const handleYearOfUse = (yearDone, yearAffect) => {
    let result = "";
    if (!yearDone) return result;
    let hieuYear = parseFloat(yearAffect * 1 - yearDone * 1);
    if (defineConfig[7]) {
      defineConfig[7].forEach((item) => {
        if (
            hieuYear <= parseFloat(item.MAX_VALUE) &&
            hieuYear >= parseFloat(item.MIN_VALUE)
        ) {
          result = item.value;
        }
      });
    }
    return result;
  }


  const handleArrDynamicFee = (
      yearDone, // năm hoàn thành xây dựng
      yearAffect, // năm hiệu lực
      yearIsr = 1,
      moneyHouse,
      packCode
  ) => {
    let yearOfUse = handleYearOfUse(yearDone, yearAffect);
    let totalDay = yearIsr * 1 * 365;
    return [
      {
        KEY_CODE: "NHA_GOITG",
        VAL_CODE: `${packCode}@${yearOfUse}`,
        TYPE_CODE: "CACHE",
      },
      {
        KEY_CODE: "NHA_HV1",
        VAL_CODE: moneyHouse,
        TYPE_CODE: "CONSTANT",
      },
      {
        KEY_CODE: "NHA_FV1",
        VAL_CODE: "0",
        TYPE_CODE: "CONSTANT",
      },
      {
        KEY_CODE: "NHA_TGBH",
        // VAL_CODE: totalDay + "",
        VAL_CODE: yearIsr*12 + "",
        TYPE_CODE: "CONSTANT",
      },
    ];
  };

  useEffect(() => {
    if (
        stateData.valueHouse &&
        stateData.yearUse &&
        stateData.effectiveDate &&
        stateData.cr_package
    ) {
      let arrayFeeDynamic = [];
      let yearAffect = moment(stateData.effectiveDate, "DD/MM/YYYY").year();
      arrayFeeDynamic = handleArrDynamicFee(
          stateData.yearUse,
          yearAffect,
          stateData.yearIsr,
          stateData.valueHouse,
          stateData.cr_package
      );
      handleCalcFee(arrayFeeDynamic);
    }
  }, [
    stateData.valueHouse,
    stateData.yearIsr,
    stateData.yearUse,
    stateData.effectiveDate,
    stateData.cr_package,
  ]);

  const toggleVAT = (value) => {
    if (value === "P") { // lấy cho cá nhân
      if (stateData.passport && stateData.address && stateData.addressCode.label) {
        listmaped["row_cn"].toggle(true);
      } else {
        listmaped["row_cn"].toggle(false);
      }
      listmaped["address"].setRequire(true);
      listmaped["addressCode"].setRequire(true);
      listmaped["passport"].setRequire(true);
      return;
    }
    listmaped["row_cn"].toggle(true);
    if (stateData.insBuyer) {
      listmaped["address"].setRequire(true);
      listmaped["addressCode"].setRequire(true);
      listmaped["passport"].setRequire(true);
    } else {
      listmaped["address"].setRequire(false);
      listmaped["addressCode"].setRequire(false);
      listmaped["passport"].setRequire(false);
    }
  };

  const chooseTypeHouse = (value) => {
    switch (value) {
      case "AD": {
        listmaped["row_require_cc"].toggle(true);
        listmaped["col_sotang"].toggle(false);
        listmaped["addressHouse"].setLabel("Số nhà, ngõ / ngách, đường");
        break;
      }
      case "VL": {
        listmaped["row_require_cc"].toggle(true);
        listmaped["col_sotang"].toggle(false);
        listmaped["addressHouse"].setLabel("Số nhà, ngõ / ngách, đường");
        break;
      }
      case "AP": {
        listmaped["row_require_cc"].toggle(false);
        listmaped["col_sotang"].toggle(true);
        listmaped["addressHouse"].setLabel(
            "Số phòng, số tầng, toà, số nhà , ngõ / ngách, đường"
        );
        setValData('sotang', '');
        break;
      }
    }
  };

  const handleCommitPayment = (value) => {
    if (value) {
      footerRef.current.setDisable(false);
      return;
    }
    footerRef.current.setDisable(true);
  };

  const handleInsBuyer = (value) => {
    if (value) {
      setValData('relationship', 'BAN_THAN');
      listmaped["address"].setRequire(true);
      listmaped["addressCode"].setRequire(true);
      listmaped["passport"].setRequire(true);
    } else {
      setValData('relationship', 'KHAC');
      if (stateData.vat === "N") {
        listmaped["address"].setRequire(false);
        listmaped["addressCode"].setRequire(false);
        listmaped["passport"].setRequire(false);
      } else {
        listmaped["address"].setRequire(true);
        listmaped["addressCode"].setRequire(true);
        listmaped["passport"].setRequire(true);
      }
    }
  }

  const handleCheckedHouse = (value) => {
    if (value) {
      listmaped["row_info_ntn_one"].toggle(false);
      listmaped["row_info_ntn_two"].toggle(false);
      listmaped["row_info_ntn_three"].toggle(false);
      if (stateData.typeHouse == "AP") {
        listmaped["row_require_cc"].toggle(false);
      } else {
        listmaped["row_require_cc"].toggle(true);
      }
    } else {
      listmaped["row_info_ntn_one"].toggle(true);
      listmaped["row_info_ntn_two"].toggle(true);
      listmaped["row_info_ntn_three"].toggle(true);
      listmaped["row_require_cc"].toggle(true);
    }
  };

  const handleHideInfo = () => {
    if (stateData.insBuyer) {
      listmaped["row_text_isr"].toggle(true);
      listmaped["row_other"].toggle(true);
      listmaped["row_other_two"].toggle(true);
      listmaped["line_form_step2"].toggle(true);
    } else {
      listmaped["row_text_isr"].toggle(false);
      listmaped["row_other"].toggle(false);
      listmaped["row_other_two"].toggle(false);
      listmaped["line_form_step2"].toggle(false);
    }
  };

  const onNextClick = async (e) => {
    try {
        switch (step) {
          case 0: {
            if (listmaped["form_step1"].ref.current.handleSubmit()) {
              handleHideInfo();
              setStep(1);
              listmaped["wizard"].ref.current.setStep(1);
              listmaped["step_layout"].ref.current.setStep(1);
            }
            break;
          }
          case 1: {
            if (stateData.checkedHouse && listmaped["form_step2"].ref.current.handleSubmit()) {
              listmaped["verifyisr"].define.data = {...stateData};
              listmaped["totalpayment"].setData(stateData.amount);
              listmaped["wizard"].ref.current.setStep(2);
              listmaped["step_layout"].ref.current.setStep(2);
              footerRef.current.setNextTitle("Thanh toán");
              footerRef.current.setDisable(true);
              setStep(2);
            }
            break;
          }
          case 2: {
            if (props.prod_define) {
              config_create_order.ORG_SELLER = props.productConfig.ORG_CODE;
            }
            const SELLER = {
              SELLER_CODE: null,
              ORG_CODE: config_create_order.ORG_SELLER, // == ORG SELLER
              ORG_TRAFFIC: "",
              TRAFFIC_LINK: "",
              ENVIROMENT: "WEB",
            };
            const PAY_INFO = {
              PAYMENT_TYPE: "CTT",
            };
            const BUYER = mapDataBuyer();
            const HOUSE_INSUR = mapHouseInsur();
            let BILL_INFO = mapInfoBill();
            if (stateData.vat === "N") {
              BILL_INFO = null;
            }
            const data = {
              SELLER,
              BUYER,
              HOUSE_INSUR,
              BILL_INFO,
              PAY_INFO
            };
            // console.log("=========== data ===============> ", data);
            const result = await createOrder(data);
            if (result) {
              // console.log("====>", result);
              window.open(
                  `${result.url_redirect}&callback=${
                      window.location.origin + window.location.pathname
                  }?payment=done`,
                  "_self"
              );
            }
            break;
          }
          default:
            break;
        }
    } catch (error) {
        console.log('next step error', error)
    }
  };

  const onPrevClick = (e) => {
    switch (step) {
      case 1: {
        setStep(0);
        listmaped["wizard"].ref.current.setStep(0);
        listmaped["step_layout"].ref.current.setStep(0);
        break;
      }
      case 2: {
        setStep(1);
        listmaped["wizard"].ref.current.setStep(1);
        listmaped["step_layout"].ref.current.setStep(1);
        setValData("flcheckbox_ck", false);
        footerRef.current.setDisable(false);
        footerRef.current.setNextTitle(l.g("bhsk.form.lbl_next"));
        break;
      }
      case 3: {
        setStep(2);
        listmaped["wizard"].ref.current.setStep(2);
        listmaped["step_layout"].ref.current.setStep(2);
        break;
      }
    }
  };

  const setDefaultScreenValue = (map) => {
    let now = moment();
    // map["dob"].define.minimumDate = {
    //   year: now.year() - 65,
    //   month: now.month() + 1,
    //   day: now.date(),
    // };
    map["dob"].define.maximumDate = {
      year: now.year() - 18,
      month: now.month() + 1,
      day: now.date(),
    };
    map["dob"].define.defaultvalue = {
      year: now.year() - 18,
      month: now.month() + 1,
      day: now.date(),
    };
    map["effectiveDate"].define.minimumDate = {
      year: now.year(),
      month: now.month() + 1,
      day: now.date() + 1,
    };
    map["button_1"].onClick = () => {
      window.location.href = "https://hdinsurance.com.vn";
    };
    map["button_2"].onClick = () => {
      window.location.href = "https://hdinsurance.com.vn";
    };

    // ẩn thôn tin ngôi nhà
    map["row_info_ntn_one"].toggle(true);
    map["row_info_ntn_two"].toggle(true);
    map["row_info_ntn_three"].toggle(true);
    map["row_require_cc"].toggle(true);
  };

  const getDefineData = async (callback) => {
    try {
      var url_df = `/api/sdk-form/define/${config_define.ORG_CODE}/${config_define.PRODUCT_CODE}/${config_define.CHANNEL}/VN`
      if (props.prod_define) {
        if (props.prod_define.sku) {
          url_df = url_df + `?sku=${props.prod_define.sku}`
        }
      }
      const data = await api.get(url_df);

      if (data.data) {
        setDefineConfig(data.data);
        callback(data.data);
        // console.log(data.data);
      } else {
      }
      // setPackages(data);
    } catch (e) {
      console.log(e);
    }
  };

  const handlePattern = (map) => {
    // if (config_define.ORG_CODE === "HDBANK_VN") {
    //   map["email"].setPattern(`[a-zA-Z0-9._%+-]+@hdbank.com.vn$`);
    // }
    if (config_define.IS_HIDE_VALUE_HOUSE) {
      map["col_valueHouse"].toggle(false);
    } else {
      map["col_valueHouse"].toggle(true);
    }
    if (config_define.IS_DISABLE_PACKAGE) {
      map["cr_package"].setDisable(true);
    } else {
      map["cr_package"].setDisable(false);
    }
  };

  const getInitData = async (ref_id, callback) => {
    try {
      const data = await api.get(`/api/sdk-form/cusinfor?ref_id=${ref_id}`);
      // console.log('=====> ref_id', ref_id);
      if (data[0]) {
        if (data[0][0].OBJ_PRODUCT) {
          const obj_user = JSON.parse(data[0][0].OBJ_PRODUCT);
          if (obj_user) {
            const buyer_info = obj_user.BUYER;
            const house_insur = obj_user.HOUSE_INSUR;
            stateData.name = buyer_info.NAME;
            stateData.dob = buyer_info.DOB;
            stateData.phone = buyer_info.PHONE ? buyer_info.PHONE.trim().replace("+", "") : "";
            stateData.email = buyer_info.EMAIL ? buyer_info.EMAIL.toLowerCase() : "";
            stateData.passport = buyer_info.IDCARD ? buyer_info.IDCARD.trim() : "";
            stateData.address = buyer_info.ADDRESS ? buyer_info.ADDRESS : "";

            // setStateData({...stateData});
            setValData('name', stateData.name);
            setValData('dob', stateData.dob);
            setValData('phone', stateData.phone);
            setValData('email', stateData.email);
            setValData('passport', stateData.passport);
            setValData('address', stateData.address);
            // console.log(stateData);
          }
        }
      }
    } catch (e) {
      console.log(e);
    }
  };


  useEffect(() => {
    getDefineData((dt_config) => {
      const {obj_config, map} = util.rootObjectComponent(props.page_layout);
      props.setOBJState(obj_config);
      setMaped(map);
      setDefine(obj_config);
      handlePattern(map);
      setDefaultScreenValue(map);
      const listTypeHouse = dt_config[0];
      const listDeductible = dt_config[1];
      const listGender = dt_config[3];
      const listPackage = dt_config[2];
      const listYearIsr = dt_config[8];
      const listVAT = dt_config[10];
      const label_checkbox = `Tôi cam kết các thông tin khai báo là chính xác, trung thực và hoàn toàn chịu trách nhiệm về các thông tin đã khai báo. Đồng thời tôi đã đọc, hiểu và đồng ý với <a href="https://hyperservices.hdinsurance.com.vn/f/8e5eb0c0bcf931186e34d68fed9af30f" target="_blank">điều kiện, điều khoản, quy tắc</a> của HDI.`;
      map["flcheckbox_ck"].setLabel(label_checkbox);
      if (listGender && listGender.length > 0) {
        const formatGender = listGender.map((item) => {
          return {label: item.label, value: item.value};
        });
        map["gender"].setData(formatGender);
        map["genderOther"].setData(formatGender);
      }
      if (listTypeHouse && listTypeHouse.length > 0) {
        const formatTypeHouse = listTypeHouse.map((item) => {
          return {label: item.TYPE_NAME, value: item.TYPE_CODE};
        });
        map["typeHouse"].setData(formatTypeHouse);
      }
      if (listPackage && listPackage.length > 0) {
        const formatPackage = listPackage.map((item) => {
          return {label: item.PACK_NAME, value: item.PACK_CODE, amount: item.AMOUNT};
        });
        map["cr_package"].setData(formatPackage);

        if (props.prod_define?.product_package) {
          setValData("cr_package", props.prod_define.product_package);
        } else {
          setValData("cr_package", formatPackage[0].value);
        }

        // setValData("cr_package", formatPackage[0].value);
        setValData("isspackage", formatPackage[0]);
        setValData('valueHouse', formatPackage[0].amount || '') // neu từ sàn thì nó có giá trị
        setBenefit(formatBenefit(dt_config[2][0]));
      }
      if (listYearIsr && listYearIsr.length > 0) {
        const formatListYearIsr = listYearIsr.map((item) => {
          return {label: item.label, value: item.value};
        });
        map["yearIsr"].setData(formatListYearIsr);
        setValData("yearIsr", formatListYearIsr[0].value);
      }
      if (listDeductible && listDeductible.length > 0) {
        const formatListDeductible = listDeductible.map((item) => {
          return {label: item.TYPE_NAME, value: item.TYPE_CODE};
        });
        map["deductible"].setData(formatListDeductible);
        setValData("deductible", formatListDeductible[0].value);
      }
      if (listVAT && listVAT.length > 0) {
        map["vat"].setDataRadio(listVAT);
        setValData("vat", listVAT[0].value);
      }
      if (props.prod_define) {
        getInitData(props.prod_define.ref_id, (load_success) => {
          // console.log('====> load_success', load_success);
        });
      }

      setLoading(false);
    });
  }, []);

  const formatBenefit = (pkinfo) => {
    return {
      label: pkinfo.PACK_NAME,
      link: pkinfo.URL_BEN ? pkinfo.URL_BEN : 'NO_ACTION',
      description: pkinfo.DESCRIPTION ? JSON.parse(pkinfo.DESCRIPTION) : [],
    };
  };

  useEffect(() => {
    // payment done
    if (props.payment && !loading && !util.isEmptyObj(listmaped)) {
      listmaped["wizard"].ref.current.setStep(3);
      listmaped["step_layout"].ref.current.setStep(3);
      setStep(3);
    }
  }, [loading]);

  return (
      <div className={styles.main_container}>
        {loading ? (
            <div className={styles.main_loading}>
              <div className={styles.ff_loading}>
                <StageSpinner size={60} frontColor="#329945" loading={loading}/>
              </div>
            </div>
        ) : (
            <div className={`container ${styles.kklm}`}>
              {isLoadingForm ? <LoadingForm/> : null}
              <DynamicRender
                  layout={define}
                  gender={stateData.gender}
                  name={stateData.name}
                  phone={stateData.phone}
                  email={stateData.email}
                  passport={stateData.passport}
                  dob={stateData.dob}
                  address={stateData.address}
                  addressCode={stateData.addressCode}
                  vat={stateData.vat}
                  insBuyer={stateData.insBuyer}
                  genderOther={stateData.genderOther}
                  nameOther={stateData.nameOther}
                  phoneOther={stateData.phoneOther}
                  emailOther={stateData.emailOther}
                  passportOther={stateData.passportOther}
                  addressOther={stateData.addressOther}
                  addressCodeOther={stateData.addressCodeOther}
                  typeHouse={stateData.typeHouse}
                  dientich={stateData.dientich}
                  yearUse={stateData.yearUse}
                  sotang={stateData.sotang}
                  valueHouse={stateData.valueHouse}
                  deductible={stateData.deductible}
                  addressHouse={stateData.addressHouse}
                  addressCodeHouse={stateData.addressCodeHouse}
                  cr_package={stateData.cr_package}
                  benefit={benefit}
                  yearIsr={stateData.yearIsr}
                  effectiveDate={stateData.effectiveDate}
                  amount={stateData.amount}
                  flcheckbox_ck={stateData.flcheckbox_ck}
                  checkedHouse={stateData.checkedHouse}
                  onInputChange={(name, value) => {
                    if (name === "vat") {
                      toggleVAT(value);
                    }
                    if (name === "typeHouse") {
                      chooseTypeHouse(value);
                    }
                    if (name === "flcheckbox_ck") {
                      handleCommitPayment(value);
                    }
                    if (name === "checkedHouse") { // check nha dung de o
                      handleCheckedHouse(value);
                    }
                    if (name === 'insBuyer') { // check nguoi mua la nguoi duoc bao hiem hay khong
                      handleInsBuyer(value);
                    }
                    setValData(name, value);
                  }}
                  onInputChangePackage={(name, value, index) => {
                    setValData("cr_package", value);
                    setValData("isspackage", defineConfig[2][index]);
                    setBenefit(formatBenefit(defineConfig[2][index]));
                    setValData('valueHouse', defineConfig[2][index].AMOUNT);
                  }}
              />
              <Footer
                  ref={footerRef}
                  isDisable={isDisableFooter}
                  step={step}
                  onPrev={onPrevClick}
                  onNext={onNextClick}
                  lastStep={3}
              />
            </div>
        )}
      </div>
  );
}

const mapStateToProps = (state) => {
  return {
    objState: state.nhatunhan,
  };
};

const mapDispatchToProps = (dispatch) => ({
  setOBJState: (obj) => dispatch(setOBJState(obj)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Main);



