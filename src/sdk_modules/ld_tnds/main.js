import React, { useEffect, useState, createRef } from "react";
import router, { useRouter } from "next/router";
import StageSpinner from "../../common/loadingpage";
import randomstring from "randomstring";
import { animateScroll } from "react-scroll";
import { connect } from "react-redux";
import Wizard from "../../common/topwizard";
import Footer from "../../common/footer/footer";
import {
  setCarList,
  setListDefineCar,
} from "../../redux/actions/action-car.js";

import { Tab } from "react-bootstrap";
import F1 from "./screen/f1.js";
import F2 from "./screen/f2.js";
import F3 from "./screen/f3.js";
import F4 from "./screen/f4.js";

import api from "../../services/Network.js";
import styles from "../../css/style.module.css";

const Main = (props) => {
  const config_define = {
    ...props.productConfig,
    vehicleType: props.productConfig.TYPE,
  };
  const footerRef = createRef();
  const F1Ref = createRef();
  const F2Ref = createRef();
  const F3Ref = createRef();
  // const F4Ref = createRef();

  const [step, setStep] = useState("0");
  const [isDisableFooter, setDisableFooter] = useState(false);

  const [loading, setLoading] = useState(false);
  const wzdata = [
    {
      step: "Bước 1",
      label: "Thông tin người mua",
      icon: "fas fa-user",
    },
    {
      step: "Bước 2",
      label: "Thông tin xe",
      icon:
        config_define.vehicleType == "car" ? "fas fa-car" : "fas fa-motorcycle",
    },
    {
      step: "Bước 3",
      label: "Xác nhận",
      icon: "fas fa-list-ul",
    },
    {
      step: "Bước 4",
      label: "Thành công",
      icon: "fas fa-check-circle",
    },
  ];
  useEffect(() => {
    try {
      if (router.router.query.payment == "done") {
        // check thanh toan thanh con redirect sang
        setStep("3");
        setDisableFooter(true);
      } else {
        getDefineCar();
        initCarInfor();
      }
    } catch (error) {
      console.log(error);
    }
  }, []);

  const getDefineCar = async () => {
    try {
      setLoading(true);
      const listDefineCar = await api.get("/api/car/define");
      if (listDefineCar) {
        setLoading(false);
        // console.log(listDefineCar);
        props.setListDefineCar(listDefineCar);
      }
    } catch (e) {
      console.log(e);
    }
  };

  const initCarInfor = () => {
    let carls = props.isrcarlist;
    var newID = randomstring.generate(7);
    const newarr = insert(carls, 1, {
      id: newID,
      ref: createRef(),
    });
    F2Ref.current.setActiveCar(newID);
    props.setCarList(newarr);
  };

  const insert = (arr, index, newItem) => [
    // inser vao vi tri trong mang
    ...arr.slice(0, index),
    newItem,
    ...arr.slice(index),
  ];

  const submitForm1 = (e) => {
    return F1Ref.current.handleSubmit();
  };
  const submitForm2 = (e) => {
    return F2Ref.current.handleSubmit();
  };
  const submitForm3 = (e) => {
    return F3Ref.current.createOrder();
  };
  const onEditClick = (id) => {
    F2Ref.current.setActiveCar(id);
    setStep("1");
    footerRef.current.setDisable(false);
  };
  const onNextClick = async (e) => {
    animateScroll.scrollToTop();
    switch (step) {
      case "0":
        if (submitForm1()) {
          setStep("1");
          if (props.isrcarlist.length > 0) {
            footerRef.current.setDisable(false);
          } else {
            footerRef.current.setDisable(true);
          }
        }
        break;
      case "1":
        if (submitForm2()) {
          setStep("2");
          footerRef.current.setDisable(true);
          footerRef.current.setNextTitle(l.g("bhsk.form.lbl_confirm"));
        }
        break;
      case "2":
        const isSuccess = await submitForm3();
        if (isSuccess) {
          // setStep("3");
          window.open(
            `${isSuccess}&callback=${
              window.location.origin + window.location.pathname
            }?payment=done`,
            "_self"
          );
        }
        break;
    }
  };
  const onPrevClick = (e) => {
    switch (step) {
      case "1":
        setStep("0");
        footerRef.current.setDisable(false);
        break;
      case "2":
        setStep("1");
        F3Ref.current.uncheckCommit();
        footerRef.current.setDisable(false);
        footerRef.current.setNextTitle(l.g("bhsk.form.lbl_next"));
        break;
      case "3":
        setStep("2");
        break;
    }
  };

  const handleSubmit = (info) => {};

  const onCbTermChange = (isDisable) => {
    footerRef.current.setDisable(isDisable);
  };

  return (
    <div className={styles.main_container}>
      {loading ? (
        <div className={styles.main_loading}>
          <div className={styles.ff_loading}>
            <StageSpinner size={60} frontColor="#329945" loading={loading} />
          </div>
        </div>
      ) : (
        <div className={`container ${styles.kklm}`}>
          <Wizard active={step} data={wzdata} />
          <Tab.Container id="xxxx" activeKey={step}>
            <Tab.Content>
              <Tab.Pane eventKey="0">
                <F1
                  ref={F1Ref}
                  onCbTermChange={onCbTermChange}
                  handleSubmit={handleSubmit}
                  vehicleType={config_define.vehicleType}
                  redirectFrom={props.redirectFrom}
                />
              </Tab.Pane>
              <Tab.Pane eventKey="1">
                <F2
                  ref={F2Ref}
                  onCbTermChange={onCbTermChange}
                  handleSubmit={handleSubmit}
                  vehicleType={config_define.vehicleType}
                  redirectFrom={props.redirectFrom}
                  config_define={config_define}
                />
              </Tab.Pane>
              <Tab.Pane eventKey="2">
                <F3
                  ref={F3Ref}
                  onCbTermChange={onCbTermChange}
                  handleSubmit={handleSubmit}
                  onEditClick={onEditClick}
                  setLoading={setLoading}
                  vehicleType={config_define.vehicleType}
                  redirectFrom={props.redirectFrom}
                  config_define={config_define}
                />
              </Tab.Pane>
              <Tab.Pane eventKey="3">
                <F4
                  // ref={F4Ref}
                  onCbTermChange={onCbTermChange}
                  handleSubmit={handleSubmit}
                />
              </Tab.Pane>
            </Tab.Content>
          </Tab.Container>

          <Footer
            ref={footerRef}
            isDisable={isDisableFooter}
            step={step}
            onPrev={onPrevClick}
            onNext={onNextClick}
            lastStep={3}
          />
        </div>
      )}
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    isrcarlist: state.isrcarlist,
  };
};

const mapDispatchToProps = (dispatch) => ({
  setCarList: (carls) => dispatch(setCarList(carls)),
  setListDefineCar: (listDefine) => dispatch(setListDefineCar(listDefine)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Main);
