import React, {
  useEffect,
  useState,
  createRef,
  forwardRef,
  useImperativeHandle,
  useRef,
} from "react";
import { Button, Col, Row, Form } from "react-bootstrap";
import currencyFormatter from "currency-formatter";
import { connect } from "react-redux";
import { FLInput, FLDate, FLSelect, FLTime } from "hdi-uikit";

import moment from "moment";

import api from "../../../services/Network.js";
import LoadingForm from "../../../common/loadingform";
import { confirmAlert } from "react-confirm-alert";
import { config } from "./const";
import styles from "../../../css/style.module.css";

let sdk_caculate_fees = [];
let sdk_caculate_fees_tndstn = [];

function usePrevious(value) {
  // custom hook lay gia tri truoc khi thay doi
  const ref = useRef();
  useEffect(() => {
    ref.current = value;
  });
  return ref.current;
}

const CarInfo = forwardRef((props, ref) => {
  const formRef = createRef();

  const [validated, setValidated] = useState(false);

  // bat buoc
  const [vehicle_group, setVehicle_group] = useState(""); // nhom xe
  const [vehicle_type, setVehicle_type] = useState(""); // loai xe
  const [purpose, setPurpose] = useState(""); // muc dich su dung
  const [seat, setSeat] = useState(""); // so cho ngoi
  const [payload, setPayload] = useState(""); // trong tai
  const [newCar, setNewCar] = useState(false);
  const [duration, setDuration] = useState(""); // thoi han HD
  const [effective_date, setEffective_date] = useState(
    moment().add(1, "days").format("DD/MM/YYYY")
  );
  const [effective_time, setEffective_time] = useState("00:00");
  const [end_date, setEnd_date] = useState(
    moment().add(1, "days").format("DD/MM/YYYY")
  );
  const [end_time, setEnd_time] = useState("00:00");
  const [tndsfee, setTndsfee] = useState(""); // phi bh tnds bat buoc
  const [plate, setPlate] = useState(""); // bien so xe
  const [frame_number, setFrame_number] = useState(""); // so khung
  const [vehicle_number, setVehicle_number] = useState(""); // so may

  // TNDS tu nguyen
  const [damage_nt3, setDamage_nt3] = useState(""); // thiet hai than the nguoi thu 3
  const [property_damage_nt3, setProperty_damage_nt3] = useState(""); // thiet hai tai san nguoi thu 3
  const [client_damage, setClient_damage] = useState(""); // thiet hai than the hanh khach
  const [voluntary_tnds_fee, setVoluntary_tnds_fee] = useState("");

  // Bao hiem tai nan lai xe, phu xe, ng ngoi tren xe
  const [ins_money, setIns_money] = useState(""); // so tien bao hiem
  const [ins_fee_lx, setIns_fee_lx] = useState(""); // phi bh tai nan lai xe, phu xe, nguoi ngoi tren xe

  // Bao hiem tnds doi voi hang hoa
  const [goods, setGoods] = useState(""); // hang hoa duoc bao hiem
  const [responsibility_level, setResponsibility_level] = useState(""); // muc tranh nhiem
  const [ins_goods_fee, setIns_gooods_fee] = useState(""); // Phí BH TNDS của chủ xe với hàng hóa

  // nguoi khac
  const [gender, setGender] = useState(""); // danh xung
  const [owner_name, setOwner_name] = useState(""); // ten chu xe
  const [owner_phone, setOwner_phone] = useState(""); // so dien thoai
  const [owner_email, setOwner_email] = useState(""); // email

  // doanh nghiep
  const [company_name, setCompany_name] = useState(""); // ten DN
  const [mst, setMst] = useState(""); // ma so thuetên tổ
  const [compan_phone, setCompany_phone] = useState(""); // so dien thoai
  const [companr_email, setCompany_email] = useState(""); // email

  const [isShowtnds, setIsShowtnds] = useState(false);
  const [isShowAccident, setIsShowAccident] = useState(false);
  const [isShowtndsForGood, setIsShowtndsForGood] = useState(false);
  const [buyFor, setBuyFor] = useState("CN"); // type
  const [isLoading, setIsLoading] = useState(false);

  const prevState = usePrevious({
    // luu gia tri truoc thay doi
    vehicle_type,
    purpose,
    seat,
    payload,
    duration,
    damage_nt3,
    property_damage_nt3,
    client_damage,
    ins_money,
    goods,
    responsibility_level,
  });

  let timeoutCalc = null;

  const minDate = () => {
    const now = moment();
    return {
      year: now.year(),
      month: now.month() + 1,
      day: now.date() + 1,
    };
  };

  const filterVehicle = (arrVehicleGroup) => {
    // chi lay GROUP o to hoac xe may voi moi page rieng
    if (props.redirectFrom == "visa" || props.vehicleType == "motor") {
      // only xe may
      return arrVehicleGroup.filter((e) => e.VH_GROUP == "XE_MAY");
    } else if (props.vehicleType == "car") {
      return arrVehicleGroup.filter((e) => e.VH_GROUP == "XE_OTO");
    } else {
      return arrVehicleGroup;
    }
  };

  const filterCarWeight = (weigh) => {
    return props.listDefineCar[22].find((o) => {
      return (
        parseFloat(o.MIN_VALUE) <= parseFloat(weigh) &&
        parseFloat(weigh) <= parseFloat(o.MAX_VALUE)
      );
    }).value;
  };
  const filterCarSeat = (numberSeats) => {
    return props.listDefineCar[23].find((vl) => vl.NUM_OF_SEAT === numberSeats)
      .value;
  };
  const createObjCalcFees = (
    vehicle_type,
    purpose,
    seat,
    payload,
    duration
  ) => {
    if (!vehicle_type) vehicle_type = "VH_TYPE";
    if (!purpose) purpose = "CAR_PURPOSE";
    if (!seat) {
      seat = "CAR_SEAT";
    } else {
      seat = filterCarSeat(seat);
    }
    if (!payload) {
      payload = "CAR_WEIGHT";
    } else {
      payload = filterCarWeight(payload);
    }
    if (!duration) {
      duration = "NUM_OF_DAY";
    } else {
      duration = parseInt(duration) * 365 + "";
    }
    return [
      {
        KEY_CODE: "BBPLA_LXE",
        VAL_CODE: `${vehicle_type}`,
      },
      {
        KEY_CODE: "BBPLA_TTAI",
        VAL_CODE: `${vehicle_type}@${payload}`,
      },
      {
        KEY_CODE: "BBPLA_CNGOI",
        VAL_CODE: `${vehicle_type}@${seat}`,
      },
      {
        KEY_CODE: "BBPLA_MDKD",
        VAL_CODE: `${vehicle_type}@${purpose}`,
      },
      {
        KEY_CODE: "BBPLA_MDKD_CNGOI",
        VAL_CODE: `${vehicle_type}@${purpose}@${seat}`,
      },
      {
        KEY_CODE: "HS_T30D",
        VAL_CODE: `${duration}`,
        TYPE_CODE: "CONSTANT",
      },
      {
        KEY_CODE: "HS_D30D",
        VAL_CODE: "0",
        TYPE_CODE: "CONSTANT",
      },
    ];
  };
  const createObjCalcFeesTndstn = (
    vehicle_type,
    purpose,
    seat,
    payload,
    damage_nt3,
    property_damage_nt3,
    client_damage,
    ins_money,
    responsibility_level,
    goods,
    duration
  ) => {
    if (
      // check truong hop chon chua du tnds tu nguyen
      !damage_nt3 ||
      !property_damage_nt3 ||
      (!client_damage && props.vehicleType == "car")
    ) {
      damage_nt3 = "";
      property_damage_nt3 = "";
      client_damage = "";
    }
    if (!ins_money) {
      ins_money = "";
    }
    if (!responsibility_level || !goods) {
      responsibility_level = "";
      goods = "";
    }
    if (!ins_money) ins_money = "BH_NTX";
    if (!vehicle_type) vehicle_type = "VH_TYPE";
    if (!purpose) purpose = "CAR_PURPOSE";
    if (!client_damage) client_damage = "TH_HK";
    if (!payload) {
      payload = "CAR_WEIGHT";
    } else {
      payload = filterCarWeight(payload);
    }
    if (!property_damage_nt3) property_damage_nt3 = "TH_TSN3";
    if (!damage_nt3) damage_nt3 = "TH_N3";
    if (!seat) {
      seat = "CAR_SEAT";
    } else {
      seat = filterCarSeat(seat);
    }
    if (!responsibility_level) responsibility_level = "BH_HH";
    if (!goods) goods = "TTAI_TAN";
    if (!duration) {
      duration = "NUM_OF_DAY";
    } else {
      duration = parseInt(duration) * 365 + "";
    }
    return [
      {
        KEY_CODE: "TN_NTX",
        VAL_CODE: `${ins_money}`,
      },
      {
        KEY_CODE: "TN_TH_HK",
        VAL_CODE: `${vehicle_type}@${purpose}@${client_damage}`,
      },
      {
        KEY_CODE: "TN_TSN3_TTAI",
        VAL_CODE: `${vehicle_type}@${payload}@${property_damage_nt3}`,
      },
      {
        KEY_CODE: "TN_N3_TTAI",
        VAL_CODE: `${vehicle_type}@${payload}@${damage_nt3}`,
      },
      {
        KEY_CODE: "TN_N3_CNGOI",
        VAL_CODE: `${vehicle_type}@${seat}@${damage_nt3}`,
      },
      {
        KEY_CODE: "TN_TSN3_CNGOI",
        VAL_CODE: `${vehicle_type}@${seat}@${property_damage_nt3}`,
      },
      {
        KEY_CODE: "TN_TSN3_LXE",
        VAL_CODE: `${vehicle_type}@${property_damage_nt3}`,
      },
      {
        KEY_CODE: "TN_N3_LXE",
        VAL_CODE: `${vehicle_type}@${damage_nt3}`,
      },
      {
        KEY_CODE: "TN_BH_HH",
        VAL_CODE: `${responsibility_level}`,
      },
      {
        KEY_CODE: "TN_TTAI_HH",
        VAL_CODE: `${goods}`,
        TYPE_CODE: `CONSTANT`,
      },
      {
        KEY_CODE: "TN_TGBH",
        VAL_CODE: `${duration}`,
        TYPE_CODE: `CONSTANT`,
      },
      {
        KEY_CODE: "TN_CNGOI",
        VAL_CODE: `${seat}`,
      },
    ];
  };
  const onChangeVehicleGroup = (value) => {
    setVehicle_group(value);
    setPurpose("");
    setSeat("");
    setPayload("");
    setVehicle_type("");
  };
  const flagStateChanged = (state = {}, prevState) => {
    if (prevState) {
      for (const property in state) {
        if (state[property] != prevState[property]) return true;
      }
      return false;
    } else return true;
  };
  const getFee = () => {
    if (timeoutCalc) {
      clearTimeout(timeoutCalc);
    }
    setIsLoading(true);
    props.onCbTermChange(true);
    timeoutCalc = setTimeout(() => {
      calcFee(sdk_caculate_fees, sdk_caculate_fees_tndstn);
    }, 2000);
  };
  useEffect(() => {
    // uncheck tnds tu nguyen
    if (
      config.XE_MAY.includes(vehicle_group) &&
      duration &&
      vehicle_group &&
      vehicle_type &&
      seat
    ) {
      // xe may
      if (
        !isShowtnds &&
        prevState.damage_nt3 &&
        prevState.property_damage_nt3
      ) {
        getFee();
      }
      if (!isShowAccident && prevState.ins_money) {
        getFee();
      }
    }
    if (
      (vehicle_group &&
        vehicle_type &&
        purpose &&
        seat &&
        payload &&
        duration) ||
      (vehicle_group &&
        vehicle_type &&
        purpose &&
        seat &&
        ![...config.NGUOIHANG, ...config.XE_MAY].includes(vehicle_group) &&
        duration)
    ) {
      if (
        !isShowtnds &&
        ((prevState.damage_nt3 && prevState.property_damage_nt3) ||
          (prevState.damage_nt3 &&
            prevState.property_damage_nt3 &&
            prevState.client_damage))
      ) {
        getFee();
      }
      if (!isShowAccident && prevState.ins_money) {
        getFee();
      }
      if (
        !isShowtndsForGood &&
        prevState.goods &&
        prevState.responsibility_level
      ) {
        getFee();
      }
    }
  }, [isShowtnds, isShowAccident, isShowtndsForGood]);
  useEffect(() => {
    if (seat > 60) {
      //validate seat
      confirmAlert({
        title: "Thông báo",
        message: "Không hỗ trợ với các loại xe lớn hơn 60 chỗ",
        buttons: [
          {
            label: "Đóng",
            onClick: () => {},
          },
        ],
      });
    } else {
      if (
        config.XE_MAY.includes(vehicle_group) &&
        duration &&
        vehicle_group &&
        vehicle_type &&
        seat
      ) {
        // truong hop xe may
        getFee();
      }
      // 2 truong hop: cho nguoi va hang, cac loai oto con lai
      if (
        (vehicle_group &&
          vehicle_type &&
          purpose &&
          seat &&
          payload &&
          duration) ||
        (vehicle_group &&
          vehicle_type &&
          purpose &&
          seat &&
          ![...config.NGUOIHANG, ...config.XE_MAY].includes(vehicle_group) &&
          duration)
      ) {
        getFee();
      }
    }
  }, [vehicle_group, vehicle_type, purpose, seat, payload, duration]);
  useEffect(() => {
    if (
      config.XE_MAY.includes(vehicle_group) &&
      duration &&
      vehicle_group &&
      vehicle_type &&
      seat
    ) {
      // xe may
      if (isShowtnds || isShowAccident) {
        if (
          ((damage_nt3 && property_damage_nt3) ||
            (damage_nt3 && property_damage_nt3 && client_damage)) &&
          flagStateChanged(
            { damage_nt3, property_damage_nt3, client_damage },
            prevState
          )
        ) {
          getFee();
        }
        if (ins_money && flagStateChanged({ ins_money }, prevState)) {
          getFee();
        }
      }
    }
    if (
      (vehicle_group &&
        vehicle_type &&
        purpose &&
        seat &&
        payload &&
        duration) ||
      (vehicle_group &&
        vehicle_type &&
        purpose &&
        seat &&
        ![...config.NGUOIHANG, ...config.XE_MAY].includes(vehicle_group) &&
        duration)
    ) {
      if (isShowtnds || isShowAccident || isShowtndsForGood) {
        if (
          ((damage_nt3 && property_damage_nt3) ||
            (damage_nt3 && property_damage_nt3 && client_damage)) &&
          flagStateChanged(
            { damage_nt3, property_damage_nt3, client_damage },
            prevState
          )
        ) {
          getFee();
        }
        if (ins_money && flagStateChanged({ ins_money }, prevState)) {
          getFee();
        }
        if (
          goods &&
          responsibility_level &&
          flagStateChanged({ goods, responsibility_level }, prevState)
        ) {
          getFee();
        }
      }
    }
  }, [
    damage_nt3,
    property_damage_nt3,
    client_damage,
    ins_money,
    goods,
    responsibility_level,
  ]);
  const calcFee = async () => {
    sdk_caculate_fees = createObjCalcFees(
      vehicle_type,
      purpose,
      seat,
      payload,
      duration
    );
    sdk_caculate_fees_tndstn = createObjCalcFeesTndstn(
      vehicle_type,
      purpose,
      seat,
      payload,
      damage_nt3,
      property_damage_nt3,
      client_damage,
      ins_money,
      responsibility_level,
      goods,
      duration
    );
    // console.log("sdk_caculate_fees", sdk_caculate_fees);
    // console.log("sdk_caculate_fees_tndstn", sdk_caculate_fees_tndstn);
    try {
      const productInfor = [
        {
          CATEGORY: "XE",
          PRODUCT_CODE: "XCG_TNDSBB_NEW",
          STBH: "800000",
          DYNAMIC_FEES: sdk_caculate_fees,
        },
        {
          CATEGORY: "XE",
          PRODUCT_CODE: "XCG_TNDSTN",
          STBH: "",
          DYNAMIC_FEES: sdk_caculate_fees_tndstn,
        },
      ];

      const responseCalc = await api.post("/api/car/fee", productInfor);
      if (responseCalc) {
        // console.log("result", responseCalc);
        setTndsfee(
          responseCalc.PRODUCT_DETAIL.length > 0
            ? responseCalc.PRODUCT_DETAIL[0].TOTAL_AMOUNT
            : ""
        );
        setVoluntary_tnds_fee(
          responseCalc.PRODUCT_DETAIL.length > 0
            ? responseCalc.PRODUCT_DETAIL[1].TOTAL_AMOUNT
            : ""
        );
        props.calcTotalAmountListCar();
        props.onCbTermChange(false);
        setIsLoading(false);
      }
    } catch (e) {
      console.log(e);
      setIsLoading(false);
    }
  };
  const calcTotalAmount = () => {
    return (
      parseInt(tndsfee ? tndsfee : "0") +
      parseInt(voluntary_tnds_fee ? voluntary_tnds_fee : "0")
    );
  };
  const filterCar = (value_code) => {
    // filter loai xe chi tiet voi moi nhom xe
    if (props.listDefineCar.length > 0) {
      return props.listDefineCar[3].filter((l) => l.VH_GROUP_DT == value_code);
    } else {
      return [];
    }
  };

  const formatCurrency = (value) => {
    return currencyFormatter.format(value, {
      code: l.g("bhsk.currency"),
      precision: 0,
      format: "%v %s",
      symbol: l.g("bhsk.currency"),
    });
  };

  const getLabelInput = (arrData, value) => {
    let result = arrData.filter((obj) => {
      return obj.value === value;
    });
    if (result[0]) {
      return result[0].label;
    } else {
      return ""; // truong hop value truyen vao k co trong list data thi set mac dinh
    }
  };

  const getTypeNameInput = (arrData, value) => {
    let result = arrData.filter((obj) => {
      return obj.value === value;
    });
    if (result[0]) {
      return result[0].TYPE_NAME;
    } else {
      return ""; // truong hop value truyen vao k co trong list data thi set mac dinh
    }
  };
  const getData = () => {
    return {
      showInfor: {
        type: buyFor,
        name:
          buyFor == "CN"
            ? props.buyerInfor.NAME
            : buyFor == "DN"
            ? company_name
            : owner_name,
        phone:
          buyFor == "CN"
            ? props.buyerInfor.PHONE
            : buyFor == "DN"
            ? compan_phone
            : owner_phone,
        email:
          buyFor == "CN"
            ? props.buyerInfor.EMAIL
            : buyFor == "DN"
            ? companr_email
            : owner_email,
        vehicle_use: getLabelInput(
          props.listDefineCar.length > 0 ? props.listDefineCar[4] : [],
          purpose
        ),
        driver_insur: getTypeNameInput(
          props.listDefineCar.length > 0 ? props.listDefineCar[11] : [],
          ins_money
        ),
        vehicle_type: getLabelInput(
          vehicle_group ? filterCar(vehicle_group) : [],
          vehicle_type
        ),
        newCar: newCar,
        plateCar: plate,
        chassis_no: frame_number, //Số khung
        engine_no: vehicle_number, // so may
        tndsfee: tndsfee,
        voluntary_tnds_fee: voluntary_tnds_fee,
        duration: `${effective_time} ${effective_date} - ${end_time} ${end_date}`,
        totalAmount: calcTotalAmount(),
      },
      dataSubmit: {
        // data create order
        TYPE: buyFor, //
        NAME:
          buyFor == "CN"
            ? props.buyerInfor.NAME
            : buyFor == "DN"
            ? company_name
            : owner_name, // name cua nguoi khac, doanh nghiep
        DOB: buyFor == "CN" ? props.buyerInfor.DOB : "", // dob cua nguoi khac
        GENDER:
          buyFor == "CN"
            ? props.buyerInfor.GENDER
            : buyFor == "NK"
            ? gender
            : "", // gender cua nguoi khac
        PROV: props.buyerInfor.PROV,
        DIST: props.buyerInfor.DIST,
        WARDS: props.buyerInfor.WARDS,
        ADDRESS: props.buyerInfor.ADDRESS,
        IDCARD: props.buyerInfor.IDCARD,
        IDCARD_D: "",
        IDCARD_P: "",
        EMAIL:
          buyFor == "CN"
            ? props.buyerInfor.EMAIL
            : buyFor == "DN"
            ? companr_email
            : owner_email, // email nguoi khac, doanh nghiep
        PHONE:
          buyFor == "CN"
            ? props.buyerInfor.PHONE
            : buyFor == "DN"
            ? compan_phone
            : owner_phone, // phone nguoi khac, doanh nghiep
        FAX: "",
        TAXCODE: mst, // ma so thue doanh nghiep
        IS_SPLIT_ORDER: "0", //Tách đơn hay gộp đơn
        IS_SEND_MAIL: "0", // Gửi GCN qua email
        VEHICLE_GROUP: vehicle_group, //Nhóm xe
        VEHICLE_TYPE: vehicle_type, //Loại xe
        VEHICLE_USE: purpose, //Mục dích sử dụng
        NONE_NUMBER_PLATE: newCar ? "1" : "0", // 1: chưa có biển số --- 0: có biển số
        NUMBER_PLATE: plate, // bien so xe
        CHASSIS_NO: frame_number, //Số khung
        ENGINE_NO: vehicle_number, // Số máy
        SEAT_NO: seat, //Số chỗ ngồi
        SEAT_CODE: filterCarSeat(seat), //KEY VALUE SEAT khi filter
        WEIGH: payload, //Trọng tải
        WEIGH_CODE: payload ? filterCarWeight(payload) : "", //KEY VALUE WEIGH khi filter
        BRAND: "", //Hãng xe
        MODEL: "", //Hiệu xe
        MFG: "", //Năm sản xuất
        CAPACITY: "", //Dung tích

        REF_VEHICLE_VALUE: "", //Giá trị xe tham khao

        VEHICLE_REGIS: "", //Ngày đăng ký xe

        //Trách nhiệm dân sự bắt buộc
        COMPULSORY_CIVIL: {
          PRODUCT_CODE: props.config_define.PRODUCT_BB,
          PACK_CODE: props.config_define.PACK_CODE_BB,
          IS_SERIAL_NUM: "", // cấp giấy chứng nhận bản cứng
          SERIAL_NUM: "",
          EFF: effective_date,
          TIME_EFF: effective_time,
          EXP: end_date,
          TIME_EXP: end_time,
          AMOUNT: 0,
          VAT: 0,
          DISCOUNT: 0,
          DISCOUNT_UNIT: 0,
          TOTAL_DISCOUNT: 0,
          TOTAL_AMOUNT: 0,
        },

        //Trách nhiệm dân sự tự nguyện
        VOLUNTARY_CIVIL: {
          PRODUCT_CODE: props.config_define.PRODUCT_BB,
          PACK_CODE: props.config_define.PACK_CODE_TN,
          INSUR_3RD: damage_nt3, //Thiệt hại thân thể người thứ 3
          INSUR_3RD_ASSETS: property_damage_nt3, // Thiệt hại tài sản người thứ 3
          INSUR_PASSENGER: client_damage, // Thiệt hại thân thể hành khách
          TOTAL_LIABILITY: "0", //Tổng hạn mức trách nhiệm
          DRIVER_NUM: "0", //Số người TG(Bảo hiểm lái, phụ xe và người ngồi trên xe)
          DRIVER_INSUR: ins_money ? ins_money : "0", //Số tiền BH (Bảo hiểm lái, phụ xe và người ngồi trên xe)
          WEIGHT_CARGO: goods, //Trọng lượng hàng hóa (Bảo hiểm TNDS của chủ xe đối với hàng hóa)
          CARGO_INSUR: responsibility_level, // Số tiền bảo hiểm hàng hóa - mức trách nhiệm (Bảo hiểm TNDS của chủ xe đối với hàng hóa)
        },
      },
    };
  };
  useImperativeHandle(ref, () => ({
    handleSubmit() {
      const form = formRef.current;
      if (form.checkValidity() === false) {
        setValidated(true);
        return false;
      }
      return getData(); // return car data
    },
    calcTotalAmountCar() {
      return calcTotalAmount();
    },
  }));
  // onChange duration
  useEffect(() => {
    switch (duration) {
      case "1": {
        setEnd_date(
          moment(effective_date, "DD/MM/YYYY")
            .add(1, "years")
            .format("DD/MM/YYYY")
        );
        setEnd_time(effective_time);
        break;
      }
      case "2": {
        setEnd_date(
          moment(effective_date, "DD/MM/YYYY")
            .add(2, "years")
            .format("DD/MM/YYYY")
        );
        setEnd_time(effective_time);
        break;
      }
      case "3": {
        setEnd_date(
          moment(effective_date, "DD/MM/YYYY")
            .add(3, "years")
            .format("DD/MM/YYYY")
        );
        setEnd_time(effective_time);
        break;
      }
      default:
        break;
    }
  }, [duration, effective_date, effective_time]);

  return (
    <div className={`bb-form f1 ${styles.form_car}`}>
      {isLoading ? <LoadingForm /> : null}

      <Form
        className={styles.f_orm}
        ref={formRef}
        noValidate
        validated={validated}
      >
        <Row className={styles.jjksx}>
          <Col md={6}>
            <p className={styles.select_related}>
              <span>{`Xe được bảo hiểm ${props.indexCar + 1}`} </span>
            </p>
          </Col>
          <Col className={styles.delete_ngbd} md={6}>
            {props.isrcarlist.length != 1 ? (
              <a onClick={(e) => props.onDelete()}>
                {l.g("bhsk.form.delete_insured_person")}
              </a>
            ) : null}
          </Col>
        </Row>

        <Row>
          <Col md={12}>
            <div className="d-md-flex">
              <p className={styles.buy_for}>Bạn mua bảo hiểm cho</p>

              <div className="rd-group">
                <input
                  className="radio"
                  type="radio"
                  name="rd_car"
                  id={`rd-car-1-${props.idCar}`}
                  value={"CN"}
                  onChange={(e) => {
                    const target = e.target;
                    setBuyFor(target.value);
                  }}
                  checked={buyFor == "CN"}
                />
                <label htmlFor={`rd-car-1-${props.idCar}`}>Cá nhân</label>
                <div className="sp-hoz" />
                <input
                  className="radio"
                  type="radio"
                  name="rd_car"
                  id={`rd-car-2-${props.idCar}`}
                  value={"NK"}
                  onChange={(e) => {
                    const target = e.target;
                    setBuyFor(target.value);
                  }}
                  checked={buyFor == "NK"}
                />
                <label htmlFor={`rd-car-2-${props.idCar}`}>Người khác</label>
                <div className="sp-hoz" />
                <input
                  className="radio"
                  type="radio"
                  name="rd_car"
                  id={`rd-car-3-${props.idCar}`}
                  value={"DN"}
                  onChange={(e) => {
                    const target = e.target;
                    setBuyFor(target.value);
                  }}
                  checked={buyFor == "DN"}
                />
                <label htmlFor={`rd-car-3-${props.idCar}`}>Doanh nghiệp</label>
              </div>
            </div>
          </Col>
        </Row>
        {buyFor == "NK" ? (
          <Row>
            <Col md={3} className="mobile-mt">
              <FLSelect
                disable={false}
                label={"Danh xưng"}
                value={gender}
                changeEvent={setGender}
                dropdown={true}
                required={true}
                data={[
                  { label: l.g("bhsk.form.f3_title15"), value: "M" },
                  { label: l.g("bhsk.form.f3_title16"), value: "F" },
                ]}
              />
            </Col>
            <Col md={3} className="mobile-mt">
              <FLInput
                changeEvent={setOwner_name}
                isUpperCase={true}
                value={owner_name}
                label={"Tên chủ xe"}
                required={true}
              />
            </Col>
            <Col md={3} className="mobile-mt">
              <FLInput
                changeEvent={setOwner_phone}
                value={owner_phone}
                label={"Số điện thoại"}
                required={true}
              />
            </Col>
            <Col md={3} className="mobile-mt">
              <FLInput
                changeEvent={setOwner_email}
                value={owner_email}
                label={"Email"}
                required={false}
              />
            </Col>
            <div className="p-line mt-15" />
          </Row>
        ) : null}
        {buyFor == "DN" ? (
          <Row>
            <Col md={3} className="mobile-mt">
              <FLInput
                changeEvent={setCompany_name}
                value={company_name}
                isUpperCase={true}
                label={"Tên tổ chức/ DN"}
                required={true}
              />
            </Col>
            <Col md={3} className="mobile-mt">
              <FLInput
                changeEvent={setMst}
                value={mst}
                label={"Mã số thuế"}
                required={true}
              />
            </Col>
            <Col md={3} className="mobile-mt">
              <FLInput
                changeEvent={setCompany_phone}
                value={compan_phone}
                label={"Số điện thoại"}
                required={true}
              />
            </Col>
            <Col md={3} className="mobile-mt">
              <FLInput
                changeEvent={setCompany_email}
                value={companr_email}
                label={"Email"}
                required={true}
              />
            </Col>
            <div className="p-line mt-15" />
          </Row>
        ) : null}

        <Row className="">
          <Col md={3} className="mobile-mt">
            <FLSelect
              disable={false}
              label={"Nhóm xe"}
              value={vehicle_group}
              changeEvent={onChangeVehicleGroup}
              dropdown={true}
              required={true}
              data={
                props.listDefineCar.length > 0
                  ? filterVehicle(props.listDefineCar[2])
                  : []
              }
            />
          </Col>
          <Col md={3} className="mobile-mt">
            <FLSelect
              disable={false}
              label={"Loại xe"}
              value={vehicle_type}
              changeEvent={setVehicle_type}
              dropdown={true}
              required={true}
              data={vehicle_group ? filterCar(vehicle_group) : []}
            />
          </Col>
          {!config.XE_MAY.includes(vehicle_group) &&
          props.vehicleType == "car" ? (
            <Col md={3} className="mobile-mt">
              <FLSelect
                disable={false}
                label={"Mục đích sử dụng"}
                value={purpose}
                changeEvent={setPurpose}
                dropdown={true}
                required={true}
                data={
                  props.listDefineCar.length > 0 ? props.listDefineCar[4] : []
                }
              />
            </Col>
          ) : null}
          {!config.XE_MAY.includes(vehicle_group) ? (
            config.NGUOIHANG.includes(vehicle_group) ? (
              <Col md={3} className="mobile-mt">
                <div className={styles.group_ipnv}>
                  <FLInput
                    changeEvent={setSeat}
                    value={seat}
                    label={"Số chỗ ngồi"}
                    required={true}
                    hideborder={true}
                  />
                  <div className={styles.ver_line}></div>
                  {/* <div className="kkjs"> */}
                  <FLInput
                    changeEvent={setPayload}
                    value={payload}
                    label={"Trọng tải (Tấn)"}
                    required={true}
                    hideborder={true}
                  />
                  {/* </div> */}
                </div>
              </Col>
            ) : (
              <Col md={3} className="mobile-mt">
                <FLInput
                  changeEvent={setSeat}
                  value={seat}
                  label={"Số chỗ ngồi"}
                  required={true}
                />
              </Col>
            )
          ) : (
            <Col md={3} className="mobile-mt">
              <FLInput
                changeEvent={setSeat}
                value={seat}
                label={"Số chỗ ngồi"}
                required={true}
              />
            </Col>
          )}

          {/* {config.NGUOIHANG.includes(NHOM_XE) && (
            <Col md={3} className="mobile-mt">
              <FLInput
                changeEvent={setPayload}
                value={payload}
                label={"Trọng tải (Tấn)"}
                required={true}
              />
            </Col>
          )} */}
        </Row>

        <Row className="">
          <Col md={6} className="">
            <Row>
              <Col md={5} className="mobile-mt">
                <FLSelect
                  disable={false}
                  label={"Thời hạn HD"}
                  value={duration}
                  changeEvent={setDuration}
                  dropdown={true}
                  required={true}
                  data={[
                    { label: "1 năm", value: "1" },
                    { label: "2 năm", value: "2" },
                    { label: "3 năm", value: "3" },
                  ]}
                />
              </Col>
              <Col md={7} className="mobile-mt">
                <div className={styles.group_ipnv}>
                  <FLDate
                    disable={false}
                    changeEvent={setEffective_date}
                    label={"Ngày hiệu lực"}
                    required={true}
                    hideborder={true}
                    value={effective_date}
                    minimumDate={minDate()}
                  />
                  <div className={styles.ver_line}></div>
                  {/* <div className="kkjs"> */}
                  <FLTime
                    disable={false}
                    changeEvent={setEffective_time}
                    label={"Giờ hiệu lực"}
                    required={true}
                    hideborder={true}
                    value={effective_time}
                  />
                  {/* </div> */}
                </div>
              </Col>
            </Row>
          </Col>
          <Col md={6} className="">
            <Row>
              <Col md={7} className="mobile-mt">
                <div className={styles.group_ipnv}>
                  <FLDate
                    disable={true}
                    changeEvent={setEnd_date}
                    label={"Ngày kết thúc"}
                    required={true}
                    hideborder={true}
                    value={end_date}
                    leftPosition={true}
                  />
                  <div className={styles.ver_line}></div>
                  {/* <div className="kkjs"> */}
                  <FLTime
                    disable={true}
                    changeEvent={setEnd_time}
                    label={"Giờ kết thúc"}
                    required={true}
                    hideborder={true}
                    value={end_time}
                    rightPosition={true}
                  />
                  {/* </div> */}
                </div>
              </Col>
              <Col md={5} className="mobile-mt">
                <FLInput
                  disable={true}
                  readonly={true}
                  changeEvent={setTndsfee}
                  value={formatCurrency(tndsfee)}
                  label={"Phí BH TNDS bắt buộc"}
                  required={false}
                />
              </Col>
            </Row>
          </Col>
        </Row>
        <Row className="">
          <Col md={3} className="mobile-mt d-flex align-items-center">
            <label className="form-switch ">
              <input
                type="checkbox"
                name={"newCar"}
                checked={newCar}
                onChange={(e) => {
                  const target = e.target;
                  setNewCar(target.checked);
                }}
              />
              <i></i> Xe mới chưa có biển số
            </label>
          </Col>
          <Col md={3} className="mobile-mt">
            <FLInput
              changeEvent={setPlate}
              value={plate}
              label={"Biển số"}
              required={!newCar}
              isUpperCase={true}
            />
          </Col>
          <Col md={3} className="mobile-mt">
            <FLInput
              changeEvent={setFrame_number}
              value={frame_number}
              label={"Số khung"}
              required={newCar}
            />
          </Col>
          <Col md={3} className="mobile-mt">
            <FLInput
              changeEvent={setVehicle_number}
              value={vehicle_number}
              label={"Số máy"}
              required={newCar}
            />
          </Col>
        </Row>
        <Row className="">
          <Col md={12} className="mobile-mt">
            <div>
              <input
                className="checkbox"
                type="checkbox"
                id={`checkbox-car-tnds-${props.idCar}`}
                onChange={(e) => {
                  const target = e.target;
                  setIsShowtnds(target.checked);
                  if (!target.checked) {
                    setDamage_nt3("");
                    setProperty_damage_nt3("");
                    setClient_damage("");
                  }
                }}
                checked={isShowtnds}
              />
              <label htmlFor={`checkbox-car-tnds-${props.idCar}`}>
                {"Bảo hiểm TNDS tự nguyện (vượt quá mức TNDS bắt buộc)"}
              </label>
            </div>
          </Col>
        </Row>
        {isShowtnds ? (
          <Row>
            <Col md={3} className="mobile-mt">
              <FLSelect
                disable={false}
                label={"Thiệt hại thân thể người thứ 3"}
                value={damage_nt3}
                changeEvent={setDamage_nt3}
                dropdown={true}
                required={true}
                data={
                  props.listDefineCar.length > 0 ? props.listDefineCar[7] : []
                }
              />
            </Col>
            <Col md={3} className="mobile-mt">
              <FLSelect
                disable={false}
                label={"Thiệt hại tài sản người thứ 3"}
                value={property_damage_nt3}
                changeEvent={setProperty_damage_nt3}
                dropdown={true}
                required={true}
                data={
                  props.listDefineCar.length > 0 ? props.listDefineCar[9] : []
                }
              />
            </Col>
            {config.MUC_DICH_USE.includes(purpose) &&
            config.THIET_HAI_THAN_THE_HK.includes(vehicle_group) ? (
              <Col md={3} className="mobile-mt">
                <FLSelect
                  disable={false}
                  label={"Thiệt hại thân thể hành khách"}
                  value={client_damage}
                  changeEvent={setClient_damage}
                  dropdown={true}
                  required={true}
                  data={
                    props.listDefineCar.length > 0 ? props.listDefineCar[8] : []
                  }
                />
              </Col>
            ) : null}

            <Col md={3} className="mobile-mt">
              <FLInput
                changeEvent={setVoluntary_tnds_fee}
                value={formatCurrency(voluntary_tnds_fee)}
                label={"Phí BH TNDS tự nguyện"}
                required={false}
                disable={true}
                readonly={true}
              />
            </Col>
          </Row>
        ) : null}

        <Row className="">
          <Col md={12} className="mobile-mt">
            <div>
              <input
                className="checkbox"
                type="checkbox"
                id={`checkbox-car-accident-${props.idCar}`}
                onChange={(e) => {
                  const target = e.target;
                  setIsShowAccident(target.checked);
                  if (!target.checked) {
                    setIns_money("");
                  }
                }}
                checked={isShowAccident}
              />
              <label htmlFor={`checkbox-car-accident-${props.idCar}`}>
                {"Bảo hiểm tai nạn lái xe, phụ xe, người ngồi trên xe"}
              </label>
            </div>
          </Col>
        </Row>
        {isShowAccident ? (
          <Row>
            <Col md={3} className="mobile-mt">
              <FLSelect
                disable={false}
                label={"Số tiền bảo hiểm"}
                value={ins_money}
                changeEvent={setIns_money}
                dropdown={true}
                required={true}
                data={
                  props.listDefineCar.length > 0 ? props.listDefineCar[11] : []
                }
              />
            </Col>
            {/* 
            <Col md={3}>
              <FLInput
                changeEvent={setIns_fee_lx}
                value={ins_fee_lx}
                label={"Phí BH tai nạn lái xe, phụ xe, người ngồi ..."}
                required={true}
                disable={true}
                readonly={true}
              />
            </Col> */}
          </Row>
        ) : null}
        {config.NGUOIHANG.includes(vehicle_group) && (
          <Row className="">
            <Col md={12} className="mobile-mt">
              <div>
                <input
                  className="checkbox"
                  type="checkbox"
                  id={`checkbox-car-tnds-for-good-${props.idCar}`}
                  onChange={(e) => {
                    const target = e.target;
                    setIsShowtndsForGood(target.checked);
                    if (!target.checked) {
                      setGoods("");
                      setResponsibility_level("");
                    }
                  }}
                  checked={isShowtndsForGood}
                />
                <label htmlFor={`checkbox-car-tnds-for-good-${props.idCar}`}>
                  {"Bảo hiểm TNDS của chủ xe đối với hàng hóa"}
                </label>
              </div>
            </Col>
          </Row>
        )}

        {isShowtndsForGood && config.NGUOIHANG.includes(vehicle_group) && (
          <Row>
            <Col md={3} className="mobile-mt">
              <FLInput
                changeEvent={setGoods}
                value={goods}
                label={"Hàng hóa được BH (Tấn)"}
                required={true}
              />
            </Col>
            <Col md={3} className="mobile-mt">
              <FLSelect
                disable={false}
                label={"Mức trách nhiệm"}
                value={responsibility_level}
                changeEvent={setResponsibility_level}
                dropdown={true}
                required={true}
                data={
                  props.listDefineCar.length > 0 ? props.listDefineCar[12] : []
                }
              />
            </Col>

            {/* <Col md={3}>
              <FLInput
                changeEvent={setIns_gooods_fee}
                value={ins_goods_fee}
                label={"Phí BH TNDS của chủ xe với hàng hóa"}
                required={true}
                disable={true}
              />
            </Col> */}
          </Row>
        )}
      </Form>
    </div>
  );
});
const mapStateToProps = (state) => {
  return {
    isrcarlist: state.isrcarlist,
    listDefineCar: state.listDefineCar,
    buyerInfor: state.buyerInfor,
  };
};

export default connect(mapStateToProps, null, null, { forwardRef: true })(
  CarInfo
);
