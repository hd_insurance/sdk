import React, { useEffect, useState, createRef } from "react";
import Router from "next/router";
import styles from "../../../css/style.module.css";

const Form4 = (props) => {
  return (
    <div className="bb-form f4">
      <div className={styles.form_register_success}>
      <div className={styles.img_regster_suc}>
          <img src={"/img/img_register_success.png"} />
        </div>
        <div className={styles.content_regis_success}>
          <p>{l.g("bhsk.form.f4_label1")}</p>
          <p>
            {`Công ty TNHH Bảo hiểm HD (HDI) chân thành cảm ơn Quý khách đã tín nhiệm và sử dụng sản phẩm bảo hiểm của Công ty chúng tôi. Chi tiết đơn bảo hiểm sẽ được gửi đến email hoặc tin nhắn của bạn trong giây lát.
Xin cảm ơn`}
          </p>
        </div>
        <div>
          <button
            className={styles.btn_discover_insur}
            onClick={() => Router.push("https://www.hdinsurance.com.vn/")}
          >
            {l.g("bhsk.form.f4_btn1")}
          </button>
          <button
            className={styles.btn_back_home}
            onClick={() =>
              Router.push("https://www.hdinsurance.com.vn/")
            }
          >
            {l.g("bhsk.form.f4_btn2")}
          </button>
        </div>
      </div>
    </div>
  );
};

export default Form4;
