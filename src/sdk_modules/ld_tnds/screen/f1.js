import React, {
  useEffect,
  useState,
  createRef,
  forwardRef,
  useImperativeHandle,
} from "react";
import TopTitle from "../../../common/toptitle";
import { connect } from "react-redux";
import { setBuyerInfor } from "../../../redux/actions/action-car";
import moment from "moment";
import { Button, Col, Row, Form } from "react-bootstrap";
import { FLInput, FLDate, FLAddress, FLSelect } from "hdi-uikit";

const Form1 = forwardRef((props, ref) => {
  const formRef = createRef();
  const [validated, setValidated] = useState(false);

  const [sdk_gender, setSdkGender] = useState("");
  const [sdk_name, setSdkName] = useState("");

  const [sdk_phonenumber, setSdkPhonenumber] = useState("");
  const [sdk_email, setSdkEmail] = useState("");
  const [sdk_passport, setSdkPassport] = useState("");
  const [sdk_dob, setSdkDob] = useState(
    moment().subtract(18, "years").format("DD/MM/YYYY")
  );
  const [sdk_address, setSdkAddress] = useState("");
  const [sdk_addressCode, setAntAddressCodeValue] = useState({
    label: "",
    prov: "",
    dist: "",
    ward: "",
  });
  const [sdk_vat, setSdkVat] = useState(false);
  const [sdk_mst, setSdkMst] = useState("");
  const [sdk_companyName, setSdkCompanyName] = useState("");

  const createBuyer = () => {
    return {
      TYPE: "CN", // mac dinh
      NAME: sdk_name, // bat buoc
      DOB: sdk_dob,
      GENDER: sdk_gender,
      ADDRESS: sdk_address,
      IDCARD: sdk_passport,
      EMAIL: sdk_email,
      PHONE: sdk_phonenumber,
      PROV: sdk_addressCode.prov,
      DIST: sdk_addressCode.dist,
      WARDS: sdk_addressCode.ward,
      IDCARD_D: "",
      IDCARD_P: "",
      FAX: "",
      TAXCODE: sdk_mst,
    };
  };
  useImperativeHandle(ref, () => ({
    handleSubmit() {
      const form = formRef.current;
      if (form.checkValidity() === false) {
        setValidated(true);
        return false;
      } else {
        props.setBuyerInfor(createBuyer());
        return true;
      }
    },
  }));
  const maximumDate = () => {
    const now = moment();
    return {
      year: now.year() - 18,
      month: now.month() + 1,
      day: now.date(),
    };
  };
  const onRdChange = (e) => {
    if (e.target.checked) {
      setMode(e.target.value);
    }
  };

  return (
    <div className="indemnify-form f1">
      <TopTitle
        title={"Thông tin người mua"}
        subtitle={"Hãy điền thông tin người mua để tiến hành mua bảo hiểm"}
      />

      <Form className="f-orm" ref={formRef} noValidate validated={validated}>
        <Row className="declaration-form">
          <div className="col-md-4 mt-15">
            <FLSelect
              disable={false}
              label={"Danh xưng"}
              value={sdk_gender}
              changeEvent={setSdkGender}
              dropdown={true}
              required={true}
              data={
                props.listDefineCar.length > 0 ? props.listDefineCar[0] : []
              }
            />
          </div>
          <div className="col-md-4 mt-15">
            <FLInput
              disable={false}
              value={sdk_name}
              changeEvent={setSdkName}
              isUpperCase={true}
              label={"Họ tên"}
              required={true}
            />
          </div>

          <div className="col-md-4 mt-15">
            <FLInput
              changeEvent={setSdkPhonenumber}
              value={sdk_phonenumber}
              label={"Số điện thoại"}
              pattern="[0-9]{9,12}"
              required={true}
            />
          </div>

          <div className="col-md-4 mt-15">
            <FLInput
              label={"Email"}
              pattern={`[a-z0-9._%+-]+@+[a-z0-9._%+-]+`}
              changeEvent={setSdkEmail}
              value={sdk_email}
              required={true}
            />
          </div>
          <div className="col-md-4 mt-15">
            <FLInput
              label={"Số CMND/ CCCD/ HC"}
              // pattern="[0-9]{9,22}"
              changeEvent={setSdkPassport}
              value={sdk_passport}
              required={false}
            />
          </div>
          <div className="col-md-4 mt-15">
            <FLDate
              disable={false}
              value={sdk_dob}
              changeEvent={setSdkDob}
              label={"Ngày sinh"}
              required={false}
              maximumDate={maximumDate()}
            />
          </div>

          <div className="col-md-12 mt-15">
            <div className="group-ipnv ">
              <FLInput
                required={true}
                value={sdk_address}
                changeEvent={setSdkAddress}
                label={l.g("bhsk.form.street")}
                hideborder={true}
              />
              <div className="ver-line"></div>
              <div className="kkjs">
                <FLAddress
                  disable={false}
                  changeEvent={setAntAddressCodeValue}
                  value={sdk_addressCode}
                  label={l.g("bhsk.form.address")}
                  required={true}
                  hideborder={true}
                />
              </div>
            </div>
          </div>
        </Row>
        {props.type == "visa" ? null : (
          <Row className="mt-15">
            <Col md={12} className=" d-flex align-items-center">
              <label className="form-switch ">
                <input
                  type="checkbox"
                  name={"newCar"}
                  checked={sdk_vat}
                  onChange={(e) => {
                    const target = e.target;
                    setSdkVat(target.checked);
                  }}
                />
                <i></i> Lấy hóa đơn VAT cho doanh nghiệp
              </label>
            </Col>
          </Row>
        )}

        {sdk_vat ? (
          <Row className="mt-15">
            <Col md={3}>
              <FLInput
                disable={false}
                value={sdk_mst}
                changeEvent={setSdkMst}
                isUpperCase={true}
                label={"Mã số thuế công ty "}
                required={true}
              />
            </Col>
            <Col md={3}>
              <FLInput
                disable={false}
                value={sdk_companyName}
                changeEvent={setSdkCompanyName}
                isUpperCase={true}
                label={"Tên công ty "}
                required={true}
              />
            </Col>
          </Row>
        ) : null}
      </Form>
    </div>
  );
});

const mapStateToProps = (state) => {
  return {
    listDefineCar: state.listDefineCar,
  };
};
const mapDispatchToProps = (dispatch) => ({
  setBuyerInfor: (infor) => dispatch(setBuyerInfor(infor)),
});

export default connect(mapStateToProps, mapDispatchToProps, null, {
  forwardRef: true,
})(Form1);
