export const config = {
  NGUOIHANG: ["XE_OTO_NGUOIHANG", "XE_OTO_CHOHANG"],
  XE_MAY: ["XE_MAY", "XE_MAYCD"],
  THIET_HAI_THAN_THE_HK: ['XE_OTO_CHONGUOI','XE_OTO_NGUOIHANG'],
  MUC_DICH_USE: ['KD'],
  O_TO: ["XE_OTO_CHOHANG", "XE_OTO_CHONGUOI", "XE_OTO_NGUOIHANG", "XE_OTO_CDUNG"]
};

export const caculate_fees = [
  {
    KEY_CODE: "BBPLA_LXE",
    VAL_CODE: "VH_TYPE",
  },
  {
    KEY_CODE: "BBPLA_TTAI",
    VAL_CODE: "VH_TYPE@CAR_WEIGHT",
  },
  {
    KEY_CODE: "BBPLA_CNGOI",
    VAL_CODE: "VH_TYPE@CAR_SEAT",
  },
  {
    KEY_CODE: "BBPLA_MDKD",
    VAL_CODE: "VH_TYPE@CAR_PURPOSE",
  },
  {
    KEY_CODE: "BBPLA_MDKD_CNGOI",
    VAL_CODE: "VH_TYPE@CAR_PURPOSE@CAR_SEAT",
  },
  {
    KEY_CODE: "HS_T30D",
    VAL_CODE: "0",
    TYPE_CODE: "CONSTANT",
  },
  {
    KEY_CODE: "HS_D30D",
    VAL_CODE: "0",
    TYPE_CODE: "CONSTANT",
  },
];

export const caculate_fees_tndstn = [
  {
    KEY_CODE: "TN_NTX",
    VAL_CODE: "BH_NTX",
  },
  {
    KEY_CODE: "TN_TH_HK",
    VAL_CODE: "VH_TYPE@CAR_PURPOSE@TH_HK",
  },
  {
    KEY_CODE: "TN_TSN3_TTAI",
    VAL_CODE: "VH_TYPE@CAR_WEIGHT@TH_TSN3",
  },
  {
    KEY_CODE: "TN_N3_TTAI",
    VAL_CODE: "VH_TYPE@CAR_WEIGHT@TH_N3",
  },
  {
    KEY_CODE: "TN_N3_CNGOI",
    VAL_CODE: "VH_TYPE@CAR_SEAT@TH_N3",
  },
  {
    KEY_CODE: "TN_TSN3_CNGOI",
    VAL_CODE: "VH_TYPE@CAR_SEAT@TH_TSN3",
  },
  {
    KEY_CODE: "TN_TSN3_LXE",
    VAL_CODE: "VH_TYPE@TH_TSN3",
  },
  {
    KEY_CODE: "TN_N3_LXE",
    VAL_CODE: "VH_TYPE@TH_N3",
  },
  {
    KEY_CODE: "TN_BH_HH",
    VAL_CODE: "BH_HH",
  },
  {
    KEY_CODE: "TN_TTAI_HH",
    VAL_CODE: "TTAI_TAN",
    TYPE_CODE: "CONSTANT",
  },
  {
    KEY_CODE: "TN_TGBH",
    VAL_CODE: "NUM_OF_DAY",
    TYPE_CODE: "CONSTANT",
  },
  {
    KEY_CODE: "TN_CNGOI",
    VAL_CODE: "CAR_SEAT",
  },
];
