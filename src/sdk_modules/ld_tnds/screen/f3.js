import React, {
  useEffect,
  useState,
  createRef,
  forwardRef,
  useImperativeHandle,
} from "react";
import router from "next/router";
import { Button, Col, Row, Popover, Form } from "react-bootstrap";
import randomstring from "randomstring";
import moment from "moment";
import { connect } from "react-redux";
import Overlay from "react-bootstrap/Overlay";
import TopTitle from "../../../common/toptitle";
import cogoToast from "cogo-toast";
import api from "../../../services/Network.js";
import LoadingForm from "../../../common/loadingform";
import currencyFormatter from "currency-formatter";
import { StageSpinner } from "react-spinners-kit";
import { confirmAlert } from "react-confirm-alert";
import styles from "../../../css/style.module.css";

const Form3 = forwardRef((props, ref) => {
  const refPreview = createRef(null);

  const [validatedVoucher, setValidatedVoucher] = useState(false);
  const [showPreview, setShowPreview] = useState(false);

  const [isVoucher, setIsVoucher] = useState(props.voucherInfo != null);

  const [target, setTarget] = useState(null);
  const [totalAmount, setTotalAmount] = useState(0);
  const [discountAmount, setDiscountAmount] = useState(0);

  const [voucherLoading, setVoucherLoading] = useState(false);
  const [voucher, setVoucher] = useState(
    props.voucherInfo ? props.voucherInfo.ACTIVATION_CODE : ""
  );
  const [voucherInfo, setVoucherInfo] = useState(props.voucherInfo);
  const [commit, setCommit] = useState(false);
  const [curentCarPreview, setCurrentCarPreview] = useState({});
  const [curentPrev, setCurentPrev] = useState({});
  const [isLoading, setIsLoading] = useState(false);
  const [showGCN, setShowGCN] = useState(true);

  const onEditClick = (carId) => {
    props.onEditClick(carId);
  };

  const handClickPreview = (event, index) => {
    setCurentPrev(props.isrcarlist[index]);
    setCurrentCarPreview(props.isrcarlist[index].showInfor);
    setShowPreview(!showPreview);
    setTarget(event.target);
  };

  const removeVoucher = () => {
    confirmAlert({
      title: "Xác nhận",
      message: "Bạn có chắc chắn muốn bỏ áp dụng mã giảm giá?",
      buttons: [
        {
          label: "Không",
          onClick: () => {
            setValidatedVoucher(false);
          },
        },
        {
          label: "Bỏ áp dụng",
          onClick: () => {
            setValidatedVoucher(false);
            setVoucherInfo(null);
            setVoucher("");
            rmvcl();
          },
        },
      ],
    });
  };

  const handleSubmitVoucher = async (event) => {
    try {
      const form = event.currentTarget;

      event.preventDefault();
      event.stopPropagation();
      setValidatedVoucher(true);
      if (form.checkValidity() === false) {
        return false;
      }
      setVoucherLoading(true);

      var browser_code = randomstring.generate(9);
      if (typeof window !== "undefined") {
        if (localStorage.getItem("browser_code")) {
          browser_code = localStorage.getItem("browser_code");
        } else {
          browser_code = randomstring.generate(9);
        }
      }
      const response = await api.post("/api/bhsk/vouchervalid", {
        code: voucher,
        org:
          props.org == "VIETJET_VN" ? "REGIS_STAFF_VJ" : "REGIS_STAFF_HDBANK",
        browser_code: browser_code,
      });
      setVoucherLoading(false);
      if (response.success) {
        if (typeof window !== "undefined") {
          localStorage.setItem("browser_code", browser_code);
        }

        setVoucherInfo(response.data[0][0]);
      } else {
        cogoToast.error(response.error_message);
      }
    } catch (e) {
      setVoucherLoading(false);
      console.log(e);
    }
  };

  const formatCurrency = (value) => {
    return currencyFormatter.format(value, {
      code: l.g("bhsk.currency"),
      precision: 0,
      format: "%v %s",
      symbol: l.g("bhsk.currency"),
    });
  };
  const formatNumber = (value) => {
    return currencyFormatter.format(value, {
      thousand: ".",
      precision: 0,
    });
  };
  const calcTotalAmountListCar = () => {
    let total = 0;
    props.isrcarlist.map((car, i) => {
      total += car.ref.current.calcTotalAmountCar();
    });
    setTotalAmount(total);
  };
  useEffect(() => {
    calcTotalAmountListCar();
  }, [props.isrcarlist]);
  useImperativeHandle(ref, () => ({
    uncheckCommit() {
      setCommit(false);
    },
    createOrder: async () => {
      let arrCar = [];
      props.isrcarlist.map((car, i) => {
        arrCar.push(car.dataSubmit);
      });
      const data = {
        ORG_CODE: props.config_define.ORG_CODE,
        CHANNEL: props.config_define.CHANNEL,
        USERNAME: props.config_define.USERNAME,
        ACTION: props.config_define.ACTION,
        VEHICLE_INSUR: arrCar, //arrCar
        BUYER: props.buyerInfor,
        PAY_INFO: {
          PAYMENT_TYPE: "CTT",
          IS_SMS: "0",
          IS_EMAIL: "0",
          PAYMENT_MIX: null,
        },
      };
      console.log(data)
      try {
        setIsLoading(true);
        const response = await api.post("/api/car/create-order", data);
        // console.log("data create order: ", data);
        if (response.Success) {
          
          // console.log(response);
          return response.Data.url_redirect;
        } else {
          if (response.Error) {
            setCommit(false);
            setIsLoading(false);
            cogoToast.error(response.ErrorMessage);
            return false;
          } else {
            setCommit(false);
            setIsLoading(false);
            cogoToast.error("Hệ thống đang bận!");
            return false;
          }
        }
      } catch (error) {
        setCommit(false);
        setIsLoading(false);
        cogoToast.error("Hệ thống đang bận!");
        return false;
      }
    },
  }));

  const previewGCN = async () => {
    try {
      const data = {
        PRODUCT_CODE: "XCG_TNDSBB_NEW",
        TEMP_CODE: "XCG_TNDSBB_VIEW",
        PACK_CODE: "TNDSBB",
        DATA_TYPE: "JSON",
        JSON_DATA: {
          NAME: curentPrev.dataSubmit.NAME,
          ADDRESS: curentPrev.dataSubmit.ADDRESS,
          PHONE: curentPrev.dataSubmit.PHONE,
          EMAIL: curentPrev.dataSubmit.EMAIL,
          CERTIFICATE_NO: "", // k co
          DOB: curentPrev.dataSubmit.DOB,
          IDCARD: curentPrev.dataSubmit.IDCARD,
          PRINCIPAL: "", // k co
          EFFECTIVE_DATE: curentPrev.dataSubmit.COMPULSORY_CIVIL.EFF,
          EXPIRATION_DATE: curentPrev.dataSubmit.COMPULSORY_CIVIL.EXP,
          PRODUCT_CODE: "XCG_TNDSBB_NEW",
          AMOUNT: formatNumber(curentPrev.showInfor.tndsfee),
          TOTAL_AMOUNT: formatNumber(curentPrev.showInfor.totalAmount),
          MONEY: "", //  k co
          NAME_CTY: "", // k co
          ADDRESS_CTY: "", // k co
          REGION: "", // k co
          BOI_THUONG: "",
          NGAY_CAP: moment().format("DD/MM/YYYY"),
          EFFECTIVE_DATE_EN: "", // k co
          EXPIRATION_DATE_EN: "", // k co
          REGION_VI: "",
          REGION_EN: "",
          DOI_TUONG: "",
          HH_TO: curentPrev.dataSubmit.COMPULSORY_CIVIL.TIME_EFF.split(":")[0],
          SS_TO: curentPrev.dataSubmit.COMPULSORY_CIVIL.TIME_EFF.split(":")[1],
          DAY_TO: "",
          MM_TO: "",
          YY_TO: "",
          HH_F: curentPrev.dataSubmit.COMPULSORY_CIVIL.TIME_EXP.split(":")[0],
          SS_F: curentPrev.dataSubmit.COMPULSORY_CIVIL.TIME_EXP.split(":")[1],
          DAY_F: "",
          MM_F: "",
          YY_F: "",
          NUMBER_PLATE: curentPrev.dataSubmit.NUMBER_PLATE,
          CHASSIS_NO: curentPrev.dataSubmit.CHASSIS_NO,
          ENGINE_NO: curentPrev.dataSubmit.ENGINE_NO,
          MFG: "",
          VEHICLE_TYPE: curentPrev.showInfor.vehicle_type,
          SEAT_NO: curentPrev.dataSubmit.SEAT_NO,
          WEIGH: curentPrev.dataSubmit.WEIGH,
          VEHICLE_USE: curentPrev.showInfor.vehicle_use,
          INSUR_3RD: "",
          INSUR_PASSENGER: "",
          INSUR_3RD_ASSETS: "",
          TOTAL_LIABILITY: "",
          DRIVER_NUM: "",
          DRIVER_INSUR: formatNumber(curentPrev.showInfor.driver_insur),
          VEHICLE_GROUP: "",
          AMOUNT_VOLUNTARY_CIVIL: "",
          TOTAL_VOLUNTARY_CIVIL: formatNumber(
            curentPrev.showInfor.voluntary_tnds_fee
          ),
        },
      };
      props.onCbTermChange(true); // on off nut Xac nhan
      setShowPreview(false);
      setIsLoading(true);
      const response = await api.post("/api/car/preview-gcn", data);
      if (response.Success) {
        setIsLoading(false);
        props.onCbTermChange(commit ? false : true); // on off nut Xac nhan
        const data = response.Data.FILE_BASE64;
        const pdfWindow = window.open("");
        pdfWindow.document.write(
          "<iframe frameborder='0' style='border:0; top:0px; left:0px; bottom:0px; right:0px; position: absolute;' width='100%' height='100%' src='data:application/pdf;base64, " +
            encodeURI(data) +
            "'></iframe>"
        );
        // console.log(response);
      } else {
        if (response.Error) {
          props.onCbTermChange(commit ? false : true);
          setIsLoading(false);
          cogoToast.error(response.ErrorMessage);
        } else {
          props.onCbTermChange(commit ? false : true);
          setIsLoading(false);
          cogoToast.error("Hệ thống đang bận!");
        }
      }
    } catch (error) {
      console.log(error);
      props.onCbTermChange(commit ? false : true);
      setIsLoading(false);
      cogoToast.error("Hệ thống đang bận!");
    }
  };

  return (
    <div className="bb-form f3">
      <TopTitle title={l.g("bhsk.form.lbbl_title_3")} />
      <Row>
        {isLoading ? <LoadingForm /> : null}
        <Col md={6}>
          <p
            className={`${styles.title_content_ndbh} ${styles.mobile_mt}`}
          >{`Để xem trước bản thể hiện giấy chứng nhận vui lòng ấn vào “Xem” trong từng cá nhân`}</p>
          <div className={styles.form_list_ndbh}>
            {props.isrcarlist.map((e, i) => {
              const carInfor = e.showInfor ? e.showInfor : {};
              return (
                <div key={i} className={styles.item_list_ndbh}>
                  <div className="row">
                    <div className="col-md-8">
                      <div className={styles.title_item_ndbh}>
                        <div className={styles.mname}>
                          <label>{i + 1}.</label>
                          <label>
                            {carInfor.newCar
                              ? carInfor.chassis_no
                              : carInfor.plateCar}
                          </label>
                        </div>

                        <label className={styles.am}>
                          {formatCurrency(carInfor.totalAmount)}
                        </label>
                      </div>
                      <div className={styles.name_package_insurance}>
                        {`Bảo hiểm TNDS (${carInfor.duration})`}
                      </div>
                    </div>
                    <div className={`col-md-4 ${styles.btn_action_item}`}>
                      <button
                        className={styles.preview_item_ndbh}
                        onClick={(event) => handClickPreview(event, i)}
                      >
                        <i className={`${styles.fas} ${styles.fa_eye}`}></i>
                        <label>{l.g("bhsk.form.label_view")}</label>
                      </button>
                      <button
                        className={styles.edit_item_ndbh}
                        onClick={() => onEditClick(e.id)}
                      >
                        <i className={`${styles.fas} ${styles.fa_edit}`}></i>
                        <label>{l.g("bhsk.form.label_edit")}</label>
                      </button>
                    </div>
                  </div>
                </div>
              );
            })}
          </div>
        </Col>
        <Col md={6}>
          <div className={`${styles.form_voucher} ${styles.mobile_mt}`}>
            {voucherInfo ? (
              <div
                className={`${styles.vc_info} ${styles.animate__animated} ${styles.animate__fadeIn}`}
              >
                <div className={styles.gift_icon}>
                  <i className={`${styles.fas} ${styles.fa_gift}`}></i>
                </div>
                <div className={styles.g_info}>
                  <h4>{voucherInfo.PRODUCT_NAME}</h4>
                  <div className={styles.g_pack}>{voucherInfo.PACK_NAME}</div>
                  <div className={styles.g_discount}>
                    DISCOUNT{" "}
                    <span>
                      -{voucherInfo.DISCOUNT}
                      {voucherInfo.DISCOUNT_UNIT == "P"
                        ? "%"
                        : l.g("bhsk.currency")}
                    </span>
                  </div>
                </div>
                {router.router.query.id && isVoucher ? null : (
                  <div className="remove-voucher">
                    <i
                      onClick={(e) => removeVoucher()}
                      className={`${styles.far} ${styles.fa_times_circle}`}
                    ></i>
                  </div>
                )}
              </div>
            ) : (
              <Form
                noValidate
                validated={validatedVoucher}
                onSubmit={handleSubmitVoucher}
              >
                <div className={styles.vcvc}>
                  <Form.Control
                    className={styles.hqk_custom_input}
                    type="text"
                    placeholder={l.g("bhsk.form.f3_title7")}
                    aria-describedby="inputGroupPrepend"
                    value={voucher}
                    onChange={(e) => setVoucher(e.target.value)}
                    required
                  />
                  <Button type="submit" disabled={voucherLoading}>
                    {voucherLoading ? (
                      <div className={styles.loading_voucher}>
                        <StageSpinner />
                      </div>
                    ) : (
                      l.g("bhsk.form.f3_title8")
                    )}
                  </Button>
                </div>
              </Form>
            )}
          </div>

          <div className={styles.form_total_payment}>
            <div className={styles.total_cost_insur}>
              <label>{`Phí bảo hiểm`}:</label>
              <label>
                {currencyFormatter.format(totalAmount, {
                  code: l.g("bhsk.currency"),
                  precision: 0,
                  format: "%v %s",
                  symbol: l.g("bhsk.currency"),
                })}
              </label>
            </div>
            <div className={styles.total_cost_insur}>
              <label>{l.g("bhsk.form.f3_title3")}:</label>
              <label>
                -
                {currencyFormatter.format(discountAmount, {
                  code: l.g("bhsk.currency"),
                  precision: 0,
                  format: "%v %s",
                  symbol: l.g("bhsk.currency"),
                })}
              </label>
            </div>
            <div className={styles.total_payment}>
              <label>{l.g("bhsk.form.f3_title4")}:</label>
              <label>
                {currencyFormatter.format(totalAmount - discountAmount, {
                  code: l.g("bhsk.currency"),
                  precision: 0,
                  format: "%v %s",
                  symbol: l.g("bhsk.currency"),
                })}
              </label>
            </div>
          </div>

          <div className={`mt-15 ${styles.confirm_terms}`}>
            <input
              className={styles.checkbox}
              type="checkbox"
              id="checkbox-1cx"
              // defaultChecked={true}
              checked={commit}
              onChange={(e) => {
                setCommit(e.target.checked);
                props.onCbTermChange(!e.target.checked);
              }}
            />
            <label htmlFor="checkbox-1cx">
              <p>
                {l.g("bhsk.form.f3_title11")}{" "}
                <a
                  href="http://hyperservices.hdinsurance.com.vn/f/b1719ef4b27b6937db8f0230f9c2242e"
                  target="_blank"
                >
                  {l.g("bhsk.form.f3_title12")}
                </a>{" "}
                {l.g("bhsk.form.f3_title13")}
              </p>
            </label>
          </div>
        </Col>
        <div ref={refPreview}>
          <Overlay
            show={showPreview}
            rootClose={true}
            onHide={() => setShowPreview(false)}
            target={target}
            placement="bottom"
            container={refPreview.current}
          >
            <Popover id={styles.popover_contained_preview}>
              <Popover.Title>
                <label className={styles.title_popover}>
                  {l.g("bhsk.form.f3_title14")}
                </label>
                <div
                  className={styles.icon_close_popover}
                  onClick={() => setShowPreview(!showPreview)}
                >
                  <i className={`${styles.fas} ${styles.fa_times}`}></i>
                </div>
              </Popover.Title>
              <Popover.Content>
                <div className="">
                  <div className={`row ${styles.detail_item_ndbh}`}>
                    <label className="col-4">
                      {curentCarPreview.type == "DN"
                        ? "Tên Doanh Nghiệp"
                        : l.g("bhsk.form.f3_title15")}
                      {/* {l.g("bhsk.form.f3_title15")}: */}
                    </label>
                    <label className="col-8">{curentCarPreview.name}</label>
                  </div>
                  <div className={`row ${styles.detail_item_ndbh}`}>
                    <label className="col-4">
                      {l.g("bhsk.form.lbl_phone_number")}:
                    </label>
                    <label className="col-8">{curentCarPreview.phone}</label>
                  </div>
                  {curentCarPreview.email ? (
                    <div className={`row ${styles.detail_item_ndbh}`}>
                      <label className="col-4">Email:</label>
                      <label className="col-8">{curentCarPreview.email}</label>
                    </div>
                  ) : null}

                  {curentCarPreview.vehicle_use ? (
                    <div className={`row ${styles.detail_item_ndbh}`}>
                      <label className="col-4">Mục đích SD:</label>
                      <label className="col-8">
                        {curentCarPreview.vehicle_use}
                      </label>
                    </div>
                  ) : null}

                  <div className={`row ${styles.detail_item_ndbh}`}>
                    <label className="col-4">Loại xe:</label>
                    <label className="col-8">
                      {curentCarPreview.vehicle_type}
                    </label>
                  </div>
                  {curentCarPreview.newCar ? (
                    <>
                      <div className={`row ${styles.detail_item_ndbh}`}>
                        <label className="col-4">Số khung:</label>
                        <label className="col-8">
                          {curentCarPreview.chassis_no}
                        </label>
                      </div>
                      <div className={`row ${styles.detail_item_ndbh}`}>
                        <label className="col-4">Số máy:</label>
                        <label className="col-8">
                          {curentCarPreview.engine_no}
                        </label>
                      </div>
                    </>
                  ) : (
                    <div className={`row ${styles.detail_item_ndbh}`}>
                      <label className="col-4">Biển số xe:</label>
                      <label className="col-8">
                        {curentCarPreview.plateCar}
                      </label>
                    </div>
                  )}

                  <div className={`row ${styles.detail_item_ndbh}`}>
                    <label className="col-4">BH TNDS bắt buộc: </label>
                    <label className="col-8">
                      {formatCurrency(curentCarPreview.tndsfee)}
                    </label>
                  </div>
                  <div className={`row ${styles.detail_item_ndbh}`}>
                    <label className="col-4">BH TNDS tự nguyện: </label>
                    <label className="col-8">
                      {formatCurrency(curentCarPreview.voluntary_tnds_fee)}
                    </label>
                  </div>
                  <div className={`row ${styles.detail_item_ndbh}`}>
                    <label className="col-4">Thời hạn BH: </label>
                    <label className="col-8">{curentCarPreview.duration}</label>
                  </div>
                  <div className={`row ${styles.detail_item_ndbh}`}>
                    <label className="col-4">Tổng tiền: </label>
                    <label className="col-8">
                      {formatCurrency(curentCarPreview.totalAmount)}
                    </label>
                  </div>
                </div>
                <div className={styles.total_payment_item_ndbh}>
                  <div className="row">
                    <div className="col-md-12">
                      <div className={styles.preview_gcn} onClick={previewGCN}>
                        Xem trước giấy chứng nhận
                      </div>
                    </div>
                  </div>
                </div>
              </Popover.Content>
            </Popover>
          </Overlay>
        </div>
      </Row>
    </div>
  );
});

const mapStateToProps = (state) => {
  return {
    isrcarlist: state.isrcarlist,
    buyerInfor: state.buyerInfor,
  };
};
const mapDispatchToProps = (dispatch) => ({});

export default connect(mapStateToProps, mapDispatchToProps, null, {
  forwardRef: true,
})(Form3);
