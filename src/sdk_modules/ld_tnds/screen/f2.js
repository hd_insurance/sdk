import React, {
  useEffect,
  useState,
  createRef,
  forwardRef,
  useImperativeHandle,
} from "react";
import TopTitle from "../../../common/toptitle";
import { connect } from "react-redux";
import { setCarList } from "../../../redux/actions/action-car.js";
import { confirmAlert } from "react-confirm-alert";
import currencyFormatter from "currency-formatter";
import Avatar from "react-avatar";
import { Button, Col, Row, Tabs, Tab, Nav, Dropdown } from "react-bootstrap";
import CarInfo from "./carinfo";
import randomstring from "randomstring";
import Popover from "react-popover";
import { isBrowser, isMobile } from "react-device-detect";
import styles from "../../../css/style.module.css";
const background = [
  "#EC884D",
  "#3EA1FE",
  "#CCBFB0",
  "#6c5ce7",
  "#fdcb6e",
  "#16a085",
];
const NHOM_XE = "XE_OTO_NGUOIHANG";
const Form2 = forwardRef((props, ref) => {
  var default_transform = -(isMobile ? 45 : 60);
  const [active, setActive] = useState(null);
  const [openRelation, setOpenRelation] = useState(false);
  const [totalAmount, setTotalAmount] = useState("0 VNĐ");

  useEffect(() => {
    if (props.isrcarlist.length != 0 && props.isrcarlist[0]) {
      setActive(props.isrcarlist[0].id);
    }
  }, []);
  // useEffect(() => {
  //   calcTotalAmountListCar();
  // }, [props.isruserlist]);
  const formatCurrency = (value) => {
    return currencyFormatter.format(value, {
      code: l.g("bhsk.currency"),
      precision: 0,
      format: "%v %s",
      symbol: l.g("bhsk.currency"),
    });
  };
  const calcTotalAmountListCar = () => {
    let total = 0;
    props.isrcarlist.map((car, i) => {
      total += car.ref.current.calcTotalAmountCar();
    });
    setTotalAmount(formatCurrency(total));
  };

  const openTab = (id) => {
    setActive(id);
  };

  const getPrositionbyId = (id) => {
    return props.isrcarlist.findIndex((x) => x.id === id);
  };

  const addCar = (isNew) => {
    props.onCbTermChange(false);
    if (props.isrcarlist.length >= 1) {
      const position = getPrositionbyId(active); // lay ra posotion cua thang dang show
      let carls = props.isrcarlist;
      let carData = props.isrcarlist[position].ref.current.handleSubmit();
      if (carData) {
        carls[position] = { ...carls[position], ...carData }; // luu carData vao list
        var newID = randomstring.generate(7);
        const newarr = insert(carls, 1, {
          id: newID,
          ref: createRef(),
        });
        setActive(newID);
        props.setCarList(newarr);
      }
    } else {
      let carls = props.isrcarlist;
      var newID = randomstring.generate(7);
      const newarr = insert(carls, 1, {
        id: newID,
        ref: createRef(),
      });
      setActive(newID);
      props.setCarList(newarr);
    }
  };

  const insert = (arr, index, newItem) => [
    // inser vao vi tri trong mang
    ...arr.slice(0, index),
    newItem,
    ...arr.slice(index),
  ];

  const popoverUserRelation = {
    isOpen: openRelation,
    place: "below",
    preferPlace: "right",
    onOuterAction: () => setOpenRelation(false),
    body: [
      <div className="list-user-relative">
        <div className="h">Chọn xe được bảo hiểm</div>
        <div className="list-user">
          {props.isrcarlist.map((item, index) => {
            return (
              <div
                className="user-item"
                onClick={(e) => {
                  openTab(item.id);
                }}
                key={index}
              >
                <Avatar
                  name={`${index + 1}`}
                  round={true}
                  color={background[index]}
                  size={32}
                />
                <div className="if">
                  <div className="name">{`Xe được bảo hiểm ${index + 1}`}</div>
                </div>
                {item.id == active ? (
                  <i className="fas fa-check-circle"></i>
                ) : null}
              </div>
            );
          })}
        </div>
      </div>,
    ],
  };

  const openUserList = () => {
    setOpenRelation(true);
  };

  useImperativeHandle(ref, () => ({
    handleSubmit() {
      if (props.isrcarlist.length >= 1) {
        const position = getPrositionbyId(active);
        if (position != -1) {
          let carData = props.isrcarlist[position].ref.current.handleSubmit();
          if (carData) {
            var l = props.isrcarlist;
            l[position] = { ...l[position], ...carData };
            props.setCarList([...l]);
            return l;
          } else {
            return false;
          }
        } else {
          return false;
        }
      } else {
        return [];
      }
    },
    setActiveCar(id) {
      setActive(id);
    },
    update() {
      if (props.isrcarlist.length > 1) {
        if (props.defaultActive == null) {
          setActive(props.isrcarlist[1].id);
        } else {
          setActive(props.defaultActive);
        }
      }
    },
  }));

  const deleteCar = () => {
    confirmAlert({
      title: "Xác nhận",
      message: "Bạn có chắc chắn muốn xóa xe được bảo hiểm này không?",
      buttons: [
        {
          label: "Không xóa",
          onClick: () => {},
        },
        {
          label: "Xóa XĐBH",
          onClick: () => {
            var l = props.isrcarlist;
            var pos_del = getPrositionbyId(active);
            l.splice(pos_del, 1);
            console.log(l);
            setCarList([...l]);
            if (l.length >= 1) {
              setActive(l[l.length - 1].id); // lui 1 index
            } else {
              props.onCbTermChange(true);
              setActive(null);
            }
          },
        },
      ],
    });
  };

  return (
    <div className="bb-form f2">
      <TopTitle title={"Thông tin xe"} subtitle={""} />

      <div className={styles.user_related_tabbed}>
        <Tab.Container id="left-tabs-example" activeKey={active}>
          <div className={styles.tab_header}>
            <div
              className={styles.xx_left}
              style={
                props.redirectFrom == "visa" ? { paddingLeft: "15px" } : {}
              }
            >
              {props.redirectFrom == "visa" ? null : (
                <div className={styles.btn_add_user_container}>
                  <Button
                    className={styles.btn_add_user}
                    onClick={(e) => addCar(false)}
                  >
                    <span>{"Thêm xe"}</span> <i className="fas fa-plus"></i>
                  </Button>
                </div>
              )}
              <div className={styles.list_usr_hor}>
                {(props.isrcarlist.length > 3
                  ? props.isrcarlist.slice(0, 3)
                  : props.isrcarlist
                ).map((caritem, index) => {
                  default_transform = (isMobile ? 45 : 60) + default_transform;
                  if (index < 2) {
                    return (
                      <div
                        key={index}
                        onClick={() => openTab(caritem.id)}
                        className={
                          active == caritem.id
                            ? `${styles.usr_item} ${styles.active}`
                            : styles.usr_item
                        }
                        style={{
                          transform: "translateX(" + default_transform + "px)",
                        }}
                      >
                        <Avatar
                          className={styles.custom_avatar_kitin}
                          name={(index + 1).toString()}
                          size={42}
                          round={true}
                          color={background[index]}
                        />
                        <div className={styles.arrow} />
                      </div>
                    );
                  } else {
                    return (
                      <div
                        key={index}
                        onClick={() => openUserList()}
                        className={styles.usr_item}
                        style={{
                          background: background[index],
                          transform: "translateX(" + default_transform + "px)",
                        }}
                      >
                        <Popover {...popoverUserRelation}>
                          <Avatar
                            name={
                              props.isrcarlist.length == 3
                                ? "3"
                                : "+ " + (props.isrcarlist.length - 2)
                            }
                            size={42}
                            round={true}
                            color={background[index]}
                          />
                        </Popover>
                        <div className={styles.arrow} />
                      </div>
                    );
                  }
                })}
              </div>
            </div>

            <div className={styles.xx_right}>
            <div className={styles.right_info}>
                <p>
                  {l.g("bhsk.form.lbl_total_am")}{" "}
                  <span className={styles.price}>{totalAmount}</span>
                </p>
              </div>
            </div>
          </div>

          <div className={styles.tab_content_x}>
            <Tab.Content>
              {props.isrcarlist.map((caritem, index) => {
                return (
                  <Tab.Pane key={index} eventKey={caritem.id}>
                    <CarInfo
                      onDelete={deleteCar}
                      idCar={caritem.id}
                      indexCar={index}
                      ref={caritem.ref}
                      calcTotalAmountListCar={calcTotalAmountListCar}
                      onCbTermChange={props.onCbTermChange}
                      vehicleType={props.vehicleType}
                      redirectFrom={props.redirectFrom}
                      config_define={props.config_define}
                    />
                  </Tab.Pane>
                );
              })}
            </Tab.Content>
          </div>
        </Tab.Container>
      </div>
    </div>
  );
});

const mapStateToProps = (state) => {
  return {
    isrcarlist: state.isrcarlist,
    listDefineCar: state.listDefineCar,
  };
};
const mapDispatchToProps = (dispatch) => ({
  setCarList: (carls) => dispatch(setCarList(carls)),
});
export default connect(mapStateToProps, mapDispatchToProps, null, {
  forwardRef: true,
})(Form2);
