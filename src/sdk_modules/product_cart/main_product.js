import React, { useEffect, useState, createRef, useRef } from "react";
import Footer from "../../common/footer/footer";
import DynamicRender from "../../common/render/DnmRender";
import StageSpinner from "../../common/loadingpage";
import util from "../../util";
import { connect } from "react-redux";
import { setBuyerInfo, setListInsurer } from "../../redux/actions/product";
import defineJSON from "./main_layout.json";
import LoadingForm from "../../common/loadingform";
import api from "../../services/Network.js";
import cogoToast from "cogo-toast";

import ProductConfig from "./prodcfg.js";
import FormValid from "./product/formvalid.js";


function Main(props) {
    const footerRef = useRef();
    const [form_main_cfg, setFormMainConfig] = useState(ProductConfig[props.product]);
    const [form_valid, setFormValid] = useState(FormValid[props.currentProduct.mar_code]);
    // const [form_valid, setFormValid] = useState(FormValid["MR_12"]);

    const [json_component, setJsonComponent] = useState(null);
    const FormComponent = ProductConfig[props.product].component
    const [currentProduct, setCurrentProduct] = useState(props.currentProduct);

    const [detailProduct, setDetailProduct] = useState(null);
    const [detailObject, setDetailObject] = useState(null);

    const [listIsuer, setListIsuer] = useState([{id:1}]);

    const deleteUser = ()=>{

    }
    const handleCalcTotalMoney = ()=>{
      
    }


    useEffect(() => {
        getLayout()
    }, []);


    useEffect(() => {
       // var buyer = {addressCode:{}, ...props.buyerInfo}
       var buyer = {...props.buyerInfo}
       props.setBuyerInfo(buyer)
    }, [props.buyerInfo]);


    const getInitDetailData =  ()=>{
      return new Promise(async (resolve, rejected)=>{
            try{
             const response_data = await api.get(`/api/product-detail/${currentProduct.sku_code}/${currentProduct.detail_id}`)
             if(response_data.Success){
                if(response_data.Data[0][0]){
                  const data = response_data.Data[0][0]
                  return resolve(data)
                }else{
                  return resolve(false)
                }
                
             }else{
              return resolve(false)
             }
          }catch(e){
            console.log(e)
            return resolve(false)
          }
      })
    }

    const getLayout = async ()=>{
      try{
        //GET INIT DETAIL DATA
        const detail_data = await getInitDetailData()
        if(detail_data){
            let form_valid = JSON.parse(detail_data?.FORM_VALID);
            setFormValid(form_valid[props.currentProduct.mar_code]);
            setDetailObject(JSON.parse(detail_data?.OBJ_INIT));
            console.log("AHIIIII", JSON.parse(detail_data?.OBJ_INIT))
            setDetailProduct(detail_data);
        }else{
          cogoToast.error("Lấy thông tin từ hệ thống lỗi, thử lại sau!")
        }

        const response_data = await api.get(`/api/layout/${form_main_cfg.layout}`)
        const layout_data = response_data.data.layout_component
        setJsonComponent(layout_data)

        //Lay detail san pham

      }catch(e){
         cogoToast.error("Lấy thông tin từ hệ thống lỗi, thử lại sau!")
      }
    }


  const onClosePopup = (isReloadData)=>{
    props.onClosePopup(isReloadData)
  }

  return (
    <div>
      
        {json_component?<FormComponent
          currentProduct={currentProduct} 
          component={json_component}
          detailObject={detailObject}
          detailProduct={detailProduct}
          onClosePopup={onClosePopup}
          form_valid={form_valid}
        />:<div>
          
        </div>}
      
    </div>
  );





}

const mapStateToProps = (state) => {
  return {
    
  };
};

const mapDispatchToProps = (dispatch) => ({
  setBuyerInfo: (obj) => dispatch(setBuyerInfo(obj)),
  setListInsurer: (obj) => dispatch(setListInsurer(obj)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Main);

// export default Main