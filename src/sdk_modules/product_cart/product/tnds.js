import React, { useEffect, useState, createRef, useRef } from "react";
import moment, { now } from "moment";
import cogoToast from "cogo-toast";
import { connect } from "react-redux";
import Footer from "../../../common/footer/footer";
import DynamicRender from "../../../common/render/DnmRender";
import StageSpinner from "../../../common/loadingpage";
import util from "../../../util";
import LoadingForm from "../../../common/loadingform";
import api from "../../../services/Network.js";
import { setBuyerInfo, setListInsurer } from "../../../redux/actions/product";
import currencyFormatter from "currency-formatter";



import Isuer from "../../ld_tnds_new/isuer";

import HandleUtil from "../../ld_tnds_new/handle";

const tnds_define = {
  PRODUCT_BB: "XCG_TNDSBB_NEW",
  PRODUCT_TN: "XCG_TNDSTN",
  PACK_CODE_BB: "TNDSBB",
  PACK_CODE_TN: "TNDSTN"
}

function Main(props) {
    const footerRef = useRef();
    const [define, setDefine] = useState({});
    const [listmaped, setMaped] = useState({});
    const [reload, setReLoad] = useState(false);


    const [step, setStep] = useState(0);
    const [isDisableFooter, setDisableFooter] = useState(false);
    const [loading, setLoading] = useState(true);
    const [loadingForm, setLoadingForm] = useState(false);
    const [confirmTerm, setConfirmTerm] = useState(false);
    const [paymentMethod, setPaymentMethod] = useState("TH");

    const [defineConfig, setDefineConfig] = useState(tnds_define);
    const [listDefineCar, setListDefineCar] = useState([]);

    const [listIsuer, setListIsuer] = useState([]);

    const [detailObject, setDetailObject] = useState(props.detailObject);


    const deleteUser = ()=>{

    }
    const handleCalcTotalMoney = ()=>{

      
    }
    const updateTotalAmount = ()=>{

      
    }

  const getDefineData = async (callback) => {
    try {
      const data = await api.get(`/api/car/define`);
      if (data) {
        setListDefineCar(data);
        callback(data);
        // console.log(data);
      }
      // setPackages(data);
    } catch (e) {
      console.log(e);
    }
  };


  useEffect(() => {
      if (!util.isEmptyObj(listmaped)) {
           
           if(detailObject==null){
              return;
           }

           if(detailObject?.LIST_INSURED.length >0){
               const rs = detailObject?.LIST_INSURED[0]?.VOLUNTARY_CIVIL?.PRODUCT_CODE;
               const duration = detailObject.DURATION;

               setDefineConfig({...defineConfig,PRODUCT_TN: rs || 'XCG_TNDSTN', DURATION: duration});
           }
          const {listNewUser, fistId} = HandleUtil.initListIssured(detailObject.LIST_INSURED);
          setListIsuer([...listNewUser])
          setActiveTab({ id: fistId});
      }
    }, [listmaped, detailObject]);

      useEffect(() => {
          if (!util.isEmptyObj(listmaped)) {
           const listUserItem = [];
           listIsuer.forEach((item, index) => {
            const object_user_component = {
              id: item.id,
              component: Isuer,
              props: {
                form_valid: props.form_valid,
                default_info: item,
                isDisable: true,
                listDefineCar: listDefineCar,
                vehicleType: props.currentProduct.ref_val, // fix tam ----------------------------------------
                define: [],
                config_define: defineConfig,
                defineFormIssuerJSON: JSON.parse(
                  JSON.stringify(listmaped["tabs_item"].component)
                ),
                ref: item.ref,
                buyerInfor: {addressCode:{prov:null, dist: null, ward:null}},
                deleteUser: deleteUser,
                handleCalcTotalMoney: handleCalcTotalMoney,
                updateTotalAmount: updateTotalAmount,
              },
            };
            listUserItem.push(object_user_component);
          });
         
          listmaped["tabs_item"].setItem(listUserItem);
          listmaped["tab_header"].setData(listUserItem);

          listmaped["tab_header"].setTotalAmount(props.detailProduct.TOTAL_AMOUNT);
          listmaped["totalpayment"].setData(props.detailProduct.TOTAL_AMOUNT);

          listmaped["tab_header"].onItemSelect = (id) => {
            const current_active_tab = getActiveTab();
            const position = getPrositionbyId(current_active_tab);
            const data = listIsuer[position]?.ref?.current?.handleSubmit();
            if (data) {
              setActiveTab({ id: id, position: getPrositionbyId(id) });
            }
          };

          listmaped["listbh_1"].onClickItemEdit = (index) => {
            setStep(0);
            footerRef.current.setDisable(false);
            footerRef.current.setNextTitle(l.g("bhsk.form.lbl_next"));
            listmaped["wizard"].ref.current.setStep(0);
            listmaped["step_layout"].ref.current.setStep(0);
            const idEditor = listIsuer[index].id;
            listmaped["tab_header"].setActive(idEditor);
            listmaped["tabs_item"].setActive(idEditor);
          };
          listmaped["listbh_1"].setPreviewGCN((curentPrev)=>{
            HandleUtil.previewGCN(curentPrev, setLoadingForm, cogoToast, api, formatNumber, moment, listDefineCar[11])
          });
          listmaped["flcheckbox_ck"].onValueChange = (val) => {
            // console.log("flcheckbox_ck ", val)
            setConfirmTerm(val)
            if (val) {
              listmaped["flcheckbox_ck"].setValue(true);
              footerRef.current.setDisable(false);
            } else {
              listmaped["flcheckbox_ck"].setValue(false);
              footerRef.current.setDisable(true);
            }
          };

          listmaped["button_1"].onClick = () => {
             window.open(
                '/product/xe-co-gioi-tnds-xe-may',
                '_blank'
             );

             if(props.onClosePopup){
                document.body.style.overflow = 'unset';
                props.onClosePopup(true)
               
             }else{
              document.body.style.overflow = 'unset';
              
             }
             
          };

          listmaped["button_2"].onClick = () => {
             document.body.style.overflow = 'unset';
             if(props.onClosePopup){
                document.body.style.overflow = 'unset';
                props.onClosePopup(true)
             }else{
              document.body.style.overflow = 'unset';
             }
          };
      }
    }, [listIsuer]);

    useEffect(() => {

        getDefineData((dfn_cfg) => {
        const { obj_config, map } = util.rootObjectComponent(props.component);
        setMaped(map);
        setDefine(obj_config);

        setTimeout(() => {
          setLoading(false);
        }, 200);
      });


    }, []);

    const formatNumber = (value) => {
      return currencyFormatter.format(value, {
        thousand: ".",
        precision: 0,
      });
    };
    const setActiveTab = ({ id, position, map = listmaped }) => {
      if (map["tab_header"]) {
        map["tab_header"].setActive(id);
        map["tabs_item"].setActive(id);
      } else {
      }
    };
    const getActiveTab = () => {
      return listmaped["tabs_item"].ref.current.getActiveTab(); // lay active tab
    };
    const getPrositionbyId = (id) => {
      return listIsuer.findIndex((x) => x.id === id);
    };

    const validSubmitIsuser = () => {
      var valid = true;
      listIsuer.forEach((item, index) => {
        if (valid) {
          const data_issuer = item.ref.current.handleSubmit();
          if (!data_issuer) {
            setActiveTab({ id: item.id, position: index });
            valid = false;
          } else {
            listIsuer[index] = { ...item, ...data_issuer };
          }
        }
      });
      return valid;
    };


    const onNextClick = async ()=>{
      
      switch (step) {
        case 0:
          const valid_isuser = validSubmitIsuser();

          if (valid_isuser) {
            listmaped["listbh_1"].setData(listIsuer);
            footerRef.current.setDisable(false);
            footerRef.current.setNextTitle("Xác nhận");
            listmaped["flcheckbox_ck"].setValue(confirmTerm);
            // //set step in wizard
            listmaped["wizard"].ref.current.setStep(1);
            // //set step screen
            listmaped["step_layout"].ref.current.setStep(1);
            setStep(1);
            if(confirmTerm){
              footerRef.current.setDisable(false);
            }else{
              footerRef.current.setDisable(true);
            }
            
          }
            break;
        case 1:
          const buyer_info = {addressCode_dn:{}, ...props.buyerInfor}

          const define = {
            ORG_CODE_CREATE: props.detailProduct.ORG_SELLER, //=== ORG_SELLER
            CHANNEL: props.detailProduct.CHANNEL,
            USERNAME: "LANDING_NEW",
            ACTION: "BH_M"
          }
          const ord_sku = {
            OSKU_CODE: props.detailProduct.OSKU_CODE,
            DETAIL_CODE: props.detailProduct.DETAIL_CODE,
            MAR_CODE: props.detailProduct.MAR_CODE
          }

          const result = await HandleUtil.createOrder(listIsuer, buyer_info, paymentMethod, define, ord_sku, api, cogoToast, setLoadingForm)

          if(result){
            setStep(2);
            listmaped["wizard"].ref.current.setStep(2);
            listmaped["step_layout"].ref.current.setStep(2);
          }
         
          // footerRef.current.setDisable(false);

         // footerRef.current.setDisable(true);
          // const result = await createOrder();
          // if (register_vcx) {
            // const resultVCX = await registerVCX();
          // }
          // if (result) {
          //   if (paymentMethod == "CTT") {
          //     window.open(
          //       `${result}&callback=${
          //         window.location.origin + window.location.pathname
          //       }?payment=done`,
          //       "_self"
          //     );
          //   } else {
          //     setStep(2);
          //     listmaped["wizard"].ref.current.setStep(2);
          //     listmaped["step_layout"].ref.current.setStep(2);
          //   }
          // }

          break;
    }

    }
    const onPrevClick = ()=>{
      switch (step) {
        case 0:
          // const isuser = handleSubmitIsuser();
          // if (isuser) {
          //   const { data, position } = isuser;
          //   let l = listIsuer;
          //   l[position] = { ...l[position], ...data };
          //   setListIsuer([...l]);
          //   listmaped["wizard"].ref.current.setStep(step - 1);
          //   listmaped["step_layout"].ref.current.setStep(step - 1);
          //   setStep(0);
          // }
          // if (listIsuer.length == 0) {
          //   listmaped["wizard"].ref.current.setStep(step - 1);
          //   listmaped["step_layout"].ref.current.setStep(step - 1);
          //   setStep(0);
          // }
          // break;
        case 1:
            listmaped["wizard"].ref.current.setStep(step - 1);
            listmaped["step_layout"].ref.current.setStep(step - 1);
            footerRef.current.setDisable(false);
            footerRef.current.setNextTitle(l.g("bhsk.form.lbl_next"));
            setStep(0);
          break;
        // case 3:
        //   // listmaped["wizard"].ref.current.setStep(step - 1);
        //   // listmaped["step_layout"].ref.current.setStep(step - 1);
        //   // setStep(2);
        //   // break;
      }
    }
  


  return (
    <div> 

         {loadingForm && <LoadingForm />}
         <DynamicRender
            layout={define}
         />

          <Footer
            ref={footerRef}
            isDisable={isDisableFooter}
            step={step}
            onPrev={onPrevClick}
            onNext={onNextClick}
            lastStep={2}
          />


    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    buyerInfor: state.buyerInfor,
    listInsurer: state.listInsurer
  };
};

const mapDispatchToProps = (dispatch) => ({
  setBuyerInfo: (obj) => dispatch(setBuyerInfo(obj)),
  setListInsurer: (obj) => dispatch(setListInsurer(obj)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Main);

