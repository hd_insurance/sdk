        const common_disabled = {
            vehicle_group: {
                    disabled: true
                },
                vehicle_type: {
                    disabled: true
                },
                duration: {
                    disabled: true
                },
                purpose:{
                    disabled: true
                },
        }
        export default  {
            MR_11: {
                ...common_disabled,
                seat:{
                  max: 5,
                  message: ["Số chỗ ngồi phải trong khoảng 1-5 chỗ"]
                },
                
                isShowtnds: {
                    disabled: true
                },
                isShowAccident: {
                    disabled: true
                }
           },
            MR_12: {
                ...common_disabled,
                seat:{
                  max: 5,
                  message: ["Số chỗ ngồi phải trong khoảng 1-5 chỗ"]
                },
                
                isShowtnds: {
                    disabled: true
                },
                isShowAccident: {
                    disabled: true
                },
                damage_nt3: {
                    disabled: true
                },
                property_damage_nt3: {
                    disabled: true
                },
                ins_money: {
                    disabled: true
                }
                
           },
           MR_13: {
                ...common_disabled,
                seat:{
                  min: 6,
                  max: 11,
                  message: ["Số chỗ ngồi phải trong khoảng 6-11 chỗ"]
                },
                isShowtnds: {
                    disabled: true
                },
                isShowAccident: {
                    disabled: true
                }
           },
           MR_14: {
                ...common_disabled,
                seat:{
                  min: 6,
                  max: 11,
                  message: ["Số chỗ ngồi phải trong khoảng 6-11 chỗ"]
                },
               
                isShowtnds: {
                    disabled: true
                },
                isShowAccident: {
                    disabled: true
                }
           },
           MR_15: {
               ...common_disabled,
               seat:{
                disabled: true
               },
               isShowtnds: {
                    disabled: true
                },
               isShowAccident: {
                    disabled: true
               }
           },
           MR_16: {
            ...common_disabled,
            seat:{
                disabled: true
            },
             isShowtnds: {
                disabled: true
             },
             isShowAccident: {
                disabled: true
             },
             ins_money: {
               disabled: true
             }
            
           },
           MR_17: {
            ...common_disabled,
            seat:{
                disabled: true
            },
            isShowtnds: {
                disabled: true
            },
            isShowAccident: {
                disabled: true
            } 
           },
           MR_18: {
            ...common_disabled,
            seat:{
                disabled: true
            },
            isShowtnds: {
                disabled: true
             },
            isShowAccident: {
                disabled: true
             },
            ins_money: {
               disabled: true
            }
           }
           
        }