import React from "react";
import Main from "./main";
import { Provider } from "react-redux";
import store from "../../redux/store";
import currencyFormatter from 'currency-formatter';

const Item = (props) => {

  const getStatusText = (status)=>{
   switch(status){
      case 'NONE_INFO':
         return "Bổ sung thông tin"
      break;
      case 'CANCEL':
         return "Đã hủy từ Sàn"
      break;
      case 'CANCEL_HDI':
         return "Hết hiệu lực voucher"
      break;
      default:
         return "Đã bổ sung thông tin"
      break;
   }
  }

  const getStatusColor = (status)=>{
   switch(status){
      case 'NONE_INFO':
         return "css_color_red"
      break;
      case 'CANCEL':
         return "css_color_red"
      break;
      case 'CANCEL_HDI':
         return "css_color_red"
      break;
      default:
         return "css_color_green"
      break;
   }
  }


  return (
    <div class={`row pl-3 pr-3 ${props.STATUS != "NONE_INFO"?"buy_done_class":""}`}>
       <div class="col-md-6 mt-15 mb-3 ">
          <div class="row pl-3 pr-3 ">
             <div class="col-3 col-md-4 mt-15 m-0 p-0 ">
                <img class="css_img_san" src={props.PATH_IMG} style={{width: 160, height: 107}} />
             </div>

             <div class="col-9 col-md-8 mt-15 m-0 d-flex justify-content-center flex-column ">
                <p class=" m-0 mb-1 css_info_order">{props.PRODUCT_NAME||"NO NAME TO SHOW"}</p>
                <p class={`m-0 ${getStatusColor(props.STATUS)} text-bold`}>
                  {props.STATUS_NAME}
                </p>
             </div>
          </div>
       </div>
       <div class="col-md-6 mt-15 mb-3 css_fee_sl ">
          <div class="row m-0 w-100 ">
             <div class="col-12 col-md-8 contain_css_sl mt-15 m-0 ">
                <div class="row m-0 css_rong_kt ">
                   <div class="col-12 col-md-5 css_soluong mt-15 m-0 kitin ">
                      <p class=" m-0 mr-2">Số lượng: </p>
                      <p class=" m-0 css_info_order">{props.COUNT}</p>
                   </div>
                   <div class="col-12 col-md-7 css_soluong mt-15 m-0 ">
                      <p class=" m-0 mr-2">Phí: </p>
                      <p class=" m-0 css_info_order">{currencyFormatter.format(props.TOTAL_AMOUNT?props.TOTAL_AMOUNT:0, { code: "vnd", precision:0, format: '%v %s', symbol: "đ" })}</p>
                   </div>
                </div>
             </div>
             <div class="col-12 col-md-4 mt-15 m-0 "><button type="button" class={`${props.STATUS == "NONE_INFO"?"add_info_css":"buy_more_css"} btn btn-primary`} onClick={()=>props.onProductItemClick(props.sku_code, props.DETAIL_CODE, props.PRODUCT_CODE, props.MAR_CODE, props.STATUS != "NONE_INFO", props.REF_VAL)}>{props.STATUS == "NONE_INFO"?"Bổ Sung Thông Tin":"Mua Thêm"}</button></div>
          </div>
       </div>
    </div>
  );
};

export default Item;
