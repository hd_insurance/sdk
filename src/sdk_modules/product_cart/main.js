import React, { useEffect, useState, createRef, useRef } from "react";
import Footer from "../../common/footer/footer";
import styles from "../../css/style.module.css";
import DynamicRender from "../../common/render/DnmRender";
import StageSpinner from "../../common/loadingpage";
import util from "../../util";
import defineJSON from "./define_layout.json";
import LoadingForm from "../../common/loadingform";
import moment from "moment";
import api from "../../services/Network.js";
import cogoToast from "cogo-toast";
import currencyFormatter from 'currency-formatter';
import ProductItem from "./product_item.js";
import { Provider } from "react-redux";
import store from "../../redux/product";
import ProductMain from "./main_product.js";


function Main(props) {
  

    const [define, setDefine] = useState({});
    const [order_info, setOrderInfo] = useState(null);
    const [buyer_info, setBuyerInfo] = useState({});
    const [listmaped, setMaped] = useState({ORDER:{}});
    const [isModalOpen, setModalOpen] = useState(false);

    const [currentProduct, setCurrentProduct] = useState({});

    const [product_list, setTempProductList] = useState([]);

    const [empty_data, setEmptyData] = useState(false);
    const [loading, setLoading] = useState(true);
   

    useEffect(() => {
      if (!util.isEmptyObj(listmaped)) {
        getInitMasterData()
        
      }
    }, [listmaped]);

    useEffect(() => {
        const { obj_config, map } = util.rootObjectComponent(defineJSON);
        setMaped(map);
        setDefine(obj_config);
        map["modal"].define.onClose = ()=>{
           setModalOpen(false)
           document.body.style.overflow = 'unset';
           getInitMasterData()
        }
      }, []);

  const getInitMasterData = async ()=>{
    try{
      const response = await api.get(`/api/product-list/master/${props.prod_define.ref_id}`);
      setLoading(false) 
      if(response.Success){
        const data = response.Data
        setOrderInfo(data.ORDER)
        initMapProductList(data.ORDER.LIST_PRODUCT)
        setBuyerInfo(data.ORDER.BUYER)
      }else{
          setEmptyData(true)
      }
    }catch(e){
        setEmptyData(true)
    } 
  }

  const initMapProductList = (list)=>{
    
    var temp_list = []
    list.forEach((item, index)=>{
     
      temp_list.push({
        props: {
          sku_code: props.prod_define.ref_id,
          onProductItemClick: onProductItemClick,
          ...item
        },
        item_component: ProductItem
      })
    })

    
    setTempProductList(temp_list)
  }

  const onProductItemClick = (sku_code, detail_id, product_code, mar_code, isBuymore, ref_val)=>{

    if(isBuymore){
      gotoBuyMore(product_code, ref_val)
    }else{
      // console.log("on product clieck ", sku_code, detail_id, product_code, isBuymore, ref_val)
      setCurrentProduct({sku_code: sku_code, detail_id: detail_id, mar_code:mar_code, product_code:product_code, ref_val:ref_val})
      // listmaped["modal"]?.ref?.current?.handleOnpenModal();
      setModalOpen(true)
      document.body.style.overflow = 'hidden';
    }
  }

  const gotoBuyMore = (product_code, ref_val)=>{
    console.log("onProductItemClick ",product_code, ref_val)
    switch(product_code){
        case "XCG_TNDSBB_NEW":
          if(ref_val=="car"){
            window.open(
              '/product/xe-co-gioi-tnds-oto.hdi',
              '_blank' // <- This is what makes it open in a new window.
            );
          }else{
            if(ref_val=="moto"){
            window.open(
              '/product/xe-co-gioi-tnds-xe-may',
              '_blank' // <- This is what makes it open in a new window.
            );
          }
          }
        break;
        case "TNDS_SAN_NEW":
          if(ref_val=="car"){
            window.open(
              '/product/xe-co-gioi-tnds-oto.hdi',
              '_blank' // <- This is what makes it open in a new window.
            );
          }else{
            if(ref_val=="moto"){
            window.open(
              '/product/xe-co-gioi-tnds-xe-may',
              '_blank' // <- This is what makes it open in a new window.
            );
          }
          }
        break;
        default:
          console.log("show war")
          cogoToast.warn(`Sản phẩm có  ${product_code} không khả dụng`);
        break;
      }
  }


  const onClosePopup = (isReloadData)=>{
    listmaped["modal"]?.ref?.current?.handleCloseModal()
    setModalOpen(false)
    document.body.style.overflow = 'unset';
    if(isReloadData){
      getInitMasterData()
    }
  }
  



  return (
    <div>
    {loading && <LoadingForm />}

      <Provider store={store}>
         {order_info && loading==false?<DynamicRender
            layout={define}
            isOpen={isModalOpen}
            order_code={order_info.ORDER_ORG}
            order_status={order_info.STATUS_NAME}
            buyer_name={order_info.BUYER.NAME}
            buy_channel={order_info.CHANNEL_NAME?order_info.CHANNEL_NAME:order_info.CHANNEL}
            product_count={`${order_info.COUNT_PRODUCT} sản phẩm`}
            payment_time={order_info.DATE_PAY}
            total_amount={currencyFormatter.format(order_info.TOTAL_AMOUNT?order_info.TOTAL_AMOUNT:0, { code: "vnd", precision:0, format: '%v %s', symbol: "VNĐ" })}
            product_items={product_list}
            product_component={<ProductMain product={currentProduct.product_code} buyerInfo={buyer_info} currentProduct={currentProduct} onClosePopup={onClosePopup}/>}
         />:null}
         {empty_data?<div>
           <h1 style={{fontSize:16, textAlign:"center", paddingTop: 50, paddingBottom: 50}}><span style={{fontSize:26, textAlign:"center", padding: 10}}>404 | </span> Không tìm thấy thông tin đơn hàng trên hệ thống.</h1>
         </div>:null}
      </Provider>
    </div>
  );
}

export default Main;
