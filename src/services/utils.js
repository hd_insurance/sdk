import globalStore from './globalStore';

const getPageConfig = (path)=>{
	const page_info = globalStore.get("pageconfig").pages.filter(function(page){
      return page.PERMALINK == path.toLowerCase().trim()
    })
    const object_1 = globalStore.get("pageconfig").webconfig
    const object_2 = page_info[0]?page_info[0]:{}
    const object_out = {...object_1, ...object_2}
    return object_out
}


export default {
	getPageConfig
};