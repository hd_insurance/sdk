
const lang_position = 0
const valid = (valid_obj={}, values={})=>{
  for (const [key, value] of Object.entries(values)) {
     if(valid_obj[key] && value){
        const config = valid_obj[key]
        const result = checkValidValue(config, value)
        if(result){
          return result
        }
     }
  }
  return false
}


const setDefault = (valid_obj={}, values={})=>{
  for (const [key, value] of Object.entries(values)) {

     if(valid_obj[key] && value){
        const config = valid_obj[key]
        const result = checkDefault(config, value)
        if(result){
          return result
        }
     }
  }
  return false
}


const checkDefault = (cfg, abs_obj)=>{
    if(cfg.disabled){
      abs_obj.setDisableEditor(true);
    }
    if(cfg.value){
      abs_obj(cfg.value);
    }
}


const checkValidValue = (cfg, value)=>{
    if(cfg.max){
      if(value>cfg.max){ //11>11
        return cfg.message[lang_position]
      }
    }
    if(cfg.min){
      if(value<cfg.min){ //11 < 6
        return cfg.message[lang_position]
      }
    }
    return null
}

export default {
  valid,
  setDefault
};