import ReactHtmlParser from 'react-html-parser';
import api from "./Network.js";
export default class Lang {
  constructor(langdata) {
  	this.loadJSON = this.loadJSON.bind(this);
  	this.setLang = this.setLang.bind(this);
  	this.getLang = this.getLang.bind(this);
  	this.get = this.get.bind(this);
    this.ghtml = this.ghtml.bind(this);
    this.getTextFromCode = this.getTextFromCode.bind(this);
    this.trans = this.trans.bind(this);
    this.langdata = langdata
  }



  setLang(lang){
  	if (typeof window !== 'undefined') {
    	localStorage.setItem('hl', lang);
	   }
  	
  }
  getLang(){
  	if (typeof window !== 'undefined') {
  		return localStorage.getItem('hl')||"vi"
  	}else{
  		return "vi";
  	}
  }

  

  get(key){
  	if(key){
      const pos = this.getLang() == "vi"?0:1
      if(this.langdata[key]){
        return this.langdata[key][pos]
      }else{
        return ""
      }
  	}else{
  		return ""
  	}
  }


  getTextFromCode(intext){

    if(intext){
      const regex_rs = intext.replace(/<lang>(.*?)<\/lang>/g, function(rg, rg2){
        return lng.get(rg2)
      })
      if(regex_rs){
        return regex_rs;
      }
    }
    return intext;
  }
  trans(intext){
    try{
       if(intext){
        if(typeof intext === 'string'){
            const regex_rs = intext.replace(/<lang>(.*?)<\/lang>/g, function(rg, rg2){
            return lng.get(rg2)
          })
          if(regex_rs){
            return regex_rs;
          }
        }else{
          return intext;
        }
        
      }
      return intext;
    }catch(e){
      return intext;
    }
   
    
  }


  ghtml(key){
    return ReactHtmlParser(this.g(key))
  }


  async loadJSON(){
  	let currentLang = localStorage.getItem('myValueInLocalStorage')
  	console.log("currentLang>> ",currentLang)
  }

}