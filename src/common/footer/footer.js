import React, {
  useEffect,
  useState,
  createRef,
  useImperativeHandle,
  forwardRef,
} from "react";
import { Button } from "react-bootstrap";
import styles from "./style.module.css";

const Footer = forwardRef((props, ref) => {
  const [disableNext, setDisableNext] = useState(props.isDisable);
  const [disablePrev, setDisablePrev] = useState(props.isDisablePrev);
  const [nextTile, setNextTitle] = useState(l.g("bhsk.form.lbl_next"));
  useImperativeHandle(ref, () => ({
    setDisable(isDisable) {
      setDisableNext(isDisable);
    },
    
    setDisablePrev(e){
      setDisablePrev(e);
    },

    setNextTitle(v) {
      setNextTitle(v);
    },
  }));

  return (
    <div className={styles.footer_form_bhsk}>
      <div className={styles.f_info}>
        <p></p>
        <a></a>
      </div>
      {props.step > 0 && props.step != props.lastStep ? (
          <Button
              className={`${styles.footer_button_prev} ${styles.j_button}`}
              // className={` ${styles.j_button}`}
              disabled={disablePrev}
              onClick={props.onPrev}
          >
            {l.g("bhsk.form.lbl_previous")}
          </Button>
      ) : null}

      {/*<div className={"div_hotline"}>*/}
      {/*  <div>Nếu bạn có bất kỳ câu hỏi nào, vui lòng gọi cho chúng tôi qua </div>*/}
      {/*  <div>Hotline 1900068898</div>*/}
      {/*</div>*/}

      {props.step != props.lastStep ? (
        <Button
          className={`${styles.footer_button} ${styles.f_button}`}
          // className={`${styles.f_button}`}
          onClick={props.onNext}
          disabled={disableNext}
        >
          {nextTile}
        </Button>
      ) : null}


    </div>
  );
});

export default Footer;
