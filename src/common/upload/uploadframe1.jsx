import React, { useEffect, useCallback, useState, createRef} from "react";
import Dropzone, {useDropzone} from 'react-dropzone'
import styles from "./style.module.css"


const UploadItem = (props) => {

   const [files, setFiles] = useState([])
   return(<div className={styles.upload_item}>
	    <Dropzone
	    accept={'image/jpeg, image/png, application/pdf'}
	    onDrop={f => {
	    	props.onFileUpload(f)
	    }}>
		  {({getRootProps, getInputProps}) => (
		    <div className={`container`}>
		      <div
		        {...getRootProps({
		          className: 'dropzone',
		          onDrop: event => event.stopPropagation()
		        })}
		      >
		        <input {...getInputProps()} />
		         <i className="fas fa-cloud-upload-alt"></i>
		      </div>
		    </div>
		  )}
		</Dropzone>



    </div>
);
}

export default UploadItem;
