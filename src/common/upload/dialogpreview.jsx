import React, { useEffect, useState, createRef } from "react";
import FilePreviewer, { FilePreviewerThumbnail } from "./preview";
import Slider from "react-slick";
// import "slick-carousel/slick/slick.css";
// import "slick-carousel/slick/slick-theme.css";
import styles from "./style.module.css";

const settings = {
  dots: true,
  infinite: false,
  speed: 500,
  slidesToShow: 5,
  slidesToScroll: 5,
  className: styles.claim_slider_preview_file,
};

const UploadPreview = (props) => {
  const [files, setFiles] = useState([]);
  const [show, setShow] = useState(false);
  const [currentFile, setCurrentFile] = useState(props.files[0]);
  const [currentIndex, setCurrentIndex] = useState(0);
  useEffect(() => {
    setShow(props.show);
  }, props.show);

  const onItemThumbClick = (index, item) => {
    setCurrentFile(item);
    setCurrentIndex(index);
  };

  const onFileDelete = (index) => {
    props.removeFile(index);
  };

  return (
    <div className={styles.dialog_preview_file_container}>
      <div className={styles.backdrop} onClick={(e) => props.onClose()}></div>
      <div className={styles.wraper_view_file}>
        <div className="preview_item_file">
          <div className={styles.prevw}>
            <div className={styles.controller}>
              <div
                className={styles.delete_file}
                onClick={(e) => onFileDelete(currentIndex)}
              >
                <i className="far fa-trash-alt"></i>
                Xoá
              </div>
            </div>
            <div className={styles.afr}>
              <FilePreviewer
                hideControls={true}
                file={{ url: currentFile.url }}
              />
            </div>
          </div>
        </div>

        <div className={styles.thumb_list}>
          <Slider {...settings}>
            {props.files.map((item, index) => {
              return (
                <div
                  className={styles.thumbnail_preview}
                  onClick={(e) => {
                    onItemThumbClick(index, item);
                  }}
                >
                  <FilePreviewerThumbnail file={{ url: item.url }} />
                </div>
              );
            })}
          </Slider>
        </div>
      </div>
    </div>
  );
};

export default UploadPreview;
