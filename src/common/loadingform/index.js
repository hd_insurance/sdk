import React from "react";
import styles from "./style.module.css";
const LoadingForm = (props) => {
  return (
    <div className={props.isFullScreen ? styles.cover_full_loading : ""}>
      <div className={props.isFullScreen ? styles.fm_loader_full : styles.fm_loader}>
        <div className={styles.loader_sdk}>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
        </div>
      </div>
    </div>
  );
};
export default LoadingForm;
