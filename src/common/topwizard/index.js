import React, { useEffect, useState, createRef } from "react";
import Item from "./item.js";
import styles from "./style.module.css";

const Wizard = (props) => {
  return (
    <div className={styles.top_wz_container}>
      <div className={`${styles.top_form_wizard} ${styles["count-"+props.data.length]}`}>
        {props.data.map((item, index) => {
          return (
            <Item
              key={index}
              done={index < props.active}
              active={index == props.active}
              {...item}
            />
          );
        })}
      </div>
    </div>
  );
};

export default Wizard;
