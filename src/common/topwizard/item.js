import React, { useEffect, useState, createRef } from "react";
import styles from "./style.module.css";

const WizardItem = (props) => {
  var class_name = styles.item_wizard;
  if (props.active) {
    class_name = `${styles.item_wizard} ${styles.active}`;
  } else if (props.done) {
    class_name = `${styles.item_wizard} ${styles.done}`;
  }
  return (
    <div onClick={props.onClick} className={class_name}>
      <div className={styles.wz_icon}>
        <i className={props.icon} />
      </div>
      <div className={styles.info}>
        <div className={styles.step}>{lng.trans(props.step)}</div>
        <div className={styles.label}>{lng.trans(props.label)}</div>
      </div>
    </div>
  );
};

export default WizardItem;
