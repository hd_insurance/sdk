import React, { useEffect, useState, createRef } from "react";
import styles from "./style.module.css"
const TopTitle = (props) => {
  return (
    <div className={styles.top_title}>
      <h1>{lng.trans(props.title)}</h1>
      {props.subtitle ? <p>{lng.trans(props.subtitle)}</p> : null}
    </div>
  );
};

export default TopTitle;
