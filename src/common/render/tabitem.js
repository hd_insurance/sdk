import React, {
  useEffect,
  useState,
  createRef,
  forwardRef,
  useImperativeHandle,
} from "react";
import { Tab, Button } from "react-bootstrap";
import styles from "../../css/style.module.css";

const TabItem = forwardRef((props, ref) => {
  const AppContext = React.createContext()
  const [items, setItems] = useState([]);
  const [active, setActive] = useState(props.component_obj.active);

  props.component_obj.setTabActive = (id) => {
    setActive(id);
    
  };

  useImperativeHandle(ref, () => ({
    getActiveTab() {
      return active;
    },
    setTabItem(list){
      setItems([...list])
    }
  }));

  useEffect(() => {
    setItems(props.component_obj.define.children);
    // console.log("tab item", props.component_obj.define.children)
    if(active == null){
      if(props.component_obj.define.children[0]){
        setActive(props.component_obj.define.children[0].id)
        // console.log("set default Id ", props.component_obj.define.children[0].id)
      }

    }
  }, [props.component_obj.define.children]);
  // props.component_obj.onReloadTabs = () => {
  //   setItems(props.component_obj.define.children);
  // };
  return (
    <div className={`${styles.tab_content_x}`}>

        {items.length > 0 ? (
          <div>
            <Tab.Container id="object-tabs" activeKey={active}>
              <Tab.Content>
                {items.map((tab_item, index) => {
                  const id = tab_item.id;
                  const ComponentObject = tab_item.component
                  return (
                    <Tab.Pane key={id} eventKey={id}>
                        <ComponentObject {...tab_item.props}/>
                    </Tab.Pane>
                  );
                })}
              </Tab.Content>
            </Tab.Container>
          </div>
        ) : (
          <div className={styles.empty_relatives}>
            <div className={styles.contain}>
              <p>{props.component_obj.text?props.component_obj.text:`Bạn có muốn thêm thông tin của người thân nhận bảo hiểm ?`}</p>
              <Button
                onClick={(e)=>props.onAddItemClick()}
              >{props.component_obj.btn_text?props.component_obj.btn_text:`Thêm người được bảo hiểm`}</Button>
            </div>
          </div>
        )}

    </div>
  );
});

export default TabItem;
