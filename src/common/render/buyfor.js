import React, {
  useEffect,
  useState,
  createRef,
  forwardRef,
  useImperativeHandle,
} from "react";
import styles from "../../css/style.module.css";
import {
  FLInput,
  FLSelect,
  FLRadio,
  FLCheckBox,
    FLAddress,
} from "hdi-uikit";


const BuyFor = forwardRef((props, ref) => {
  const [activeKey, setActiveKey] = useState(props.defaultActiveKey || 0);
  const [value, setValue] = useState(props.config.default?props.config.default:"BAN_THAN");

  const [type, setType] = useState("CN");

  const [gender, setGender] = useState("");
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [phone, setPhone] = useState("");
  const [isRequiredDistric, setRequireDistric] = useState(false);

  const [mst_dn, setMSTDN] = useState("");
  const [name_dn, setNameDN] = useState("");
  const [email_dn, setEmailDN] = useState("");
  const [phone_dn, setPhoneDN] = useState("");

  const [address, setAddress] = useState(null);
  const [addressCode, setAddressCode] = useState({
    label: null,
    prov: null,
    dist: null,
    ward: null,
  });



  const [dtobject, setObject] = useState({
    for: value || "CN",
    type: type,
    data_value: {
      gender: "",
      name: "",
      email: "",
      phone: "",
      name_dn: "",
      email_dn: "",
      phone_dn: "",
      mst_dn: "",
      address: "",
      addressCode: {},
    }
  });

  useImperativeHandle(ref, () => ({
    setStep(step) {
      setActiveKey(step);
    },
  }));

  const disabled = [0, 2]

  const data = props.config.data

  useEffect(() => {
    
    if(props.value){
      setObject(props.value)
      setGender(props.value.data_value.gender)
      // setName(props.value.data_value.name)
      // setEmail(props.value.data_value.email)
      // setPhone(props.value.data_value.phone)

      // setMSTDN(props.value.data_value.mst_dn)
      // setNameDN(props.value.data_value.name_dn)
      // setEmailDN(props.value.data_value.email_dn)
      // setPhoneDN(props.value.data_value.phone_dn)
      setAddress(props.value.data_value.address)
      setAddressCode(props.value.data_value.addressCode)
      if(props.value.data_value.address && props?.value?.data_value?.address?.length > 0 ){
        setRequireDistric(true)
      }
    }
    
  }, [props.buyerInfor]);


  return (
    <div>
    <div class="row">
       <div class="col-md-12 mt-15  ">
         <FLRadio
                  label={"Bạn mua bảo hiểm cho:"}
                  data={data}
                  value={value}
                  changeEvent={(value)=>{
                      dtobject.for = value
                      setValue(value)

                      if(value == "NGUOI_KHAC" || value == "CA_NHAN" || value == "BAN_THAN"){
                        setType("CN")
                        dtobject.type = "CN"
                      }else if(value == "DOANH_NGHIEP"){
                        setType("DN")
                        dtobject.type = "DN"
                      }
                      setObject({...dtobject})
                      props.changeEvent(dtobject)
                  }}
                />
       </div>
    </div>

    {(type=="CN" && value!="BAN_THAN")&&<div className="row">
          <div className="col-md-3">
              <FLSelect
                  data={props.gender_define}
                  value={gender}
                  label={"Danh xưng"}
                  required={true}
                  changeEvent={(vl)=>{
                    dtobject.data_value.gender = vl
                    props.changeEvent(dtobject)
                    setObject({...dtobject})
                    setGender(vl)
                  }}
                />
          </div>
          <div className="col-md-3">
              <FLInput
                label={"Họ Tên"}
                value={name}
                required={true}
                changeEvent={(vl)=>{
                  dtobject.data_value.name = vl
                  props.changeEvent(dtobject)
                  setObject({...dtobject})
                  setName(vl)
                }}
              />
          </div>
          <div className="col-md-3">
            <FLInput
                label={"Số điện thoại"}
                pattern={"[0-9]{10,11}"}
                value={phone}
                required={true}
                changeEvent={(vl)=>{
                  dtobject.data_value.phone = vl
                  setPhone(vl)
                  setObject({...dtobject})
                  props.changeEvent(dtobject)
                }}
              />
          </div>
          <div className="col-md-3">
            <FLInput
                label={"Email"}
                value={email}
                changeEvent={(vl)=>{
                  dtobject.data_value.email = vl
                  setEmail(vl)
                  setObject({...dtobject})
                  props.changeEvent(dtobject)
                }}
            />
          </div>
      </div>}
      


    {type=="DN"&&<div className="row">
          <div className="col-md-3">
               <FLInput
                label={"Tên tổ chức/ DN"}
                required={true}
                value={name_dn}
                changeEvent={(vl)=>{
                  dtobject.data_value.name_dn = vl
                  setNameDN(vl)
                  setObject({...dtobject})
                  props.changeEvent(dtobject)
                }}
                />
          </div>
          <div className="col-md-3">
              <FLInput
                label={"Mã số thuế"}
                required={true}
                value={mst_dn}
                changeEvent={(vl)=>{
                  dtobject.data_value.mst = vl
                  setMSTDN(vl)
                  props.changeEvent(dtobject)
                }}
              />
          </div>
          <div className="col-md-3">
            <FLInput
                label={"Số điện thoại"}
                pattern={"[0-9]{10,11}"}
                required={true}
                value={phone_dn}
                changeEvent={(vl)=>{
                  dtobject.data_value.phone_dn = vl
                  setPhoneDN(vl)
                  setObject({...dtobject})
                  props.changeEvent(dtobject)
                }}
              />
          </div>
          <div className="col-md-3">
            <FLInput
                label={"Email"}
                value={email_dn}
                changeEvent={(vl)=>{
                  dtobject.data_value.email_dn = vl
                  setEmailDN(vl)
                  setObject({...dtobject})
                  props.changeEvent(dtobject)
                }}
            />
          </div>
      </div>}


      <div className='row'>
        <div className="col-md-12 mt-15">
          <div className="group-ipnv mobile-mt">
            <FLInput
                required={false}
                value={address}
                label={l.g("bhsk.form.street")}
                hideborder={true}
                changeEvent={(vl)=>{
                  if(vl.length > 0){
                    setRequireDistric(true)
                  }else{
                    setRequireDistric(false)
                  }
                  dtobject.data_value.address = vl
                  setAddress(vl)
                  setObject({...dtobject})
                  props.changeEvent(dtobject)
                }}
            />
            <div className="ver-line"></div>
            <div className="kkjs">
              <FLAddress
                  disable={false}
                  value={addressCode}
                  label={l.g("bhsk.form.address")}
                  required={isRequiredDistric}
                  hideborder={true}
                  changeEvent={(vl)=>{
                    dtobject.data_value.addressCode = vl
                    setAddressCode(vl)
                    setObject({...dtobject})
                    props.changeEvent(dtobject)
                  }}
              />
            </div>
          </div>
        </div>
      </div>

    </div>
  );
});

export default BuyFor;
