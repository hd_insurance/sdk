import React, {
  useEffect,
  useState,
  createRef,
  forwardRef,
  useImperativeHandle,
} from "react";
import { Tab } from "react-bootstrap";

const StepContainer = forwardRef((props, ref) => {
  const [activeKey, setActiveKey] = useState(props.defaultActiveKey || 0);
  useImperativeHandle(ref, () => ({
    setStep(step) {
      setActiveKey(step);
    },
  }));
  return (
    <Tab.Container id="xxxx" activeKey={activeKey}>
      <Tab.Content>{props.children}</Tab.Content>
    </Tab.Container>
  );
});

export default StepContainer;
