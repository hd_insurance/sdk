import React, {
  useEffect,
  useState,
  createRef,
  useImperativeHandle,
  forwardRef,
} from "react";
import { Button } from "react-bootstrap";
import styles from "../footer/style.module.css";

const Footer = forwardRef((props, ref) => {
  const [disableNext, setDisableNext] = useState(props.define.isDisableNext);
  const [nextTile, setNextTitle] = useState(l.g("bhsk.form.lbl_next"));
  const [step, setStep] = useState(0);
  const [lastStep, setLastStep] = useState(props.define.lastStep);

  useImperativeHandle(ref, () => ({
    setDisable(isDisable) {
      setDisableNext(isDisable);
    },
    setNextTitle(v) {
      setNextTitle(v);
    },
    onPrevClick(){
      setStep(step - 1);
    },
    onNextClick(){
      setStep(step + 1);
    }
  }));

  return (
    <div className={styles.footer_form_bhsk}>
      <div className={styles.f_info}>
        <p></p>
        <a></a>
      </div>

      {step != lastStep ? (
        <Button
          className={`${styles.footer_button} ${styles.f_button}`}
          onClick={props.onNext}
          disabled={disableNext}
        >
          {nextTile}
        </Button>
      ) : null}
      {step > 0 && step != lastStep ? (
        <Button
          className={`${styles.footer_button_prev} ${styles.j_button}`}
          onClick={props.onPrev}
        >
          {l.g("bhsk.form.lbl_previous")}
        </Button>
      ) : null}
    </div>
  );
});

export default Footer;
