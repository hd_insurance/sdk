import React, {
  useEffect,
  useState,
  createRef,
  forwardRef,
  useImperativeHandle,
} from "react";
import styles from "../../css/style.module.css";
import Popover from "react-popover";
import Avatar from "react-avatar";
import { isBrowser, isMobile } from "react-device-detect";
import { Button, Col, Row, Tabs, Tab, Nav, Dropdown } from "react-bootstrap";
import currencyFormatter from "currency-formatter";

const cn = (...args) => args.filter(Boolean).join(" ");

const TabHeader = forwardRef((props, ref) => {
  var default_transform = -(isMobile ? 45 : 60);
  const background = [
    "#EC884D",
    "#3EA1FE",
    "#CCBFB0",
    "#6c5ce7",
    "#fdcb6e",
    "#16a085",
  ];
  const [listItem, setListItem] = useState(props.component_obj.data || []);
  const [active, setActive] = useState(props.component_obj.active);
  const [openRelation, setOpenRelation] = useState(false);

  props.component_obj.reload = () => {
    // console.log("vao reload", props.component_obj.data)
    setListItem(props.component_obj.data);
  };

  useEffect(() => {
    setListItem(props.component_obj.data);
    // console.log("vao use eff", props.component_obj)
  }, [props.component_obj.data]);

  props.component_obj.setTabActive = (id) => {
    setActive(id);
  };

  useImperativeHandle(ref, () => ({}));

  const formatCurrency = (value) => {
    return currencyFormatter.format(value, {
      code: l.g("bhsk.currency"),
      precision: 0,
      format: "%v %s",
      symbol: l.g("bhsk.currency"),
    });
  };

  const openTab = (id) => {
    if (props.component_obj.onItemSelect) {
      //&& id!=active
      props.component_obj.onItemSelect(id);
    }
  };

  const popoverUserRelation = {
    isOpen: openRelation,
    place: "below",
    preferPlace: "right",
    onOuterAction: () => setOpenRelation(false),
    body: [
      <div className={styles.list_user_relative}>
        <div className={styles.h}>Chọn người được bảo hiểm</div>
        <div className={styles.list_user}>
          {listItem.map((item, index) => {
            return (
              <div
                className={styles.user_item}
                onClick={(e) => {
                  openTab(item.id);
                }}
                key={index}
              >
                <Avatar
                  name={
                    item.name ? item.name.toString() : (index + 1).toString()
                  }
                  round={true}
                  color={background[index]}
                  size={32}
                />
                <div className={styles.if}>
                  <div className={styles.name}>
                    {item.name ? item.name : `Người được bảo hiểm ${index + 1}`}
                  </div>
                  <div className={styles.package}>
                    {item.isspackage ? item?.isspackage?.PACK_NAME : ""}
                  </div>
                </div>
                {item.id == active ? (
                  <i className={`${styles.fas} ${styles.fa_check_circle}`}></i>
                ) : null}
              </div>
            );
          })}
        </div>
      </div>,
    ],
  };

  return (
    <div className={styles.tab_header}>
      <div className={styles.xx_left}>
        <div
          className={cn(
            styles.btn_add_user_container,
            props.component_obj?.define?.isHideButtonAdd && styles.width_auto
          )}
        >
          {!props.component_obj?.define?.isHideButtonAdd && (
            <Button
              className={styles.btn_add_user}
              onClick={(e) => {
                if (props.onAddItemClick) {
                  props.onAddItemClick();
                }
              }}
            >
              <span>{lng.trans(props.define.labelAdd)}</span>{" "}
              <i className="fas fa-plus"></i>
            </Button>
          )}
        </div>

        {
          <div className={styles.list_usr_hor}>
            {(listItem.length > 3 ? listItem.slice(0, 3) : listItem).map(
              (item, index) => {
                default_transform = (isMobile ? 45 : 60) + default_transform;
                if (index < 2) {
                  return (
                    <div
                      key={index}
                      onClick={() => openTab(item.id)}
                      className={
                        active == item.id
                          ? `${styles.usr_item} ${styles.active}`
                          : styles.usr_item
                      }
                      style={{
                        transform: "translateX(" + default_transform + "px)",
                      }}
                    >
                      <Avatar
                        className={styles.custom_avatar_kitin}
                        name={
                          item.name
                            ? item.name.toString()
                            : (index + 1).toString()
                        }
                        size={42}
                        round={true}
                        color={background[index]}
                      />

                      <div className={styles.arrow} />
                    </div>
                  );
                } else {
                  return (
                    <div
                      key={index}
                      onClick={() => setOpenRelation(true)}
                      className={styles.usr_item}
                      style={{
                        background: background[index],
                        transform: "translateX(" + default_transform + "px)",
                      }}
                    >
                      <Popover {...popoverUserRelation}>
                        <Avatar
                          className={styles.custom_avatar_kitin}
                          name={
                            listItem.length == 3
                              ? "3"
                              : "+ " + (listItem.length - 2)
                          }
                          size={42}
                          round={true}
                          color={background[index]}
                        />
                      </Popover>
                      <div className={styles.arrow} />
                    </div>
                  );
                }
              }
            )}
          </div>
        }
      </div>

      <div className={styles.xx_right}>
        <div className={styles.right_info}>
          <p>
            {lng.trans(props.define.labelAmount)}
            <label className={`${styles.price} ${styles.css_pr_total_money}`}>
              {currencyFormatter.format(props.component_obj.amout, {
                code: l.g("bhsk.currency"),
                precision: 0,
                format: "%v %s",
                symbol: l.g("bhsk.currency"),
              })}
            </label>
          </p>
        </div>
      </div>
    </div>
  );
});

export default TabHeader;
