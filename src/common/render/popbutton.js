import React, {
  useEffect,
  useState,
  createRef,
  forwardRef,
  useImperativeHandle,
} from "react";
import { Button } from "react-bootstrap";
import styles from "../../css/style.module.css";
import Popover from "react-popover";
import cogoToast from "cogo-toast";

const PopButton = forwardRef((props, ref) => {
  const [openBenefit, setOpenBenefit] = useState(false);
  const [dataConfig, setDataConfig] = useState({
    description: [],
    label: "",
    link: null,
  });

  useEffect(() => {
    setDataConfig(props.data);
  }, [props.data]);

  const openInNewTab = () => {
    if (dataConfig.link === "NO_ACTION") return;
    if (dataConfig.link) {
      if (typeof window !== "undefined") {
        setOpenBenefit(false);
        const newWindow = window.open(
          dataConfig.link,
          "_blank",
          "noopener,noreferrer"
        );
        if (newWindow) {
          newWindow.opener = null;
        }
      }
    } else {
      cogoToast.error("Vui lòng chọn gói bảo hiểm.");
    }
  };

  const popoverBenefit = {
    isOpen: openBenefit,
    place: "below",
    preferPlace: "right",
    onOuterAction: () => setOpenBenefit(false),
    body: [
      <div className={styles.list_benefit_relative}>
        {/* <div className={styles.h}>{pkg ? pkg.PACK_NAME : null}</div> */}
        <div className={styles.h}>{dataConfig.label}</div>
        <div className={styles.list_xxx}>
          {dataConfig.description?.map((item, index) => {
            // console.log(item);
            return (
              <div key={index} className={styles.ite}>
                {item.BENEFITS.map((ben, id2) => {
                  return <div>{`${ben.NAME}${ben.VAL_IN_WORD ?  ': ' +ben.VAL_IN_WORD : '' }`}</div>;
                })}
              </div>
            );
          })}

          <Button
            className={styles.view_detail_benefit}
            onClick={(e) => openInNewTab()}
          >
            {props.label_button_in_pop ? props.label_button_in_pop : "Chi tiết"}
          </Button>
        </div>
      </div>,
    ],
  };

  const actionopenBenefit = () => {
    setOpenBenefit(true);
    if (dataConfig.link) {
      setOpenBenefit(true);
    } else {
     
    }
  };
  useImperativeHandle(ref, () => ({
    setData(datacfg) {
      setDataConfig(datacfg);
    },
  }));
  return (
    <div className={"w-100 position-relative"}>
      <Popover {...popoverBenefit}>
        <Button
          className={styles.view_ql}
          onClick={(e) => {
            actionopenBenefit(e);
          }}
        >
          <i
            className={`${styles.fa} ${styles.fas} ${styles.fa_shield_alt}`}
          ></i>
          {` ${props.label ? props.label : "Xem quyền lợi"}`}
        </Button>
      </Popover>
    </div>
  );
});

export default PopButton;
