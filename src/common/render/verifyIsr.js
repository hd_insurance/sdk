import React, {
  useEffect,
  useState,
  createRef,
  forwardRef,
  useImperativeHandle,
} from "react";
import { Form, Button } from "react-bootstrap";
import styles from "../../css/style.module.css";
import currencyFormatter from "currency-formatter";
import moment from "moment";

const VerifyIsr = forwardRef((props, ref) => {
  const { data } = props;
  console.log(data);
  const [result, setResult] = useState({
    typeHouse: "",
    dientich: "",
    sotang: "",
    yearUse: "",
    valueHouse: "",
    addressCodeHouse: "",
    cr_package: "",
    gender: "",
    name: "",
    phone: "",
    email: "",
    passport: "",
    address: "",
    isspackage: "",
    yearIsr: "",
    effectiveDate: "",
    endDate: "",
  });
  const checkObject = (obj) => {
    if (obj && Object.keys(obj).length === 0 && obj.constructor === Object)
      return false;
    return true;
  };
  const handleAddress = (address) => {
    if (checkObject(address)) {
      return address?.label.split(" - ").reverse().join(", ");
    }
    return "";
  };

  useEffect(() => {
    if (checkObject(data)) {
      let objTemp = {};
      objTemp.typeHouse = data.typeHouse;
      objTemp.dientich = data.dientich;
      objTemp.sotang = data.sotang;
      objTemp.yearUse = data.yearUse;
      objTemp.valueHouse = data.valueHouse;
      objTemp.addressHouse = `${data.addressHouse}, ${handleAddress(
        data.addressCodeHouse
      )}`;
      objTemp.cr_package = data.cr_package;
      objTemp.isspackage = data.isspackage.label;
      objTemp.yearIsr = data.yearIsr;
      objTemp.effectiveDate = data.effectiveDate;
      objTemp.endDate = moment(data.effectiveDate, "DD/MM/YYYY").add(data.yearIsr, "years").format("DD/MM/YYYY");
      if (data.insBuyer) {
        objTemp.gender = data.gender;
        objTemp.name = data.name;
        objTemp.phone = data.phone;
        objTemp.email = data.email;
        objTemp.passport = data.passport;
        objTemp.address = `${data.address}, ${handleAddress(data.addressCode)}`;
      } else {
        objTemp.gender = data.genderOther;
        objTemp.name = data.nameOther;
        objTemp.phone = data.phoneOther;
        objTemp.email = data.emailOther;
        objTemp.passport = data.passportOther;
        objTemp.address = `${data.addressOther}, ${handleAddress(
          data.addressCodeOther
        )}`;
      }
      setResult(objTemp);
    }
  }, [props.data]);

  return (
    <div className={`${styles.ctn_veriry_isr} ${props.extend_class}`}>
      <div className="row m-0">
        <div className="col-4">NĐBH:</div>
        <div className={`col-8 ${styles.text_bold_result}`}>
          {`${result.gender === "M" ? "(Ông)" : "(Bà)"} ${result.name}`}
        </div>
      </div>
      <div className="row m-0 mt-3">
        <div className="col-4">SĐT:</div>
        <div className={`col-8 ${styles.text_bold_result}`}>{result.phone}</div>
      </div>
      <div className="row m-0 mt-3">
        <div className="col-4">Email:</div>
        <div className={`col-8 ${styles.text_bold_result}`}>{result.email}</div>
      </div>
      <div className="row m-0 mt-3">
        <div className="col-4">CMND/ CCCD/ HC:</div>
        <div className={`col-8 ${styles.text_bold_result}`}>
          {result.passport}
        </div>
      </div>
      <div className="row m-0 mt-3">
        <div className="col-4">Địa chỉ liên hệ:</div>
        <div className={`col-8 ${styles.text_bold_result}`}>
          {result.address}
        </div>
      </div>
      <div className="row m-0 mt-3">
        <div className="col-4">Loại nhà:</div>
        <div className={`col-8 ${styles.text_bold_result}`}>
          {result.typeHouse === "AD"
            ? "Nhà Liền Kề"
            : result.typeHouse === "VL"
            ? "Biệt Thự"
            : "Chung Cư"}
        </div>
      </div>
      <div className="row m-0 mt-3">
        <div className="col-4">Diện tích:</div>
        <div
          className={`col-8 ${styles.text_bold_result}`}
        >{`${result.dientich}m2`}</div>
      </div>
      <div className="row m-0 mt-3">
        <div className="col-4">Số năm đưa vào sử dụng:</div>
        <div className={`col-8 ${styles.text_bold_result}`}>
          {result.yearUse}
        </div>
      </div>
      {result.sotang && (
        <div className="row m-0 mt-3">
          <div className="col-4">Số tầng:</div>
          <div className={`col-8 ${styles.text_bold_result}`}>
            {result.sotang}
          </div>
        </div>
      )}
      <div className="row m-0 mt-3">
        <div className="col-4">Giá trị XD ngôi nhà:</div>
        <div className={`col-8 ${styles.text_bold_result}`}>
          {currencyFormatter.format(result.valueHouse, {
            code: l.g("bhsk.currency"),
            precision: 0,
            format: "%v %s",
            symbol: l.g("bhsk.currency"),
          })}
        </div>
      </div>
      <div className="row m-0 mt-3">
        <div className="col-4">Địa chỉ được bảo hiểm:</div>
        <div className={`col-8 ${styles.text_bold_result}`}>
          {result.addressHouse}
        </div>
      </div>
      <div className="row m-0 mt-3">
        <div className="col-4">Thời hạn bảo hiểm:</div>
        <div className={`col-8 ${styles.text_bold_result}`}>
          {`${result.yearIsr} năm (${result.effectiveDate} -  ${result.endDate})`}
        </div>
      </div>
      <div className="row m-0 mt-3">
        <div className="col-4">Gói bảo hiểm:</div>
        <div className={`col-8 ${styles.text_bold_result}`}>
          {result.isspackage}
        </div>
      </div>
    </div>
  );
});

export default VerifyIsr;
