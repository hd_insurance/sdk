import React, {
  useEffect,
  useState,
  createRef,
  forwardRef,
  useImperativeHandle,
} from "react";
import styles from "../../css/style.module.css";
import Popover from "react-popover";
import {FLInput, FLMultiSelect, WrapSelect} from "hdi-uikit";


const Main = forwardRef((props, ref) => {
  const [openRelation, setShowRelation] = useState(false);
  const [component_obj, setComponentObject] = useState({ render: 0 });
  const [isDisable, setDisable] = useState(props.isDisable || false);
  const [relation, setRelation] = useState(
    props.value || {
      title: "",
      value: "",
    }
  );
  const [listData, setListData] = useState(props.data || []);

  const relationSelect = (value) => {
    setRelation(value);
    setShowRelation(false);
    if (props.changeEvent) {
      props.changeEvent(value);
    }
  };

  useEffect(() => {
    if (props.component_obj.define.list) {
      setListData(props.component_obj.define.list);
      // console.log(props.component_obj.define.list)
    }
    props.component_obj.openRelation = (isOpen) => {
      setShowRelation(isOpen)
    }
    setComponentObject(props.component_obj);
  }, [props.component_obj]);


  useEffect(() => {
    setRelation(props.value)
  }, [props.value]);
  useEffect(() => {
    setDisable(props.isDisable)
  }, [props.isDisable]);



  const openPop = () => {
    if (isDisable) return 1;
    setShowRelation(true);
  };

  const popoverPropsRelation = {
    isOpen: openRelation,
    place: "below",
    preferPlace: "right",
    onOuterAction: () => setShowRelation(false),
    body: [
      <FLMultiSelect
        title={lng.trans(props.component_obj?.define?.title)}
        relationList={listData}
        selected={relationSelect}
      />,
    ],
  };

  const handleClick = () => {
    if(!isDisable){
      setShowRelation(true);
    }else return false
  };

  useImperativeHandle(ref, () => ({}));
  return (
    <div render={component_obj.render}>
      <Popover {...popoverPropsRelation}>
        {props.component_obj?.define?.is_input ? (
            <WrapSelect handleClick={handleClick} >
              <FLInput
                  {...props}
                  disable={props.isDisable}
                  // readonly={readonly}
                  label={props.component_obj?.define?.title}
                  hideborder={props.hideborder}
                  onFocus={(e) => openPop()}
                  value={relation?.title}
                  required={props.component_obj?.define?.is_required}
              />
            </WrapSelect>
        ):(
            <a className={styles.relation} onClick={(e) => openPop()}>
              {relation?.title
                  ? `${lng.trans(props.component_obj?.define?.title_after)}: ` + relation.title
                  : lng.trans(props.component_obj?.define?.title)}
              <i className={`${styles.fas} ${styles.fa_sort_down}`}></i>
            </a>
        )}

      </Popover>
    </div>
  );
});

export default Main;
