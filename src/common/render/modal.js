import React, {
  useEffect,
  useState,
  createRef,
  forwardRef,
  useImperativeHandle,
} from "react";
import Sheet from 'react-modal-sheet';

const ModalRender = forwardRef((props, ref) => {
  const [show, setShow] = useState(props.config.isOpen);
  // const [ComponentContent, setComponent] = useState(props.config.product_component);
  const ComponentDiv = props.config.product_component


  
  useImperativeHandle(ref, () => ({
    handleOnpenModal() {
        setShow(true);
    },
    handleCloseModal() {
        setShow(false);
    },
    setContent(component_view) {
      // const ProductItem = props.config.product_component
      // console.log(props.config.product_component)
      // setComponent(ProductItem)
    }
  }));


  useEffect(() => {
    setShow(props.config.isOpen);
  }, [props.config.isOpen]);


  const onModalClose = ()=>{
     setShow(false)
     if(props.config.onClose){
      props.config.onClose()
     }
  }

  return (
    <div>
      <Sheet isOpen={show} onClose={() => onModalClose()}>
        <Sheet.Container>
          <Sheet.Header />
          <Sheet.Content>
          <div className="form-contain">
            {ComponentDiv?ComponentDiv:null}
          </div>
          </Sheet.Content>
        </Sheet.Container>
        <Sheet.Backdrop 
          onClick={() => onModalClose()}
        ></Sheet.Backdrop>
      </Sheet>
    </div>
  );
});

export default ModalRender;
