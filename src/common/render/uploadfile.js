import React, {
  useEffect,
  useState,
  createRef,
  forwardRef,
  useImperativeHandle,
} from "react";
import UploadFrame1 from "../upload/uploadframe1";
import UploadFrame2 from "../upload/uploadframe2";
import UploadPreview from "../upload/dialogpreview";
import api from "../../services/Network.js";
import styles_cs from "../../css/style.module.css";
import { FilePreviewerThumbnail } from "../upload/preview";
import randomstring from "randomstring";

import styles from "../upload/style.module.css";

const Main = forwardRef((props, ref) => {
  const [files, setFiles] = useState([]);
  const [listPreview, setListPreview] = useState(false);
  const removeFile = (index) => {
    const fId = files[index].fileId;
    // props.onFileDelete(fId);
    const newarr = [...files];
    newarr.splice(index, 1);
    setFiles(newarr);
    setListPreview(false);
  };
  const showPreviewfile = (index) => {
    setListPreview(true);
  };

  const onFileUpload = async (out_file) => {
    try {
      // let formData = new FormData();

      const fileType = out_file[0]?.name?.split(".").pop();
      const newFileName = props.pre_file_name
        ? props.pre_file_name + "_" + randomstring.generate(7) + "." + fileType
        : out_file[0]?.name;
      // formData.append("files", out_file[0], newFileName);
      // const response = await api.upload(formData);
      const newf = files;
      const fileReader = new FileReader();
      fileReader.onload = (fileLoad) => {
        const { result } = fileReader;
        newf.push({
          file: out_file[0],
          newFileName,
          url: result,
        });
        setFiles([...newf]);
      };

      fileReader.readAsDataURL(out_file[0]);
    } catch (error) {
      console.log(error);
    }
  };
  useImperativeHandle(ref, () => ({
    getListFile() {
      if (files.length == 0) return [];
      return files;
    },
  }));
  return (
    <div>
      <div>{props.component_obj.define.label}</div>
      <div
        className={`${props.extend_class} ${styles.upload_container} ${styles.upload_cs}`}
      >
        {listPreview && (
          <UploadPreview
            files={files}
            index={0}
            removeFile={removeFile}
            onClose={() => setListPreview(false)}
          />
        )}
        {files.length > 0 ? (
          <div className={styles.wrap_item1}>
            <UploadFrame1 onFileUpload={onFileUpload} />
          </div>
        ) : (
          <div className={styles.wrap_item2}>
            <UploadFrame2 onFileUpload={onFileUpload} />
          </div>
        )}

        {(files.length > 3
          ? files.slice(files.length - 3, files.length)
          : files
        ).map((item, index) => {
          if (index == 2) {
            return (
              <div
                className={`${styles.wrap_item1} ${styles.last_item} ml-10`}
                key={index}
                onClick={(e) => showPreviewfile(index)}
              >
                {files.length > 3 && (
                  <div className={styles.overlay_item}>
                    <span className={styles.span_text}>
                      +{files.length - 3}
                    </span>
                  </div>
                )}
                <div className={styles.preview_item}>
                  <FilePreviewerThumbnail file={{ url: item.url }} />
                </div>
              </div>
            );
          } else {
            return (
              <div
                className={`${styles.wrap_item1} ml-10`}
                key={index}
                onClick={(e) => showPreviewfile(index)}
              >
                <div className={styles.preview_item}>
                  <FilePreviewerThumbnail file={{ url: item.url }} />
                </div>
              </div>
            );
          }
        })}
      </div>
    </div>
  );
});

export default Main;
