import React, {
  useEffect,
  useState,
  createRef,
  forwardRef,
  useImperativeHandle,
} from "react";
import styles from "../../css/style.module.css";

const Banner = forwardRef((props, ref) => {
  return (
    <div
      className={`${styles.css_bg_banner} ${props.extend_class} d-flex align-items-center`}
      style={{
        backgroundImage: `url(${props.url})`,
        width: props.width,
        height: props.height,
        backgroundRepeat: "no-repeat",
        // backgroundPosition: 'center',
        backgroundSize: "cover",
      }}
    >
        <div className="container">
            <div className="row ">
                <div className="col-md-6">
                    <h3 className={`${styles.h3_title_bn_one}`}>{props.title_one}</h3>
                    <h2 className={`${styles.h2_title_bn_two}`}>{props.title_two}</h2>
                    <p className={`${styles.p_title_bn_one}`}>{props.slogan}</p>
                </div>
            </div>
        </div>
    </div>
  );
});

export default Banner;
