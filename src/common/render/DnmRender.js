import React, { useEffect, useState, createRef, forwardRef } from "react";
import {
  FLInput,
  FLDate,
  FLAddress,
  FLSelect,
  FLRadio,
  FLCheckBox,
  FLSwitch,
  FLAmount,
  FLTime,
  FLYear,
  ListItem,
  UploadFrame,
  AccordionBts,
  FLSuggesstion,
  FLBankList,
  Text,
  BeneficiaryBank,
  BeneficiaryUser
} from "hdi-uikit";
import { Button, Accordion } from "react-bootstrap";
import ReactHtmlParser from "react-html-parser";
import styles from "../../css/style.module.css";
import TopTitle from "../toptitle";
import Wizard from "./wizardrender";
import StepContainer from "./stepcontainerrender";
import StepRender from "./steprender";
import FormRender from "./formrender";
import TabContainer from "./tabcontainer";
import TabHeader from "./tabheader";
import TabItem from "./tabitem";
import Item from "./item";
import PopButton from "./popbutton";
import LongRadio from "./longradio";
import Listbh from "./listbh";
import Listbh_tnds from "./listbhTnds";
import Voucher from "./voucher";
import TotalPayment from "./totalpayment";
import DivRender from "./divrender";
import RelationSelect from "./relationList";
import LockOutList from "./lockoutlist";
import VerifyIsr from "./verifyIsr";
import UploadFile from "./uploadfile";
import Banner from "./banner";
import Modal from "./modal";
import FooterRender from "./footerRender";
import BuyFor from "./buyfor";



const Layout = forwardRef((props, ref) => {
  

  const level = props.level ? parseInt(props.level) : 0;
  const hasLayout = (item) => {
    return item.layout && item.layout.length > 0;
  };
  const { layout, from } = props;
  const renderLayout = (callback) => {
    if (level == 0) {
      switch (layout.type) {
        case "screen":
          return <Layout {...props} layout={layout.layout} level={level + 1} />;
        case "item":
          return (
            <Item key={`${from}-level-${level}-${props.id}`}>
              {hasLayout(layout) && (
                <Layout {...props} layout={layout.layout} level={level + 1} />
              )}
            </Item>
          );
        default:
          return <div></div>;
      }
    } else {
      return layout.map((item, i) => {
        if (level == 1) {
          if (layout.length - 1 == i) {
            setTimeout(() => {
              if (callback) {
                callback();
              }
            }, 200);
          }
        }
        //for loop to render layout
        switch (item.type) {
          case "button":
            return (
              <Button
                key={`${from}-level-${level}-${i}`}
                className={`${item.extend_class}`}
                onClick={() => item.onClick()}
              >
                {lng.trans(item.define.value)}
              </Button>
            );
          case "uploadFile":
            return (
              <UploadFile
                key={`${from}-level-${level}-${i}`}
                className={`${item.extend_class}`}
                ref={item.ref}
                component_obj={item}
                pre_file_name={item?.define?.pre_file_name}
              />
            );
          case "img":
            return (
              <img
                key={`${from}-level-${level}-${i}`}
                className={item.extend_class}
                src={item.define.url}
                style={{ width: item.define.width, height: item.define.height }}
              />
            );
         case "banner": {
            return (
              !item.define?.isHide && <Banner 
                key={`${from}-level-${level}-${i}`}
                url={item.define.url}
                width={item.define.width} 
                height={item.define.height}
                title_one={item.define.title_one}
                title_two={item.define.title_two}
                slogan={item.define.slogan}
              />
            );
         }
         case "container": {
            return (
              <div
                key={`${from}-level-${level}-${i}`}
                className={`${item.extend_class} container`}
              >
                {hasLayout(item) && !item.define?.isHide && (
                  <Layout {...props} layout={item.layout} level={level + 1} />
                )}
             </div>
            )
         }

         case "bgContent": {
          return (
            !item.define?.isHide && <div
              key={`${from}-level-${level}-${i}`}
              className={` ${item.extend_class}`}
              style={{
                backgroundImage: `url(${item.define.url})`,
                width: item.define.width,
                height: item.define.height,
                backgroundRepeat: "no-repeat",
                // backgroundPosition: 'center',
                backgroundSize: "cover",
              }}
            >
               {hasLayout(item) && !item.define?.isHide && (
                  <Layout {...props} layout={item.layout} level={level + 1} />
                )}
            </div>
          )
         }
         case "modal": {
          if (item.mapstate) {
              item.define.product_component = props[item.mapstate.product_component];
              item.define.isOpen = props[item.mapstate.isOpen];
          }
          return (
            !item.define?.isHide && <Modal 
              key={`${from}-level-${level}-${i}`}
              ref={item.ref}
              config={item.define}
            >
              {hasLayout(item) && !item.define?.isHide && (
                  <Layout {...props} layout={item.layout} level={level + 1} />
                )}
            </Modal>
          )
         }

         case "divProduct": {
          return (
            !item.define?.isHide && <div
              key={`${from}-level-${level}-${i}`}
              className={` ${item.extend_class}`}
              style={{
                backgroundColor: `${item.define.bgColor}`,
                border: `${item.define.boder}`,
                borderRadius: '16px'
              }}
            >
               {hasLayout(item) && !item.define?.isHide && (
                  <Layout {...props} layout={item.layout} level={level + 1} />
                )}
            </div>
          )
         }

         case "footerForm": {
          return <FooterRender 
                    className={`${item.extend_class}`}  
                    ref={item.ref}  
                    key={`${from}-level-${level}-${i}`}
                    define={item.define}
          />
         }
              
          case "div":
            return (
              <DivRender
                key={`${from}-level-${level}-${i}`}
                component_obj={item}
                extend_class={`${item.extend_class} ${
                  item.define?.isHide ? styles.hide : ""
                }`}
                onDivClick={item.onClick}
              >
                {hasLayout(item) && !item.define.isHide && (
                  <Layout {...props} layout={item.layout} level={level + 1} />
                )}
              </DivRender>
            );
          case "listitem":
            if (item.mapstate) {
              item.define.product_items = props[item.mapstate.product_items];
            }
            return (
              <ListItem
                key={`${from}-level-${level}-${i}`}
                product_items={item.define.product_items}
                extend_class={`${item.extend_class} ${
                  item.define?.isHide ? styles.hide : ""
                }`}
               
              >
               
              </ListItem>
            );
          case "totalpayment":
            return (
              <TotalPayment
                key={`${from}-level-${level}-${i}`}
                data={item.define.data}
                component_obj={item}
              />
            );
          case "voucher":
            return <Voucher key={`${from}-level-${level}-${i}`} />;
          case "listbh":
            return (
              <Listbh
                key={`${from}-level-${level}-${i}`}
                ref={item.ref}
                component_obj={item}
                onDataChange={item.onDataChange}
              />
            );
          case "listbh_tnds":
            return (
              <Listbh_tnds
                key={`${from}-level-${level}-${i}`}
                ref={item.ref}
                component_obj={item}
                onDataChange={item.onDataChange}
              />
            );
          case "verifyisr":
            return (
              <VerifyIsr
                key={`${from}-level-${level}-${i}`}
                className={` ${item.extend_class}`}
                data={item.define.data}
              />
            );
          case "para":
            if (item.mapstate) {
              item.define.value = props[item.mapstate.state_value];
            }
            return (
              <p
                key={`${from}-level-${level}-${i}`}
                className={` ${item.extend_class}`}
              >
                {lng.trans(item.define.value)}
              </p>
            );
          case "relationselect":
            let component_props_relation = {};
            if (item.mapstate) {
              component_props_relation.value = props[item.mapstate.state_value];
              if(props[item.mapstate.state_changeEvent]){
                component_props_relation.changeEvent = props[
                  item.mapstate.state_changeEvent
                ].bind(this, item.template_id);
              }
              
            } else {
              component_props_relation.value = item.define.value;
              component_props_relation.changeEvent = item.onValueChange;
            }

            return (
              <RelationSelect
                render={item.render}
                key={`${from}-level-${level}-${i}`}
                description={item.define.description}
                data={item.define.list}
                extend_class={item.extend_class}
                component_obj={item}
                isDisable={item.define.disable}
                {...component_props_relation}
              />
            );
          case "lockoutlist":
            return (
              <LockOutList
                key={`${from}-level-${level}-${i}`}
                onDataChange={item.onDataChange}
                data={item.define.list}
                component_obj={item}
                ref={item.ref}
                extend_class={item.extend_class}
              />
            );
          case "longradio":
            return (
              <LongRadio
                key={`${from}-level-${level}-${i}`}
                description={item.define.description}
                data={item.define.list}
                value={item.define.value}
                extend_class={item.extend_class}
                changeEvent={item.onValueChange}
              />
            );
          case "popbutton":
            let component_props_pop = {};
            if (item.mapstate) {
              component_props_pop.data = props[item.mapstate.state_value];
            } else {
              component_props_pop.data = {};
            }
            return (
              <PopButton
                key={`${from}-level-${level}-${i}`}
                ref={item.ref}
                component_obj={item}
                label={lng.trans(item.define.label)}
                label_button_in_pop={lng.trans(item.define.label_button_in_pop)}
                {...component_props_pop}
              />
            );
          case "lineinform":
            return (
              !item.define?.isHide && (
                <div
                  className={`${styles.p_line} mt-15`}
                  key={`${from}-level-${level}-${i}`}
                />
              )
            );
          case "item":
            return (
              <Item key={`${from}-level-${level}-${i}`}>
                {hasLayout(item) && (
                  <Layout {...props} layout={item.layout} level={level + 1} />
                )}
              </Item>
            );
          case "tabsitem":
            return (
              <TabItem
                key={`${from}-level-${level}-${i}`}
                component_obj={item}
                ref={item.ref}
                onAddItemClick={item.addItemClick}
              />
            );

          case "tabheader":
            return (
              <TabHeader
                define={item.define}
                key={`${from}-level-${level}-${i}`}
                component_obj={item}
                onAddItemClick={item.addItemClick}
              />
            );
          case "tabcontainer":
            return (
              <TabContainer key={`${from}-level-${level}-${i}`}>
                {hasLayout(item) && (
                  <Layout {...props} layout={item.layout} level={level + 1} />
                )}
              </TabContainer>
            );

          case "toptitle":
            return (
              <TopTitle
                key={`${from}-level-${level}-${i}`}
                title={item.define.title}
                subtitle={item.define.sub_title}
              />
            );
          case "form":
            return (
              <FormRender
                key={`${from}-level-${level}-${i}`}
                ref={item.ref}
                extend_class={item.extend_class}
              >
                {hasLayout(item) && (
                  <Layout {...props} layout={item.layout} level={level + 1} />
                )}
              </FormRender>
            );
          case "step":
            return (
              <StepRender
                key={`${from}-level-${level}-${i}`}
                step={item.define.order}
                eventKey={item.define.order}
              >
                {hasLayout(item) && (
                  <Layout {...props} layout={item.layout} level={level + 1} />
                )}
              </StepRender>
            );

          case "row":
            var item_row_props = {}
            if(item.properties){
              item_row_props = item.properties["view_"+item.template_id]||{}
            }
            if (item_row_props.hide){
              return (<div></div>)
            }
            return (
              <div
                className={`row ${item.extend_class} ${
                  (item.define?.isHide || item_row_props.hide) ? styles.hide : ""
                }`}
                key={`${from}-level-${level}-${i}`}
                ref={item.ref}
              >
                {hasLayout(item) && !item.define?.isHide && (
                  <Layout {...props} layout={item.layout} level={level + 1} />
                )}
              </div>
            );
          case "col":
            
            var item_col_props = {}
             if(item.properties){
              item_col_props = item.properties["view_"+item.template_id]||{}
             }
            // if(item.template_id == "dead_date_tainan"){
            //   console.log("properties >> ",item_col_props, (item.define?.isHide || item_col_props.hide))
            // }
            if (item_col_props.hide){
              return (<div></div>)
            }

            return (
              <div
                className={`${item.col} mt-15 ${item.extend_class} ${
                  (item.define?.isHide || item_col_props.hide) ? styles.hide : ""
                }`}
                key={`${from}-level-${level}-${i}`}
              >
                {hasLayout(item) && !item.define?.isHide && (
                  <Layout {...props} {...item.properties} layout={item.layout} level={level + 1} />
                )}
              </div>
            );
          case "group_ipnv":
            return (
              <div
                className={`${styles.group_ipnv} ${item.extend_class}`}
                key={`${from}-level-${level}-${i}`}
              >
                {hasLayout(item) && !item?.define?.isHide && (
                  <Layout {...props} layout={item.layout} level={level + 1} />
                )}
              </div>
            );
          case "center_line":
            return (
              !item?.define?.isHide && (
                <div
                  key={`${from}-level-${level}-${i}`}
                  className={`${styles.center_line} ${item.extend_class}`}
                ></div>
              )
            );
          case "fladdress":
            let component_props_address = {};
            if (item.mapstate) {
              component_props_address.value = props[item.mapstate.state_value];
              if(props[
                item.mapstate.state_changeEvent
              ]){
                component_props_address.changeEvent = props[
                  item.mapstate.state_changeEvent
                ].bind(this, item.template_id);
              }
              
            } else {
              component_props_address.value = item.define.value;
              component_props_address.changeEvent = item.onValueChange;
            }

            return (
              <FLAddress
                key={`${from}-level-${level}-${i}`}
                disable={item.define.disable}
                template_id={item.template_id}
                label={lng.trans(item.define.label)}
                required={item.define.is_required}
                hideborder={item.define.hideborder}
                extend_class={item.extend_class}
                ref={item.ref}
                mode={item.define.mode}
                component_obj={item}
                pattern={item.define.pattern}
                {...component_props_address}
              />
            );
          
          case "single_select":
            let component_props_sglc = {};
            if (item.mapstate) {
              component_props_sglc.value = props[item.mapstate.state_value];

              if(props[item.mapstate.state_changeEvent]){
                component_props_sglc.changeEvent = props[
                  item.mapstate.state_changeEvent
                ].bind(this, item.template_id);
              }
              
            } else {
              component_props_sglc.value = item.define.value;
              component_props_sglc.changeEvent = item.onValueChange;
            }
            return (
              !item?.define?.isHide && (
                <FLSelect
                  key={`${from}-level-${level}-${i}`}
                  data={item.define.list}
                  label={lng.trans(item.define.label)}
                  required={item.define.is_required}
                  disable={item.define.disable}
                  template_id={item.template_id}
                  extend_class={item.extend_class}
                  hideborder={item.define.hideborder}
                  is_search={item.define.is_search}
                  ref={item.ref}
                  component_obj={item}
                  {...component_props_sglc}
                />
              )
            );
          case "flinput":
            let component_props = {};
            if (item.mapstate) {
              component_props.value = props[item.mapstate.state_value];

              if(props[item.mapstate.state_changeEvent]){
                component_props.changeEvent = props[
                  item.mapstate.state_changeEvent
                ].bind(this, item.template_id);
              }
              

            } else {

              component_props.value = item.define.value;
              component_props.changeEvent = item.onValueChange;
            }


           


            return (
              !item.define?.isHide && (
                <FLInput
                  key={`${from}-level-${level}-${i}`}
                  isUpperCase={item.define.isUpperCase}
                  label={lng.trans(item.define.label)}
                  required={item.define.is_required}
                  disable={item.define.disable}
                  template_id={item.template_id}
                  hideborder={item.define.hideborder}
                  extend_class={item.extend_class}
                  ref={item.ref}
                  component_obj={item}
                  pattern={item.define.pattern}
                  type={item.define.type}
                  disableDecima={item.define.disableDecima}
                  {...component_props}
                />
              )
            );
          case "flamount":
            let component_props_amount = {};
            if (item.mapstate) {
              component_props_amount.value = props[item.mapstate.state_value];
              if(props[item.mapstate.state_changeEvent]){
                component_props_amount.changeEvent = props[
                  item.mapstate.state_changeEvent
                ].bind(this, item.template_id);
              }
              
            } else {
              component_props_amount.value = item.define.value;
              component_props_amount.changeEvent = item.onValueChange;
            }
            return (
              !item?.define?.isHide && (
                <FLAmount
                  key={`${from}-level-${level}-${i}`}
                  isUpperCase={item.define.isUpperCase}
                  label={lng.trans(item.define.label)}
                  required={item.define.is_required}
                  disable={item.define.disable}
                  template_id={item.template_id}
                  hideborder={item.define.hideborder}
                  extend_class={item.extend_class}
                  ref={item.ref}
                  component_obj={item}
                  pattern={item.define.pattern}
                  {...component_props_amount}
                />
              )
            );
          case "flsuggession":
            let component_props_sgs = {};
            if (item.mapstate) {
              component_props_sgs.value = props[item.mapstate.state_value];
              if(props[item.mapstate.state_changeEvent]){
                component_props_sgs.changeEvent = props[
                  item.mapstate.state_changeEvent
                ].bind(this, item.template_id);
              }
              
            } else {
              
            }
            return (
              !item?.define?.isHide && (
                <FLSuggesstion
                  key={`${from}-level-${level}-${i}`}
                  isUpperCase={item.define.isUpperCase}
                  label={lng.trans(item.define.label)}
                  required={item.define.is_required}
                  disable={item.define.disable}
                  template_id={item.template_id}
                  hideborder={item.define.hideborder}
                  extend_class={item.extend_class}
                  ref={item.ref}
                  component_obj={item}
                  pattern={item.define.pattern}
                  {...component_props_sgs}
                />
              )
            );
          case "flradio":
            let component_props_radio = {};
            if (item.mapstate) {
              component_props_radio.value = props[item.mapstate.state_value];
              if( props[item.mapstate.state_changeEvent]){
                component_props_radio.changeEvent = props[
                  item.mapstate.state_changeEvent
                ].bind(this, item.template_id);
              }
            } else {
              component_props_radio.value = item.define.value;
              component_props_radio.changeEvent = item.onValueChange;
            }
            return (
              <FLRadio
                key={`${from}-level-${level}-${i}`}
                // value={item.define.value}
                label={item.define.label}
                data={item.define.list}
                template_id={item.template_id}
                // changeEvent={item.onValueChange}
                extend_class={item.extend_class}
                ref={item.ref}
                component_obj={item}
                {...component_props_radio}
              />
            );
          case "fldate":
            let component_props_date = {};
            if (item.mapstate) {
              component_props_date.value = props[item.mapstate.state_value];
              if( props[item.mapstate.state_changeEvent]){
                component_props_date.changeEvent = props[
                  item.mapstate.state_changeEvent
                ].bind(this, item.template_id);
              }
              
            } else {
              component_props_date.value = item.define.value;
              component_props_date.changeEvent = item.onValueChange;
            }
            return (
              <FLDate
                key={`${from}-level-${level}-${i}`}
                label={lng.trans(item.define.label)}
                selectedDay={item.define.defaultvalue}
                required={item.define.is_required}
                disable={item.define.disable}
                minimumDate={item.define.minimumDate}
                maximumDate={item.define.maximumDate}
                template_id={item.template_id}
                extend_class={item.extend_class}
                hideborder={item.define.hideborder}
                ref={item.ref}
                component_obj={item}
                {...component_props_date}
              />
            );
          case "fltime":
            let component_props_time = {};
            if (item.mapstate) {
              component_props_time.value = props[item.mapstate.state_value];
              component_props_time.changeEvent = props[
                item.mapstate.state_changeEvent
              ].bind(this, item.template_id);
            } else {
              component_props_time.value = item.define.value;
              component_props_time.changeEvent = item.onValueChange;
            }
            return (
              <FLTime
                key={`${from}-level-${level}-${i}`}
                label={item.define.label}
                required={item.define.is_required}
                disable={item.define.disable}
                template_id={item.template_id}
                extend_class={item.extend_class}
                hideborder={item.define.hideborder}
                ref={item.ref}
                component_obj={item}
                {...component_props_time}
              />
            );

          case "flyear":
            let component_props_year = {};
            if (item.mapstate) {
              component_props_year.value = props[item.mapstate.state_value];
              component_props_year.changeEvent = props[
                item.mapstate.state_changeEvent
              ].bind(this, item.template_id);
            } else {
              component_props_year.value = item.define.value;
              component_props_year.changeEvent = item.onValueChange;
            }
            return (
              <FLYear
                key={`${from}-level-${level}-${i}`}
                data={item.define.list}
                label={item.define.label}
                required={item.define.is_required}
                disable={item.define.disable}
                template_id={item.template_id}
                extend_class={item.extend_class}
                hideborder={item.define.hideborder}
                ref={item.ref}
                component_obj={item}
                {...component_props_year}
              />
            );

          case "flcheckbox":
            let component_props_flcb = {};
            if (item.mapstate) {
              component_props_flcb.value = props[item.mapstate.state_value];
              component_props_flcb.changeEvent = props[
                item.mapstate.state_changeEvent
              ].bind(this, item.template_id);
            } else {
              component_props_flcb.value = item.define.value;
              component_props_flcb.changeEvent = item.onValueChange;
            }
            var item_flcb_props = {}
            if(item.properties){
              item_flcb_props = item.properties["view_"+item.template_id]||{}
            }

            return (
              <FLCheckBox
                key={`${from}-level-${level}-${i}`}
                label={ReactHtmlParser(item_flcb_props.label?item_flcb_props.label:item.define.label)}
                required={item.define.is_required}
                disable={item.define.disable}
                template_id={item.template_id}
                extend_class={item.extend_class}
                component_obj={item}
                {...component_props_flcb}
              />
            );
          case "flswitch":
            let component_props_swtch = {};

            if (item.mapstate) {
              component_props_swtch.value = props[item.mapstate.state_value];
              component_props_swtch.changeEvent = props[
                item.mapstate.state_changeEvent
              ].bind(this, item.template_id);
            } else {
              component_props_swtch.value = item.define.value;
              component_props_swtch.changeEvent = item.onValueChange;
            }
            return (
              <FLSwitch
                key={`${from}-level-${level}-${i}`}
                label={ReactHtmlParser(lng.trans(item.define.label))}
                required={item.define.is_required}
                disable={item.define.disable}
                template_id={item.template_id}
                extend_class={item.extend_class}
                component_obj={item}
                {...component_props_swtch}
              />
            );
          case "step_layout":
            return (
              <StepContainer key={`${from}-level-${level}-${i}`} ref={item.ref}>
                {hasLayout(item) && (
                  <Layout {...props} layout={item.layout} level={level + 1} />
                )}
              </StepContainer>
            );
          case "wizard":
            return (
              <Wizard
                key={`${from}-level-${level}-${i}`}
                data={item.define.data}
                ref={item.ref}
              />
            );
          case "buyfor":
            let component_props_buyfor = {}
            if (item.mapstate) {

              // "gender_define": "gender_define",
              //               "state_value": "buyFor",
              //               "state_changeEvent": "onChangeBuyForData"

              component_props_buyfor.value = props[item.mapstate.state_value];
              component_props_buyfor.gender_define = props[item.mapstate.gender_define];


              if(props[item.mapstate.state_changeEvent]){
                  component_props_buyfor.changeEvent = props[
                    item.mapstate.state_changeEvent
                  ].bind(this, item.template_id);
                }
              }
            return (

              <BuyFor
                key={`${from}-level-${level}-${i}`}
                config={item.define}
                ref={item.ref}
                {...component_props_buyfor}
              />
            );
          case "upload_frame":

            let component_props_upload = {}
            if (item.mapstate) {
              if(props[item.mapstate.layout_data]){
                  component_props_upload.data = props[item.mapstate.layout_data]
              }
              if(props[item.mapstate.onFileUpdate]){
                  component_props_upload.onFileUpdate = props[
                    item.mapstate.onFileUpdate
                  ]
                }
              }
            return (
              <div>
                <UploadFrame
                  key={`${from}-level-${level}-${i}`}
                  data={item.define.data}
                  viewOnly={item.define.viewOnly}
                  language={item.define.language}
                  ref={item.ref}
                  {...component_props_upload}
                />
              </div>
            );
          case "text":

            var item_text_props = {}


            if(item.properties){
              item_text_props = item.properties["view_"+item.template_id]||{}
            }

            // if(item.template_id == "txt_nguoithuhuong"){
            //     console.log("item_text_props", item)
            // }


            let component_props_text= {}
            if (item.mapstate) {
              if(props[item.mapstate.state_value]){
                  component_props_text.value = props[item.mapstate.state_value]
              }
            }
            return (
              
                <Text
                  key={`${from}-level-${level}-${i}`}
                  value={item.define.value}
                  ref={item.ref}
                  extend_class={item.extend_class}
                  {...component_props_text}
                  {...item_text_props}
                />
              
            );
           case "fa-icon":
            return (
              
                <i class={`fas ${item.define.icon}`} key={`${from}-level-${level}-${i}`}></i>
              
            );
           case "bank_beneficiary":
            return (
                <BeneficiaryBank
                 key={`${from}-level-${level}-${i}`}
                 language={item.define.language}
                 ref={item.ref}/>
            );
            case "user_beneficiary":
            let component_props_bene_user = {}
            if (item.mapstate) {
              if(props[item.mapstate.lang]){
                  component_props_bene_user.lang = props[item.mapstate.lang]
              }
              if(props[item.mapstate.onBeneChange]){
                  component_props_bene_user.onBeneChange = props[item.mapstate.onBeneChange]
              }
            }
            return (
                <BeneficiaryUser
                  key={`${from}-level-${level}-${i}`}
                  ref={item.ref}
                  language={item.define.language}
                  {...component_props_bene_user}
                />
              
            );
          case "accordion":
            const current = props[item.mapstate.current_position] || "0"
            return (
              <div>
                 <Accordion defaultActiveKey={"0"} activeKey={current}>
                  {item.layout.map((acc_item, index)=>{

                      return(
                            <div className={`${current == index.toString()?"active-item":""} accordion-f-item`} key={index.toString()}>
                              <div className="accordion-header" onClick={()=>{
                                  if (item.mapstate) {
                                    if(props[item.mapstate.onConfirmTabChange]){
                                      props[item.mapstate.onConfirmTabChange](index.toString())
                                    }
                                  }
                              }}>
                               <div className="title">
                                 <Layout {...props} layout={acc_item.accordion_name} level={level + 1} />
                               </div>
                               <i class="fas fa-angle-down"></i>
                              </div>
                              <Accordion.Collapse className="accordion-body" eventKey={index.toString()}>
                                <div className="bod-ctx">
                                    <Layout {...props} layout={acc_item.layout} level={level + 1} />
                                </div>
                              </Accordion.Collapse>
                            </div>
                        )

                    })}
                </Accordion>
              </div>
            );

          default:
            return <div />;
        }

      });
    }
  };

  return <React.Fragment>{renderLayout()}</React.Fragment>;
});

export default Layout;
