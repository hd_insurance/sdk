import React, {
  useEffect,
  useState,
  createRef,
  forwardRef,
  useImperativeHandle,
} from "react";
import { Form, Button } from "react-bootstrap";
import styles from "../../css/style.module.css";

const Voucher = forwardRef((props, ref) => {
  const [voucherLoading, setVoucherLoading] = useState(false);
  useImperativeHandle(ref, () => ({
   
  }));
  return (
    <div className={`${styles.form_voucher} ${styles.mobile_mt}`}>
      <Form
        noValidate
        //   validated={validatedVoucher}
        //   onSubmit={handleSubmitVoucher}
      >
        <div className={styles.vcvc}>
          <Form.Control
            className={styles.hqk_custom_input}
            type="text"
            placeholder={l.g("bhsk.form.f3_title7")}
            aria-describedby="inputGroupPrepend"
            //   value={voucher}
            //   onChange={(e) => setVoucher(e.target.value)}
            required
          />
          <Button type="button" disabled={voucherLoading}>
            {voucherLoading ? (
              <div className={styles.loading_voucher}>
                <StageSpinner />
              </div>
            ) : (
              l.g("bhsk.form.f3_title8")
            )}
          </Button>
        </div>
      </Form>
    </div>
  );
});

export default Voucher;
