import React, {
  useEffect,
  useState,
  createRef,
  forwardRef,
  useImperativeHandle,
} from "react";
import styles from "../../css/style.module.css";
import currencyFormatter from "currency-formatter";
import { Button, Col, Row, Popover, Overlay } from "react-bootstrap";

const Listbh = forwardRef((props, ref) => {
  const [target, setTarget] = useState(null);
  const [curentCarPreview, setCurrentCarPreview] = useState({});
  const [curentPrev, setCurentPrev] = useState({});
  const [showPreview, setShowPreview] = useState(false);
  const [listIssur, setListIssur] = useState([]);

  const refPreview = createRef(null);

  const handClickPreview = (event, index) => {
    setCurrentCarPreview(listIssur[index]);
    setCurentPrev(listIssur[index]);
    setShowPreview(!showPreview);
    setTarget(event.target);
  };

  const formatCurrency = (value) => {
    return currencyFormatter.format(value, {
      code: l.g("bhsk.currency"),
      precision: 0,
      format: "%v %s",
      symbol: l.g("bhsk.currency"),
    });
  };

  useImperativeHandle(ref, () => ({}));

  useEffect(() => {
    if (props.component_obj.data != null) {
      // console.log(props.component_obj.data);
      setListIssur(props.component_obj.data);
    }

    // console.log("props.component_obj.data ", props.component_obj.data)
  }, [props.component_obj.data]);

  const onEditClick = (index) => {
    if (props.component_obj.onClickItemEdit) {
      props.component_obj.onClickItemEdit(index);
    }
  };


  return (
    <div>
      <div className={styles.form_list_ndbh}>
        {listIssur.map((e, i) => {
          const carInfor = e;
          const total_am = carInfor.TOTAL_AMOUNT * 1;
          return (
            <div key={i} className={styles.item_list_ndbh}>
              <div className="row">
                <div className="col-md-8">
                  <div className={styles.title_item_ndbh}>
                    <div className={styles.mname}>
                      <label>{i + 1}.</label>
                      <label>
                        {carInfor.NUMBER_PLATE
                          ? carInfor.NUMBER_PLATE
                          : carInfor.CHASSIS_NO}
                      </label>
                    </div>

                    <label className={styles.am}>
                      {formatCurrency(total_am)}
                    </label>
                  </div>
                  <div className={styles.name_package_insurance}>
                    {`Bảo hiểm TNDS `}
                  </div>
                </div>
                <div className={`col-md-4 ${styles.btn_action_item}`}>
                  <button
                    className={styles.preview_item_ndbh}
                    onClick={(event) => handClickPreview(event, i)}
                  >
                    <i className={`${styles.fas} ${styles.fa_eye}`}></i>
                    <label>{l.g("bhsk.form.label_view")}</label>
                  </button>
                  <button
                    className={styles.edit_item_ndbh}
                    onClick={() => onEditClick(i)}
                  >
                    <i className={`${styles.fas} ${styles.fa_edit}`}></i>
                    <label>{l.g("bhsk.form.label_edit")}</label>
                  </button>
                </div>
              </div>
            </div>
          );
        })}

        <div ref={refPreview}>
          <Overlay
            show={showPreview}
            rootClose={true}
            onHide={() => setShowPreview(false)}
            target={target}
            placement="bottom"
            container={refPreview.current}
          >
            <Popover id={styles.popover_contained_preview}>
              <Popover.Title>
                <label className={styles.title_popover}>
                  {"Thông tin của xe được bảo hiểm"}
                </label>
                <div
                  className={styles.icon_close_popover}
                  onClick={() => setShowPreview(!showPreview)}
                >
                  <i className={`${styles.fas} ${styles.fa_times}`}></i>
                </div>
              </Popover.Title>
              <Popover.Content>
                <div className="scroll-y">
                  <div className={`row ${styles.detail_item_ndbh}`}>
                    <label className="col-4">
                      {/*{console.log("chim ",curentCarPreview.TYPE)}*/}
                      {(curentCarPreview.TYPE == "DN" || curentCarPreview.TYPE == "CQ")
                        ? "Tên Doanh Nghiệp"
                        : curentCarPreview.GENDER == "M"
                        ? l.g("bhsk.form.f3_title15")
                        : l.g("bhsk.form.f3_title16")}
                      {/* {l.g("bhsk.form.f3_title15")}: */}
                    </label>
                    <label className="col-8">{curentCarPreview.NAME}</label>
                  </div>
                  <div className={`row ${styles.detail_item_ndbh}`}>
                    <label className="col-4">
                      {l.g("bhsk.form.lbl_phone_number")}:
                    </label>
                    <label className="col-8">{curentCarPreview.PHONE}</label>
                  </div>
                  {curentCarPreview.EMAIL ? (
                    <div className={`row ${styles.detail_item_ndbh}`}>
                      <label className="col-4">Email:</label>
                      <label className="col-8">{curentCarPreview.EMAIL}</label>
                    </div>
                  ) : null}

                  {curentCarPreview.VEHICLE_USE_LABEL ? (
                    <div className={`row ${styles.detail_item_ndbh}`}>
                      <label className="col-4">Mục đích SD:</label>
                      <label className="col-8">
                        {curentCarPreview.VEHICLE_USE_LABEL}
                      </label>
                    </div>
                  ) : null}

                  <div className={`row ${styles.detail_item_ndbh}`}>
                    <label className="col-4">Loại xe:</label>
                    <label className="col-8">
                      {curentCarPreview.VEHICLE_TYPE_LABEL}
                    </label>
                  </div>
                  {curentCarPreview.newCar ? (
                    <>
                      <div className={`row ${styles.detail_item_ndbh}`}>
                        <label className="col-4">Số khung:</label>
                        <label className="col-8">
                          {curentCarPreview.CHASSIS_NO}
                        </label>
                      </div>
                      <div className={`row ${styles.detail_item_ndbh}`}>
                        <label className="col-4">Số máy:</label>
                        <label className="col-8">
                          {curentCarPreview.ENGINE_NO}
                        </label>
                      </div>
                    </>
                  ) : (
                    <div className={`row ${styles.detail_item_ndbh}`}>
                      <label className="col-4">Biển số xe:</label>
                      <label className="col-8">
                        {curentCarPreview.NUMBER_PLATE}
                      </label>
                    </div>
                  )}

                  <div className={`row ${styles.detail_item_ndbh}`}>
                    <label className="col-4">BH TNDS bắt buộc: </label>
                    <label className="col-8">
                      {formatCurrency(
                        curentCarPreview.COMPULSORY_CIVIL?.AMOUNT
                      )}
                    </label>
                  </div>
                  <div className={`row ${styles.detail_item_ndbh}`}>
                    <label className="col-4">Bảo hiểm tự nguyện: </label>
                    <label className="col-8">
                      {formatCurrency(
                        curentCarPreview.VOLUNTARY_CIVIL
                          ? curentCarPreview.VOLUNTARY_CIVIL.TOTAL_AMOUNT
                          : 0
                      )}
                    </label>
                  </div>
                  <div className={`row ${styles.detail_item_ndbh}`}>
                    <label className="col-4">Thời hạn BH: </label>
                    <label className="col-8">
                    {/*{console.log(curentCarPreview)}*/}
                      {curentCarPreview.COMPULSORY_CIVIL
                        ? curentCarPreview.COMPULSORY_CIVIL.EFF+" - "+curentCarPreview.COMPULSORY_CIVIL.EXP
                        : "1 năm"}
                    </label>
                  </div>
                  <div className={`row ${styles.detail_item_ndbh}`}>
                    <label className="col-4">Tổng tiền: </label>
                    <label className="col-8">
                      {formatCurrency(curentCarPreview.TOTAL_AMOUNT * 1)}
                    </label>
                  </div>
                </div>
               
              </Popover.Content>
            </Popover>
          </Overlay>
        </div>
      </div>
    </div>
  );
});

export default Listbh;
