import React, {
  useEffect,
  useState,
  createRef,
  forwardRef,
  useImperativeHandle,
} from "react";
import styles from "../../css/style.module.css";

import {Button, Col, Row, Popover, Overlay, Modal} from "react-bootstrap";

const LockoutList = forwardRef((props, ref) => {
  var handleEventDataCallback = null;
  const refPreview = createRef(null);
  const refConditions = createRef(null);
  const [showListCondition, setShowListCondition] = useState(false);
  const [target, setTarget] = useState(null);
  const [listIsuer, setListIsuer] = useState([]);

  const [listSelect, setListSelect] = useState({});

  const [config, setConfig] = useState({
    EXCLUDE_COND1: {
      label: lng.trans(props.component_obj?.define?.label_ex_cond1),
        // "NĐBH có đang trong thời gian điều trị bệnh hoặc điều trị thương tật?",
      sublabel:
      lng.trans(props.component_obj?.define?.sublabel_ex_cond1),
        // "Chọn người mắc một hay các bệnh: ung thư, u bướu các loại, huyết áp, tim mạch....?",
      select: false,
    },
    EXCLUDE_COND2: {
      label:
      lng.trans(props.component_obj?.define?.label_ex_cond2),
        // "NĐBH có bị bệnh tâm thần, ung thư, bệnh phong, thương tật vĩnh viễn quá 70%?",
      sublabel:
      lng.trans(props.component_obj?.define?.sublabel_ex_cond2),
        // "Chọn người bị tâm thần, thần kinh, bệnh phong thương tật vĩnh viễn quá 70%. Người đang điều trị bệnh hoặc thương tật",
      select: false,
    },
    EXCLUDE_COND3: {
      label: props.component_obj?.define?.label_ex_cond3,
      sublabel: props.component_obj?.define?.sublabel_ex_cond3,
      select: false,
    },
  });

  const handClickPreview = (event, index) => {
    // setCurentPrev(props.isrcarlist[index]);
    // setCurrentCarPreview(props.isrcarlist[index].showInfor);
    setShowPreview(!showPreview);
    setTarget(event.target);
  };

  const formatCurrency = (value) => {
    return currencyFormatter.format(value, {
      code: l.g("bhsk.currency"),
      precision: 0,
      format: "%v %s",
      symbol: l.g("bhsk.currency"),
    });
  };

  useImperativeHandle(ref, () => ({
    setData(data) {
      setListIsuer([...data]);
    },
    setConfig(cfg) {},
    onDataChange(handleEvent) {
      handleEventDataCallback = handleEvent;
    },
  }));

  useEffect(() => {
    var isValid = false;
    for (const [key, value] of Object.entries(config)) {
      config[key].select = false;
      listSelect[key] = {};
      listIsuer.map((user, index) => {
        if (user[key] == true) {
          listSelect[key]["_" + index] = true;
          config[key].select = true;
        }
      });
    }
    setConfig({ ...config });
    // console.log("config >> ", config)
  }, [listIsuer]);

  useEffect(() => {}, [props.component_obj.data]);

  const condExcludeFillter = (key, item, index) => {
    return item == true;
  };

  const removeAllCb = (condition_key) => {
    listIsuer.map((user, index) => {
      user[condition_key] = false;
      return user;
    });
    setListIsuer([...listIsuer]);
    listSelect[condition_key] = {};
    setListSelect({ ...listSelect });
    config[condition_key].select = false;
    setConfig({ ...config });
    if (props.onDataChange) {
      props.onDataChange(listIsuer);
    } else {
      console.error("handleEventDataCallback is required");
      throw "handleEventDataCallback - onDataChange is required";
    }
  };

  const rdListener = (e) => {
    console.log("remove all select", e.target.value);
    // console.log(e.target.name, e.target.value)
    if (e.target.value.toString() == "true") {
      console.log("add");
      setTarget(e.target);
      setShowListCondition(e.target.name);
    } else {
      console.log("remove");
      removeAllCb(e.target.name);
    }
  };

  const confirmConditions = () => {
    const lengtharr = listSelect[showListCondition];
    let isValid = false;
    Object.keys(lengtharr).map((checkitem, index) => {
      if (lengtharr[checkitem] == true) {
        isValid = true;
      }
    });
    config[showListCondition].select = isValid;
    listIsuer.map((item, index) => {
      item[showListCondition] = lengtharr["_" + index];
      return item;
    });
    setShowListCondition(false);
    setConfig({ ...config });
    if (props.onDataChange) {
      props.onDataChange(listIsuer);
    } else {
      console.error("handleEventDataCallback is required");
      throw "handleEventDataCallback - onDataChange is required";
    }
  };

  const onUserChangeCondition = (checked, index) => {
    // const arr = listIsuer
    // arr[index][showListCondition] = checked
    // setListIsuer([...arr])
    if (listSelect[showListCondition]) {
      listSelect[showListCondition]["_" + index] = checked;
    } else {
      listSelect[showListCondition] = {};
      listSelect[showListCondition]["_" + index] = checked;
    }

    setListSelect({ ...listSelect });
  };

  const checkedBox = (cond, index) => {
    if (listSelect[cond]) {
      if (listSelect[cond]["_" + index]) {
        return listSelect[cond]["_" + index];
      }
    }

    return false;
  };

  return (
    <div className="mt-15">
      {Object.entries(config).map((item, index) => {
        return (
          <div className="mt-15">
            {item[1]?.label && (
               <>
                 <p>{item[1].label}</p>
                 <div className={styles.rd_group}>
                   <input
                       className={styles.radio}
                       type="radio"
                       name={item[0]}
                       value={true}
                       id={`${item[0]}-radio-1`}
                       onChange={rdListener}
                       checked={config[item[0]].select}
                   />
                   <label htmlFor={`${item[0]}-radio-1`}>{lng.trans(props.component_obj?.define?.label_co)}</label>
                   <div className={styles.sp_hoz} />
                   <input
                       className={styles.radio}
                       type="radio"
                       name={item[0]}
                       value={false}
                       id={`${item[0]}-radio-2`}
                       onChange={rdListener}
                       checked={!config[item[0]].select}
                   />
                   <label htmlFor={`${item[0]}-radio-2`}>{lng.trans(props.component_obj?.define?.label_khong)}</label>
                 </div>
               </>
            ) }

          </div>
        );
      })}

      <Modal show={showListCondition} onHide={() => setShowListCondition(false)} centered>
        <Modal.Header closeButton className={styles.custom_header_preview}>
          Chọn người trong danh sách loại trừ
        </Modal.Header>
        <Modal.Body>
          <span className={styles.sub_title_modal}>
            {showListCondition ? config[showListCondition].sublabel : ""}
          </span>
          <div className={styles.content_popover_custom}>
            <div className="list-ndbh">
              {listIsuer.map((user, index) => {
                return (
                    <div className={styles.itenm_list_ndbh_new}>
                      <input
                          className={styles.checkbox}
                          type="checkbox"
                          id={"listiss-" + user.id}
                          checked={checkedBox(showListCondition, index)}
                          onChange={(e) => {
                            onUserChangeCondition(e.target.checked, index);
                          }}
                      />
                      <label for={"listiss-" + user.id}>
                        {user?.name?.toUpperCase()} ({" "}
                        {user.isspackage ? user.isspackage.PACK_NAME : ""} )
                      </label>
                    </div>
                );
              })}
              <div className={styles.form_btn_confirm_nkdbh_new}>
                <button
                    className={styles.btn_confirm_nkdbh_new}
                    onClick={(e) => confirmConditions()}
                >
                  {lng.trans(props.component_obj?.define?.label_xac_nhan)}
                </button>
              </div>
            </div>
          </div>
        </Modal.Body>
      </Modal>
    </div>
  );
});

export default LockoutList;
