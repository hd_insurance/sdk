import React, { useEffect, useState, createRef, forwardRef } from "react";
import {
  FLInput,
  FLDate,
  FLAddress,
  FLSelect,
  FLRadio,
  FLCheckBox,
  FLSwitch,
  FLAmount,
  UploadFrame
} from "hdi-uikit";
import { Button } from "react-bootstrap";
import ReactHtmlParser from "react-html-parser";
import styles from "../../css/style.module.css";
import TopTitle from "../toptitle";
import Wizard from "./wizardrender";
import StepContainer from "./stepcontainerrender";
import StepRender from "./steprender";
import FormRender from "./formrender";
import TabContainer from "./tabcontainer";
import TabHeader from "./tabheader";
import TabItem from "./tabitem";
import Item from "./item";
import PopButton from "./popbutton";
import LongRadio from "./longradio";
import Listbh from "./listbh";
import Voucher from "./voucher";
import TotalPayment from "./totalpayment";
import DivRender from "./divrender";
import RelationSelect from "./relationList";
import LockOutList from "./lockoutlist";

const Layout = forwardRef((props, ref) => {
  const level = props.level ? parseInt(props.level) : 0;
  const hasLayout = (item) => {
    return item.layout && item.layout.length > 0;
  };
  const { layout, from } = props;

  const renderLayout = (callback) => {
    if (level == 0) {
      switch (layout.type) {
        case "screen":
          return <Layout {...props} layout={layout.layout} level={level + 1} />;
        case "item":
          return (
            <Item key={`${from}-level-${level}-${props.id}`}>
              {hasLayout(layout) && (
                <Layout {...props} layout={layout.layout} level={level + 1} />
              )}
            </Item>
          );
        default:
          return <div></div>;
      }
    } else {
      return layout.map((item, i) => {
        if (level == 1) {
          if (layout.length - 1 == i) {
            setTimeout(() => {
              if (callback) {
                callback();
              }
            }, 200);
          }
        }
        //for loop to render layout
        switch (item.type) {
          case "button":
            return (
              <Button
                key={`${from}-level-${level}-${i}`}
                className={`${item.extend_class}`}
              >
                {item.define.value}
              </Button>
            );
          case "img":
            return (
              <img
                key={`${from}-level-${level}-${i}`}
                className={item.extend_class}
                src={item.define.url}
                style={{ width: item.define.width, height: item.define.height }}
              />
            );
          case "div":
            return (
              <DivRender
                key={`${from}-level-${level}-${i}`}
                component_obj={item}
                extend_class={item.extend_class}
                onDivClick={item.onClick}
              >
                {hasLayout(item) && (
                  <Layout {...props} layout={item.layout} level={level + 1} />
                )}
              </DivRender>
            );
          case "totalpayment":
            return <TotalPayment key={`${from}-level-${level}-${i}`} data={item.define.data} />;
          case "voucher":
            return <Voucher key={`${from}-level-${level}-${i}`} />;
          case "listbh":
            return (
              <Listbh
                key={`${from}-level-${level}-${i}`}
                ref={item.ref}
                component_obj={item}
                onDataChange={item.onDataChange}
              />
            );
          case "para":
            return (
              <p
                key={`${from}-level-${level}-${i}`}
                className={` ${item.extend_class}`}
              >
                {item.define.value}
              </p>
            );
          case "relationselect":
            return (
              <RelationSelect
                render={item.render}
                key={`${from}-level-${level}-${i}`}
                description={item.define.description}
                data={item.define.list}
                value={item.define.value}
                extend_class={item.extend_class}
                changeEvent={item.onValueChange}
                component_obj={item}
                isDisable={item.isDisable}
              />
            );
          case "lockoutlist":
            return (
              <LockOutList
                key={`${from}-level-${level}-${i}`}
                onDataChange={item.onDataChange}
                data={item.define.list}
                component_obj={item}
                ref={item.ref}
                extend_class={item.extend_class}
              />
            );
          case "longradio":
            return (
              <LongRadio
                key={`${from}-level-${level}-${i}`}
                description={item.define.description}
                data={item.define.list}
                value={item.define.value}
                extend_class={item.extend_class}
                changeEvent={item.onValueChange}
              />
            );
          case "popbutton":
            return (
              <PopButton
                key={`${from}-level-${level}-${i}`}
                ref={item.ref}
                component_obj={item}
              />
            );
          case "lineinform":
            return (
              <div
                className={`${styles.p_line} mt-15`}
                key={`${from}-level-${level}-${i}`}
              />
            );
          case "item":
            return (
              <Item key={`${from}-level-${level}-${i}`}>
                {hasLayout(item) && (
                  <Layout {...props} layout={item.layout} level={level + 1} />
                )}
              </Item>
            );
          case "tabsitem":
            return (
              <TabItem
                key={`${from}-level-${level}-${i}`}
                text ={item.text}
                btn_text ={item.btn_text}
                component_obj={item}
                ref={item.ref}
                onAddItemClick={item.addItemClick}
              />
            );
          case "tabheader":
            return (
              <TabHeader
                define={item.define}
                key={`${from}-level-${level}-${i}`}
                component_obj={item}
                onAddItemClick={item.addItemClick}
              />
            );
          case "tabcontainer":
            return (
              <TabContainer key={`${from}-level-${level}-${i}`}>
                {hasLayout(item) && (
                  <Layout {...props} layout={item.layout} level={level + 1} />
                )}
              </TabContainer>
            );
          case "toptitle":
            return (
              <TopTitle
                key={`${from}-level-${level}-${i}`}
                title={item.define.title}
                subtitle={item.define.sub_title}
              />
            );
          case "form":
            return (
              <FormRender
                key={`${from}-level-${level}-${i}`}
                ref={item.ref}
                extend_class={item.extend_class}
              >
                {hasLayout(item) && (
                  <Layout {...props} layout={item.layout} level={level + 1} />
                )}
              </FormRender>
            );
          case "step":
            return (
              <StepRender
                key={`${from}-level-${level}-${i}`}
                step={item.define.order}
                eventKey={item.define.order}
              >
                {hasLayout(item) && (
                  <Layout {...props} layout={item.layout} level={level + 1} />
                )}
              </StepRender>
            );
          case "row":
            return (
              <div
                className={`row ${item.extend_class} ${
                  item.define?.isHide ? styles.hide : ""
                }`}
                key={`${from}-level-${level}-${i}`}
                ref={item.ref}
              >
                {hasLayout(item) && !item.define.isHide && (
                  <Layout {...props} layout={item.layout} level={level + 1} />
                )}
              </div>
            );
          case "col":
            return (
              <div
                className={`${item.col} mt-15 ${item.extend_class} ${
                  item.define?.isHide ? styles.hide : ""
                }`}
                key={`${from}-level-${level}-${i}`}
              >
                {hasLayout(item) && !item.define.isHide && (
                  <Layout {...props} layout={item.layout} level={level + 1} />
                )}
              </div>
            );
          case "group_ipnv":
            return (
              <div
                className={`${styles.group_ipnv} ${item.extend_class}`}
                key={`${from}-level-${level}-${i}`}
              >
                {hasLayout(item) && (
                  <Layout {...props} layout={item.layout} level={level + 1} />
                )}
              </div>
            );
          case "center_line":
            return (
              <div
                key={`${from}-level-${level}-${i}`}
                className={`${styles.center_line} ${item.extend_class}`}
              ></div>
            );
          case "fladdress":
            return (
              <FLAddress
                key={`${from}-level-${level}-${i}`}
                disable={item.define.disable}
                template_id={item.template_id}
                value={item.define.value}
                label={item.define.label}
                required={item.define.is_required}
                hideborder={item.define.hideborder}
                extend_class={item.extend_class}
                ref={item.ref}
                changeEvent={item.onValueChange}
                component_obj={item}
                pattern={item.define.pattern}
              />
            );
          case "single_select":
            return (
              <FLSelect
                key={`${from}-level-${level}-${i}`}
                data={item.define.list}
                label={item.define.label}
                required={item.define.is_required}
                disable={item.define.disable}
                value={item.define.value}
                template_id={item.template_id}
                changeEvent={item.onValueChange}
                extend_class={item.extend_class}
                hideborder={item.define.hideborder}
                ref={item.ref}
                component_obj={item}
              />
            );
          case "flinput":
            return (
              <FLInput
                key={`${from}-level-${level}-${i}`}
                value={item.define.value}
                isUpperCase={item.define.isUpperCase}
                label={item.define.label}
                required={item.define.is_required}
                disable={item.define.disable}
                template_id={item.template_id}
                changeEvent={item.onValueChange}
                hideborder={item.define.hideborder}
                extend_class={item.extend_class}
                ref={item.ref}
                component_obj={item}
                pattern={item.define.pattern}
              />
            );
          case "flamount":
            return (
              <FLAmount
                key={`${from}-level-${level}-${i}`}
                value={item.define.value}
                isUpperCase={item.define.isUpperCase}
                label={item.define.label}
                required={item.define.is_required}
                disable={item.define.disable}
                template_id={item.template_id}
                changeEvent={item.onValueChange}
                hideborder={item.define.hideborder}
                extend_class={item.extend_class}
                ref={item.ref}
                component_obj={item}
                pattern={item.define.pattern}
              />
            );
          case "flradio":
            return (
              <FLRadio
                key={`${from}-level-${level}-${i}`}
                value={item.define.value}
                label={item.define.label}
                data={item.define.list}
                template_id={item.template_id}
                changeEvent={item.onValueChange}
                extend_class={item.extend_class}
                ref={item.ref}
                component_obj={item}
              />
            );
          case "fldate":
            return (
              <FLDate
                key={`${from}-level-${level}-${i}`}
                value={item.define.value}
                label={item.define.label}
                selectedDay={item.define.defaultvalue}
                required={item.define.is_required}
                disable={item.define.disable}
                minimumDate={item.define.minimumDate}
                maximumDate={item.define.maximumDate}
                template_id={item.template_id}
                changeEvent={item.onValueChange}
                extend_class={item.extend_class}
                hideborder={item.define.hideborder}
                ref={item.ref}
                component_obj={item}
              />
            );
          case "flcheckbox":
            return (
              <FLCheckBox
                key={`${from}-level-${level}-${i}`}
                value={item.define.value}
                label={ReactHtmlParser(item.define.label)}
                required={item.define.is_required}
                disable={item.define.disable}
                template_id={item.template_id}
                changeEvent={item.onValueChange}
                extend_class={item.extend_class}
                component_obj={item}
              />
            );
          case "flswitch":
            return (
              <FLSwitch
                key={`${from}-level-${level}-${i}`}
                value={item.define.value}
                label={ReactHtmlParser(item.define.label)}
                required={item.define.is_required}
                disable={item.define.disable}
                template_id={item.template_id}
                changeEvent={item.onValueChange}
                extend_class={item.extend_class}
                component_obj={item}
              />
            );
          case "step_layout":
            return (
              <StepContainer key={`${from}-level-${level}-${i}`} ref={item.ref}>
                {hasLayout(item) && (
                  <Layout {...props} layout={item.layout} level={level + 1} />
                )}
              </StepContainer>
            );
          case "wizard":
            return (
              <Wizard
                key={`${from}-level-${level}-${i}`}
                data={item.define.data}
                ref={item.ref}
              />
            );
          case "upload_frame":
            return (
              <div>
              Upload cho nay
              <UploadFrame
                key={`${from}-level-${level}-${i}`}
                data={item.define.data}
                ref={item.ref}
              />
              </div>
            );
          default:
            return <div />;
        }
      });
    }
  };

  return <React.Fragment>{renderLayout(props.onRenderSuccess)}</React.Fragment>;
});

export default Layout;
