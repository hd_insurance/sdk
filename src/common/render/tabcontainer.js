import React, {
  useEffect,
  useState,
  createRef,
  forwardRef,
  useImperativeHandle,
} from "react";
import { Tab } from "react-bootstrap";
import styles from "../../css/style.module.css";

const TabContainer = forwardRef((props, ref) => {
  const [activeKey, setActiveKey] = useState(props.defaultActiveKey || 0);
  useImperativeHandle(ref, () => ({
    setStep(step) {
      setActiveKey(step);
    },
  }));
  return (
    <div className={styles.user_related_tabbed}>
      <Tab.Container id="left-tabs-example" activeKey={activeKey} >
        {props.children}
      </Tab.Container>
    </div>
  );
});

export default TabContainer;
