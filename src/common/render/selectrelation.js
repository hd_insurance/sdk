import React, {
  useEffect,
  useState,
  createRef,
  forwardRef,
  useImperativeHandle,
} from "react";
import styles from "../../css/style.module.css";
import Popover from "react-popover";
import { FLMultiSelect } from "hdi-uikit";
const relationList = [
  {
    title: "Con cái",
    subtitle: "Bạn mua cho ai",
    list: [
      { title: "Con trai", value: "CON_CAI-M" },
      { title: "Con gái", value: "CON_CAI-F" },
    ],
  },
  {
    title: "Vợ/chồng",
    subtitle: "Bạn mua cho ai",
    list: [
      { title: "Chồng", value: "VO_CHONG-M" },
      { title: "Vợ", value: "VO_CHONG-F" },
    ],
  },
];
const Main = forwardRef((props, ref) => {
  const [openRelation, setShowRelation] = useState(false);
  const [relation, setRelation] = useState({
    title: props.value.title || "",
    value: props.value.value || "",
  });
  const [listData, setListData] = useState(props.data || relationList);
  const relationSelect = (value) => {
    setRelation(value);
    setShowRelation(false);
    if (props.changeEvent) {
      props.changeEvent(value);
    }
  };
  const getRelationTitleByValue = (value) => {
    switch (value) {
      case "BO_ME-M":
        return "Bố";
      case "BO_ME-F":
        return "Mẹ";
      case "CON_CAI-M":
        return "Con trai";
      case "CON_CAI-F":
        return "Con gái";
      case "VO_CHONG-M":
        return "Chồng";
      case "VO_CHONG-F":
        return "Vợ";
      default:
        return value;
    }
  };
  const popoverPropsRelation = {
    isOpen: openRelation,
    place: "below",
    preferPlace: "right",
    onOuterAction: () => setShowRelation(false),
    body: [
      <FLMultiSelect
        title={"Chọn mối quan hệ"}
        relationList={listData}
        selected={relationSelect}
      />,
    ],
  };
  useImperativeHandle(ref, () => ({}));
  return (
    <div className={props.component_obj.define.isHide ? styles.hide : ""}>
      <Popover {...popoverPropsRelation}>
        <a className={styles.relation} onClick={(e) => setShowRelation(true)}>
          {relation.title
            ? `${`Mối quan hệ`}: ` + getRelationTitleByValue(relation.value)
            : "Chọn mối quan hệ"}
          <i className={`${styles.fas} ${styles.fa_sort_down}`}></i>
        </a>
      </Popover>
    </div>
  );
});

export default Main;
