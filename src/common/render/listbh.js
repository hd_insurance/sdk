import React, {
  useEffect,
  useState,
  createRef,
  forwardRef,
  useImperativeHandle,
} from "react";
import styles from "../../css/style.module.css";
import currencyFormatter from "currency-formatter";
import { Button, Col, Row, Popover, Overlay, Accordion, Modal } from "react-bootstrap";
import { isMobile } from 'react-device-detect';


const Listbh = forwardRef((props, ref) => {
  const [target, setTarget] = useState(null);
  const [countExclude, setCountExclude] = useState(0);
  const [currentPreview, setCurrentPreview] = useState({});
  const refNGKDBH = createRef(null);
  const [showListChangeNDBH, setShowListChangeNDBH] = useState(false);
  const [showPreview, setShowPreview] = useState(false);
  const [listIssur, setListIssur] = useState([]);
  const [rm_exclude, setListExclude] = useState({});
  const [keyAc, setKeyAc] = useState(true);
  const [show, setShow] = useState(false);

  const [config, setConfig] = useState({
    EXCLUDE_COND1: {
      label:
        "NĐBH có đang trong thời gian điều trị bệnh hoặc điều trị thương tật?",
      sublabel:
        "Chọn người mắc một hay các bệnh: ung thư, u bướu các loại, huyết áp, tim mạch....?",
      select: false,
    },
    EXCLUDE_COND2: {
      label:
        "NĐBH có bị bệnh tâm thần, ung thư, bệnh phong, thương tật vĩnh viễn quá 70%?",
      sublabel:
        "Chọn người bị tâm thần, thần kinh, bệnh phong thương tật vĩnh viễn quá 70%. Người đang điều trị bệnh hoặc thương tật",
      select: false,
    },
    EXCLUDE_COND3: {
      label:
        "NĐBH có người nào bị tàn phế hoặc thương tật vĩnh viễn từ 50% trở lên không?",
      sublabel:
        "Chọn người bị tàn phế hoặc thương tật vĩnh viễn từ 50% trở lên",
      select: false,
    },
  });
  const [amountConfig, setAmoutConfig] = useState({
    price: 0,
    total: 0,
  });
  const refPreview = createRef(null);

  const handClickPreview = (event, id) => {
    setCurrentPreview(listIssur.filter(p => p.id === id)[0]);
    setShow(!show)
    setTarget(event.target);
  };

  const formatCurrency = (value) => {
    return currencyFormatter.format(value, {
      code: l.g("bhsk.currency"),
      precision: 0,
      format: "%v %s",
      symbol: l.g("bhsk.currency"),
    });
  };

  useImperativeHandle(ref, () => ({
    setData(usrList) {
      setListIssur(props.component_obj.data);
      setCountExclude(props.component_obj.data.filter(condExclude).length);
      props.component_obj.data.filter(condExclude).map((user, index) => {
        rm_exclude["_" + index] = false;
      });
    },
  }));

  useEffect(() => {
    if (props.component_obj.data != null) {
      setListIssur(props.component_obj.data);
      setCountExclude(props.component_obj.data.filter(condExclude).length);

      props.component_obj.data.filter(condExclude).map((user, index) => {
        rm_exclude["_" + index] = false;
      });
    }
  }, [props.component_obj.data]);
  useEffect(() => {
    if (listIssur.filter(condNDBH).length > 3) {
      setKeyAc(false);
    } else {
      setKeyAc(true);
    }
  }, [listIssur])

  const condExclude = (item) => {
    var isValid = false;
    for (const [key, value] of Object.entries(config)) {
      if (item[key] == true) {
        isValid = true;
      }
    }

    return isValid;
  };

  const condNDBH = (item) => {
    var isValid = true;
    for (const [key, value] of Object.entries(config)) {
      if (item[key] == true) {
        isValid = false;
      }
    }
    return isValid;
  };

  const handClickChangeNDBH = (event) => {
    setShowListChangeNDBH(!showListChangeNDBH);
    setTarget(event.target);
    // const newarr = props.isruserlist.filter(function(item) {
    //   return ((item.EXCLUDE_COND2 == 0)||(item.EXCLUDE_COND1 == 0))
    // })
    // setListExclude(newarr)
  };

  const confirmNKDBH = () => {
    setShowListChangeNDBH(false);
    listIssur.filter(condExclude).map((user, index) => {
      for (const [key, value] of Object.entries(config)) {
        if (user[key] == true) {
          if (rm_exclude["_" + index] == true) {
            user[key] = false;
          }
        }
      }
      return user;
    });
    setCountExclude(listIssur.filter(condExclude).length);

    if (props.onDataChange) {
      props.onDataChange(listIssur);
    } else {
      console.error("handleEventDataCallback is required");
      throw "handleEventDataCallback - onDataChange is required";
    }

    setListIssur(listIssur);
  };

  const remove_exclude_checked = (isChecked, index) => {
    if (isChecked.toString() == "true") {
      rm_exclude["_" + index] = true;
    } else {
      //add item to object
      rm_exclude["_" + index] = false;
    }

    setListExclude(rm_exclude);
    console.log(rm_exclude);
  };

  const onEditClick = (id) => {
    if (props.component_obj.onClickItemEdit) {
      props.component_obj.onClickItemEdit(id);
    }
  };

  const genderShow = (gender) => {
    return gender == "M" ? "Mr" : "Ms";
  };

  var listbh_mobile = <div className={styles.accordion_listndbh}>
    <Accordion defaultActiveKey={"0"} activeKey={keyAc}>
      <div className={styles.listndbh_header} onClick={(e) => setKeyAc(!keyAc)}>
        <div className="title">
          {props?.component_obj?.define?.label_list_ndbh} <span className={styles.count_ndbh}>{` (${listIssur.filter(condNDBH).length})`}</span>
        </div>
        <div className={styles.title_right}>
          <span>{keyAc ? 'Ẩn' : 'Chi tiết'}</span>
          <i className={`fas ${keyAc ? 'fa-angle-up' : 'fa-angle-down'}`} />
        </div>
      </div>
      <Accordion.Collapse className="upload-body" eventKey={keyAc}>
        <div className={styles.form_list_ndbh}>
          {listIssur.filter(condNDBH).map((item, index) => {
            return (
              <div className={styles.item_list_ndbh}>
                <div className="row">
                  <div className="col-md-8">
                    <div className={styles.title_item_ndbh}>
                      <div className={styles.mname}>
                        <label>
                          {index + 1}. {`${genderShow(item.gender)}.`}
                        </label>
                        <label>{item?.name?.toUpperCase()}</label>
                      </div>

                      <label className={styles.am}>
                        {formatCurrency(item.amount)}
                      </label>
                    </div>
                    <div className={styles.name_package_insurance}>
                      {`${item.isspackage ? item.isspackage.PACK_NAME : ""} (${item.affectday
                        } - ${item.expiration})`}
                    </div>
                  </div>
                  <div className={`col-md-4 ${styles.btn_action_item}`}>
                    <button
                      className={styles.preview_item_ndbh}
                      onClick={(event) => handClickPreview(event, item.id)}
                    >
                      <i className={`${styles.fas} ${styles.fa_eye}`}></i>
                      <label>{l.g("bhsk.form.label_view")}</label>
                    </button>
                    <button
                      className={styles.edit_item_ndbh}
                      onClick={() => onEditClick(item.id)}
                    >
                      <i className={`${styles.fas} ${styles.fa_edit}`}></i>
                      <label>{l.g("bhsk.form.label_edit")}</label>
                    </button>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      </Accordion.Collapse>
    </Accordion>
  </div>

  var listbh_web = <div className={styles.form_list_ndbh}>
    {/* <p>{JSON.stringify(listIssur.filter(condNDBH))}</p> */}
    {listIssur.filter(condNDBH).map((item, index) => {
      return (
        <div className={styles.item_list_ndbh}>
          <div className="row">
            <div className="col-md-8">
              <div className={styles.title_item_ndbh}>
                <div className={styles.mname}>
                  <label>
                    {index + 1}. {`${genderShow(item.gender)}.`}
                  </label>
                  <label>{item?.name?.toUpperCase()}</label>
                </div>

                <label className={styles.am}>
                  {formatCurrency(item.amount)}
                </label>
              </div>
              <div className={styles.name_package_insurance}>
                {`${item.isspackage ? item.isspackage.PACK_NAME : ""} (${item.affectday
                  } - ${item.expiration})`}
              </div>
            </div>
            <div className={`col-md-4 ${styles.btn_action_item}`}>
              <button
                className={styles.preview_item_ndbh}
                onClick={(event) => handClickPreview(event, item.id)}
              >
                <i className={`${styles.fas} ${styles.fa_eye}`}></i>
                <label>{l.g("bhsk.form.label_view")}</label>
              </button>
              <button
                className={styles.edit_item_ndbh}
                onClick={() => onEditClick(item.id)}
              >
                <i className={`${styles.fas} ${styles.fa_edit}`}></i>
                <label>{l.g("bhsk.form.label_edit")}</label>
              </button>
            </div>
          </div>
        </div>
      );
    })}
  </div>

  var listLockout = <div className={`row ${styles.form_content_ngkdbh}`}>
    <div className={`col-9 ${styles.ngkdbh_name}`}>
      {lng.trans(props.component_obj?.define?.label_nguoi_k_dcbh)}(
      {` ${countExclude}`})
      {/* Người không được bảo hiểm */}
    </div>
    <div className={`col-3 ${styles.change_ngdbh}`}
      onClick={(e) => {
        handClickChangeNDBH(e);
      }}
    >
      {lng.trans(props.component_obj?.define?.label_thay_doi)}
    </div>
    <div className={`col-12 ${styles.content_ngkdbh}`}>
      {/* Những người trong danh sách này sẽ không được tính bảo hiểm */}
      {lng.trans(props.component_obj?.define?.label_nhungngkbh)}
      <div>
        {listIssur.filter(condExclude).map((user, index) => {
          return (
            <div className={styles.itenm_list_ndbh}>
              <label for={"user-item-" + user.id}>
                {user?.name?.toUpperCase()} ({" "}
                {user.isspackage ? user.isspackage.PACK_NAME : ""} )
              </label>
            </div>
          );
        })}
      </div>

    </div>
  </div>

  var content_popup_lockout = <div>
    {listIssur.filter(condExclude).map((user, index) => {
      return (
        <div className={styles.itenm_list_ndbh}>
          <input
            className={styles.checkbox}
            type="checkbox"
            id={"user-item-" + user.id}
            onChange={(e) => {
              remove_exclude_checked(e.target.checked, index);
            }}
          />
          <label for={"user-item-" + user.id}>
            {user?.name?.toUpperCase()} ({" "}
            {user.isspackage ? user.isspackage.PACK_NAME : ""} )
          </label>
        </div>
      );
    })}
  </div>
  return (
    <>
      {isMobile ? listbh_mobile : listbh_web}

      {/* List lockout */}
      {countExclude > 0 && listLockout}

      <Modal show={showListChangeNDBH} onHide={() => setShowListChangeNDBH(false)} centered>
        <Modal.Header closeButton className={styles.custom_header_preview}>
          Chọn người được bảo hiểm
        </Modal.Header>
        <Modal.Body>
          <span className={styles.sub_title_modal}>
            {lng.trans(props.component_obj?.define?.label_chon_nguoi)}
          </span>
          <div className={styles.content_popover_custom}>
            <div className="list-ndbh">
              {content_popup_lockout}
              <div className={styles.form_btn_confirm_nkdbh_new}>
                <button className={styles.btn_confirm_nkdbh_new} onClick={(e) => confirmNKDBH()}>
                  {lng.trans(props.component_obj?.define?.label_xac_nhan)}
                </button>
              </div>
            </div>
          </div>
        </Modal.Body>
      </Modal>

      <Modal show={show} onHide={() => setShow(false)} centered>
        <Modal.Header closeButton className={styles.custom_header_preview}>
          Thông tin người được bảo hiểm
        </Modal.Header>
        <Modal.Body>
          <div className="">
            <div className={styles.detail_item_ndbh}>
              <label>
                {lng.trans(props.component_obj?.define?.label_hoten)}:
              </label>
              <label>{currentPreview.name}</label>
            </div>
            <div className={styles.detail_item_ndbh}>
              <label >
                {lng.trans(props.component_obj?.define?.label_cmnd)}:
              </label>
              <label >{currentPreview.passport}</label>
            </div>
            <div className={styles.detail_item_ndbh}>
              <label>
                {lng.trans(props.component_obj?.define?.label_dob)}:
              </label>
              <label >{currentPreview.dob}</label>
            </div>
            <div className={styles.detail_item_ndbh}>
              <label>
                {lng.trans(props.component_obj?.define?.label_sdt)}:
              </label>
              <label>{currentPreview.phone}</label>
            </div>
            {currentPreview.email && (
              <div className={styles.detail_item_ndbh}>
                <label>Email:</label>
                <label>{currentPreview.email}</label>
              </div>
            )}
            <div className={styles.detail_item_ndbh}>
              <label>
                {lng.trans(props.component_obj?.define?.label_diachi)}:
              </label>
              <label>
                {currentPreview.address ? currentPreview.address + ' - ' : ''}
                {currentPreview.addressCode ? currentPreview.addressCode.label : ""}
              </label>
            </div>
            <div className={styles.detail_item_ndbh}>
              <label>
                {lng.trans(props.component_obj?.define?.label_goibh)}:
              </label>
              <label>
                {currentPreview.isspackage
                  ? currentPreview.isspackage.PACK_NAME
                  : ""}
              </label>
            </div>
            <div className={styles.detail_item_ndbh}>
              <label>
                {lng.trans(props.component_obj?.define?.label_thoihanbh)}:
              </label>
              <label>{`${currentPreview.affectday} - ${currentPreview.expiration}`}</label>
            </div>
            {props.component_obj?.define?.label_nguoi_th && (
              <div className={styles.detail_item_ndbh}>
                <label>
                  {lng.trans(props.component_obj?.define?.label_nguoi_th)}:
                </label>
                <label>{currentPreview?.beneficiary === 'NDBH' ? 'Người được bảo hiểm' : currentPreview?.beneficiary_name}</label>
              </div>
            )}
            {currentPreview?.beneficiary === 'KHAC' && (
              <>
                <div className={styles.detail_item_ndbh}>
                  <label>
                    {lng.trans(props.component_obj?.define?.label_relationship)}:
                  </label>
                  <label>{currentPreview?.beneficiary_rela?.title}</label>
                </div>
                <div className={styles.detail_item_ndbh}>
                  <label>
                    {lng.trans(props.component_obj?.define?.label_cmnd_th)}:
                  </label>
                  <label>{currentPreview?.beneficiary_cmt}</label>
                </div>
              </>
            )}
          </div>
          <div className={styles.total_payment_item_ndbh}>
            <div className={`row ${styles.row_kutin}`}>
              <label className="col-4">
                {l.g("bhsk.form.lbl_total_am2")}
              </label>
              <label className="col-8">
                {formatCurrency(currentPreview.amount)}
              </label>
            </div>
          </div>
        </Modal.Body>
      </Modal>
    </>
  );
});

export default Listbh;
