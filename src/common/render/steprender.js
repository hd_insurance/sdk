import React, {
  useEffect,
  useState,
  createRef,
  forwardRef,
  useImperativeHandle,
} from "react";
import { Tab } from "react-bootstrap";
import styles from "../../css/style.module.css";

const StepRender = forwardRef((props, ref) => {
  useImperativeHandle(ref, () => ({}));
  return (
    <Tab.Pane eventKey={props.eventKey} key={props.step}>
      <div className={`${styles.indemnify_form} ${styles.f1}`}>
        {props.children}
      </div>
    </Tab.Pane>
  );
});

export default StepRender;
