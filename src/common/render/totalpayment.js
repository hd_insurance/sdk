import React, {
  useEffect,
  useState,
  createRef,
  forwardRef,
  useImperativeHandle,
} from "react";
import { Form, Button } from "react-bootstrap";
import styles from "../../css/style.module.css";
import currencyFormatter from "currency-formatter";

const TotalPayment = forwardRef((props, ref) => {
  const formatCurrency = (value) => {
    return currencyFormatter.format(value, {
      code: l.g("bhsk.currency"),
      precision: 0,
      format: "%v %s",
      symbol: l.g("bhsk.currency"),
    });
  };
  useImperativeHandle(ref, () => ({}));
  return (
    <div className={styles.form_total_payment}>
      <div className={styles.total_cost_insur}>
        <label>{lng.trans(props.component_obj?.define?.label_phibh||"Phí bảo hiểm")}:</label>
        <label>
          {currencyFormatter.format(props.data, {
            code: l.g("bhsk.currency"),
            precision: 0,
            format: "%v %s",
            symbol: l.g("bhsk.currency"),
          })}
        </label>
      </div>
      <div className={styles.total_cost_insur}>
        <label>{l.g("bhsk.form.f3_title3")}:</label>
        <label>
          -
          {currencyFormatter.format(0, {
            code: l.g("bhsk.currency"),
            precision: 0,
            format: "%v %s",
            symbol: l.g("bhsk.currency"),
          })}
        </label>
      </div>
      <div className={styles.total_payment}>
        <label>{l.g("bhsk.form.f3_title4")}:</label>
        <label>
          {currencyFormatter.format(props.data, {
            code: l.g("bhsk.currency"),
            precision: 0,
            format: "%v %s",
            symbol: l.g("bhsk.currency"),
          })}
        </label>
      </div>
    </div>
  );
});

export default TotalPayment;
