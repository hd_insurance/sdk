import React, {
  useEffect,
  useState,
  createRef,
  forwardRef,
  useImperativeHandle,
} from "react";
import styles from "../../css/style.module.css";
import { FLRadio } from "hdi-uikit";

const LongRadio = forwardRef((props, ref) => {
  useImperativeHandle(ref, () => ({}));
  return (
    <div className={props.extend_class}>
      {props.description.map((e, i) => {
        return <p key={i}>{e}</p>;
      })}
      <FLRadio value={props.value} data={props.data} changeEvent={props.changeEvent}/>
    </div>
  );
});

export default LongRadio;
