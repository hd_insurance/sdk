import React, {
  useEffect,
  useState,
  createRef,
  forwardRef,
  useImperativeHandle,
} from "react";
import { Tab } from "react-bootstrap";
import styles from "../../css/style.module.css";

const Item = forwardRef((props, ref) => {
  const [activeKey, setActiveKey] = useState(props.defaultActiveKey || 0);
  useImperativeHandle(ref, () => ({
    setStep(step) {
      setActiveKey(step);
    },
  }));
  return (
    <Tab.Pane  eventKey={0}>
      {props.children}
    </Tab.Pane>
  );
});

export default Item;
