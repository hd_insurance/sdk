import React, { useEffect, useState, createRef, forwardRef, useImperativeHandle } from "react";
import Wizard from "../../common/topwizard";

const WizardRender = forwardRef((props, ref) => {
  const [data, setData] = useState(props.data);
  const [activeTab, setActiveTab] = useState(
    props.defaultActive ? props.defaultActive : 0
  );
  useImperativeHandle(ref, () => ({
    setStep(step) {
      setActiveTab(step);
    },
  }));

  return <Wizard active={activeTab} data={data} />;
});

export default WizardRender;
