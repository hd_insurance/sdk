import React, {
  useEffect,
  useState,
  createRef,
  forwardRef,
  useImperativeHandle,
} from "react";
import { Form, Button } from "react-bootstrap";
import styles from "../../css/style.module.css";
import ReactHtmlParser from "react-html-parser";

const DivRender = forwardRef((props, ref) => {
  useImperativeHandle(ref, () => ({}));
  const handleOnclick = () => {
    if (props.onDivClick) {
      props.onDivClick();
    }
  };
  return (
    <div className={`${props.extend_class}`} onClick={handleOnclick}>
      {props.children}
      {ReactHtmlParser(lng.trans(props.component_obj.define.label))}
    </div>
  );
});

export default DivRender;
