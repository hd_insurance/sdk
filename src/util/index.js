import { createRef } from "react";
import LayoutModel from "../models/layoutModel";
import TabsHeader from "../models/tabsheader";
import TabsItem from "../models/tabsitem";
import ListIssuer from "../models/ListIssuer.js";
import SingleSelect from "../models/SingleSelect.js";
import RelationSelect from "../models/RelationSelect.js";
import LockoutList from "../models/LockoutList.js";
import TotalPayment from "../models/totalpayment.js";
import UploadModel from "../models/uploadModel";
import currencyFormatter from "currency-formatter";
import moment, { now } from "moment";
import randomstring from "randomstring";

const formatCurrency = (value) => {
  return currencyFormatter.format(value, {
    code: "VNĐ",
    precision: 0,
    format: "%v %s",
    symbol: "VNĐ",
  });
};

const converObjectComponent = (layout, maplst, state) => {

  var listLayout = [];
  layout.map((item, index) => {
    var newObj = null;
    switch (item.type) {
      case "tabheader":
        newObj = new TabsHeader();
        break;
      case "tabsitem":
        newObj = new TabsItem();
        newObj.text = item.text
        newObj.btn_text = item.btn_text
        newObj.component = item.component
        break;
      case "listbh":
        newObj = new ListIssuer();
        break;
      case "listbh_tnds":
        newObj = new ListIssuer();
        break;
      case "single_select":
        newObj = new SingleSelect();
        break;
      case "relationselect":
        newObj = new RelationSelect();
        break;
      case "lockoutlist":
        newObj = new LockoutList();
        break;
      case "totalpayment":
        newObj = new TotalPayment();
        break;
      case "uploadFile":
        newObj = new UploadModel();
        break;
      default:
        newObj = new LayoutModel();
        break;
    }
    if(state){
      if(state["view_"+item.template_id]){
        newObj.properties = state
      }else{
        state["view_"+item.template_id] = null
        newObj.properties = state
      } 
    }else{
      newObj.properties = {}
    }
    
    newObj.template_id = item.template_id;
    newObj.name = item.name;
    newObj.type = item.type;
    newObj.extend_class = item.extend_class;
    newObj.define = item.define;
    newObj.mapstate = item.mapstate;
    newObj.ref = createRef();
    
    if (item.col) {
      newObj.col = item.col;
    }
    if (item.layout) {
      newObj.layout = converObjectComponent(item.layout, maplst, state);
    }
    if (item.accordion_name){
      newObj.accordion_name = converObjectComponent(item.accordion_name, maplst, state);
    }
    maplst[item.template_id] = newObj;
    listLayout.push(newObj);
    return 1;
  });
  return listLayout;
};

const rootObjectComponent = (define, state) => {
  var map = {};
  const obj_config = {
    template_id: define.template_id,
    type: define.type,
    name: define.name,
    layout: converObjectComponent(define.layout, map, state),
  };

  return { obj_config, map };
};

const randomID = () => {
  return randomstring.generate(7);
};
const isEmptyObj = (obj) => {
  return obj && Object.keys(obj).length === 0 && obj.constructor === Object;
};
const insert = (arr, index, newItem) => [
  ...arr.slice(0, index),

  newItem,

  ...arr.slice(index),
];
const getPrositionbyId = (list, id) => {
  return list.findIndex((x) => x.id == id);
};


const getParseSchema = (data, id)=>{
  var data_return = data
  id.split("/").forEach((item, index)=>{
    if(index>0){
      if(data_return[item]){
        data_return = data_return[item]
      }else{
        return null
      }
      
    }
  })

  return data_return


}


const formSchemaInstance = async (jsonschema)=>{
  // try {
  //   let schema = await $RefParser.dereference(jsonschema);
  //   console.log("cho do 2",schema);
  // }
  // catch(err) {
  //   console.error(err);
  // }

  // if(jsonschema.properties){
  //   for (const [key, value] of Object.entries(jsonschema.properties)) {
  //     console.log("item ", key, value.type)
  //     if(value.type == "object"){
  //         formSchemaInstance(value)
  //     }
  //   }
  // }else{

  // }
}
const getLanguage = ()=>{
  if(typeof document !== 'undefined'){
    let cookieValue = document.cookie.replace(/(?:(?:^|.*;\s*)current_language\s*\=\s*([^;]*).*$)|^.*$/, "$1");
    if(cookieValue){
      return cookieValue;
    }
  }
  return "vi"
}
const getAge = (dobDate, effDate) => {
  var a = moment(effDate, 'DD-MM-YYYY');
  var b = moment(dobDate, 'DD-MM-YYYY');

  var _years = a.diff(b, 'year');
  b.add(_years, 'years');

  var _months = a.diff(b, 'months');
  b.add(_months, 'months');

  var _days = a.diff(b, 'days');

  // console.log(_years + ' years ' + _months + ' months ' + _days + ' days');
  return { y: _years, m: _months, d: _days };
};

export default {
  // convertData,
  rootObjectComponent,
  isEmptyObj,
  insert,
  randomID,
  getPrositionbyId,
  formatCurrency,
  formSchemaInstance,
  getParseSchema,
  getLanguage,
  getAge
};
