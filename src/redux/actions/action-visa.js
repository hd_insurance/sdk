import { SET_CARD_OWNER_INFOR, SET_SCROLL_TO_BT, SET_SCROLL_TO_QL } from "../actionTypes/visa.js";



export const setCardOwner = (info) => ({
   type: SET_CARD_OWNER_INFOR,
   data: info
});
export const setScrollToBT = (info) => ({
   type: SET_SCROLL_TO_BT,
   data: info
});
export const setScrollToQL = (info) => ({
   type: SET_SCROLL_TO_QL,
   data: info
});

