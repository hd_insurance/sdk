import { SET_OBJ_STATE, SET_DATA_INPUT } from "../actionTypes/lachan";


export const setOBJState = (info) => ({
   type: SET_OBJ_STATE,
   data: info
});

export const setDataInput = (data) => ({
   type: SET_DATA_INPUT,
   data: data
})