import { UPDATE_CAR_LIST, SET_LIST_DEFINE_CAR, SET_BUYER_INFOR } from "../actionTypes/car.js";



export const setCarList = (list) => ({
   type: UPDATE_CAR_LIST,
   data: list
});
export const setListDefineCar = (list) => ({
   type: SET_LIST_DEFINE_CAR,
   data: list
});
export const setBuyerInfor = (infor) =>({
   type : SET_BUYER_INFOR,
   data: infor
})


