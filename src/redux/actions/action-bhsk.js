import { UPDATE_PACKAGE, UPDATE_USERS, REG_CHECKED } from "../actionTypes/bhsk.js";

export const fetchPackages = (list) => ({
   type: UPDATE_PACKAGE,
   data: list
});

export const setUserList = (list) => ({
   type: UPDATE_USERS,
   data: list
});

export const checkRegistered = (checked) => ({
   type: REG_CHECKED,
   checked: checked
});

