import { UPDATE_USERS } from "../actionTypes/bhsk.js";

export default function setUserList(state = [], action = {}) {
	switch (action.type) {
		case UPDATE_USERS:
            return action.data;
		default: 
			return state;
	}
}