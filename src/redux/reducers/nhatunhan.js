import { SET_OBJ_STATE } from "../actionTypes/nhatunhan";

export default function nhatunhan(state = {}, action = {}) {
  switch (action.type) {
    case SET_OBJ_STATE: {
      return { ...state, ...action.data };
    }
    default:
      return state;
  }
}
