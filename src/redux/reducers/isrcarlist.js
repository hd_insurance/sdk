import { UPDATE_CAR_LIST } from "../actionTypes/car.js";

export default function setCarList(state = [], action = {}) {
	switch (action.type) {
		case UPDATE_CAR_LIST:
            return action.data;
		default: 
			return state;
	}
}

