import { UPDATE_PACKAGE } from "../actionTypes/bhsk.js";

export default function getPackage(state = [], action = {}) {
	switch (action.type) {
		case UPDATE_PACKAGE:
            return action.data;
		default: 
			return state;
	}
}