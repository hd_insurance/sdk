import { combineReducers } from 'redux';
import counterReducer from './counterReducer';
import insurancepackages from './insurancepackages';
import isruserlist from './isruserlist.js'
import regchecked from './regchecked.js'
import isrcarlist from './isrcarlist.js'
import listDefineCar from './listDefineCar.js'
import buyerInfor from './buyerInfor'
import visa from './visa'
import lachan from './lachan'

export default combineReducers({
    data:insurancepackages,
    isruserlist: isruserlist,
    regchecked: regchecked,
    isrcarlist: isrcarlist,
    listDefineCar: listDefineCar,
    buyerInfor: buyerInfor,
    visa: visa,
    lachan: lachan
});