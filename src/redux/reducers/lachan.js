import { SET_OBJ_STATE, SET_DATA_INPUT } from "../actionTypes/lachan";

export default function nhatunhan(state = {}, action = {}) {
  switch (action.type) {
    case SET_OBJ_STATE: {
      return { ...state, ...action.data };
    }
    case SET_DATA_INPUT: {
      console.log("action.data", action.data)
      return { ...state, data_input: { ...state.data_input, ...action.data } };
    }
    default:
      return state;
  }
}
