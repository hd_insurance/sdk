import {  SET_LIST_DEFINE_CAR } from "../actionTypes/car.js";
export default function setDefineCar(state = [], action = {}) {
	switch (action.type) {
		case SET_LIST_DEFINE_CAR:
            return action.data;
		default: 
			return state;
	}
}