import { SET_BUYER_INFOR } from "../actionTypes/car.js";

let initState = {
  TYPE: "CN",
  NAME: "",
  DOB: "",
  GENDER: "",
  ADDRESS: "",
  IDCARD: "",
  EMAIL: "",
  PHONE: "",
  PROV: "",
  DIST: "",
  WARDS: "",
  IDCARD_D: "",
  IDCARD_P: "",
  FAX: "",
  TAXCODE: "",
};

export default function setBuyerInfor(state = initState, action = {}) {
  switch (action.type) {
    case SET_BUYER_INFOR: {
      return { ...state, ...action.data };
    }

    default:
      return state;
  }
}
