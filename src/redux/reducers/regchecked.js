import { REG_CHECKED } from "../actionTypes/bhsk.js";

export default function regchecked(checked = {}, action = {}) {
	switch (action.type) {
		case REG_CHECKED:
            return action.checked;
		default: 
			return checked;
	}
}