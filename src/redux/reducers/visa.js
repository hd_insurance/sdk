import { SET_CARD_OWNER_INFOR, SET_SCROLL_TO_BT, SET_SCROLL_TO_QL } from "../actionTypes/visa.js";

let initState = {
  cardOwner: {},
  scrollToBT: null,
  scrollToQL: null,
};

export default function visaState(state = initState, action = {}) {
  switch (action.type) {
    case SET_CARD_OWNER_INFOR:
      return { ...state, cardOwner: action.data };
    case SET_SCROLL_TO_BT:
      return { ...state, scrollToBT: action.data };
    case SET_SCROLL_TO_QL:
      return { ...state, scrollToQL: action.data };
    default:
      return { ...state };
  }
}
