import {createStore, combineReducers} from 'redux';


const SET_BUYER_INFO = "SET_BUYER_INFO"
const SET_INSURER_INFO = "SET_INSURER_INFO"


function buyerInfor(state = {}, action = {}) {
  switch (action.type) {
    case SET_BUYER_INFO: {
      return { ...state, ...action.data };
    }
    default:
      return state;
  }
}

function listInsurer(state = {}, action = {}) {
  switch (action.type) {
    case SET_INSURER_INFO: {
      return { ...state, ...action.data };
    }
    default:
      return state;
  }
}




const rootReducer = combineReducers({
    buyerInfor: buyerInfor,
    listInsurer: listInsurer
});
const store = createStore(rootReducer);

export default store;
