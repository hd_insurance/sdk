export default class Trigger {
  static events = {};

  addListener = (nameEvent, template_id, calback) => {
    const obb = {};
    obb[nameEvent] = calback;
    Trigger.events[template_id] = { ...obb };
    // console.log("Add listener", Trigger.events[id])
  };
}
